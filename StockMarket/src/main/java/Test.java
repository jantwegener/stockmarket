

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.List;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.xy.DefaultXYDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

import jtw.stock.control.ClosingStockController;
import jtw.stock.io.HistoryReader;
import jtw.stock.io.StooqComHistoryReader;
import jtw.stock.model.Stock;
import jtw.stock.util.ValueFinder;
import jtw.stock.view.StockView;
import jtw.util.MathUtil;
import jtw.util.ObjectToDouble;

public class Test {
	
	public static void main(String[] args) throws Exception {
		Calendar date1 = Calendar.getInstance();
		date1.set(2000, 1, 1);
		Stock s1 = new Stock("a", date1, Currency.getInstance("EUR"), 1, 1, 2, 2, 5);

		Calendar date2 = Calendar.getInstance();
		date2.set(2000, 1, 2);
		Stock s2 = new Stock("a", date2, Currency.getInstance("EUR"), 1, 1, 8, 4, 5);

		Calendar date3 = Calendar.getInstance();
		date3.set(2000, 1, 3);
		Stock s3 = new Stock("a", date3, Currency.getInstance("EUR"), 1, 1, 2, 2, 5);

		Calendar date4 = Calendar.getInstance();
		date4.set(2000, 1, 4);
		Stock s4 = new Stock("a", date4, Currency.getInstance("EUR"), 1, 1, 5, 3, 5);

		Calendar date5 = Calendar.getInstance();
		date5.set(2000, 1, 5);
		Stock s5 = new Stock("a", date5, Currency.getInstance("EUR"), 1, 1, 2, 1, 5);
		
		List<Stock> stockList = new ArrayList<>();
		stockList.add(s1);
		stockList.add(s2);
		stockList.add(s3);
		stockList.add(s4);
		stockList.add(s5);
		
		double max = ValueFinder.findYMax(stockList, -5, 10);
		System.out.println(max);
		double min = ValueFinder.findYMin(stockList, 0, 10);
		System.out.println(min);
		
		ObjectToDouble<Stock> otd = new ObjectToDouble<Stock>() {
			@Override
			public double objToDouble(Stock o) {
				return o.getHigh();
			}
		};
		double y = MathUtil.average(stockList, otd, 0, 10);
		System.out.println(y);
		
	}
	
	public static void main2(String[] args) throws Exception {
		ApplicationFrame frame = new ApplicationFrame("Stock");
		frame.setSize(800, 600);
		frame.setContentPane(StockView.getInstance());
		frame.pack();
		RefineryUtilities.centerFrameOnScreen(frame);
		frame.setVisible(true);
		
		ClosingStockController c = new ClosingStockController("ProSiebenSat1.csv");
		c.computePlot();
	}

	public static void main3(String[] args) throws Exception {
		HistoryReader reader = new StooqComHistoryReader();
//		List<Stock> stockList = reader.read("ProSiebenSat1-Aktie/DE000PSM7770");
//		List<Stock> stockList = reader.read("DeutscheBörse.csv");
		List<Stock> stockList = null;//reader.read("ProSiebenSat1.csv");
		reader.read("psm.de");
		stockList = reader.getDailyList();

		double[][] data = new double[2][stockList.size()];

		int count = 0;
		for (Stock s : stockList) {
			data[0][count] = s.getDate().getTimeInMillis();
			data[1][count++] = s.getClosing();
		}
		System.out.println("Transformed " + count + " stocks");

		DefaultXYDataset dataset = new DefaultXYDataset();
		dataset.addSeries("Price", data);

		final XYItemRenderer renderer = new StandardXYItemRenderer();
		NumberAxis yaxis = new NumberAxis("Price");
		yaxis.setAutoRangeIncludesZero(false);
		yaxis.setAutoRange(true);
		XYPlot plot = new XYPlot(dataset, new DateAxis("Date"), new NumberAxis("Price"), renderer);
		plot.setOrientation(PlotOrientation.VERTICAL);
		JFreeChart chart = new JFreeChart("Combined  XY Plot", JFreeChart.DEFAULT_TITLE_FONT, plot, true);

		final ChartPanel panel = new ChartPanel(chart, true, true, true, true, true);
		panel.setPreferredSize(new java.awt.Dimension(500, 270));

		ApplicationFrame frame = new ApplicationFrame("Stock");
		frame.setContentPane(panel);
		frame.pack();
		RefineryUtilities.centerFrameOnScreen(frame);
		frame.setVisible(true);
	}

}
