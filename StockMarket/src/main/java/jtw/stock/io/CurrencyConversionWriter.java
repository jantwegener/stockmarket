package jtw.stock.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

import org.antlr.v4.runtime.misc.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jtw.stock.main.StockApplicationSettings;

public class CurrencyConversionWriter {

	private Log LOGGER = LogFactory.getLog(CurrencyConversionWriter.class);
	
	public void writeCurrencyConversions(Map<Pair<String, String>, Pair<Double, Double>> currencyConversionMap) throws IOException {
		File currencyFile = new File(StockApplicationSettings.getLocalPath(CurrencyConversionReader.CURRENCY_FILE, CurrencyConversionReader.CURRENCY_FILE_ENDING));
		if (!currencyFile.exists()) {
			String errorMsg = String.format("The path '%s' does not exist.", currencyFile.getAbsolutePath());
			LOGGER.error(errorMsg);
			throw new IOException(errorMsg);
		}
		
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(currencyFile))) {
			for (Pair<String, String> code2code : currencyConversionMap.keySet()) {
				writer.write(code2code.a);
				writer.write(';');
				writer.write(code2code.b);
				writer.write(';');
				Pair<Double, Double> money2money = currencyConversionMap.get(code2code);
				writer.write(String.valueOf(money2money.a));
				writer.write(';');
				writer.write(String.valueOf(money2money.b));
				writer.newLine();
			}
		}
	}
	
}
