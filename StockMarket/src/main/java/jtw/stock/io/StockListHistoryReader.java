package jtw.stock.io;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jtw.stock.model.Stock;
import jtw.stock.model.registry.Country2CurrencyRegistry;

/**
 * A reader for reading from a daily stock list.
 * 
 * @author Jan-Thierry Wegener
 */
public class StockListHistoryReader extends HistoryReader<Stock> {

	private final transient Log log = LogFactory.getLog(StockListHistoryReader.class);
	
	private List<Stock> inputList = null;
	
	public StockListHistoryReader() {
	}
	
	public void setInputList(List<Stock> inputList) {
		this.inputList = inputList;
	}

	@Override
	public int doRead(String stockName) throws IOException {
		reset();

		Calendar week = Calendar.getInstance();
		week.set(1970, 1, 1);
		Stock weekStock = new Stock(stockName, week, Country2CurrencyRegistry.getInstance().getCurrencyFromStockName(stockName), -1, -1, Long.MIN_VALUE, Long.MAX_VALUE, 0);
		Calendar month = Calendar.getInstance();
		month.set(1970, 1, 1);
		Stock monthStock = new Stock(stockName, month, Country2CurrencyRegistry.getInstance().getCurrencyFromStockName(stockName), -1, -1, Long.MIN_VALUE, Long.MAX_VALUE, 0);

		int dataRead = 0;
		for (Stock record : inputList) {
			handleRecord(stockName, record, weekStock, monthStock);
			dataRead++;
		}
		
		return dataRead;
	}

	@Override
	protected Calendar getDate(Stock record) {
		return record.getDate();
	}

	@Override
	protected double getOpening(Stock record) {
		return record.getOpening();
	}

	@Override
	protected double getClosing(Stock record) {
		return record.getClosing();
	}

	@Override
	protected double getHigh(Stock record) {
		return record.getHigh();
	}

	@Override
	protected double getLow(Stock record) {
		return record.getLow();
	}

	@Override
	protected long getVolume(Stock record) {
		return record.getVolume();
	}
	
}
