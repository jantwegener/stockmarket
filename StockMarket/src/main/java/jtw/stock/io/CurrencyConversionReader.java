package jtw.stock.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.antlr.v4.runtime.misc.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jtw.stock.main.StockApplicationSettings;

public class CurrencyConversionReader {

	private Log LOGGER = LogFactory.getLog(CurrencyConversionReader.class);

	final static String CURRENCY_FILE = "currencyConversion";
	final static String CURRENCY_FILE_ENDING = "map";
	
	public Map<Pair<String, String>, Pair<Double, Double>> loadCurrencyConversions() throws IOException {
		File currencyFile = new File(StockApplicationSettings.getLocalPath(CURRENCY_FILE, CURRENCY_FILE_ENDING));
		if (!currencyFile.exists()) {
			String errorMsg = String.format("The path '%s' does not exist.", currencyFile.getAbsolutePath());
			LOGGER.error(errorMsg);
			throw new IOException(errorMsg);
		}
		
		Map<Pair<String, String>, Pair<Double, Double>> currencyConversionMap = new HashMap<>();
		try (BufferedReader reader = new BufferedReader(new FileReader(currencyFile))) {
			String line = null;
			while ((line = reader.readLine()) != null) {
				if (line.trim().equals("")) {
					continue;
				}
				
				String[] tmp = line.split(";");
				Pair<String, String> code2code = new Pair<>(tmp[0], tmp[1]);
				Pair<Double, Double> money2money = new Pair<>(Double.valueOf(tmp[2]), Double.valueOf(tmp[3]));
				currencyConversionMap.put(code2code, money2money);
			}
		}
		
		return currencyConversionMap;
	}
	
}
