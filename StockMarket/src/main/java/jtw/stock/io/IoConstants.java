package jtw.stock.io;

public class IoConstants {

	public static final String COL_DATE = "Date";
	public static final String COL_OPEN = "Open";
	public static final String COL_CLOSE = "Close";
	public static final String COL_HIGH = "High";
	public static final String COL_LOW = "Low";
	public static final String COL_VOLUME = "Volume";
	
	public static final String DB_FILE_ENDING = ".csv";
	
	public static final String INTERNAL_DATE_PATTERN = "dd.MM.yyyy";
	
}
