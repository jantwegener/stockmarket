package jtw.stock.io;

import static jtw.stock.io.IoConstants.COL_CLOSE;
import static jtw.stock.io.IoConstants.COL_DATE;
import static jtw.stock.io.IoConstants.COL_HIGH;
import static jtw.stock.io.IoConstants.COL_LOW;
import static jtw.stock.io.IoConstants.COL_OPEN;
import static jtw.stock.io.IoConstants.COL_VOLUME;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.Calendar;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jtw.stock.main.StockApplicationSettings;
import jtw.stock.model.Stock;
import jtw.stock.model.registry.Country2CurrencyRegistry;
import jtw.util.DateUtil;
import jtw.util.DoubleUtil;

public class InternalHistoryReader extends HistoryReader<CSVRecord> {
	
	private final transient Log log = LogFactory.getLog(InternalHistoryReader.class);
	
	private String dateFormat = "dd.MM.yyyy";
	private String fallbackDateFormat = "yyyy-MM-dd";
	
	@Override
	public int doRead(String stockName) throws IOException {
		reset();

		Calendar week = Calendar.getInstance();
		week.set(1970, 1, 1);
		Stock weekStock = new Stock(stockName, week, Country2CurrencyRegistry.getInstance().getCurrencyFromStockName(stockName), -1, -1, -Double.MAX_VALUE, Double.MAX_VALUE, 0);
		Calendar month = Calendar.getInstance();
		month.set(1970, 1, 1);
		Stock monthStock = new Stock(stockName, month, Country2CurrencyRegistry.getInstance().getCurrencyFromStockName(stockName), -1, -1, -Double.MAX_VALUE, Double.MAX_VALUE, 0);
		
		String file = StockApplicationSettings.getLocalPath(stockName, ".csv");
		
		int dataRead = 0;
		try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
			Iterable<CSVRecord> recordIterable = CSVFormat.RFC4180.withFirstRecordAsHeader().parse(reader);
			for (CSVRecord record : recordIterable) {
				handleRecord(stockName, record, weekStock, monthStock);
				dataRead++;
			}
		}
		
		return dataRead;
	}

	@Override
	protected Calendar getDate(CSVRecord record) {
		Calendar date = null;
		try {
			date = DateUtil.toCalendar(record.get(COL_DATE), dateFormat);
		} catch (IllegalArgumentException | ParseException e) {
			// try the fallback date
			try {
				date = DateUtil.toCalendar(record.get(COL_DATE), fallbackDateFormat);
			} catch (IllegalArgumentException | ParseException e1) {
				log.warn("Problem in record: " + record);
			}
		}
		return date;
	}

	@Override
	protected double getOpening(CSVRecord record) {
		double open = 0;
		try {
			open = DoubleUtil.toDouble(record.get(COL_OPEN));
		} catch (IllegalArgumentException e) {
			log.warn("Problem in record (missing opening): " + record);
		}
		return open;
	}

	@Override
	protected double getClosing(CSVRecord record) {
		double close = 0;
		try {
			close = DoubleUtil.toDouble(record.get(COL_CLOSE));
		} catch (IllegalArgumentException e) {
			log.warn("Problem in record (missing closing): " + record);
		}
		return close;
	}

	@Override
	protected double getHigh(CSVRecord record) {
		double high = 0;
		try {
			high = DoubleUtil.toDouble(record.get(COL_HIGH));
		} catch (IllegalArgumentException e) {
			log.warn("Problem in record (missing high): " + record);
		}
		return high;
	}

	@Override
	protected double getLow(CSVRecord record) {
		double low = 0;
		try {
			low = DoubleUtil.toDouble(record.get(COL_LOW));
		} catch (IllegalArgumentException e) {
			log.warn("Problem in record (missing low): " + record);
		}
		return low;
	}

	@Override
	protected long getVolume(CSVRecord record) {
		long volume = 0;
		try {
			volume = DoubleUtil.toLong(record.get(COL_VOLUME).replaceAll("\\.", ""), 1);
		} catch (IllegalArgumentException e) {
			log.warn("Problem in record (missing volume): " + record);
		}
		return volume;
	}
	
}
