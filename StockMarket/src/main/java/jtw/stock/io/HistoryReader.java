package jtw.stock.io;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jtw.stock.model.Stock;
import jtw.stock.model.registry.Country2CurrencyRegistry;

public abstract class HistoryReader<E> {
	
	private final transient Log log = LogFactory.getLog(HistoryReader.class);
	
	private List<Stock> dailyList = new ArrayList<>();

	private List<Stock> weeklyList = new ArrayList<>();

	private List<Stock> monthlyList = new ArrayList<>();

	public List<Stock> getDailyList() {
		return dailyList;
	}
	
	public List<Stock> getWeeklyList() {
		return weeklyList;
	}
	
	public List<Stock> getMonthlyList() {
		return monthlyList;
	}

	public Stock getFirstDailyStock() {
		return dailyList.get(0);
	}

	public Stock getFirstWeeklyStock() {
		return weeklyList.get(0);
	}

	public Stock getFirstMonthlyStock() {
		return monthlyList.get(0);
	}

	public Stock getLastDailyStock() {
		return dailyList.get(dailyList.size() - 1);
	}

	public Stock getLastWeeklyStock() {
		return weeklyList.get(weeklyList.size() - 1);
	}

	public Stock getLastMonthlyStock() {
		return monthlyList.get(monthlyList.size() - 1);
	}
	
	protected void addDaily(Stock stock) {
		dailyList.add(stock);
	}

	protected void addWeekly(Stock stock) {
		weeklyList.add(stock);
	}

	protected void addMonthly(Stock stock) {
		monthlyList.add(stock);
	}
	
	protected void reset() {
		dailyList.clear();
		weeklyList.clear();
		monthlyList.clear();
	}
	
	/**
	 * Reads the stocks into memory.
	 * 
	 * @param stockName The stock name to read. E.g., wdi.de, amz.us, ...
	 * 
	 * @return The number of data read, i.e., the number of days.
	 * 
	 * @throws IOException If an {@link IOException} occurs.
	 */
	public final int read(String stockName) throws IOException {
		log.debug("Reading stock: '" + stockName + "'");
		return doRead(stockName);
	}

	/**
	 * Does the actual reading of the stocks into memory.
	 * 
	 * @param stockName The stock name to read. E.g., wdi.de, amz.us, ...
	 * 
	 * @return The number of data read, i.e., the number of days.
	 * 
	 * @throws IOException If an {@link IOException} occurs.
	 */
	protected abstract int doRead(String stockName) throws IOException;
	
	protected void handleRecord(String stockName, E record, Stock weekStock, Stock monthStock) {
		// date
		Stock stock = new Stock();
		stock.setName(stockName);
		stock.setCurrency(Country2CurrencyRegistry.getInstance().getCurrencyFromStockName(stockName));
		stock.setDate(getDate(record));
		// open
		stock.setOpening(getOpening(record));
		// close
		stock.setClosing(getClosing(record));
		// high
		stock.setHigh(getHigh(record));
		// low
		stock.setLow(getLow(record));
		// volume
		stock.setVolume(getVolume(record));

		addDaily(stock);

		if (stock.getDate().get(Calendar.WEEK_OF_YEAR) == weekStock.getDate().get(Calendar.WEEK_OF_YEAR)) {
			weekStock.setClosing(stock.getClosing());
			weekStock.addVolume(stock.getVolume());
			weekStock.setHigh(Math.max(weekStock.getHigh(), stock.getHigh()));
			weekStock.setLow(Math.min(weekStock.getLow(), stock.getLow()));
		} else {
			// new week starts
			weekStock = new Stock(stock);
			addWeekly(weekStock);
		}

		if (stock.getDate().get(Calendar.MONTH) == monthStock.getDate().get(Calendar.MONTH)) {
			monthStock.setClosing(stock.getClosing());
			monthStock.addVolume(stock.getVolume());
			monthStock.setHigh(Math.max(monthStock.getHigh(), stock.getHigh()));
			monthStock.setLow(Math.min(monthStock.getLow(), stock.getLow()));
		} else {
			// new week starts
			monthStock = new Stock(stock);
			addMonthly(monthStock);
		}
	}

	protected abstract Calendar getDate(E record);
	
	protected abstract double getOpening(E record);
	
	protected abstract double getClosing(E record);
	
	protected abstract double getHigh(E record);
	
	protected abstract double getLow(E record);

	protected abstract long getVolume(E record);
	
}
