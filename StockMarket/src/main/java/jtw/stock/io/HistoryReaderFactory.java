package jtw.stock.io;

public class HistoryReaderFactory {
	
	public static HistoryReader getHistoryReader(String stockName) {
		if (stockName.endsWith(".us")) {
			return new NasdaqComHistoryReader();
		}
		return new StooqComHistoryReader();
	}
	
}
