package jtw.stock.io;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.antlr.v4.runtime.misc.Pair;

import jtw.stock.main.StockApplicationSettings;
import jtw.util.IntegerUtil;
import jtw.util.StringUtil;

/**
 * This class is for reading a custom index file.
 * 
 * @author Jan-Thierry Wegener
 */
public class CustomIndexReader {
	
	private List<Pair<String, Integer>> indexQuantityList = null;
	
	/**
	 * Reads the given index.
	 * 
	 * @param indexName The index to read.
	 * 
	 * @throws IOException
	 */
	public void read(String indexName) throws IOException {
		final String indexFile = StockApplicationSettings.getLocalPath(indexName, ".index");
		indexQuantityList = readStockList(indexFile);
	}
	
	/**
	 * Returns the list containing the stock names and their quantities.
	 * 
	 * @return A list containing the stock names and their quantities.
	 */
	public List<Pair<String, Integer>> getIndexContentList() {
		return indexQuantityList;
	}
	
	/**
	 * Reads the stock names from the index list file.
	 * 
	 * @param indexFile The index list file to read.
	 * 
	 * @return A list containing the names of the stocks within the index file.
	 * 
	 * @throws IOException If an {@link IOException} occurs.
	 */
	private List<Pair<String, Integer>> readStockList(String indexFile) throws IOException {
		List<Pair<String, Integer>> stockList = new ArrayList<>();
		try (BufferedReader reader = new BufferedReader(new FileReader(indexFile))) {
			String line = null;
			while ((line = reader.readLine()) != null) {
				line = line.trim();
				
				if (line.startsWith("#") || StringUtil.isEmpty(line, true)) {
					continue;
				}
				
				// the line contains the name of the index and the quantity
				String[] stockQuantity = line.split("\\s+", 2);
				String stockName = stockQuantity[0];
				Integer quantity = 1;
				if (stockQuantity.length > 1) {
					quantity =IntegerUtil.toInt(stockQuantity[1], 1);
				}
				stockList.add(new Pair<String, Integer>(stockName, quantity));
			}
		}
		return stockList;
	}

}
