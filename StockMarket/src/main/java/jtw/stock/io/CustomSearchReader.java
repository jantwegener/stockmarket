package jtw.stock.io;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jtw.stock.main.StockApplicationSettings;
import jtw.stock.model.CustomSearchFormula;

public class CustomSearchReader {

	private Log log = LogFactory.getLog(CustomSearchReader.class);
	
	private List<CustomSearchFormula> formulaList = new ArrayList<>();
	
	private String searchFileName;
	
	public CustomSearchReader(String searchFileName) {
		this.searchFileName = searchFileName;
	}
	
	public List<CustomSearchFormula> getFormulaList() {
		return formulaList;
	}

	/**
	 * Reads the given index.
	 * 
	 * @param indexName The index to read.
	 * 
	 * @throws IOException
	 */
	public void read() throws IOException {
		final String historyFile = StockApplicationSettings.getLocalPath(searchFileName, ".his");
		
		try (BufferedReader reader = new BufferedReader(new FileReader(historyFile))) {
			CustomSearchFormula formula = null;
			while ((formula = readNextFormula(reader)) != null && !formula.isEmpty()) {
				formulaList.add(formula);
			}
		}
	}
	
	private CustomSearchFormula readNextFormula(BufferedReader reader) throws IOException {
		String line = null;
		boolean stop = false;
		boolean readText = false;
		boolean readParameter = false;
		boolean readName = true;
		CustomSearchFormula formula = new CustomSearchFormula();
		
		String name = "";
		StringBuilder textBuilder = new StringBuilder();
		List<Parameter> paramList = new ArrayList<>();
		
		while (!stop && (line = reader.readLine()) != null) {
			line = line.trim();
			if (line.isEmpty()) {
				continue;
			}
			
			if (line.startsWith("----")) {
				// first time we start reading the text
				// second time we start reading the parameters
				readText = !readText;
				if (!readText) {
					readParameter = true;
				}
			} else {
				if (readName) {
					name = line;
					readName = false;
				} else if (readText) {
					textBuilder.append(line);
				} else if (readParameter) {
					if (";".equals(line)) {
						// we finished our formula
						stop = true;
					} else {
						// A=class:parameter1,parameter2
						String[] tmpArray = line.split("=", 2);
						String paramName = tmpArray[0];
						tmpArray = tmpArray[1].split(":", 2);
						String clazzName = tmpArray[0];
						String[] paramArray = tmpArray[1].split(",");
						
						Parameter p = new Parameter(paramName, clazzName, paramArray);
						paramList.add(p);
					}
				} else {
					log.fatal("History file is damaged.");
					throw new IOException("History file is damaged.");
				}
			}
		}

		formula.init(paramList.size());
		formula.setName(name);
		formula.setText(textBuilder.toString());
		for (int i = 0; i < paramList.size(); i++) {
			String nameParam = paramList.get(i).getParamName();
			String nameClazz = paramList.get(i).getClazzName();
			String[] parameter = paramList.get(i).getParamArray();
			formula.setClazz(i, nameClazz);
			formula.setParameterName(i, nameParam);
			formula.setParameterArray(i, parameter);
		}
		
		return formula;
	}
	
	private class Parameter {
		
		private final String paramName;
		private final String clazzName;
		private final String[] paramArray;
		
		public Parameter(String paramName, String clazzName, String[] paramArray) {
			super();
			this.paramName = paramName;
			this.clazzName = clazzName;
			this.paramArray = paramArray;
		}
		
		public String getParamName() {
			return paramName;
		}

		public String getClazzName() {
			return clazzName;
		}

		public String[] getParamArray() {
			return paramArray;
		}

	}
	
}
