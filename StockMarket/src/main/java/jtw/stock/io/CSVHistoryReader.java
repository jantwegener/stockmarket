package jtw.stock.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.util.Calendar;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jtw.stock.main.StockApplicationSettings;
import jtw.stock.model.Stock;
import jtw.stock.model.registry.Country2CurrencyRegistry;
import jtw.util.DateUtil;
import jtw.util.DoubleUtil;

public abstract class CSVHistoryReader extends HistoryReader<CSVRecord> {
	
	private final transient Log log = LogFactory.getLog(CSVHistoryReader.class);
	
	private String dateFormat = "dd.mm.YYYY";
	
	public String getDateFormat() {
		return dateFormat;
	}
	
	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}
	
	protected String getLocalCSVPath(String stockName) {
		return StockApplicationSettings.getBasePath() + stockName + ".csv";
	}
	
	protected String getDateColName() {
		return "Date";
	}
	
	protected String getOpenColName() {
		return "Open";
	}
	
	protected String getCloseColName() {
		return "Close";
	}
	
	protected String getHighColName() {
		return "High";
	}
	
	protected String getLowColName() {
		return "Low";
	}
	
	protected String getVolumeColName() {
		return "Volume";
	}
	
	/**
	 * Returns the latest {@link Stock} in the daily list.
	 * 
	 * @return The latest stock.
	 */
	protected Stock getLatestStock() {
		return getDailyList().get(getDailyList().size() - 1);
	}
	
	/**
	 * Returns the download site.
	 * 
	 * @param stockName The name of the stock.
	 * 
	 * @return The site to download the data from.
	 */
	protected abstract String getDownloadSite(String stockName);
	
	@Override
	public int doRead(String stockName) throws IOException {
		// download the file
		String content = downloadFile(stockName);
		// read the downloaded file
		int dataRead = readEntries(stockName, content);
		return dataRead;
	}
	
	/**
	 * Downloads the file for the given stock and returns its content.
	 * 
	 * @param stockName The stock file to download.
	 * 
	 * @return The content of the downloaded file.
	 * 
	 * @throws IOException If an {@link IOException} occurs.
	 */
	protected abstract String downloadFile(String stockName) throws IOException;
	
	/**
	 * Reads the entries from the downloaded content.
	 * 
	 * @param stockName   The name of the stock.
	 * @param fileContent The content of the downloaded file.
	 * 
	 * @return The number of entries read.
	 * 
	 * @throws IOException
	 */
	protected int readEntries(String stockName, String fileContent) throws IOException {
		reset();
		
		Calendar week = Calendar.getInstance();
		week.set(1970, 1, 1);
		Stock weekStock = new Stock(stockName, week,
				Country2CurrencyRegistry.getInstance().getCurrencyFromStockName(stockName), -1, -1, -Double.MAX_VALUE,
				Double.MAX_VALUE, 0);
		Calendar month = Calendar.getInstance();
		month.set(1970, 1, 1);
		Stock monthStock = new Stock(stockName, month,
				Country2CurrencyRegistry.getInstance().getCurrencyFromStockName(stockName), -1, -1, -Double.MAX_VALUE,
				Double.MAX_VALUE, 0);
		
		int dataRead = 0;
		try (BufferedReader reader = new BufferedReader(new StringReader(fileContent))) {
			Iterable<CSVRecord> recordIterable = CSVFormat.RFC4180.withFirstRecordAsHeader().parse(reader);
			for (CSVRecord record : recordIterable) {
				handleRecord(stockName, record, weekStock, monthStock);
				dataRead++;
			}
		}
		
		return dataRead;
	}
	
	@Override
	protected Calendar getDate(CSVRecord record) {
		Calendar date = null;
		try {
			date = DateUtil.toCalendar(record.get(getDateColName()), dateFormat);
		} catch (IllegalArgumentException | ParseException e) {
			log.warn("Problem in record: " + record);
		}
		return date;
	}
	
	@Override
	protected double getOpening(CSVRecord record) {
		double open = 0;
		try {
			open = DoubleUtil.toDouble(record.get(getOpenColName()));
		} catch (IllegalArgumentException e) {
			log.warn("Problem in record (missing opening): " + record);
		}
		return open;
	}
	
	@Override
	protected double getClosing(CSVRecord record) {
		double close = 0;
		try {
			close = DoubleUtil.toDouble(record.get(getCloseColName()));
		} catch (IllegalArgumentException e) {
			log.warn("Problem in record (missing closing): " + record);
		}
		return close;
	}
	
	@Override
	protected double getHigh(CSVRecord record) {
		double high = 0;
		try {
			high = DoubleUtil.toDouble(record.get(getHighColName()));
		} catch (IllegalArgumentException e) {
			log.warn("Problem in record (missing high): " + record);
		}
		return high;
	}
	
	@Override
	protected double getLow(CSVRecord record) {
		double low = 0;
		try {
			low = DoubleUtil.toDouble(record.get(getLowColName()));
		} catch (IllegalArgumentException e) {
			log.warn("Problem in record (missing low): " + record);
		}
		return low;
	}
	
	@Override
	protected long getVolume(CSVRecord record) {
		long volume = 0;
		try {
			volume = DoubleUtil.toLong(record.get(getVolumeColName()).replaceAll("\\.", ""), 1);
		} catch (IllegalArgumentException e) {
			log.warn("Problem in record (missing volume): " + record);
		}
		return volume;
	}
	
}
