package jtw.stock.io;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.antlr.v4.runtime.misc.Pair;

import jtw.stock.main.StockApplicationSettings;

/**
 * This class is for writing custom indexes.
 * 
 * @author Jan-Thierry Wegener
 */
public class CustomIndexWriter {
	
	private List<Pair<String, Integer>> stockQuantityList;
	
	public CustomIndexWriter() {
	}
	
	public void write(String indexName) throws IOException {
		String indexFile = StockApplicationSettings.getLocalPath(indexName, ".index");
		
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(indexFile))) {
			writer.write("# custom index 1.0");
			writer.newLine();
			for (Pair<String, Integer> p : stockQuantityList) {
				writer.write(p.a + " " + p.b);
				writer.newLine();
			}
			writer.newLine();
		}
	}

	/**
	 * Returns the list containing the stock names and their quantities.
	 * 
	 * @return A list containing the stock names and their quantities.
	 */
	public void setStockQuantityList(List<Pair<String, Integer>> stockQuantityList) {
		this.stockQuantityList = stockQuantityList;
	}
	
}
