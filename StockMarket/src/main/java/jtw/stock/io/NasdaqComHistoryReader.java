package jtw.stock.io;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.List;
import java.util.ListIterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import jtw.stock.model.Stock;
import jtw.stock.model.registry.Country2CurrencyRegistry;
import jtw.util.DateUtil;
import jtw.util.DoubleUtil;
import jtw.util.io.Copy;

public class NasdaqComHistoryReader extends HistoryReader<NasdaqComHistoryReader.JsonStockRecord> {
	
	// https://api.nasdaq.com/api/quote/MSFT/historical?assetclass=stocks&fromdate=2011-11-28&limit=9999&todate=2021-11-28
	
	private static final DateTimeFormatter URL_DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	private static final String dateFormat = "MM/dd/yyyy";
	
	private final transient Log log = LogFactory.getLog(CSVHistoryReader.class);
	
	public NasdaqComHistoryReader() {
	}
	
	protected String getDownloadSite(String stockName) {
		stockName = stockName.replaceFirst("\\.us", "").toUpperCase();
		LocalDate today = LocalDate.now();
		LocalDate tenYearBack = today.minusYears(10);
		
		return String.format("https://api.nasdaq.com/api/quote/%s/historical?assetclass=stocks&fromdate=%s&limit=9999&todate=%s", stockName, tenYearBack.format(URL_DATE_FORMATTER), today.format(URL_DATE_FORMATTER));
	}
	
	@Override
	public int doRead(String stockName) throws IOException {
		// download the file
		String jsonStr = downloadFile(stockName);
		// read the downloaded file
		int dataEntries = readEntries(stockName, jsonStr);
		return dataEntries;
	}
	
	private int readEntries(String stockName, String jsonStr) throws IOException {
		Gson gson = new Gson();
		JsonObject root = JsonParser.parseString(jsonStr).getAsJsonObject();
		int status = root.get("status").getAsJsonObject().get("rCode").getAsInt();
		if (status != 200) {
			String errorMsg = root.get("status").getAsJsonObject().get("bCodeMessage").getAsJsonArray().get(0).getAsJsonObject().get("errorMessage").getAsString();
			throw new IOException("Error downloading '" + stockName + "': rCode=" + status + ", message=" + errorMsg);
		}
		int records = root.get("data").getAsJsonObject().get("totalRecords").getAsInt();
		if (records <= 0) {
			throw new IOException("Error downloading '" + stockName + "': rCode=" + status + ", message=" + records + " records downloaded");
		}
		JsonObject rowData = root.get("data").getAsJsonObject().get("tradesTable").getAsJsonObject();
		JsonRowTradersTable tradersTable = gson.fromJson(rowData, JsonRowTradersTable.class);

		Calendar week = Calendar.getInstance();
		week.set(1970, 1, 1);
		Stock weekStock = new Stock(stockName, week,
				Country2CurrencyRegistry.getInstance().getCurrencyFromStockName(stockName), -1, -1, -Double.MAX_VALUE,
				Double.MAX_VALUE, 0);
		Calendar month = Calendar.getInstance();
		month.set(1970, 1, 1);
		Stock monthStock = new Stock(stockName, month,
				Country2CurrencyRegistry.getInstance().getCurrencyFromStockName(stockName), -1, -1, -Double.MAX_VALUE,
				Double.MAX_VALUE, 0);
		
		int dataEntries = 0;
		// the rows are sorted in reversed order
		ListIterator<JsonStockRecord> iter = tradersTable.rows.listIterator(tradersTable.rows.size());
		while (iter.hasPrevious()) {
			JsonStockRecord record = iter.previous();
			handleRecord(stockName, record, weekStock, monthStock);
			dataEntries++;
		}
		return dataEntries;
	}

	private String downloadFile(String stockName) throws IOException {
		ByteArrayOutputStream outputBuffer = new ByteArrayOutputStream(1024);
		String downloadPath = getDownloadSite(stockName);

		String contentType = Copy.copyFromURL(downloadPath, outputBuffer);
		String[] contentTypeArray = contentType != null ? contentType.split(";\\s*") : null;
		
		String content;
		if (contentTypeArray != null && "application/json".equals(contentTypeArray[0])) {
			if (contentTypeArray.length > 1) {
				if (contentTypeArray[1].startsWith("charset=")) {
					content = outputBuffer.toString(contentTypeArray[1].replaceFirst("charset=", ""));
				} else {
					content = outputBuffer.toString(contentTypeArray[1]);
				}
			} else {
				content = outputBuffer.toString();
			}
		} else {
			if (contentTypeArray != null && contentTypeArray.length > 1) {
				content = outputBuffer.toString(contentTypeArray[1]);
			} else {
				content = outputBuffer.toString();
			}
		}
		
		return content;
	}
	
	@Override
	protected Calendar getDate(JsonStockRecord record) {
		Calendar date = null;
		try {
			date = DateUtil.toCalendar(record.getDate(), dateFormat);
		} catch (IllegalArgumentException | ParseException e) {
			log.warn("Problem in record (missing date): " + record);
		}
		return date;
	}
	
	@Override
	protected double getOpening(JsonStockRecord record) {
		double open = 0;
		try {
			// remove dollar sign
			open = DoubleUtil.toDouble(removeDollar(record.getOpen()));
		} catch (IllegalArgumentException e) {
			log.warn("Problem in record (missing opening): " + record);
		}
		return open;
	}
	
	@Override
	protected double getClosing(JsonStockRecord record) {
		double close = 0;
		try {
			// remove dollar sign
			close = DoubleUtil.toDouble(removeDollar(record.getClose()));
		} catch (IllegalArgumentException e) {
			log.warn("Problem in record (missing closing): " + record);
		}
		return close;
	}
	
	@Override
	protected double getHigh(JsonStockRecord record) {
		double high = 0;
		try {
			// remove dollar sign
			high = DoubleUtil.toDouble(removeDollar(record.getHigh()));
		} catch (IllegalArgumentException e) {
			log.warn("Problem in record (missing high): " + record);
		}
		return high;
	}
	
	@Override
	protected double getLow(JsonStockRecord record) {
		double low = 0;
		try {
			// remove dollar sign
			low = DoubleUtil.toDouble(removeDollar(record.getLow()));
		} catch (IllegalArgumentException e) {
			log.warn("Problem in record (missing low): " + record);
		}
		return low;
	}
	
	@Override
	protected long getVolume(JsonStockRecord record) {
		long volume = 0;
		try {
			volume = DoubleUtil.toLong(record.getVolume().replaceAll("\\.", "").replaceAll(",", ""), 1);
		} catch (IllegalArgumentException e) {
			log.warn("Problem in record (missing volume): " + record);
		}
		return volume;
	}
	
	private String removeDollar(String strWithDollar) {
		if (strWithDollar == null) {
			return null;
		}
		return strWithDollar.replaceFirst("\\$", "");
	}
	
	/**
	 * A small wrapper class to make it easier to handle lists.
	 */
	private class JsonRowTradersTable {
		
		private List<JsonStockRecord> rows;
		
	}
	
	/**
	 * The class representing a record in the JSON-file.
	 */
	public class JsonStockRecord {
		
		private String date;
		private String open;
		private String close;
		private String high;
		private String low;
		private String volume;
		
		public String getDate() {
			return date;
		}
		public void setDate(String date) {
			this.date = date;
		}
		public String getOpen() {
			return open;
		}
		public void setOpen(String open) {
			this.open = open;
		}
		public String getClose() {
			return close;
		}
		public void setClose(String close) {
			this.close = close;
		}
		public String getHigh() {
			return high;
		}
		public void setHigh(String high) {
			this.high = high;
		}
		public String getLow() {
			return low;
		}
		public void setLow(String low) {
			this.low = low;
		}
		public String getVolume() {
			return volume;
		}
		public void setVolume(String volume) {
			this.volume = volume;
		}
		
		@Override
		public String toString() {
			return "JsonStockRecord [date=" + date + ", open=" + open + ", close=" + close + ", high=" + high + ", low="
					+ low + ", volume=" + volume + "]";
		}
		
	}
	
}
