package jtw.stock.io;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jtw.stock.main.StockApplicationSettings;
import jtw.stock.model.CustomSearchFormula;

public class CustomSearchWriter {
	
	private Log log = LogFactory.getLog(CustomSearchReader.class);
	
	private List<CustomSearchFormula> formulaList = new ArrayList<>();
	
	public CustomSearchWriter() {
	}
	
	public void setFormulaList(List<CustomSearchFormula> formulaList) {
		this.formulaList = formulaList;
	}
	
	public void write() throws IOException {
		final String historyFile = StockApplicationSettings.getLocalPath("search", ".his");
		
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(historyFile))) {
			for (CustomSearchFormula formula : formulaList) {
				write(writer, formula);
			}
		}
		
	}

	private void write(BufferedWriter writer, CustomSearchFormula formula) throws IOException {
		if (log.isDebugEnabled()) {
			log.debug("writing formula: " + formula);
		}
		
		// [name]
		// ----
		// [text]
		// ----
		// A=[class:Paramter1,Parameter2]
		// ;
		writer.write(formula.getName());
		writer.newLine();
		
		writer.write("----");
		writer.newLine();
		writer.write(formula.getText());
		writer.newLine();
		writer.write("----");
		writer.newLine();
		
		int numOfClazzes = formula.getClazzArray().length;
		for (int i = 0; i < numOfClazzes; i++) {
			writer.write(formula.getParamNameArray()[i]);
			writer.write("=");
			writer.write(formula.getClazzArray()[i]);
			writer.write(":");
			
			StringBuilder builder = new StringBuilder();
			int len = formula.getParameterArray()[i].length;
			for (int j = 0; j < len; j++) {
				builder.append(formula.getParameterArray()[i][j]);
				if (j + 1 < len) {
					builder.append(',');
				}
			}
			writer.write(builder.toString());
			writer.newLine();
		}
		
		writer.write(';');
		writer.newLine();
	}
	
}
