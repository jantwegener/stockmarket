package jtw.stock.io;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import jtw.util.io.Copy;

public class StooqComHistoryReader extends CSVHistoryReader {
	
	// https://stooq.com/q/d/l/?s=psm.de&i=d
	
	public StooqComHistoryReader() {
		setDateFormat("yyyy-MM-dd");
	}
	
	@Override
	protected String getDownloadSite(String stockName) {
		return "https://stooq.com/q/d/l/?s=" + stockName + "&i=d";
	}
	
	@Override
	protected String downloadFile(String stockName) throws IOException {
		ByteArrayOutputStream outputBuffer = new ByteArrayOutputStream(1024);
		String downloadPath = getDownloadSite(stockName);
		
		String contentType = Copy.copyFromURL(downloadPath, outputBuffer);
		String[] contentTypeArray = contentType != null ? contentType.split(";\\w*") : null;
		
		String content;
		if (contentTypeArray != null && contentTypeArray.length > 1) {
			content = outputBuffer.toString(contentTypeArray[1]);
		} else {
			content = outputBuffer.toString();
		}
		
		return content;
	}
	
}
