package jtw.stock.calculator;

import java.awt.BasicStroke;
import java.awt.Color;

import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;

import jtw.stock.view.component.HighlightValueAxis;

public class IndicatorATRCalculationFinishedListener extends StandardStockCalculationFinishedAdapter {
	
	public IndicatorATRCalculationFinishedListener() {
	}

	@Override
	protected void manipulatePlot(XYPlot plot) {
		ValueMarker p0 = new ValueMarker(0, Color.BLUE, new BasicStroke(1));
	    plot.addRangeMarker(p0);
	}

	@Override
	protected boolean isSubPlot() {
		return true;
	}

	@Override
	protected ValueAxis getYAxis() {
		return new HighlightValueAxis("ATR");
	}
	
}
