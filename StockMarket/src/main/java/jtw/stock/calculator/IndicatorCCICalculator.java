package jtw.stock.calculator;

import static jtw.stock.main.StockApplicationSettings.TIME_FRAME_MAX;

import java.util.ArrayList;
import java.util.List;

import org.jfree.data.xy.DefaultXYDataset;
import org.jfree.data.xy.XYDataset;

import jtw.stock.main.StockApplicationSettings;
import jtw.stock.model.Stock;
import jtw.stock.model.registry.StockDataRegistry;
import jtw.util.IntegerUtil;
import jtw.util.MathUtil;
import jtw.util.ObjectToDouble;

/**
 * Calculates the Commodity Channel Indicator.
 * 
 * @author Jan-Thierry Wegener
 */
public class IndicatorCCICalculator extends StockCalculator {
	
	private final static double CONSTANT = 0.015;
	
	private int period = 0;
	
	private ObjectToDouble<Double> otd = new ObjectToDouble<Double>() {
		@Override
		public double objToDouble(Double o) {
			return o;
		}
	};
	
	public IndicatorCCICalculator() {
	}

	public IndicatorCCICalculator(int period) {
		this.period = period;
	}
	
	@Override
	public String getDisplayName() {
		return "CCI";
	}
	
	public int getPeriod() {
		return period;
	}

	@Override
	public String[] getParameterNameArray() {
		return new String[] {"Period"};
    }

	@Override
	public String[] getParameterDefaultValueArray() {
		return new String[] {"14"};
    }

	@Override
	public void setParameter(String ... params) {
		period = IntegerUtil.toInt(params[0], 14);
    }

	@Override
	protected XYDataset doCalculation() {
		List<Stock> allStockList = StockDataRegistry.getInstance().getStockList(getStockName(), getTimeBase(), TIME_FRAME_MAX);
		List<Stock> currStockList = StockDataRegistry.getInstance().getStockList(getStockName(), getTimeBase());
		int currSize = currStockList.size();
		int size = allStockList.size();
		double[][] data = new double[2][currSize];
		
		List<Double> typicalPriceList = new ArrayList<>();
		
		int j = 0;
		for (int i = 0; i < size; i++) {
			Stock s = allStockList.get(i);

			double pt = calculateTypicalPrice(allStockList, i - period, i);
			
			typicalPriceList.add(pt);
			
			double sma = MathUtil.average(typicalPriceList, otd, i - period+1, i+1);
			double md = MathUtil.meanAbsoluteDeviation(typicalPriceList, otd, sma, i - period+1, i+1);
			double cci = (pt - sma) / (CONSTANT * md);
			
			if (StockApplicationSettings.isInTimeFrame(s)) {
				data[0][j] = s.getDate().getTimeInMillis();
				data[1][j] = cci;
				j++;
			}
		}

		DefaultXYDataset dataset = new DefaultXYDataset();
		dataset.addSeries(getDisplayName(), data);
		
		return dataset;
	}
	
	private double calculateTypicalPrice(List<Stock> stockList, int startIndex, int stopIndex) {
		double high = stockList.get(stopIndex).getHigh();
		double low = stockList.get(stopIndex).getLow();
		double close = stockList.get(stopIndex).getClosing();
		return (high + low + close) / 300d;
	}

}
