package jtw.stock.calculator;

import java.util.concurrent.ExecutionException;

import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jfree.data.xy.XYDataset;

import jtw.stock.main.StockApplicationSettings;
import jtw.stock.model.registry.StockCalculatorFinishedListenerRegistry;
import jtw.stock.view.StockView;

public abstract class StockCalculator {

	private Log log = LogFactory.getLog(StockCalculator.class);
	
	/**
	 * The calculation finished listener.
	 */
	private CalculationFinishedListener calculationFinishedListener;
	
	private SwingWorker<XYDataset, Void> worker;
	
	private String stockName;
	
	private String additionalInfo = null;
	
	private int timeBase = -1;
	
	public StockCalculator() {
	}
	
	/**
	 * Returns the name of the chart that shall be displayed to the user.
	 * 
	 * @return The displayed name of the chart.
	 */
	public abstract String getDisplayName();
	
	/**
	 * Returns the tooltip for the calculator.
	 * 
	 * @return The tooltip for the calculator.
	 */
	public String getTooltip() {
		return "";
	}

	/**
	 * Returns the name of the calculator. By default the class name is returned.
	 * 
	 * @return The name of the calculator.
	 */
	public String getName() {
		return this.getClass().getName();
	}
	
	public void setStockName(String stockName) {
		this.stockName = stockName;
	}
	
	protected String getStockName() {
		String ret = StockApplicationSettings.getStockName();
		if (stockName != null) {
			ret = stockName;
		}
		return ret;
	}
	
	public int getTimeBase() {
		int retVal = StockApplicationSettings.getTimeBase();
		if (timeBase >= 0) {
			retVal = timeBase;
		}
		return retVal;
	}
	
	/**
	 * Sets a new time base for the calculations. A negative value resets the time base to the standard time base from the settings.
	 * 
	 * @param timeBase A new time base, or resets the time base to the standard if negative.
	 * 
	 * @see StockApplicationSettings#getTimeBase()
	 * @see StockApplicationSettings#TIME_BASE_DAILY
	 * @see StockApplicationSettings#TIME_BASE_WEEKLY
	 * @see StockApplicationSettings#TIME_BASE_MONTHLY
	 */
	public void setTimeBase(int timeBase) {
		this.timeBase = timeBase;
	}
	
	/**
	 * Starts the calculation within a new thread. It must be made sure that the {@link CalculationFinishedListener} is set.
	 */
	public void start() {
		final StockCalculator thisCalculator = this;
		worker = new SwingWorker<XYDataset, Void>() {
			
			@Override
			protected XYDataset doInBackground() {
				XYDataset ds = doCalculation();
				return ds;
			}
			
			@Override
			public void done() {
				try {
					get();
					
					// inform the listener
					calculationFinishedListener.calculationFinished(thisCalculator);
				} catch (InterruptedException | ExecutionException e) {
					log.error(e.getMessage(), e);
					JOptionPane.showMessageDialog(StockView.getInstance(), "Error after calculation has finished: \"" + e.getMessage() + "\"", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
			
		};

		worker.execute(); 
	}
	
	public XYDataset getDataset() {
		XYDataset retval = null;
		try {
			retval = worker.get();
		} catch (InterruptedException | ExecutionException e) {
			log.error(e.getMessage(), e);
			JOptionPane.showMessageDialog(StockView.getInstance(), "Error after calculation has finished: \"" + e.getMessage() + "\"", "Error", JOptionPane.ERROR_MESSAGE);
		}
		return retval;
	}
	
	/**
	 * Computes the dataset for the graph.
	 * 
	 * @return The computed data set.
	 */
	protected abstract XYDataset doCalculation();
	
	/**
	 * Returns an array containing the parameter names.
	 * 
	 * @return The names of the parameters.
	 */
	public abstract String[] getParameterNameArray();
	
	/**
	 * Returns an array containing the default values.
	 * 
	 * @return The default values.
	 */
	public abstract String[] getParameterDefaultValueArray();
	
	/**
	 * Sets the parameters before calculating the dataset.
	 * 
	 * @param params The parameters.
	 */
	public abstract void setParameter(String ... params);

	public CalculationFinishedListener getCalculationFinishedListener() {
		return calculationFinishedListener;
	}
	
	public void initStandardCalculationFinishedListener() {
		setCalculationFinishedListener(StockCalculatorFinishedListenerRegistry.getInstance().getCalculationFinishedListener(getClass()));
	}

	public void setCalculationFinishedListener(CalculationFinishedListener calculationFinishedListener) {
		this.calculationFinishedListener = calculationFinishedListener;
	}
	
	/**
	 * Sets additional information about the calculator. This field can be freely used for gaining information after the calculator has finished and is not used in the calculation.
	 * 
	 * @param additionalInfo The additional information.
	 */
	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}
	
	/**
	 * Return the additional information set.
	 * 
	 * @return The additional information.
	 */
	public String getAdditionalInfo() {
		return additionalInfo;
	}

}
