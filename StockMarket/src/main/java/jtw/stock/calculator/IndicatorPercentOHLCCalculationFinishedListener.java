package jtw.stock.calculator;

import java.awt.BasicStroke;
import java.awt.Color;

import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;

import jtw.stock.view.component.HighlightValueAxis;

public class IndicatorPercentOHLCCalculationFinishedListener extends StandardStockCalculationFinishedAdapter {
	
	public IndicatorPercentOHLCCalculationFinishedListener() {
	}

	@Override
	protected void manipulatePlot(XYPlot plot) {
		ValueMarker p100 = new ValueMarker(5, Color.BLUE, new BasicStroke(1));
		ValueMarker pm100 = new ValueMarker(-5, Color.RED, new BasicStroke(1));
	    plot.addRangeMarker(p100);
	    plot.addRangeMarker(pm100);
	    
//	    plot.getRangeAxis().setRange(new Range(-160, 160));
	}

	@Override
	protected boolean isSubPlot() {
		return true;
	}

	@Override
	protected ValueAxis getYAxis() {
		return new HighlightValueAxis("POHLC");
	}
	
}
