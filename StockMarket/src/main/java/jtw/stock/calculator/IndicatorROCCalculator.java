package jtw.stock.calculator;

import static jtw.stock.main.StockApplicationSettings.TIME_FRAME_MAX;

import java.util.List;

import org.jfree.data.xy.DefaultXYDataset;
import org.jfree.data.xy.XYDataset;

import jtw.stock.main.StockApplicationSettings;
import jtw.stock.model.Stock;
import jtw.stock.model.registry.StockDataRegistry;
import jtw.util.IntegerUtil;

public class IndicatorROCCalculator extends StockCalculator {

	private int period = 0;
	
	public IndicatorROCCalculator() {
	}
	
	public IndicatorROCCalculator(int period) {
		this.period = period;
	}

	@Override
	public String getDisplayName() {
		return "ROC";
	}

	@Override
	protected XYDataset doCalculation() {
		List<Stock> allStockList = StockDataRegistry.getInstance().getStockList(getStockName(), getTimeBase(), TIME_FRAME_MAX);
		List<Stock> currStockList = StockDataRegistry.getInstance().getStockList(getStockName(), getTimeBase());
		int currSize = currStockList.size() - period;
		int size = allStockList.size();
		double[][] data = new double[2][currSize];
		
		// ROC = [(Close - Close n periods ago) / (Close n periods ago)] * 100
		int j = 0;
		for (int i = period; i < size; i++) {
			Stock s = allStockList.get(i);
			Stock sn = allStockList.get(i - period);

			if (StockApplicationSettings.isInTimeFrame(s)) {
				double roc = 100d * (s.getClosing() - sn.getClosing()) / sn.getClosing();
				
				data[0][j] = s.getDate().getTimeInMillis();
				data[1][j] = roc;
				j++;
			}
		}

		DefaultXYDataset dataset = new DefaultXYDataset();
		dataset.addSeries(getDisplayName(), data);
		
		return dataset;
	}

	@Override
	public String[] getParameterNameArray() {
		return new String[] {"Period"};
	}

	@Override
	public String[] getParameterDefaultValueArray() {
		return new String[] {"12"};
	}

	@Override
	public void setParameter(String... params) {
		period = IntegerUtil.toInt(params[0], 12);
	}

}
