package jtw.stock.calculator;

import java.awt.BasicStroke;
import java.awt.Color;

import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.Range;

import jtw.stock.view.component.HighlightValueAxis;

public class IndicatorTSICalculationFinishedListener extends StandardStockCalculationFinishedAdapter {
	
	public IndicatorTSICalculationFinishedListener() {
	}

	@Override
	protected void manipulatePlot(XYPlot plot) {
		// TODO
		ValueMarker p80 = new ValueMarker(80, Color.BLUE, new BasicStroke(1));
		ValueMarker p0 = new ValueMarker(0, Color.GREEN, new BasicStroke(1));
		ValueMarker pm80 = new ValueMarker(-80, Color.RED, new BasicStroke(1));
	    plot.addRangeMarker(p80);
	    plot.addRangeMarker(p0);
	    plot.addRangeMarker(pm80);
	    
	    plot.getRangeAxis().setRange(new Range(-105, 105));
	}

	@Override
	protected boolean isSubPlot() {
		return true;
	}

	@Override
	protected ValueAxis getYAxis() {
		return new HighlightValueAxis("TSI");
	}
	
}
