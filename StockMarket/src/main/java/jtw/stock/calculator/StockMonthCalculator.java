package jtw.stock.calculator;

import java.util.Calendar;
import java.util.List;

import org.jfree.data.xy.DefaultXYDataset;

import jtw.stock.model.Stock;
import jtw.stock.model.registry.StockDataRegistry;

/**
 * Computes the month for each stock. Hereby, the months start with January = 1 and end with December = 12.
 * Note, that the month is increased by 1.
 * 
 * @see Calendar#MONTH
 * @see Calendar#JANUARY
 * @see Calendar#FEBRUARY
 * @see Calendar#MARCH
 * @see Calendar#APRIL
 * @see Calendar#MAY
 * @see Calendar#JUNE
 * @see Calendar#JULY
 * @see Calendar#AUGUST
 * @see Calendar#SEPTEMBER
 * @see Calendar#OCTOBER
 * @see Calendar#NOVEMBER
 * @see Calendar#DECEMBER
 * 
 * @author Jan-Thierry Wegener
 */
public class StockMonthCalculator extends StockCalculator {

	public String getDisplayName() {
		return getStockName() + " (month)";
	}

	@Override
	public String[] getParameterNameArray() {
		return new String[] {};
    }

	@Override
	public String[] getParameterDefaultValueArray() {
		return new String[] {};
    }

	@Override
	public void setParameter(String ... params) {
    }

	@Override
	protected DefaultXYDataset doCalculation() {
		List<Stock> stockList = StockDataRegistry.getInstance().getStockList(getStockName());
		int size = stockList.size();
		double[][] data = new double[2][size];

		for (int i = 0; i < size; i++) {
			Stock s = stockList.get(i);
			data[0][i] = s.getDate().getTimeInMillis();
			data[1][i] = s.getDate().get(Calendar.MONTH) + 1;
		}

		DefaultXYDataset dataset = new DefaultXYDataset();
		dataset.addSeries(getDisplayName(), data);
		
		return dataset;
	}
	
}
