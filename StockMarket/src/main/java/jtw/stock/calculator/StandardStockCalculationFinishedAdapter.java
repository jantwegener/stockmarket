package jtw.stock.calculator;

import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.xy.XYDataset;

import jtw.stock.model.registry.StockPlotColorRegistry;
import jtw.stock.model.registry.StockRendererRegistry;
import jtw.stock.view.StockView;
import jtw.stock.view.component.HighlightValueAxis;

public class StandardStockCalculationFinishedAdapter implements CalculationFinishedListener {

	public StandardStockCalculationFinishedAdapter() {
	}

	/**
	 * Marks whether the plot shall be added as a sub plot or not. By default
	 * <code>false</code> is returned. This means that the graph is added as a graph
	 * in the main graph.
	 * 
	 * @return <code>true</code> if the plot shall be added as a sub plot (i.e., as
	 *         a separate plot), <code>false</code> if it shall be added as a graph
	 *         in the main graph.
	 */
	protected boolean isSubPlot() {
		return false;
	}

	@Override
	public void calculationFinished(StockCalculator thisCalculator) {
		XYDataset dataset = thisCalculator.getDataset();
		
		final XYItemRenderer renderer = StockRendererRegistry.getInstance().getRenderer(thisCalculator.getName());
		renderer.setSeriesPaint(0, StockPlotColorRegistry.getInstance().getColor(thisCalculator.getName()));
		for (int i = 1; i < dataset.getSeriesCount(); i++) {
			renderer.setSeriesPaint(i, StockPlotColorRegistry.getInstance().getColor(thisCalculator.getName() + "/" + i));
		}
		
		XYPlot plot = getPlot(dataset, getXAxis(), getYAxis(), renderer);
		manipulatePlot(plot);
		addHighlightTick(dataset, plot);
		StockView.getInstance().addPlot(getPlotName(thisCalculator), plot, isSubPlot());
	}
	
	protected void addHighlightTick(XYDataset dataset, XYPlot plot) {
		if (dataset.getItemCount(0) > 0 && plot.getRangeAxis() instanceof HighlightValueAxis) {
			HighlightValueAxis yAxis = (HighlightValueAxis) plot.getRangeAxis();
			if (isSubPlot()) {
				yAxis.addHighlightTick(dataset.getYValue(0, dataset.getItemCount(0) - 1));
			} else {
				StockView.Y_AXIS_PRICE.addHighlightTick(dataset.getYValue(0, dataset.getItemCount(0) - 1));
			}
		}
	}
	
	protected void manipulatePlot(XYPlot plot) {
	}
	
	protected XYPlot getPlot(XYDataset dataset, ValueAxis xAxis, ValueAxis yAxis, XYItemRenderer renderer) {
		return new XYPlot(dataset, xAxis, yAxis, renderer);
	}
	
	/**
	 * Returns an x-axis for the plot. It must return a new x-axis. Otherwise, auto range does not work properly.
	 * <p>
	 * Made final to avoid making the same mistake again.
	 * </p>
	 * 
	 * @return A new {@link DateAxis} object.
	 */
	protected final ValueAxis getXAxis() {
		return new DateAxis("date");
	}

	/**
	 * Returns an y-axis for the plot. It must return a new y-axis. Otherwise, auto range does not work properly.
	 * In fact, the returned y-axis should not matter, since the one from the {@link CombinedDomainXYPlot} is used.
	 * 
	 * @return A new {@link DateAxis} object.
	 */
	protected ValueAxis getYAxis() {
		return new HighlightValueAxis();
	}

	/**
	 * Returns the name of the plot. By default the name of the calculator is
	 * returned.
	 * 
	 * @param thisCalculator
	 *            The calculator that has finished and whose name shall be returned.
	 * 
	 * @return The name of the plot.
	 */
	protected String getPlotName(StockCalculator thisCalculator) {
		return thisCalculator.getName();
	}

}
