package jtw.stock.calculator;

public interface CalculationFinishedListener {
	
	/**
	 * Informs the listener which calculator has finished. Is called by the calculator itself.
	 * 
	 * @param thisCalculator The calculator that finished.
	 */
	public void calculationFinished(StockCalculator thisCalculator);
	
}
