package jtw.stock.calculator;

import static jtw.stock.main.StockApplicationSettings.TIME_FRAME_MAX;

import java.util.List;

import org.jfree.data.xy.DefaultXYDataset;

import jtw.stock.main.StockApplicationSettings;
import jtw.stock.model.Stock;
import jtw.stock.model.registry.StockDataRegistry;
import jtw.util.IntegerUtil;

public class StockExponentialMovingAverageCalculator extends StockCalculator {

	private double alpha = -1;
	
	public StockExponentialMovingAverageCalculator() {
	}
	
	public String getDisplayName() {
		return "EMA (" + alpha + ")";
	}
	
	public String getName() {
		return super.getName() + ":" + alpha;
	}

	@Override
	public String[] getParameterNameArray() {
		return new String[] {"Period"};
    }

	@Override
	public String[] getParameterDefaultValueArray() {
		return new String[] {"20"};
    }

	@Override
	public void setParameter(String ... params) {
		int days = IntegerUtil.toInt(params[0], 20);
		setDays(days);
    }

	@Override
	protected DefaultXYDataset doCalculation() {
		List<Stock> allStockList = StockDataRegistry.getInstance().getStockList(getStockName(), getTimeBase(), TIME_FRAME_MAX);
		List<Stock> currStockList = StockDataRegistry.getInstance().getStockList(getStockName(), getTimeBase());
		int currSize = currStockList.size();
		int size = allStockList.size();
		double[][] data = new double[2][currSize];
		
		int j = 0;
		double pre = StockDataRegistry.getInstance().getReferencedStockValue(allStockList.get(0));
		for (int i = 0; i < size; i++) {
			Stock s = allStockList.get(i);
			pre = alpha * StockDataRegistry.getInstance().getReferencedStockValue(s) + (1 - alpha) * pre;
			if (StockApplicationSettings.isInTimeFrame(s)) {
				data[0][j] = s.getDate().getTimeInMillis();
				data[1][j] = pre;
				j++;
			}
		}
		
		DefaultXYDataset dataset = new DefaultXYDataset();
		dataset.addSeries(getDisplayName(), data);
		
		return dataset;
	}
	

	public double getAlpha() {
		return alpha;
	}

	public void setAlpha(double alpha) {
		this.alpha = alpha;
	}
	
	public void setDays(int d) {
		alpha = 1d / d;
	}

}
