package jtw.stock.calculator;

import static jtw.stock.main.StockApplicationSettings.TIME_FRAME_MAX;

import java.util.ArrayList;
import java.util.List;

import org.jfree.data.xy.DefaultXYDataset;
import org.jfree.data.xy.XYDataset;

import jtw.stock.main.StockApplicationSettings;
import jtw.stock.model.Stock;
import jtw.stock.model.registry.StockDataRegistry;
import jtw.util.IntegerUtil;
import jtw.util.MathUtil;
import jtw.util.ObjectToDouble;

/**
 * The calculator for the stochastic (STOCH) indicator.
 * 
 * @author Jan-Thierry Wegener
 *
 * @see {@link https://www.tradingtechnologies.com/xtrader-help/x-study/technical-indicator-definitions/stochastic-stoch/}
 */
public class IndicatorSTOCHCalculator extends StockCalculator {

	private int period;
	
	private int kma;
	
	private int dma;

	private ObjectToDouble<Double> otd = new ObjectToDouble<Double>() {
		@Override
		public double objToDouble(Double o) {
			return o;
		}
	};
	
	public IndicatorSTOCHCalculator() {
	}

	public IndicatorSTOCHCalculator(int period, int kma, int dma) {
		this.period = period;
		this.kma = kma;
		this.dma = dma;
	}
	
	@Override
	public String getDisplayName() {
		return "STOCH";
	}
	
	public String getName() {
		return super.getName() + ":" + period + ":" + kma + ":" + dma;
	}

	@Override
	public String[] getParameterNameArray() {
		return new String[] {"Period", "Kma", "Dma"};
    }

	@Override
	public String[] getParameterDefaultValueArray() {
		return new String[] {"14", "5", "3"};
    }

	@Override
	public void setParameter(String ... params) {
		period = IntegerUtil.toInt(params[0], 14);
		kma = IntegerUtil.toInt(params[0], 5);
		dma = IntegerUtil.toInt(params[0], 3);
    }

	@Override
	protected XYDataset doCalculation() {
		List<Stock> allStockList = StockDataRegistry.getInstance().getStockList(getStockName(), getTimeBase(), TIME_FRAME_MAX);
		List<Stock> currStockList = StockDataRegistry.getInstance().getStockList(getStockName(), getTimeBase());
		int currSize = currStockList.size();
		int size = allStockList.size();
		double[][] dataFastK = new double[2][currSize];
		double[][] dataSlowK = new double[2][currSize];
		double[][] dataSlowD = new double[2][currSize];

		List<Double> valPriceList = new ArrayList<>();
		List<Double> fastKPriceList = new ArrayList<>();
		List<Double> slowKPriceList = new ArrayList<>();
		
		int j = 0;
		for (int i = 0; i < size; i++) {
			Stock s = allStockList.get(i);

			double val = ((double)(s.getClosing() - s.getLow())) / (double)(s.getHigh() - s.getLow());
			valPriceList.add(val);
			
			double fastK = 100d * MathUtil.average(valPriceList, otd, i - period+1, i+1);
			fastKPriceList.add(fastK);
			
			double slowK = MathUtil.average(fastKPriceList, otd, i - kma+1, i+1);
			slowKPriceList.add(slowK);
			
			double slowD = MathUtil.average(slowKPriceList, otd, i - dma+1, i+1);
			
			if (StockApplicationSettings.isInTimeFrame(s)) {
				dataFastK[0][j] = s.getDate().getTimeInMillis();
				dataFastK[1][j] = fastK;

				dataSlowK[0][j] = s.getDate().getTimeInMillis();
				dataSlowK[1][j] = slowK;

				dataSlowD[0][j] = s.getDate().getTimeInMillis();
				dataSlowD[1][j] = slowD;
				
				j++;
			}
		}

		DefaultXYDataset dataset = new DefaultXYDataset();
		dataset.addSeries(getDisplayName() + " (Fast%K)", dataFastK);
		dataset.addSeries(getDisplayName() + " (Slow%K)", dataSlowK);
		dataset.addSeries(getDisplayName() + " (Slow%D)", dataSlowD);
		
		return dataset;
	}

	public int getPeriod() {
		return period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}

	public int getKma() {
		return kma;
	}

	public void setKma(int kma) {
		this.kma = kma;
	}

	public int getDma() {
		return dma;
	}

	public void setDma(int dma) {
		this.dma = dma;
	}
}
