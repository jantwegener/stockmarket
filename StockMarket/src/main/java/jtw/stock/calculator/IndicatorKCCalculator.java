package jtw.stock.calculator;

import static jtw.stock.main.StockApplicationSettings.TIME_FRAME_MAX;

import java.util.ArrayList;
import java.util.List;

import org.jfree.data.xy.DefaultXYDataset;
import org.jfree.data.xy.XYDataset;

import jtw.stock.main.StockApplicationSettings;
import jtw.stock.model.Stock;
import jtw.stock.model.registry.StockDataRegistry;
import jtw.util.DoubleUtil;
import jtw.util.IntegerUtil;
import jtw.util.MathUtil;
import jtw.util.ObjectToDouble;

/**
 * The calculator for the Keltner channel (KC) indicator.
 * 
 * @author Jan-Thierry Wegener
 * 
 * @see {@link https://www.tradingtechnologies.com/xtrader-help/x-study/technical-indicator-definitions/keltner-channel-kc/}
 */
public class IndicatorKCCalculator extends StockCalculator {
	
	private int period;

	private double factor;

	private ObjectToDouble<Double> otd = new ObjectToDouble<Double>() {
		@Override
		public double objToDouble(Double o) {
			return o;
		}
	};
	
	public IndicatorKCCalculator() {
	}
	
	public IndicatorKCCalculator(int period, double factor) {
		this.period = period;
		this.factor = factor;
	}

	@Override
	public String getDisplayName() {
		return "KC";
	}
	
	public String getName() {
		return super.getName() + ":" + period + ":" + factor;
	}

	@Override
	public String[] getParameterNameArray() {
		return new String[] {"Period", "Factor"};
    }

	@Override
	public String[] getParameterDefaultValueArray() {
		return new String[] {"20", "2"};
    }

	@Override
	public void setParameter(String ... params) {
		period = IntegerUtil.toInt(params[0], 20);
		factor = DoubleUtil.toDouble(params[0], 2d);
    }

	@Override
	protected XYDataset doCalculation() {
		List<Stock> allStockList = StockDataRegistry.getInstance().getStockList(getStockName(), getTimeBase(), TIME_FRAME_MAX);
		List<Stock> currStockList = StockDataRegistry.getInstance().getStockList(getStockName(), getTimeBase());
		int size = allStockList.size();
		int currSize = Math.min(currStockList.size(), size - 1);
		double[][] dataUp = new double[2][currSize];
		double[][] dataMid = new double[2][currSize];
		double[][] dataLow = new double[2][currSize];

		List<Double> typicalPriceList = new ArrayList<>();
		List<Double> atrList = new ArrayList<>();
		
		int j = 0;
		for (int i = 1; i < size; i++) {
			Stock s = allStockList.get(i);
			Stock sPrev = allStockList.get(i - 1);
			
			double typicalPrice = (s.getHigh() + s.getLow() + s.getClosing()) / 300d;
			typicalPriceList.add(typicalPrice);

			double mid = MathUtil.average(typicalPriceList, otd, i - period+1, i+1);
			double atr = calculateTrueRange(s, sPrev);
			atrList.add(atr);
			double y = MathUtil.average(atrList, otd, i - period+1, i+1);

			double up = mid + factor * y;
			double low = mid - factor * y;
			
			if (StockApplicationSettings.isInTimeFrame(s)) {
				dataUp[0][j] = s.getDate().getTimeInMillis();
				dataUp[1][j] = up;

				dataMid[0][j] = s.getDate().getTimeInMillis();
				dataMid[1][j] = mid;

				dataLow[0][j] = s.getDate().getTimeInMillis();
				dataLow[1][j] = low;
				
				j++;
			}
		}

		DefaultXYDataset dataset = new DefaultXYDataset();
		dataset.addSeries(getDisplayName() + " (up)", dataUp);
		dataset.addSeries(getDisplayName() + " (mid)", dataMid);
		dataset.addSeries(getDisplayName() + " (low)", dataLow);
		
		return dataset;
	}

	private double calculateTrueRange(Stock s, Stock sPrev) {
		double high = s.getHigh();
		double low = s.getLow();
		double closePrev = sPrev.getClosing();
		return MathUtil.max((high - low), Math.abs(high - closePrev), Math.abs(low - closePrev));
	}

	public int getPeriod() {
		return period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}

	public double getFactor() {
		return factor;
	}

	public void setFactor(double factor) {
		this.factor = factor;
	}

}
