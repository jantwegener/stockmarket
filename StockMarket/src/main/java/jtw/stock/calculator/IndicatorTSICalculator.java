package jtw.stock.calculator;

import static jtw.stock.main.StockApplicationSettings.TIME_FRAME_MAX;

import java.util.List;

import org.jfree.data.xy.DefaultXYDataset;
import org.jfree.data.xy.XYDataset;

import jtw.stock.main.StockApplicationSettings;
import jtw.stock.model.Stock;
import jtw.stock.model.registry.StockDataRegistry;
import jtw.util.IntegerUtil;

public class IndicatorTSICalculator extends StockCalculator {

	private int r;
	private int s;
	private int signal;
	
	public IndicatorTSICalculator() {
	}

	public IndicatorTSICalculator(int r, int s, int signal) {
		this.r = r;
		this.s = s;
		this.signal = signal;
	}
	
	@Override
	public String getDisplayName() {
		return "TSI";
	}
	
	public String getName() {
		return super.getName() + ":" + r + ":" + s + ":" + signal;
	}

	@Override
	public String[] getParameterNameArray() {
		return new String[] {"r", "s", "signal"};
    }

	@Override
	public String[] getParameterDefaultValueArray() {
		return new String[] {"25", "13", "7"};
    }

	@Override
	public void setParameter(String ... params) {
		r = IntegerUtil.toInt(params[0], 25);
		s = IntegerUtil.toInt(params[1], 13);
		signal = IntegerUtil.toInt(params[2], 7);
    }

	@Override
	protected XYDataset doCalculation() {
		List<Stock> allStockList = StockDataRegistry.getInstance().getStockList(getStockName(), getTimeBase(), TIME_FRAME_MAX);
		List<Stock> currStockList = StockDataRegistry.getInstance().getStockList(getStockName(), getTimeBase());
		int size = allStockList.size();
		int currSize = Math.min(currStockList.size(), size - 1);
		double[][] data = new double[2][currSize];
		double[][] dataSignal = new double[2][currSize];

		double sigPrev = 0;
		double emaMTMPrev = 0;
		double emaMTMAbsPrev = 0;
		double emaEmaMTMPrev = 0;
		double emaEmaMTMAbsPrev = 0;
		
		int j = 0;
		for (int i = 1; i < size; i++) {
			Stock stock = allStockList.get(i);
			Stock stockPrev = allStockList.get(i - 1);

			double closeToday = stock.getClosing();
			double closePrev = stockPrev.getClosing();
			
			double mtm = closeToday - closePrev;
			double mtmAbs = Math.abs(closeToday - closePrev);

			double emaMTM = 2d / (r + 1d) * (mtm - emaMTMPrev) + emaMTMPrev;
			double emaMTMAbs = 2d / (r + 1d) * (mtmAbs - emaMTMAbsPrev) + emaMTMAbsPrev;

			double emaEmaMTM = 2d / (s + 1d) * (emaMTM - emaEmaMTMPrev) + emaEmaMTMPrev;
			double emaEmaMTMAbs = 2d / (s + 1d) * (emaMTMAbs - emaEmaMTMAbsPrev) + emaEmaMTMAbsPrev;
			
			double tsi = 100d * emaEmaMTM / emaEmaMTMAbs;
			
			double sig = 2d / (signal + 1d) * (tsi - sigPrev) + sigPrev;
			
			if (StockApplicationSettings.isInTimeFrame(stock)) {
				data[0][j] = stock.getDate().getTimeInMillis();
				data[1][j] = tsi;
				
				dataSignal[0][j] = stock.getDate().getTimeInMillis();
				dataSignal[1][j] = sig;
				j++;
			}

			emaMTMPrev = emaMTM;
			emaMTMAbsPrev = emaMTMAbs;
			emaEmaMTMPrev = emaEmaMTM;
			emaEmaMTMAbsPrev = emaEmaMTMAbs;

			sigPrev = sig;
		}

		DefaultXYDataset dataset = new DefaultXYDataset();
		dataset.addSeries(getDisplayName(), data);
		dataset.addSeries(getDisplayName() + " (signal)", dataSignal);
		
		return dataset;
	}

	public int getR() {
		return r;
	}

	public void setR(int r) {
		this.r = r;
	}

	public int getS() {
		return s;
	}

	public void setS(int s) {
		this.s = s;
	}

	public int getSignal() {
		return signal;
	}

	public void setSignal(int signal) {
		this.signal = signal;
	}

}
