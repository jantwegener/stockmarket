package jtw.stock.calculator;

import java.awt.BasicStroke;
import java.awt.Color;

import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.Range;

import jtw.stock.view.component.HighlightValueAxis;

public class IndicatorROCCalculationFinishedListener extends StandardStockCalculationFinishedAdapter {
	
	public IndicatorROCCalculationFinishedListener() {
	}

	@Override
	protected void manipulatePlot(XYPlot plot) {
		ValueMarker p30 = new ValueMarker(30, Color.RED, new BasicStroke(1));
		ValueMarker p70 = new ValueMarker(70, Color.BLUE, new BasicStroke(1));
		ValueMarker pn0 = new ValueMarker(0, Color.DARK_GRAY, new BasicStroke(1, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_BEVEL, 1f, new float[]{3, 6}, 0));
		ValueMarker n30 = new ValueMarker(-30, Color.RED, new BasicStroke(1));
		ValueMarker n70 = new ValueMarker(-70, Color.BLUE, new BasicStroke(1));
		
	    plot.addRangeMarker(p30);
	    plot.addRangeMarker(p70);
	    plot.addRangeMarker(pn0);
	    plot.addRangeMarker(n30);
	    plot.addRangeMarker(n70);
	    
	    plot.getRangeAxis().setRange(new Range(-100, 100));
	}

	@Override
	protected boolean isSubPlot() {
		return true;
	}

	@Override
	protected ValueAxis getYAxis() {
		return new HighlightValueAxis("ROC");
	}
	
}
