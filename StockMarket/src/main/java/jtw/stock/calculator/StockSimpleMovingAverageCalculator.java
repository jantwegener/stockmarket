package jtw.stock.calculator;

import static jtw.stock.main.StockApplicationSettings.TIME_FRAME_MAX;

import java.util.List;

import org.jfree.data.xy.DefaultXYDataset;

import jtw.stock.model.Stock;
import jtw.stock.model.registry.StockDataRegistry;
import jtw.util.IntegerUtil;
import jtw.util.MathUtil;
import jtw.util.ObjectToDouble;

public class StockSimpleMovingAverageCalculator extends StockCalculator {
	
	private int days = 0;
	
	private ObjectToDouble<Stock> otd = new ObjectToDouble<Stock>() {
		@Override
		public double objToDouble(Stock stock) {
			return StockDataRegistry.getInstance().getReferencedStockValue(stock);
		}
	};
	
	public StockSimpleMovingAverageCalculator() {
	}
	
	public String getDisplayName() {
		return "SMA (" + days + ")";
	}
	
	public String getName() {
		return super.getName() + ":" + days;
	}

	@Override
	public String[] getParameterNameArray() {
		return new String[] {"Period"};
    }

	@Override
	public String[] getParameterDefaultValueArray() {
		return new String[] {"20"};
    }

	@Override
	public void setParameter(String ... params) {
		days = IntegerUtil.toInt(params[0], 20);
    }

	@Override
	protected DefaultXYDataset doCalculation() {
		List<Stock> allStockList = StockDataRegistry.getInstance().getStockList(getStockName(), getTimeBase(), TIME_FRAME_MAX);
		List<Stock> currStockList = StockDataRegistry.getInstance().getStockList(getStockName(), getTimeBase());
		int currSize = currStockList.size();
		int size = allStockList.size();
		double[][] data = new double[2][currSize];
		
		for (int i = 0; i < currSize; i++) {
			Stock s = allStockList.get(size - (currSize - i));
			data[0][i] = s.getDate().getTimeInMillis();
			// + 1 since the bound is exclusive
			data[1][i] = MathUtil.average(allStockList, otd, size - (currSize - i) - days + 1, (size - (currSize - i) + 1));
		}
		
		DefaultXYDataset dataset = new DefaultXYDataset();
		dataset.addSeries(getDisplayName(), data);
		
		return dataset;
	}
	
	public void setDays(int days) {
		this.days = days;
	}
	
	public int getDays() {
		return days;
	}

}
