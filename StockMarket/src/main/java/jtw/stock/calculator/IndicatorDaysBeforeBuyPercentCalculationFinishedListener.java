package jtw.stock.calculator;

import org.jfree.chart.axis.ValueAxis;

import jtw.stock.view.component.HighlightValueAxis;

public class IndicatorDaysBeforeBuyPercentCalculationFinishedListener extends StandardStockCalculationFinishedAdapter {

	public IndicatorDaysBeforeBuyPercentCalculationFinishedListener() {
	}
	
	@Override
	protected boolean isSubPlot() {
		return true;
	}

	@Override
	protected ValueAxis getYAxis() {
		return new HighlightValueAxis("DBBP");
	}

}
