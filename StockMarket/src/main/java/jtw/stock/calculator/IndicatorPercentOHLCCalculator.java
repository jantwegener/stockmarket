package jtw.stock.calculator;

import static jtw.stock.main.StockApplicationSettings.TIME_FRAME_MAX;

import java.util.List;

import org.jfree.data.xy.DefaultXYDataset;
import org.jfree.data.xy.XYDataset;

import jtw.stock.main.StockApplicationSettings;
import jtw.stock.model.Stock;
import jtw.stock.model.registry.StockDataRegistry;

public class IndicatorPercentOHLCCalculator extends StockCalculator {
	
	public IndicatorPercentOHLCCalculator() {
	}

	@Override
	public String getDisplayName() {
		return "POHLC";
	}

	@Override
	public String[] getParameterNameArray() {
		return new String[] {};
    }

	@Override
	public String[] getParameterDefaultValueArray() {
		return new String[] {};
    }

	@Override
	public void setParameter(String ... params) {
    }

	@Override
	protected XYDataset doCalculation() {
		List<Stock> allStockList = StockDataRegistry.getInstance().getStockList(getStockName(), getTimeBase(), TIME_FRAME_MAX);
		List<Stock> currStockList = StockDataRegistry.getInstance().getStockList(getStockName(), getTimeBase());
		int currSize = currStockList.size();
		int size = allStockList.size();
		double[][] dataOH = new double[2][currSize];
		double[][] dataOL = new double[2][currSize];
		double[][] dataCH = new double[2][currSize];
		double[][] dataCL = new double[2][currSize];
		double[][] dataOC = new double[2][currSize];
		
		int j = 0;
		for (int i = 0; i < size; i++) {
			Stock s = allStockList.get(i);
			
			// open - high
			double oh = 100d * (s.getHigh() - s.getOpening()) / s.getOpening();
			// open - low
			double ol = 100d * (s.getOpening() - s.getLow()) / s.getOpening();
			// close - high
			double ch = 100d * (s.getHigh() - s.getClosing()) / s.getClosing();
			// close - low
			double cl = 100d * (s.getClosing() - s.getLow()) / s.getClosing();
			// open - close
			double oc = 100d * Math.abs(s.getClosing() - s.getOpening()) / s.getOpening();
			
			if (StockApplicationSettings.isInTimeFrame(s)) {
				dataOH[0][j] = s.getDate().getTimeInMillis();
				dataOH[1][j] = oh;

				dataOL[0][j] = s.getDate().getTimeInMillis();
				dataOL[1][j] = ol;

				dataCH[0][j] = s.getDate().getTimeInMillis();
				dataCH[1][j] = ch;

				dataCL[0][j] = s.getDate().getTimeInMillis();
				dataCL[1][j] = cl;

				dataOC[0][j] = s.getDate().getTimeInMillis();
				dataOC[1][j] = oc;
				j++;
			}
		}

		DefaultXYDataset dataset = new DefaultXYDataset();
		dataset.addSeries("open - high", dataOH);
		dataset.addSeries("open - low", dataOL);
		dataset.addSeries("close - high", dataCH);
		dataset.addSeries("close - low", dataCL);
		dataset.addSeries("open - close", dataOC);
		
		return dataset;
	}
	
}
