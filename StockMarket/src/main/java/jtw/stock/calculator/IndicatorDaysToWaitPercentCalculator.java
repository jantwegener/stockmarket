package jtw.stock.calculator;

import static jtw.stock.main.StockApplicationSettings.TIME_FRAME_MAX;

import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import org.jfree.data.xy.DefaultXYDataset;
import org.jfree.data.xy.XYDataset;

import jtw.stock.main.StockApplicationSettings;
import jtw.stock.model.Stock;
import jtw.stock.model.registry.StockDataRegistry;
import jtw.util.DoubleUtil;
import jtw.util.MathUtil;
import jtw.util.ObjectToDouble;

public class IndicatorDaysToWaitPercentCalculator extends StockCalculator {
	
	private double percent;
	
	private int period = 100;

	private ObjectToDouble<Double> otd = new ObjectToDouble<Double>() {
		@Override
		public double objToDouble(Double o) {
			return o;
		}
	};
	
	public IndicatorDaysToWaitPercentCalculator() {
	}
	
	public IndicatorDaysToWaitPercentCalculator(double percent) {
		this.percent = percent;
	}

	@Override
	public String getDisplayName() {
		return "DTWP";
	}

	@Override
	protected XYDataset doCalculation() {
		List<Stock> allStockList = StockDataRegistry.getInstance().getStockList(getStockName(), getTimeBase(), TIME_FRAME_MAX);
		List<Stock> currStockList = StockDataRegistry.getInstance().getStockList(getStockName(), getTimeBase());
		int currSize = currStockList.size() - 1;
		int size = allStockList.size();
		double[][] data = new double[2][currSize];
		double[][] dataSMA = new double[2][currSize];
		
		List<Double> daysList = new ArrayList<>();
		
		int j = 0;
		for (int i = 1; i < size; i++) {
			Stock s = allStockList.get(i);
			Stock sn = searchStock(i, allStockList);

			if (StockApplicationSettings.isInTimeFrame(s)) {
				// default value if no matching stock is found
				double days = Double.NaN;
				if (sn != null) {
					days = ChronoUnit.DAYS.between(s.getDate().toInstant(), sn.getDate().toInstant()); 
				}
				
				data[0][j] = s.getDate().getTimeInMillis();
				data[1][j] = days;
				
				daysList.add(days);
				
				dataSMA[0][j] = s.getDate().getTimeInMillis();
				dataSMA[1][j] = MathUtil.average(daysList, otd, i - period+1, i+1);
				
				j++;
			}
		}

		DefaultXYDataset dataset = new DefaultXYDataset();
		dataset.addSeries(getDisplayName(), data);
//		dataset.addSeries(getDisplayName() + " (SMA)", dataSMA);
		
		return dataset;
	}
	
	private Stock searchStock(int currIndex, List<Stock> allStockList) {
		Stock currStock = allStockList.get(currIndex);
		double minPrice = StockDataRegistry.getInstance().getReferencedStockValue(currStock) * (1 + percent);
		
		Stock found = null;
		for (int i = currIndex + 1; i < allStockList.size() && found == null; i++) {
			Stock s = allStockList.get(i);
			if (StockDataRegistry.getInstance().getReferencedStockValue(s) > minPrice) {
				found = s;
			}
		}
		
		return found;
	}
	
	@Override
	public String[] getParameterNameArray() {
		return new String[] { "Percent" };
	}

	@Override
	public String[] getParameterDefaultValueArray() {
		return new String[] { "0.05" };
	}

	@Override
	public void setParameter(String... params) {
		percent = DoubleUtil.toDouble(params[0], 0.05);
	}
	
}
