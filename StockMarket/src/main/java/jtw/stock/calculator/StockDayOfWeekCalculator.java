package jtw.stock.calculator;

import java.util.Calendar;
import java.util.List;

import org.jfree.data.xy.DefaultXYDataset;

import jtw.stock.model.Stock;
import jtw.stock.model.registry.StockDataRegistry;

/**
 * Computes the day of the week for each stock. Hereby, the week starts with Monday = 1 and ends with Sunday = 7.
 * Note, that the day of the week is reduced by 1.
 * 
 * @see Calendar#DAY_OF_WEEK
 * @see Calendar#MONDAY
 * @see Calendar#TUESDAY
 * @see Calendar#WEDNESDAY
 * @see Calendar#THURSDAY
 * @see Calendar#FRIDAY
 * @see Calendar#SATURDAY
 * @see Calendar#SUNDAY
 * 
 * @author Jan-Thierry Wegener
 */
public class StockDayOfWeekCalculator extends StockCalculator {

	public String getDisplayName() {
		return getStockName() + " (day of week)";
	}
	
	@Override
	public String getTooltip() {
		return "Returns the day of the week. 1 = Monday, 2 = Tuesday, ..., 6 = Saturday, 0 = Sunday";
	}
	
	@Override
	public String[] getParameterNameArray() {
		return new String[] {};
    }

	@Override
	public String[] getParameterDefaultValueArray() {
		return new String[] {};
    }

	@Override
	public void setParameter(String ... params) {
    }

	@Override
	protected DefaultXYDataset doCalculation() {
		List<Stock> stockList = StockDataRegistry.getInstance().getStockList(getStockName());
		int size = stockList.size();
		double[][] data = new double[2][size];

		for (int i = 0; i < size; i++) {
			Stock s = stockList.get(i);
			data[0][i] = s.getDate().getTimeInMillis();
			data[1][i] = s.getDate().get(Calendar.DAY_OF_WEEK) - 1;
		}

		DefaultXYDataset dataset = new DefaultXYDataset();
		dataset.addSeries(getDisplayName(), data);
		
		return dataset;
	}
	
}
