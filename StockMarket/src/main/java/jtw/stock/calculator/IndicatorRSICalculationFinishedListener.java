package jtw.stock.calculator;

import java.awt.BasicStroke;
import java.awt.Color;

import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.Range;

import jtw.stock.view.component.HighlightValueAxis;

public class IndicatorRSICalculationFinishedListener extends StandardStockCalculationFinishedAdapter {
	
	public IndicatorRSICalculationFinishedListener() {
	}

	@Override
	protected void manipulatePlot(XYPlot plot) {
		ValueMarker p30 = new ValueMarker(30, Color.RED, new BasicStroke(1));
		ValueMarker p70 = new ValueMarker(70, Color.BLUE, new BasicStroke(1));
	    plot.addRangeMarker(p30);
	    plot.addRangeMarker(p70);
	    
	    plot.getRangeAxis().setRange(new Range(0, 100));
	}

	@Override
	protected boolean isSubPlot() {
		return true;
	}

	@Override
	protected ValueAxis getYAxis() {
		return new HighlightValueAxis("RSI");
	}
	
}
