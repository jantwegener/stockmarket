package jtw.stock.calculator;

import org.jfree.chart.axis.ValueAxis;

import jtw.stock.view.component.HighlightValueAxis;

public class StockVolumeCalculationFinishedListener extends StandardStockCalculationFinishedAdapter {
	
	public StockVolumeCalculationFinishedListener() {
	}

	protected boolean isSubPlot() {
		return true;
	}

	protected ValueAxis getYAxis() {
		return new HighlightValueAxis("Volume");
	}
	
}
