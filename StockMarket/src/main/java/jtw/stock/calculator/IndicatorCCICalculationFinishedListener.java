package jtw.stock.calculator;

import java.awt.BasicStroke;
import java.awt.Color;

import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.Range;

import jtw.stock.view.component.HighlightValueAxis;

public class IndicatorCCICalculationFinishedListener extends StandardStockCalculationFinishedAdapter {
	
	public IndicatorCCICalculationFinishedListener() {
	}

	@Override
	protected void manipulatePlot(XYPlot plot) {
		// TODO
		ValueMarker p100 = new ValueMarker(100, Color.BLUE, new BasicStroke(1));
		ValueMarker pm100 = new ValueMarker(-100, Color.RED, new BasicStroke(1));
	    plot.addRangeMarker(p100);
	    plot.addRangeMarker(pm100);
	    
	    plot.getRangeAxis().setRange(new Range(-160, 160));
	}

	@Override
	protected boolean isSubPlot() {
		return true;
	}

	@Override
	protected ValueAxis getYAxis() {
		return new HighlightValueAxis("CCI");
	}
	
}
