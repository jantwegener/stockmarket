package jtw.stock.calculator.report;

import java.util.ArrayList;
import java.util.List;

import org.jfree.data.xy.XYDataset;

import jtw.stock.calculator.StockCalculator;

public class TSIReportCalculationFinishedListener extends StandardReportCalculationFinishedListener {

	protected TSIReportCalculationFinishedListener() {
	}

	@Override
	protected List<ReportResult> getResult(StockCalculator calculator) {
		XYDataset ds = calculator.getDataset();
		
		List<ReportResult> resultList = new ArrayList<>();
		// crossover
		ReportResult r;
		if (getLastY(ds, 0) > getLastY(ds, 1)) {
			r = new ReportResult(calculator.getDisplayName() + " (signal line)", ReportResult.BUY, "-");
		} else if (getLastY(ds) < getLastY(ds, 1)) {
			r = new ReportResult(calculator.getDisplayName() + " (signal line)", ReportResult.SELL, "-");
		} else {
			r = new ReportResult(calculator.getDisplayName() + " (signal line)", ReportResult.HOLD, "-");
		}
		resultList.add(r);
		
		// > 0 or < 0
		if (getLastY(ds, 0) > 0) {
			r = new ReportResult(calculator.getDisplayName() + " (zero)", ReportResult.BUY, "-");
		} else if (getLastY(ds) < 0) {
			r = new ReportResult(calculator.getDisplayName() + " (zero)", ReportResult.SELL, "-");
		} else {
			r = new ReportResult(calculator.getDisplayName() + " (zero)", ReportResult.HOLD, "-");
		}
		resultList.add(r);
		
		return resultList;
	}

}
