package jtw.stock.calculator.report;

public class ReportResult {

	public static final int BUY = 1;
	public static final int HOLD = 2;
	public static final int SELL = 4;
	
	private final String name;
	
	private final int suggestion;
	
	private final String text;
	
	public ReportResult(String name, int suggestion, String text) {
		this.name = name;
		this.suggestion = suggestion;
		this.text = text;
	}

	public String getName() {
		return name;
	}

	public int getSuggestion() {
		return suggestion;
	}
	
	public String getText() {
		return text;
	}
	
}
