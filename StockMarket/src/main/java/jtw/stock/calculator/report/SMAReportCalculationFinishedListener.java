package jtw.stock.calculator.report;

import java.util.ArrayList;
import java.util.List;

import org.jfree.data.xy.XYDataset;

import jtw.stock.calculator.StockCalculator;
import jtw.stock.model.registry.StockDataRegistry;

public class SMAReportCalculationFinishedListener extends StandardReportCalculationFinishedListener {

	protected SMAReportCalculationFinishedListener() {
	}

	@Override
	protected List<ReportResult> getResult(StockCalculator calculator) {
		XYDataset ds = calculator.getDataset();
		
		XYDataset dsCurr = StockDataRegistry.getInstance().getReferencedDataset();

		List<ReportResult> result = new ArrayList<>();
		
		ReportResult r;
		if (getLastY(dsCurr) > getLastY(ds)) {
			r = new ReportResult(calculator.getDisplayName(), ReportResult.BUY, "-");
		} else if (getLastY(dsCurr) < getLastY(ds)) {
			r = new ReportResult(calculator.getDisplayName(), ReportResult.SELL, "-");
		} else {
			r = new ReportResult(calculator.getDisplayName(), ReportResult.HOLD, "-");
		}
		result.add(r);
		
		return result;
	}
	
}
