package jtw.stock.calculator.report;

import java.util.List;

import javax.swing.JOptionPane;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jfree.data.xy.XYDataset;

import jtw.stock.calculator.CalculationFinishedListener;
import jtw.stock.calculator.StockCalculator;
import jtw.stock.view.StockView;
import jtw.stock.view.report.ReportDialog;

public abstract class StandardReportCalculationFinishedListener implements CalculationFinishedListener {
	
	private ReportDialog reportDialog;
	
	private final transient Log log = LogFactory.getLog(StandardReportCalculationFinishedListener.class);

	protected StandardReportCalculationFinishedListener() {
	}

	@Override
	public void calculationFinished(StockCalculator thisCalculator) {
		if (log.isDebugEnabled()) {
			log.debug("Calculator finished (display: " +  thisCalculator.getDisplayName() + "; name:    " +  thisCalculator.getName() + ")");
		}
		
		try {
			List<ReportResult> resultList = getResult(thisCalculator);
			reportDialog.setResult(resultList);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			JOptionPane.showMessageDialog(StockView.getInstance(), "Error computing report: \"" + e.getMessage() + "\"", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	/**
	 * Calculates and returns the results for the report. The suggestion should represent whether to buy, sell or hold the stock.
	 * 
	 * @param calculator The calculator for that the result shall be returned.
	 * 
	 * @return The result.
	 */
	protected abstract List<ReportResult> getResult(StockCalculator calculator);

	protected double getLastX(XYDataset ds) {
		return getLastX(ds, 0);
	}
	
	protected double getLastX(XYDataset ds, int series) {
		return ds.getXValue(series, ds.getItemCount(series) - 1);
	}
	
	protected double getLastY(XYDataset ds) {
		return getLastY(ds, 0);
	}

	protected double getLastY(XYDataset ds, int series) {
		return ds.getYValue(series, ds.getItemCount(series) - 1);
	}
	
	/**
	 * Returns the current {@link ReportDialog}.
	 * 
	 * @return The dialog.
	 */
	protected ReportDialog getReportDialog() {
		return reportDialog;
	}

	/**
	 * Sets the given {@link ReportDialog} as the current dialog. Must be set before any other method is called.
	 * 
	 * @param report The report dialog to use.
	 */
	public void setReportDialog(ReportDialog report) {
		this.reportDialog = report;
	}
	
}
