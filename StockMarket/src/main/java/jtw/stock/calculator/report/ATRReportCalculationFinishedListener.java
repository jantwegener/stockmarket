package jtw.stock.calculator.report;

import java.util.ArrayList;
import java.util.List;

import org.jfree.data.xy.XYDataset;

import jtw.stock.calculator.StockCalculator;
import jtw.stock.model.registry.StockDataRegistry;

public class ATRReportCalculationFinishedListener extends StandardReportCalculationFinishedListener {

	protected ATRReportCalculationFinishedListener() {
	}

	@Override
	protected List<ReportResult> getResult(StockCalculator calculator) {
		XYDataset ds = calculator.getDataset();

		XYDataset dsCurr = StockDataRegistry.getInstance().getReferencedDataset();

		List<ReportResult> resultList = new ArrayList<>();

		double stopLoss = getLastY(dsCurr) - getLastY(ds);
		double takeProfit = getLastY(dsCurr) + 2 * getLastY(ds);
		
		String textStopLoss = String.format("%.2f", stopLoss);
		String textTakeProfit = String.format("%.2f", takeProfit);

		ReportResult stopLossResult = new ReportResult(calculator.getDisplayName() + " (stop loss)", ReportResult.SELL, textStopLoss);
		ReportResult takeProfitResult = new ReportResult(calculator.getDisplayName() + " (take profit)", ReportResult.BUY, textTakeProfit);
		
		resultList.add(stopLossResult);
		resultList.add(takeProfitResult);
		
		return resultList;
	}

}
