package jtw.stock.calculator.report;

import java.util.ArrayList;
import java.util.List;

import org.jfree.data.xy.XYDataset;

import jtw.stock.calculator.StockCalculator;

public class RSIReportCalculationFinishedListener extends StandardReportCalculationFinishedListener {

	protected RSIReportCalculationFinishedListener() {
	}

	@Override
	protected List<ReportResult> getResult(StockCalculator calculator) {
		XYDataset ds = calculator.getDataset();

		List<ReportResult> result = new ArrayList<>();
		
		if (getLastY(ds) > 70) {
			ReportResult r = new ReportResult(calculator.getDisplayName() + " (over/underbought)", ReportResult.SELL, "-");
			result.add(r);
		} else if (getLastY(ds) < 30) {
			ReportResult r = new ReportResult(calculator.getDisplayName() + " (over/underbought)", ReportResult.BUY, "-");
			result.add(r);
		} else {
			ReportResult r = new ReportResult(calculator.getDisplayName() + " (over/underbought)", ReportResult.HOLD, "-");
			result.add(r);
		}
		return result;
	}

}
