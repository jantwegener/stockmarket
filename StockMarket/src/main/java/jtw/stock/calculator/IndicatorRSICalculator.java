package jtw.stock.calculator;

import static jtw.stock.main.StockApplicationSettings.TIME_FRAME_MAX;

import java.util.List;

import org.jfree.data.xy.DefaultXYDataset;
import org.jfree.data.xy.XYDataset;

import jtw.stock.main.StockApplicationSettings;
import jtw.stock.model.Stock;
import jtw.stock.model.registry.StockDataRegistry;
import jtw.util.IntegerUtil;

/**
 * Calculates the Relative Strength Index.
 * 
 * @author Jan-Thierry Wegener
 */
public class IndicatorRSICalculator extends StockCalculator {

	private int period;

	public IndicatorRSICalculator() {
	}

	public IndicatorRSICalculator(int period) {
		this.period = period;
	}
	
	@Override
	public String getDisplayName() {
		return "RSI (" + period + ")";
	}
	
	public String getName() {
		return super.getName() + ":" + period;
	}

	@Override
	public String[] getParameterNameArray() {
		return new String[] {"Period"};
    }

	@Override
	public String[] getParameterDefaultValueArray() {
		return new String[] {"14"};
    }

	@Override
	public void setParameter(String ... params) {
		period = IntegerUtil.toInt(params[0], 14);
    }

	@Override
	protected XYDataset doCalculation() {
		List<Stock> allStockList = StockDataRegistry.getInstance().getStockList(getStockName(), getTimeBase(), TIME_FRAME_MAX);
		List<Stock> currStockList = StockDataRegistry.getInstance().getStockList(getStockName(), getTimeBase());
		int size = allStockList.size();
		int currSize = Math.min(currStockList.size(), size - 1);
		double[][] data = new double[2][currSize];

		double upPrev = 0;
		double downPrev = 0;
		
		int j = 0;
		for (int i = 1; i < size; i++) {
			Stock s = allStockList.get(i);
			Stock sprev = allStockList.get(i - 1);

			double up = Math.max(0, s.getClosing() - sprev.getClosing());
			double down = Math.max(0, sprev.getClosing() - s.getClosing());

			double upN = ((period - 1) * upPrev + up) / period;
			double downN = ((period - 1) * downPrev + down) / period;
			double rs = upN / downN;
			double rsi = 100d - 100d / (1d + rs);
			
			if (StockApplicationSettings.isInTimeFrame(s)) {
				data[0][j] = s.getDate().getTimeInMillis();
				data[1][j] = rsi;
				j++;
			}
			
			upPrev = upN;
			downPrev = downN;
		}

		DefaultXYDataset dataset = new DefaultXYDataset();
		dataset.addSeries(getDisplayName(), data);
		
		return dataset;
	}
	
	public int getPeriod() {
		return period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}

}
