package jtw.stock.calculator;

import static jtw.stock.main.StockApplicationSettings.TIME_FRAME_MAX;

import java.util.ArrayList;
import java.util.List;

import org.jfree.data.xy.DefaultXYDataset;
import org.jfree.data.xy.XYDataset;

import jtw.stock.main.StockApplicationSettings;
import jtw.stock.model.Stock;
import jtw.stock.model.registry.StockDataRegistry;
import jtw.util.DoubleUtil;
import jtw.util.IntegerUtil;
import jtw.util.MathUtil;
import jtw.util.ObjectToDouble;

/**
 * The calculator for the Bollinger Bands (BB) indicator.
 * 
 * @author Jan-Thierry Wegener
 *
 * @see {@link https://www.tradingtechnologies.com/xtrader-help/x-study/technical-indicator-definitions/bollinger-band-bbands/}
 */
public class IndicatorBBCalculator extends StockCalculator {
	
	private int period;

	private double factor;

	private ObjectToDouble<Double> otd = new ObjectToDouble<Double>() {
		@Override
		public double objToDouble(Double o) {
			return o;
		}
	};
	
	public IndicatorBBCalculator() {
	}
	
	public IndicatorBBCalculator(int period, double factor) {
		this.period = period;
		this.factor = factor;
	}

	@Override
	public String getDisplayName() {
		return "BB";
	}
	
	public String getName() {
		return super.getName() + ":" + period + ":" + factor;
	}

	@Override
	public String[] getParameterNameArray() {
		return new String[] {"Period", "Factor"};
    }

	@Override
	public String[] getParameterDefaultValueArray() {
		return new String[] {"20", "2"};
    }

	@Override
	public void setParameter(String ... params) {
		period = IntegerUtil.toInt(params[0], 20);
		factor = DoubleUtil.toDouble(params[0], 2d);
    }

	@Override
	protected XYDataset doCalculation() {
		List<Stock> allStockList = StockDataRegistry.getInstance().getStockList(getStockName(), getTimeBase(), TIME_FRAME_MAX);
		List<Stock> currStockList = StockDataRegistry.getInstance().getStockList(getStockName(), getTimeBase());
		int currSize = currStockList.size();
		int size = allStockList.size();
		double[][] dataUp = new double[2][currSize];
		double[][] dataMid = new double[2][currSize];
		double[][] dataLow = new double[2][currSize];

		List<Double> valList = new ArrayList<>();
		
		int j = 0;
		for (int i = 0; i < size; i++) {
			Stock s = allStockList.get(i);
			
			double value = StockDataRegistry.getInstance().getReferencedStockValue(s);
			valList.add(value);

			double mid = MathUtil.average(valList, otd, i - period+1, i+1);
			double stdDev = MathUtil.standardDeviation(valList, otd, mid, i - period+1, i+1);

			double up = mid + factor * stdDev;
			double low = mid - factor * stdDev;
			
			if (StockApplicationSettings.isInTimeFrame(s)) {
				dataUp[0][j] = s.getDate().getTimeInMillis();
				dataUp[1][j] = up;

				dataMid[0][j] = s.getDate().getTimeInMillis();
				dataMid[1][j] = mid;

				dataLow[0][j] = s.getDate().getTimeInMillis();
				dataLow[1][j] = low;
				
				j++;
			}
		}

		DefaultXYDataset dataset = new DefaultXYDataset();
		dataset.addSeries(getDisplayName() + " (up)", dataUp);
		dataset.addSeries(getDisplayName() + " (mid)", dataMid);
		dataset.addSeries(getDisplayName() + " (low)", dataLow);
		
		return dataset;
	}

	public int getPeriod() {
		return period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}

	public double getFactor() {
		return factor;
	}

	public void setFactor(double factor) {
		this.factor = factor;
	}

}
