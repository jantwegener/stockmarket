package jtw.stock.calculator;

import java.util.List;

import org.jfree.data.xy.DefaultXYDataset;
import org.jfree.data.xy.XYBarDataset;
import org.jfree.data.xy.XYDataset;

import jtw.stock.model.Stock;
import jtw.stock.model.registry.StockDataRegistry;

public class StockVolumeCalculator extends StockCalculator {

	@Override
	public String getDisplayName() {
		return getStockName() + " (volume)";
	}

	@Override
	public String[] getParameterNameArray() {
		return new String[] {};
    }

	@Override
	public String[] getParameterDefaultValueArray() {
		return new String[] {};
    }

	@Override
	public void setParameter(String ... params) {
    }

	@Override
	protected XYDataset doCalculation() {
		List<Stock> stockList = StockDataRegistry.getInstance().getStockList(getStockName());
		int size = stockList.size();
		double[][] data = new double[2][size];

		for (int i = 0; i < size; i++) {
			Stock s = stockList.get(i);
			data[0][i] = s.getDate().getTimeInMillis();
			data[1][i] = s.getVolume();
		}

		DefaultXYDataset dataset = new DefaultXYDataset();
		dataset.addSeries(getDisplayName(), data);

		return new XYBarDataset(dataset, 2);
	}

}
