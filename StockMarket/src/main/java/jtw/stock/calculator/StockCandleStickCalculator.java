package jtw.stock.calculator;

import java.util.Date;
import java.util.List;

import org.jfree.data.xy.DefaultOHLCDataset;
import org.jfree.data.xy.OHLCDataItem;
import org.jfree.data.xy.XYDataset;

import jtw.stock.model.Stock;
import jtw.stock.model.registry.StockDataRegistry;

public class StockCandleStickCalculator extends StockCalculator {

	@Override
	public String getDisplayName() {
		return getStockName() + " (candle)";
	}

	@Override
	public String[] getParameterNameArray() {
		return new String[] {};
    }

	@Override
	public String[] getParameterDefaultValueArray() {
		return new String[] {};
    }

	@Override
	public void setParameter(String ... params) {
    }

	@Override
	protected XYDataset doCalculation() {
		List<Stock> stockList = StockDataRegistry.getInstance().getStockList(getStockName());
		int size = stockList.size();
		
		OHLCDataItem[] data = new OHLCDataItem[size];
		for (int i = 0; i < stockList.size(); i++) {
			Stock s = stockList.get(i);
			Date date = s.getDate().getTime();
			double open = s.getOpening();
			double high = s.getHigh();
			double low = s.getLow();
			double close = s.getClosing();
			double volume = s.getVolume();

			data[i] = new OHLCDataItem(date, open, high, low, close, volume);
		}
		
		DefaultOHLCDataset dataset = new DefaultOHLCDataset(getStockName(), data);

		return dataset;
	}

}
