package jtw.stock.calculator;

import java.util.List;

import org.jfree.data.xy.DefaultXYDataset;
import org.jfree.data.xy.XYDataset;

import jtw.stock.model.Stock;
import jtw.stock.model.registry.StockDataRegistry;

/**
 * Computes the rise and fall in percents relative to its maximum and minimum.
 * 
 * @author Jan-Thierry Wegener
 */
public class StockPercentageCalculator extends StockCalculator {
	
	@Override
	public String getDisplayName() {
		return getStockName() + " (percent)";
	}
	
	@Override
	protected XYDataset doCalculation() {
		List<Stock> stockList = StockDataRegistry.getInstance().getStockList(getStockName());
		int size = stockList.size();
		
		double min = Double.MAX_VALUE;
		double max = Double.MIN_VALUE;
		for (int i = 0; i < size; i++) {
			Stock s = stockList.get(i);
			double refStockValue = StockDataRegistry.getInstance().getReferencedStockValue(s);
			if (refStockValue < min) {
				min = refStockValue;
			}
			if (refStockValue > max) {
				max = refStockValue;
			}
		}
		// we will subtract the min from each value
		// so to ever reach 100% we have to subtract min here as well
		max -= min;
		
		double[][] data = new double[2][size];
		
		for (int i = 0; i < size; i++) {
			Stock s = stockList.get(i);
			double refStockValue = StockDataRegistry.getInstance().getReferencedStockValue(s);
			// normalize to 0 with the min
			refStockValue -= min;
			data[0][i] = s.getDate().getTimeInMillis();
			// percent of the maximum
			data[1][i] = 100d * refStockValue / max;
		}
		
		DefaultXYDataset dataset = new DefaultXYDataset();
		dataset.addSeries(getDisplayName(), data);
		
		return dataset;
	}
	
	@Override
	public String[] getParameterNameArray() {
		return new String[] {};
	}
	
	@Override
	public String[] getParameterDefaultValueArray() {
		return new String[] {};
	}
	
	@Override
	public void setParameter(String... params) {
	}
	
}
