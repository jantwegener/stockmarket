package jtw.stock.calculator;

import static jtw.stock.main.StockApplicationSettings.TIME_FRAME_MAX;

import java.util.List;

import org.jfree.data.xy.DefaultXYDataset;
import org.jfree.data.xy.XYDataset;

import jtw.stock.main.StockApplicationSettings;
import jtw.stock.model.Stock;
import jtw.stock.model.registry.StockDataRegistry;

public class IndicatorOBVCalculator extends StockCalculator {

	public IndicatorOBVCalculator() {
	}
	
	@Override
	public String getDisplayName() {
		return "OBV";
	}

	@Override
	public String[] getParameterNameArray() {
		return new String[] {};
    }

	@Override
	public String[] getParameterDefaultValueArray() {
		return new String[] {};
    }

	@Override
	public void setParameter(String ... params) {
    }

	@Override
	protected XYDataset doCalculation() {
		List<Stock> allStockList = StockDataRegistry.getInstance().getStockList(getStockName(), getTimeBase(), TIME_FRAME_MAX);
		List<Stock> currStockList = StockDataRegistry.getInstance().getStockList(getStockName(), getTimeBase());
		int size = allStockList.size();
		int currSize = Math.min(currStockList.size(), size - 1);
		double[][] data = new double[2][currSize];

		double obvPrev = 0;
		
		int j = 0;
		for (int i = 1; i < size; i++) {
			Stock s = allStockList.get(i);
			Stock sprev = allStockList.get(i - 1);

			double closeToday = s.getClosing();
			double closePrev = sprev.getClosing();
			double volume = 0;
			if (closeToday > closePrev) {
				volume = s.getVolume();
			} else if (closeToday < closePrev) {
				volume = -s.getVolume();
			}
			
			double obv = obvPrev + volume;
			
			if (StockApplicationSettings.isInTimeFrame(s)) {
				data[0][j] = s.getDate().getTimeInMillis();
				data[1][j] = obv;
				j++;
			}
			
			obvPrev = obv;
		}

		DefaultXYDataset dataset = new DefaultXYDataset();
		dataset.addSeries(getDisplayName(), data);
		
		return dataset;
	}

}
