package jtw.stock.calculator;

import static jtw.stock.main.StockApplicationSettings.TIME_FRAME_MAX;

import java.util.List;

import org.jfree.data.xy.DefaultXYDataset;
import org.jfree.data.xy.XYDataset;

import jtw.stock.main.StockApplicationSettings;
import jtw.stock.model.Stock;
import jtw.stock.model.registry.StockDataRegistry;
import jtw.util.IntegerUtil;
import jtw.util.MathUtil;

/**
 * Calculates the Average True Range.
 * 
 * @author Jan-Thierry Wegener
 */
public class IndicatorATRCalculator extends StockCalculator {
	
	private int period = 0;
	
	public IndicatorATRCalculator() {
	}

	public IndicatorATRCalculator(int period) {
		this.period = period;
	}
	
	@Override
	public String getDisplayName() {
		return "ATR (" + period + ")";
	}
	
	public void setPeriod(int period) {
		this.period = period;
	}
	
	public int getPeriod() {
		return period;
	}

	@Override
	public String[] getParameterNameArray() {
		return new String[] {"Period"};
    }

	@Override
	public String[] getParameterDefaultValueArray() {
		return new String[] {"14"};
    }

	@Override
	public void setParameter(String ... params) {
		period = IntegerUtil.toInt(params[0], 14);
    }

	@Override
	protected XYDataset doCalculation() {
		List<Stock> allStockList = StockDataRegistry.getInstance().getStockList(getStockName(), getTimeBase(), TIME_FRAME_MAX);
		List<Stock> currStockList = StockDataRegistry.getInstance().getStockList(getStockName(), getTimeBase());
		int size = allStockList.size();
		int currSize = Math.min(currStockList.size(), size - 1);
		double[][] data = new double[2][currSize];

		double atrPrev = 0;
		
		int j = 0;
		for (int i = 1; i < size; i++) {
			Stock s = allStockList.get(i);
			Stock sPrev = allStockList.get(i - 1);
			
			double tr = calculateTrueRange(s, sPrev);
			double atr = (atrPrev * (period - 1d) + tr) / period;
			
			if (StockApplicationSettings.isInTimeFrame(s)) {
				data[0][j] = s.getDate().getTimeInMillis();
				data[1][j] = atr;
				j++;
			}
			
			atrPrev = atr;
		}

		DefaultXYDataset dataset = new DefaultXYDataset();
		dataset.addSeries(getDisplayName(), data);
		
		return dataset;
	}
	
	private double calculateTrueRange(Stock s, Stock sPrev) {
		double high = s.getHigh();
		double low = s.getLow();
		double closePrev = sPrev.getClosing();
		return MathUtil.max((high - low), Math.abs(high - closePrev), Math.abs(low - closePrev));
	}

}

