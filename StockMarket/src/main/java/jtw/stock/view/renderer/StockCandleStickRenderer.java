package jtw.stock.view.renderer;

import static jtw.stock.main.StockApplicationSettings.TIME_BASE_DAILY;
import static jtw.stock.main.StockApplicationSettings.TIME_BASE_WEEKLY;
import static jtw.stock.main.StockApplicationSettings.TIME_BASE_MONTHLY;

import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CrosshairState;
import org.jfree.chart.plot.PlotRenderingInfo;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.CandlestickRenderer;
import org.jfree.chart.renderer.xy.XYItemRendererState;
import org.jfree.data.xy.XYDataset;

import jtw.stock.main.StockApplicationSettings;

public class StockCandleStickRenderer extends CandlestickRenderer {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private boolean dynamicWidth = true;
	
	/**
	 * The base width.
	 */
	private double baseWidth = 3.0d;
	
	public StockCandleStickRenderer() {
	}

	/**
	 * Returns the base width. The base width is used to compute the width of the candles depended on the used time base.
	 * 
	 * @return The base width.
	 */
	public double getBaseCandleWidth() {
		return baseWidth;
	}

	/**
	 * Sets a new base width for the computation of the candle widths.
	 * 
	 * @param baseWidth The new base width.
	 */
	public void setBaseCandleWidth(double baseWidth) {
		this.baseWidth = baseWidth;
	}

	public boolean isDynamicWidth() {
		return dynamicWidth;
	}
	
	public void setDynamicWidth(boolean dynamicWidth) {
		this.dynamicWidth = dynamicWidth;
	}
	
    @Override
	public void drawItem(Graphics2D g2, XYItemRendererState state, Rectangle2D dataArea, PlotRenderingInfo info,
			XYPlot plot, ValueAxis domainAxis, ValueAxis rangeAxis, XYDataset dataset, int series, int item, 
			CrosshairState crosshairState, int pass) {
    	if (isDynamicWidth()) {
    		double cw;
    		switch (StockApplicationSettings.getTimeBase()) {
    			case TIME_BASE_DAILY:
    				cw = baseWidth;
    				break;
    			case TIME_BASE_WEEKLY:
    				cw = 2 * baseWidth;
    				break;
    			case TIME_BASE_MONTHLY:
    				cw = 3 * baseWidth;
    				break;
    			default:
    				cw = baseWidth;
    		}
    		setCandleWidth(cw);
    	}
    	super.drawItem(g2, state, dataArea, info, plot, domainAxis, rangeAxis, dataset, series, item, crosshairState, pass);
    }
	
}
