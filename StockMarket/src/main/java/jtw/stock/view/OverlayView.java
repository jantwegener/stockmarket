package jtw.stock.view;

import java.awt.Graphics2D;
import java.util.HashMap;
import java.util.Map;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.panel.AbstractOverlay;
import org.jfree.chart.panel.Overlay;

import jtw.stock.view.overlay.OverlayObject;

public class OverlayView {
	
	private static OverlayView INSTANCE = null;
	
	/**
	 * A map between the id and the {@link OverlayObject}.
	 */
	private final Map<String, OverlayObject> overlayObjectMap = new HashMap<>();

	public synchronized static OverlayView getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new OverlayView();
			INSTANCE.init();
		}
		return INSTANCE;
	}
	
	/**
	 * Initializes the overlay.
	 */
	public void init() {
		Overlay overlay = new StockOverlay();
		StockView.getInstance().addOverlay(overlay);
	}
	
	/**
	 * Adds a new object to the overlay.
	 * 
	 * @param oo The object to add.
	 */
	public void add(OverlayObject oo) {
		overlayObjectMap.put(oo.getId(), oo);
		repaint();
	}
	
	/**
	 * Removes the object corresponding to the id.
	 * 
	 * @param id The id of the object to remove.
	 * 
	 * @return The removed object, or null if none has been removed.
	 */
	public OverlayObject remove(String id) {
		OverlayObject oo = overlayObjectMap.remove(id);
		repaint();
		return oo;
	}
	
	public void repaint() {
		StockView.getInstance().repaint();
	}
	
	private class StockOverlay extends AbstractOverlay implements Overlay {

		@Override
		public void paintOverlay(Graphics2D g2, ChartPanel chartPanel) {
			for (OverlayObject oo : overlayObjectMap.values()) {
				oo.paint(g2, chartPanel);
			}
		}
		
	}

}
