package jtw.stock.view.overlay;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.text.SimpleDateFormat;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.plot.XYPlot;

import jtw.util.PaintUtil;

public class CrosshairOverlayObject extends OverlayObject {

	private Point2D middlePoint = null;
	private XYPlot plot = null;
	private Rectangle2D screenDataArea = null;
	
	public CrosshairOverlayObject(String name) {
		super(name);
		
		// set default main color to gray
		setMainColor(Color.LIGHT_GRAY);
	}

	public void updatePoint(Point2D middlePoint) {
		this.middlePoint = middlePoint;
	}
	
	public void updatePlot(XYPlot plot) {
		this.plot = plot;
	}
	
	public void updateScreenDataArea(Rectangle2D screenDataArea) {
		this.screenDataArea = screenDataArea;
	}
	
	@Override
	protected Rectangle2D doPaint(Graphics2D g2, ChartPanel chartPanel) {
		Rectangle2D boundingBox = null;
		if (middlePoint != null && plot != null && screenDataArea != null) {
			Point2D middleScreenPoint = getScreenPoint(middlePoint, plot, screenDataArea);

			Rectangle2D vis = chartPanel.getScreenDataArea();
			
			String price = String.format("%.2f", middlePoint.getY());
			String date = SimpleDateFormat.getDateInstance(SimpleDateFormat.MEDIUM).format(Double.valueOf(middlePoint.getX()));
			PaintUtil.drawString(g2, price, vis.getX() + vis.getWidth(), middleScreenPoint.getY(), true, false);
			PaintUtil.drawString(g2, date, middleScreenPoint.getX(), vis.getY(), false, true);
			
			Stroke dashedStroke = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 1f, new float[]{3, 6}, 0);
			g2.setStroke(dashedStroke);
			PaintUtil.drawLine(g2, vis.getX(), middleScreenPoint.getY(), vis.getX() + vis.getWidth(), middleScreenPoint.getY());
			PaintUtil.drawLine(g2, middleScreenPoint.getX(), vis.getY(), middleScreenPoint.getX(), vis.getY() + vis.getHeight());
			
			boundingBox = new Rectangle2D.Double(middleScreenPoint.getX() - 10, middleScreenPoint.getY() - 10, 20, 20);
		}
		return boundingBox;
	}

}
