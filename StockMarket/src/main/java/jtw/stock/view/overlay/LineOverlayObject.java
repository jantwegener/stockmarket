package jtw.stock.view.overlay;

import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.plot.XYPlot;

import jtw.util.NullUtil;
import jtw.util.PaintUtil;

public class LineOverlayObject extends OverlayObject {

	private Point2D startPoint = null;
	private Point2D endPoint = null;
	
	private XYPlot plot = null;
	private Rectangle2D screenDataArea = null;

	private boolean paintPercent = true;
	private boolean paintSlope = true;
	private boolean paintTime = true;

	public LineOverlayObject(String name) {
		super(name);
	}

	public LineOverlayObject(String name, boolean paintPercent, boolean paintSlope) {
		super(name);
		this.paintPercent = paintPercent;
		this.paintSlope = paintSlope;
	}
	
	public void updatePoints(Point2D startPoint, Point2D endPoint) {
		this.startPoint = startPoint;
		this.endPoint = endPoint;
	}
	
	public void updatePlot(XYPlot plot) {
		this.plot = plot;
	}
	
	public void updateScreenDataArea(Rectangle2D screenDataArea) {
		this.screenDataArea = screenDataArea;
	}

	@Override
	protected Rectangle2D doPaint(Graphics2D g2, ChartPanel chartPanel) {
		Rectangle2D boundingBox = null;
		
		if (startPoint != null && endPoint != null) {
			Point2D endScreenPoint = getScreenPoint(endPoint, plot, screenDataArea);
			Point2D startScreenPoint = getScreenPoint(startPoint, plot, screenDataArea);
			
			if (NullUtil.noneNull(startScreenPoint, endScreenPoint)) {
				PaintUtil.drawLine(g2, startScreenPoint, endScreenPoint);
				
				boundingBox = new Rectangle2D.Double(startScreenPoint.getX(), startScreenPoint.getY(), 0, 0);
				boundingBox.add(endScreenPoint);
				
				if (paintPercent) {
					String percent = String.format("%.2f %%", computePercent());
					int percentWidth = g2.getFontMetrics().stringWidth(percent);
					PaintUtil.drawString(g2, percent, endScreenPoint.getX() - percentWidth, endScreenPoint.getY());
				}
				
				if (paintSlope) {
					String slope = String.format("%.2f", computeSlope(startPoint, endPoint));
					int percentWidth = g2.getFontMetrics().stringWidth(slope);
					PaintUtil.drawString(g2, slope, endScreenPoint.getX() - percentWidth, endScreenPoint.getY());
				}
				
				if (paintTime) {
					String days = String.format("%.2f days", computeTime());
					int daysWidth = g2.getFontMetrics().stringWidth(days);
					PaintUtil.drawString(g2, days, endScreenPoint.getX() - daysWidth, endScreenPoint.getY() + g2.getFontMetrics().getHeight());
				}
			}
		}
		
		return boundingBox;
	}
	
	private double computePercent() {
		double ys = startPoint.getY();
		double ye = endPoint.getY();
		
		double diff = ye - ys;
		
		return diff / ys * 100d;
	}
	
	private double computeSlope(Point2D startPoint, Point2D endPoint) {
		double ydiff = -(endPoint.getY() - startPoint.getY());
		double xdiff = (endPoint.getX() - startPoint.getX()) / 1000 / 60 / 60 / 24;
		
		return ydiff / xdiff;
	}
	
	private double computeTime() {
		double start = Math.min(startPoint.getX(), endPoint.getX());
		double end = Math.max(startPoint.getX(), endPoint.getX());
		
		double daysBetween = ChronoUnit.DAYS.between(Instant.ofEpochMilli((long) start), Instant.ofEpochMilli((long) end));
		
		return daysBetween;
	}

	public boolean isPaintPercent() {
		return paintPercent;
	}

	public void setPaintPercent(boolean paintPercent) {
		this.paintPercent = paintPercent;
	}

	public boolean isPaintSlope() {
		return paintSlope;
	}

	public void setPaintSlope(boolean paintSlope) {
		this.paintSlope = paintSlope;
	}

}
