package jtw.stock.view.overlay;

import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import org.jfree.chart.ChartPanel;

public class TestOverlayObject extends OverlayObject {

	public TestOverlayObject(String n) {
		super(n);
	}

	@Override
	protected Rectangle2D doPaint(Graphics2D g2, ChartPanel chartPanel) {
		g2.drawString(getId(), 50, 50);
		return null;
	}

}
