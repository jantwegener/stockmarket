package jtw.stock.view.overlay;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.ui.RectangleEdge;

import jtw.stock.view.action.overlay.AbstractOverlayAction;
import jtw.util.NullUtil;
import jtw.util.PaintUtil;

public abstract class OverlayObject {
	
	private String name;
	
	private static int CURR_ID = 0;
	
	private final int id = (CURR_ID++);
	
	private boolean selected = true;
	
	private boolean visible = true;
	
	private Color mainColor = Color.BLACK;
	
	/**
	 * The action class responsible for creating the object.
	 */
	@SuppressWarnings("rawtypes")
	private Class<? extends AbstractOverlayAction> actionClazz;
	
	public OverlayObject() {
		this("");
	}

	public OverlayObject(String n) {
		this.name = n;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void paint(Graphics2D g, ChartPanel chartPanel) {
		if  (visible) {
			Graphics2D g2 = (Graphics2D) g.create();
			
			g2.setColor(mainColor);
			Rectangle2D r = doPaint(g2, chartPanel);
			
			if (selected) {
				Stroke dashedStroke = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 1f, new float[]{9}, 0);
				g2.setStroke(dashedStroke);
				g2.setColor(Color.GRAY);
				PaintUtil.drawRect(g2, r);
			}
			
			g2.dispose();
		}
	}
	
	/**
	 * Paints the object on the overlay.
	 * 
	 * @param g2 The graphics object to paint on.
	 * @param chartPanel The chart panel.
	 * 
	 * @return The bounding box used for painting the object inside. If the object is not painted, <code>null</code> is returned.
	 */
	protected abstract Rectangle2D doPaint(Graphics2D g2, ChartPanel chartPanel);

	/**
	 * Returns the point on the screen.
	 * 
	 * @param dataPoint The value point from the data set.
	 * 
	 * @return The corresponding point on the screen.
	 */
	protected Point2D getScreenPoint(Point2D dataPoint, XYPlot plot, Rectangle2D screenDataArea) {
		Point2D ret = null;
		if (NullUtil.noneNull(dataPoint, plot, screenDataArea)) {
			ValueAxis xAxis = plot.getDomainAxis();
			RectangleEdge xAxisEdge = plot.getDomainAxisEdge();
	
			ValueAxis yAxis = plot.getRangeAxis();
			RectangleEdge yAxisEdge = plot.getRangeAxisEdge();

			if (NullUtil.noneNull(xAxis, yAxis)) {
				double x = xAxis.valueToJava2D(dataPoint.getX(), screenDataArea, xAxisEdge);
				double y = yAxis.valueToJava2D(dataPoint.getY(), screenDataArea, yAxisEdge);
				
				ret = new Point2D.Double(x, y);
			}
		}
		return ret;
	}
	
	/**
	 * Resets all visible graphics the object might have drawn.
	 */
	public void reset() {
	}

	public String getId() {
		return getClass().getName() + "." + id;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public Color getMainColor() {
		return mainColor;
	}

	public void setMainColor(Color mainColor) {
		this.mainColor = mainColor;
	}

	@SuppressWarnings("rawtypes")
	public Class<? extends AbstractOverlayAction> getActionClass() {
		return actionClazz;
	}

	@SuppressWarnings("rawtypes")
	public void setActionClass(Class<? extends AbstractOverlayAction> actionClazz) {
		this.actionClazz = actionClazz;
	}

}
