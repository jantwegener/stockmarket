package jtw.stock.view;

import java.awt.FlowLayout;

import javax.swing.JPanel;
import javax.swing.JProgressBar;

/**
 * A simple view for a progress bar. The progress view must only be handled by
 * actions. An action manipulating the progress view must mark that in the
 * comment of the action class.
 * 
 * @author Jan-Thierry Wegener
 */
public class ProgressView extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static ProgressView INSTANCE = null;

	private JProgressBar mainProgressBar = new JProgressBar();

	private ProgressView() {
		super(new FlowLayout(FlowLayout.RIGHT));

		mainProgressBar.setStringPainted(true);
		mainProgressBar.setValue(100);

		add(mainProgressBar);
	}

	public synchronized static ProgressView getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new ProgressView();
		}
		return INSTANCE;
	}
	
	/**
	 * Resets the value of the progress bar to 0 and initiates a new maximal value.
	 * 
	 * @param max The new maximal value.
	 */
	public void reset(int max) {
		synchronized (mainProgressBar) {
			mainProgressBar.setValue(0);
			mainProgressBar.setMaximum(max);
		}
	}
	
	/**
	 * Increments the value of the progress bar to its maximal value.
	 */
	public void incrementToMax() {
		synchronized (mainProgressBar) {
			int max = mainProgressBar.getMaximum();
			mainProgressBar.setValue(max);
		}
	}
	
	/**
	 * Increments the value of the main progress bar by 1.
	 */
	public void increment() {
		synchronized (mainProgressBar) {
			int val = mainProgressBar.getValue();
			mainProgressBar.setValue(++val);
		}
	}
	
	public void startIndeterminedMode() {
		synchronized (mainProgressBar) {
			mainProgressBar.setStringPainted(false);
			mainProgressBar.setIndeterminate(true);
		}
	}
	
	public void stopIndeterminedMode() {
		synchronized (mainProgressBar) {
			mainProgressBar.setIndeterminate(false);
			mainProgressBar.setStringPainted(true);
		}
	}

}
