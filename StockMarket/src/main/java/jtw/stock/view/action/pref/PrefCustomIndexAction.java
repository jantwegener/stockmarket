package jtw.stock.view.action.pref;

import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.antlr.v4.runtime.misc.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jtw.stock.io.CustomIndexReader;
import jtw.stock.io.CustomIndexWriter;
import jtw.stock.main.StockApplicationSettings;
import jtw.stock.view.StockView;
import jtw.stock.view.action.AbstractStockAction;
import jtw.stock.view.component.ListInputDialog;
import jtw.stock.view.component.PrefCustomIndexDialog;

public class PrefCustomIndexAction extends AbstractStockAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Log log = LogFactory.getLog(PrefCustomIndexAction.class);

	@Override
	public void actionPerformed(ActionEvent e) {
		final String ending = ".index";
		String[] alreadyLoadedArray = StockApplicationSettings.loadAlreadyAvailableArray(ending);

		ListInputDialog dialog = new ListInputDialog("Customize Index...", true, "", "Selectable Custom Index", "Custom Index Name", alreadyLoadedArray);
		dialog.showDialog();
		if (dialog.getButtonPressed() == ListInputDialog.YES_OPTION) {
			try {
				String indexName = dialog.getInput();
				List<Pair<String, Integer>> indexContentList;
				if (StockApplicationSettings.isFileExists(indexName, ending)) {
					// load the file
					CustomIndexReader reader = new CustomIndexReader();
					reader.read(indexName);
					indexContentList = reader.getIndexContentList();
				} else {
					indexContentList = new ArrayList<>();
				}
				
				PrefCustomIndexDialog prefDialog = new PrefCustomIndexDialog(indexName, true);
				prefDialog.setIndexContentList(indexContentList);
				prefDialog.showDialog();
				if (prefDialog.getButtonPressed() == ListInputDialog.YES_OPTION) {
					// save the new dialog
					List<Pair<String, Integer>> stockQuantityList = prefDialog.getInput();
					CustomIndexWriter indexWriter = new CustomIndexWriter();
					indexWriter.setStockQuantityList(stockQuantityList);
					indexWriter.write(indexName);
				}
			} catch (IOException ioe) {
				log.fatal(ioe.getMessage(), ioe);
				JOptionPane.showMessageDialog(StockView.getInstance(), "Error loading custom index: \"" + ioe.getMessage() + "\"", "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

}
