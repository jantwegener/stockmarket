package jtw.stock.view.action.indicator.trend;

import jtw.stock.calculator.StockCalculator;
import jtw.stock.calculator.StockExponentialMovingAverageCalculator;
import jtw.stock.view.action.chart.AbstractChartAction;

public class ChartExponentialMovingAverageAction extends AbstractChartAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private double alpha = -1;

	public ChartExponentialMovingAverageAction() {
	}
	
	/**
	 * Sets the alpha by the number of days to consider. Hereby, the formula alpha = 1 / d is used.
	 * 
	 * @param d The number of days.
	 */
	public void setDays(int d) {
		alpha = 1d / d;
	}
	
	@Override
	protected StockCalculator getCalculator() {
		StockExponentialMovingAverageCalculator calc = new StockExponentialMovingAverageCalculator();
		calc.setAlpha(alpha);
		return calc;
	}

	public double getAlpha() {
		return alpha;
	}

	public void setAlpha(double alpha) {
		this.alpha = alpha;
	}

}
