package jtw.stock.view.action.overlay;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.general.DatasetUtilities;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.RectangleEdge;

import jtw.stock.model.registry.StockDataRegistry;
import jtw.stock.view.OverlaySummaryView;
import jtw.stock.view.OverlayView;
import jtw.stock.view.StockView;
import jtw.stock.view.action.AbstractStockAction;
import jtw.stock.view.overlay.OverlayObject;

public abstract class AbstractOverlayAction<E extends OverlayObject> extends AbstractStockAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * The mouse listener added to the overlay.
	 */
	protected MouseCountListener listener = null;
	
	private boolean datasetDependent = false;
	
	/**
	 * The overlay object that is used in this action.
	 */
	protected E overlayObject;

	public AbstractOverlayAction() {
	}
	
	@Override
	public final void actionPerformed(ActionEvent e) {
		overlayObject = getOverlayObject();
		// install the object in the summary view
		OverlaySummaryView.getInstance().add(overlayObject);
		
		initMouseListener();
		
		// call specific actions to perform
		doActionPerformed(e);
	}
	
	private void initMouseListener() {
		int mouseClickNeeded = getMouseClickNeeded();
		if (mouseClickNeeded > 0) {
			// add the mouse listener
			listener = new MouseCountListener(mouseClickNeeded);
			StockView.getInstance().addChartMouseListener(listener);
		}
	}
	
	/**
	 * Performs specific actions after the overlay is added to the summary.
	 * 
	 * @param e The original {@link ActionEvent}.
	 */
	protected void doActionPerformed(ActionEvent e) {
	}
	
	protected final void updatePoints(Point2D[] pointArray, XYPlot[] plotArray, Rectangle2D[] screenDataAreaArray, int numValidPoints) {
		doUpdatePoints(pointArray, plotArray, screenDataAreaArray, numValidPoints);
		OverlayView.getInstance().repaint();
	}
	
	/**
	 * Does the real update when new points arrive.
	 * 
	 * @param pointArray The array of points.
	 * @param plotArray The array of {@link XYPlot}s where the mouse event originally occurred.
	 * @param numValidPoints The number of points that are valid and filled in the array. 
	 */
	protected abstract void doUpdatePoints(Point2D[] pointArray, XYPlot[] plotArray, Rectangle2D[] screenDataAreaArray, int numValidPoints);
	
	/**
	 * Returns the number of mouse clicks needed for the action. After the maximal number is reached, the {@link MouseListener}
	 * is removed from the overlay view.
	 * 
	 * @return The number of mouse clicks for the action.
	 */
	protected int getMouseClickNeeded() {
		return 0;
	}
	
	/**
	 * Returns the object to add to the overlay. Each time this method is called, a new object is created.
	 * In order to receive the already created object, use {@link #getCurrentOverlayObject()} instead.
	 * 
	 * @return The object to add to the overlay.
	 */
	protected final E getOverlayObject() {
		E obj = doGetOverlayObject();
		obj.setActionClass(this.getClass());
		return obj;
	}

	/**
	 * Returns the object to add to the overlay. Each time this method is called, a new object is created.
	 * In order to receive the already created object, use {@link #getCurrentOverlayObject()} instead.
	 * 
	 * @return The object to add to the overlay.
	 */
	protected abstract E doGetOverlayObject();
	
	/**
	 * Returns the current {@link OverlayObject} which was returned by {@link #getOverlayObject()}.
	 * 
	 * @return The current object.
	 */
	protected E getCurrentOverlayObject() {
		return overlayObject;
	}
	
	/**
	 * Sets the current overlay object.
	 * 
	 * @param obj The overlay object to set.
	 */
	public void setCurrentOverlayObject(E obj) {
		overlayObject = obj;
	}

	/**
	 * Returns the point within the graph. This means the x and y values corresponding to the data set instead of the screen.
	 * 
	 * @param p
	 * 
	 * @return
	 */
	protected Point2D getValuePoint(Point2D p) {
		Point2D retPoint = null;
		if (p != null) {
			retPoint = getValuePoint(p.getX(), p.getY());
		}
		return retPoint;
	}
	
	protected final Point2D getValuePoint(double mx, double my) {
        Rectangle2D dataArea = StockView.getInstance().getScreenDataArea((int)mx, (int)my);
        if (dataArea == null) {
        	dataArea = StockView.getInstance().getScreenDataArea();
        }
        
        XYPlot plot = StockView.getInstance().getCombinedPlot().findSubplot(StockView.getInstance().getChartRenderingInfo().getPlotInfo(), new Point2D.Double(mx, my));
        if (plot == null) {
        	plot = StockView.getInstance().getCombinedPlot();
        }
        
        ValueAxis xAxis = plot.getDomainAxis();
        double x = xAxis.java2DToValue(mx, dataArea, RectangleEdge.BOTTOM);
        // make the crosshairs disappear if the mouse is out of range
        if (!xAxis.getRange().contains(x)) { 
            x = Double.NaN;
        }
        double y = 0d;
        if (isDatasetDependent()) {
        	XYDataset ds = StockDataRegistry.getInstance().getReferencedDataset();
        	
        	y = DatasetUtilities.findYValue(ds, 0, x);
        } else {
        	ValueAxis yAxis = plot.getRangeAxis();
        	y = yAxis.java2DToValue(my, dataArea, RectangleEdge.LEFT);
        }
        Point2D p = new Point2D.Double(x, y);
        return p;
	}

	public void setDatasetDependent(boolean datasetDependent) {
		this.datasetDependent = datasetDependent;
	}
	
	protected boolean isDatasetDependent() {
		return datasetDependent;
	}

	protected class MouseCountListener implements ChartMouseListener {
		
		private final Point2D[] pointDataArray;
		private final XYPlot[] plotArray;
		private final Rectangle2D[] screenDataAreaArray;
		
		private int currPoint = 0;
		
		public MouseCountListener(int mouseClickCount) {
			pointDataArray = new Point2D[mouseClickCount];
			plotArray = new XYPlot[mouseClickCount];
			screenDataAreaArray = new Rectangle2D[mouseClickCount];
		}
		
		@Override
		public void chartMouseClicked(ChartMouseEvent event) {
			synchronized (pointDataArray) {
				XYPlot plot = StockView.getInstance().getCombinedPlot().findSubplot(StockView.getInstance().getChartRenderingInfo().getPlotInfo(), event.getTrigger().getPoint());
				plotArray[currPoint] = plot;
				pointDataArray[currPoint] = getValuePoint(event.getTrigger().getX(), event.getTrigger().getY());
				screenDataAreaArray[currPoint] = getScreenDataArea(event.getTrigger().getPoint());
				currPoint++;

				if (currPoint >= pointDataArray.length) {
					StockView.getInstance().removeChartMouseListener(this);
				}
				updatePoints(pointDataArray, plotArray, screenDataAreaArray, currPoint);
			}
		}
		
		@Override
		public void chartMouseMoved(ChartMouseEvent event) {
			synchronized (pointDataArray) {
				if (currPoint < pointDataArray.length) {
					XYPlot plot = StockView.getInstance().getCombinedPlot().findSubplot(StockView.getInstance().getChartRenderingInfo().getPlotInfo(), event.getTrigger().getPoint());

					if (plot != null) {
						plotArray[currPoint] = plot;
						pointDataArray[currPoint] = getValuePoint(event.getTrigger().getX(), event.getTrigger().getY());
						screenDataAreaArray[currPoint] = getScreenDataArea(event.getTrigger().getPoint());
						updatePoints(pointDataArray, plotArray, screenDataAreaArray, currPoint + 1);
					}
				}
			}
		}
		
		private Rectangle2D getScreenDataArea(Point p) {
			return StockView.getInstance().getScreenDataArea(p.x, p.y);
		}
		
		public int getCurrNumPoint() {
			return currPoint;
		}
		
		public Point2D[] getPoints() {
			return pointDataArray;
		}
	}
	
	public void reset(ActionEvent e) {
		getCurrentOverlayObject().reset();

		initMouseListener();
		
		// call specific actions to perform
		doActionPerformed(e);
	}

}
