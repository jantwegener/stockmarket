package jtw.stock.view.action.overlay;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import org.jfree.chart.plot.XYPlot;
import org.jfree.data.general.DatasetUtilities;
import org.jfree.data.xy.XYDataset;

import jtw.stock.model.registry.StockDataRegistry;
import jtw.stock.util.ValueFinder;
import jtw.stock.view.overlay.LineOverlayObject;

public class SupportLineOverlayAction extends AbstractOverlayAction<LineOverlayObject> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private boolean searchMax;
	
	public SupportLineOverlayAction() {
	}

	@Override
	protected LineOverlayObject doGetOverlayObject() {
		String name = "Support Line " + (isSearchMax() ? " (max)" : "(min)");
		return new LineOverlayObject(name, false, false);
	}

	@Override
	protected void doUpdatePoints(Point2D[] pointArray, XYPlot[] plotArray, Rectangle2D[] screenDataAreaArray, int numValidPoints) {
		if (numValidPoints >= 2) {
			double startX = pointArray[0].getX();
			double endX = pointArray[1].getX();
			
			XYDataset ds = StockDataRegistry.getInstance().getReferencedDataset();
			
			int[] indicesStart = DatasetUtilities.findItemIndicesForX(ds, 0, startX);
			int[] indicesEnd = DatasetUtilities.findItemIndicesForX(ds, 0, endX);
			if (indicesStart.length > 0 && indicesEnd.length > 0) {
				int startIndex = indicesStart[0] > 0 ? indicesStart[0] : 0;
				int stopIndex = indicesEnd[0] > 0 ? indicesEnd[0] : ds.getItemCount(0) - 1;

				Point2D startPoint = findValue(ds, startIndex, 25);
				Point2D endPoint = findValue(ds, stopIndex, 25);
				
				// shift the end of the line two years into the future
				// the computed value must not be an int, otherwise we have an overflow
				double twoYear = 1d * 365 * 24 * 60 * 60 * 1000;
				double x = ds.getXValue(0, ds.getItemCount(0) - 1) + twoYear;
				double m = (endPoint.getY() - startPoint.getY()) / (endPoint.getX() - startPoint.getX());
				double b = startPoint.getY() - m * startPoint.getX();
				double y = m * x + b;
				
				Point2D finalPoint = new Point2D.Double(x, y);

				getCurrentOverlayObject().updatePoints(startPoint, finalPoint);
				getCurrentOverlayObject().updatePlot(plotArray[0]);
				getCurrentOverlayObject().updateScreenDataArea(screenDataAreaArray[0]);
			}
		}
	}
	
	private Point2D findValue(XYDataset ds, int index, int radius) {
		Point2D p;
		if (isSearchMax()) {
			p = ValueFinder.findPointMaxInRadius(ds, index, radius);
		} else {
			p = ValueFinder.findPointMinInRadius(ds, index, radius);
		}
		return p;
	}

	protected int getMouseClickNeeded() {
		return 2;
	}

	public boolean isSearchMax() {
		return searchMax;
	}

	public void setSearchMax(boolean searchMax) {
		this.searchMax = searchMax;
	}
	
}
