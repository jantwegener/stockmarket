package jtw.stock.view.action.file;

/**
 * An interface for an action that shall be performed after the file has been opened.
 * 
 * @author Jan-Thierry Wegener
 */
public interface IPostOpenAction {
	
	public void performPostAction(String stockName);
	
}
