package jtw.stock.view.action.chart;

import jtw.stock.calculator.StockCalculator;
import jtw.stock.calculator.StockCandleStickCalculator;

public class ChartCandleStickAction extends AbstractChartAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ChartCandleStickAction() {
	}

	@Override
	protected StockCalculator getCalculator() {
		return new StockCandleStickCalculator();
	}

}
