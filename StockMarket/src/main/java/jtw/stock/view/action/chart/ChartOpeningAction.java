package jtw.stock.view.action.chart;

import jtw.stock.calculator.StockCalculator;
import jtw.stock.calculator.StockOpeningCalculator;

public class ChartOpeningAction extends AbstractChartAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ChartOpeningAction() {
	}

	@Override
	protected StockCalculator getCalculator() {
		return new StockOpeningCalculator();
	}

}
