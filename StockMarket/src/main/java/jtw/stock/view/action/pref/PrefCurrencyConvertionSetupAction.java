package jtw.stock.view.action.pref;

import java.awt.event.ActionEvent;
import java.io.IOException;

import javax.swing.JOptionPane;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jtw.stock.model.registry.CurrencyConversionRegistry;
import jtw.stock.view.StockView;
import jtw.stock.view.action.AbstractStockAction;
import jtw.stock.view.component.ListInputDialog;
import jtw.stock.view.component.PrefCurrencyConversionDialog;

public class PrefCurrencyConvertionSetupAction extends AbstractStockAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Log LOGGER = LogFactory.getLog(PrefCurrencyConvertionSetupAction.class);

	@Override
	public void actionPerformed(ActionEvent e) {

		PrefCurrencyConversionDialog dialog = new PrefCurrencyConversionDialog("Currency Conversion Preference", true);
		dialog.showDialog();
		if (dialog.getButtonPressed() == ListInputDialog.YES_OPTION) {
			String currencyCode1 = dialog.getCurrenyCode1();
			String currencyCode2 = dialog.getCurrenyCode2();
			double money1 = dialog.getConversion1();
			double money2 = dialog.getConversion2();
			
			CurrencyConversionRegistry.getInstance().addCurrencyConversion(currencyCode1, currencyCode2, money1, money2);
			try {
				CurrencyConversionRegistry.getInstance().saveConversionMap();
			} catch (IOException ioe) {
				LOGGER.error(ioe.getMessage(), ioe);
				JOptionPane.showMessageDialog(StockView.getInstance(), "Error saving convertion rates: \"" + ioe.getMessage() + "\"", "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	
}
