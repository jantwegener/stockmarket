package jtw.stock.view.action.file;

import static jtw.stock.view.action.file.BacktestingAction.SPECIAL_BUY_DAY;
import static jtw.stock.view.action.file.BacktestingAction.SPECIAL_BUY_PRICE;
import static jtw.stock.view.action.file.BacktestingAction.SPECIAL_SELL_DAY;
import static jtw.stock.view.action.file.BacktestingAction.SPECIAL_TODAY;

import java.io.IOException;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

import org.antlr.v4.runtime.misc.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jtw.stock.io.CustomIndexReader;
import jtw.stock.main.StockApplicationSettings;
import jtw.stock.model.Stock;
import jtw.stock.model.registry.StockDataRegistry;
import jtw.stock.view.ProgressView;
import jtw.stock.view.StockView;
import jtw.stock.view.action.AbstractIndexSearcherStockAction;
import jtw.stock.view.action.file.util.customsearch.CustomSearchCalculatorHandler;
import jtw.stock.view.action.file.util.customsearch.CustomSearchVisitor;
import jtw.stock.view.action.file.util.customsearch.StockValueVisitor;
import jtw.stock.view.component.BacktestingDialog;
import jtw.stock.view.component.ListInputDialog;
import jtw.stock.view.component.SearchPanel.ExpandablePanel;
import jtw.stock.view.component.StrategyFinderDialog;
import jtw.stock.view.component.StrategyFinderResult;
import jtw.stock.view.component.StrategyFinderResultDialog;

public class StrategyFinderAction extends AbstractIndexSearcherStockAction<StrategyFinderDialog> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Log LOGGER = LogFactory.getLog(StrategyFinderAction.class);
	
	public StrategyFinderAction() {
	}
	
	@Override
	protected String getListInputDialogTitle() {
		return "Strategy Finder...";
	}
	
	@Override
	protected StrategyFinderDialog getNewStockDialog() {
		return new StrategyFinderDialog(getListInputDialogTitle(), true);
	}

	@Override
	protected void handleYesOption(StrategyFinderDialog backtestingDialog, ListInputDialog dialog) {
		LOGGER.debug("StrategyFinderAction YES option");
		StockView.getInstance().resetMenuBar();
		
		String indexName = dialog.getInput();

		String searchBuyText = backtestingDialog.getBuyInput();
		String searchSellText = backtestingDialog.getSellInput();
		List<ExpandablePanel> panelBuyList = backtestingDialog.getBuyInputParameterList();
		List<ExpandablePanel> panelBuyComparisonList = backtestingDialog.getBuyComparisonInputParameterList();
		List<ExpandablePanel> panelSellList = backtestingDialog.getSellInputParameterList();
		double startMoney = backtestingDialog.getMoneyInput();
		double transactionFee = backtestingDialog.getTransactionFeeInput();
		int selectedPriceStructureBuy = backtestingDialog.getSelectedPriceStructureBuy();
		int selectedPriceStructureSell = backtestingDialog.getSelectedPriceStructureSell();
		int handleBankruptcy = backtestingDialog.getSelectedHandleBankruptcy();
		Calendar startDate = backtestingDialog.getStartDateInput();
		Calendar endDate = backtestingDialog.getEndDateInput();
		boolean isSameDaySellAllowed = backtestingDialog.isSameDaySellAllowed();
		
		String customTitle = backtestingDialog.getCustomTitle();
		
		// correct the start and end date so that we can use before and after correctly (inclusive instead of exclusive)
		startDate.add(Calendar.DATE, -1);
		endDate.add(Calendar.DATE, +1);
		
		dialog.dispose();
		
		searchIndex(indexName, searchBuyText, searchSellText, panelBuyList, panelBuyComparisonList, panelSellList, startMoney, transactionFee,
				selectedPriceStructureBuy, selectedPriceStructureSell, handleBankruptcy, startDate, endDate, isSameDaySellAllowed, customTitle);
	}
	
	/**
	 * @param indexName
	 */
	private void searchIndex(String indexName, String searchBuyText, String searchSellText,
			List<ExpandablePanel> panelBuyList, List<ExpandablePanel> panelBuyComparisonList, List<ExpandablePanel> panelSellList, double startMoney, double transactionFee,
			int priceStructureBuy, int priceStructureSell, int handleBankruptcy, Calendar startDate, Calendar endDate,
			boolean isSameDaySellAllowed, String customTitle) {
		
		SwingWorker<Pair<List<String>, StrategyFinderResult>, Void> worker = new SwingWorker<Pair<List<String>, StrategyFinderResult>, Void>() {
			
			private Pair<Stock, Double> getPrice(Stock stoday, Stock stomorrow, int priceStructure) {
				Pair<Stock, Double> retval = null;
				switch (priceStructure) {
					case BacktestingDialog.BACKTESTING_PRICE_STRUCTURE_THIS_DAY_CLOSING:
						retval = new Pair<>(stoday, stoday.getClosing());
						break;
					case BacktestingDialog.BACKTESTING_PRICE_STRUCTURE_NEXT_DAY_OPENING:
						retval = new Pair<>(stomorrow, stomorrow.getOpening());
						break;
					case BacktestingDialog.BACKTESTING_PRICE_STRUCTURE_NEXT_DAY_CLOSING:
						retval = new Pair<>(stomorrow, stomorrow.getClosing());
						break;
					case BacktestingDialog.BACKTESTING_PRICE_STRUCTURE_HALF_OPEN_CLOSE:
						retval = new Pair<>(stoday, (stoday.getOpening() + stoday.getClosing()) / 2d);
						break;
					case BacktestingDialog.BACKTESTING_PRICE_STRUCTURE_HALF_HIGH_LOW:
						retval = new Pair<>(stoday, (stoday.getHigh() + stoday.getLow()) / 2d);
						break;
				}
				return retval;
			}
			
			@Override
			protected Pair<List<String>, StrategyFinderResult> doInBackground() throws Exception {
				List<String> usedStockList = new ArrayList<>();
				StrategyFinderResult result = new StrategyFinderResult();
				result.setStartMoney(startMoney);
				result.setMoney(startMoney);
				
				CustomIndexReader indexReader = new CustomIndexReader();
				indexReader.read(indexName);
				
				List<Pair<String, Integer>> stockList = indexReader.getIndexContentList();
				long daysBetweenStartAndEndDate = ChronoUnit.DAYS.between(startDate.toInstant(), endDate.toInstant());
				ProgressView.getInstance().reset(stockList.size() + (int)daysBetweenStartAndEndDate);
				
				Map<String, Map<String, Double>> specialVariableMapMap = new HashMap<>();
				
				Map<String, CustomSearchCalculatorHandler> buyCalculatorMap = new HashMap<>();
				Map<String, CustomSearchCalculatorHandler> buyComparisonCalculatorMap = new HashMap<>();
				Map<String, CustomSearchCalculatorHandler> sellCalculatorMap = new HashMap<>();
				
				Calendar minDate = Calendar.getInstance();
				// store all data to the registry
				for (Pair<String, Integer> stockAmountPair : stockList) {
					String stockName = stockAmountPair.a;
					
					List<Stock> dailyStockList = StockDataRegistry.getInstance().getDailyStockList(stockName);
					
					if (dailyStockList.get(0).getDate().before(minDate)) {
						minDate = dailyStockList.get(0).getDate();
					}

					final Map<String, Double> specialVariableMap = new HashMap<String, Double>();
					specialVariableMap.put(SPECIAL_BUY_DAY, 0.0);
					specialVariableMap.put(SPECIAL_SELL_DAY, 0.0);
					specialVariableMap.put(SPECIAL_TODAY, 0.0);
					specialVariableMap.put(SPECIAL_BUY_PRICE, 0.0);
					
					specialVariableMapMap.put(stockName, specialVariableMap);
					
					CustomSearchCalculatorHandler buyHandler = new CustomSearchCalculatorHandler();
					buyHandler.setStockName(stockName);
					buyHandler.setExpandablePanelList(panelBuyList);
					buyHandler.compute();
					
					CustomSearchCalculatorHandler buyComparisonHandler = new CustomSearchCalculatorHandler();
					buyComparisonHandler.setStockName(stockName);
					buyComparisonHandler.setExpandablePanelList(panelBuyComparisonList);
					buyComparisonHandler.compute();
					
					CustomSearchCalculatorHandler sellHandler = new CustomSearchCalculatorHandler();
					sellHandler.setStockName(stockName);
					sellHandler.setExpandablePanelList(panelSellList);
					sellHandler.compute();

					buyCalculatorMap.put(stockName, buyHandler);
					buyComparisonCalculatorMap.put(stockName, buyComparisonHandler);
					sellCalculatorMap.put(stockName, sellHandler);
					
					ProgressView.getInstance().increment();
				}

				if (minDate.before(startDate)) {
					minDate = startDate;
				}
				
				// we buy before selling
				boolean buyOption = true;
				// the stock that we bought, well, we have none at the beginning
				String currentlyHoldingStock = null;
				// when bankrupt, we stop
				boolean continueSimulation = true;
				// the current date of the simulation
				Calendar currDate = minDate;
				while (currDate.before(endDate) && continueSimulation) {
					// a marker to not sell the stocks on the same day unless explicitly allowed
					boolean stillSameDay = false;
					
					List<String> todayStockList = new ArrayList<>();
					Map<String, Integer> stockIndexMap = new HashMap<>();
					// search all valid stock names for the day
					// there might be some stocks that we cannot trade due to a bank holiday
					for (Pair<String, Integer> stockAmountPair : stockList) {
						String stockName = stockAmountPair.a;
						int index = StockDataRegistry.getInstance().getDailyStockIndex(stockName, currDate);
						if (index >= 0) {
							todayStockList.add(stockName);
							stockIndexMap.put(stockName, index);
						}
					}
					
					// buying our stocks
					if (buyOption) {
						// search valid buys
						List<String> validForBuyList = new ArrayList<>();
						for (String stockName : todayStockList) {
							if (isValidBuy(stockName, buyCalculatorMap.get(stockName), stockIndexMap.get(stockName))) {
								validForBuyList.add(stockName);
							}
						}
						
						// search best buy
						double maxValue = Double.MIN_VALUE;
						String maxStockName = null;
						for (String stockName : validForBuyList) {
							double value = calcBuyValue(stockName, buyComparisonCalculatorMap.get(stockName), stockIndexMap.get(stockName));
							if (value > maxValue) {
								maxValue = value;
								maxStockName = stockName;
							}
						}
						
						// finally buy the stock
						if (maxStockName != null) {
							continueSimulation = buyStock(result, maxStockName, specialVariableMapMap.get(maxStockName), stockIndexMap.get(maxStockName));
							// next we can sell
							buyOption = false;
							// still the same day
							stillSameDay = true;
							// the name of the stocks we just bought
							currentlyHoldingStock = maxStockName;
						}
					}
					
					// selling our stocks
					if (!buyOption && (!stillSameDay || isSameDaySellAllowed)) {
						// check if we can sell the stock today
						Integer todayIndex = stockIndexMap.get(currentlyHoldingStock);
						if (todayIndex != null && isValidSell(currentlyHoldingStock, sellCalculatorMap.get(currentlyHoldingStock), specialVariableMapMap.get(currentlyHoldingStock), todayIndex)) {
							continueSimulation = sellStock(result, currentlyHoldingStock, specialVariableMapMap.get(currentlyHoldingStock), todayIndex);
							// next we can buy
							buyOption = true;
							// we do not hold any stock now
							currentlyHoldingStock = null;
						}
					}
					
					currDate.add(Calendar.DATE, 1);
					// update the progress
					ProgressView.getInstance().increment();
				}
				
				ProgressView.getInstance().incrementToMax();
				
				return new Pair<List<String>, StrategyFinderResult>(usedStockList, result);
			}
			
			private boolean sellStock(StrategyFinderResult result, String stockName, Map<String, Double> specialVariableMap, int day) throws IOException {
				// init variables
				boolean continueSimulation = true;
				
				List<Stock> dailyStockList = StockDataRegistry.getInstance().getStockList(stockName, StockApplicationSettings.TIME_BASE_DAILY, StockApplicationSettings.TIME_FRAME_MAX);
				if (day + 1 < dailyStockList.size()) {
					specialVariableMap.put(SPECIAL_TODAY, Double.valueOf(day));
					double money = result.getMoney();
					int stocks = result.getStockNum();
					
					// sell the stocks
					Stock stoday = dailyStockList.get(day);
					Stock stomorrow = dailyStockList.get(day + 1);
					Pair<Stock, Double> stockPricePair = getPrice(stoday, stomorrow, priceStructureSell);
					Stock s = stockPricePair.a;
					double price = stockPricePair.b;
					
					money += stocks * price - transactionFee;
					
					if (money <= 0) {
						LOGGER.info("Stock: " + stockName + ", Bankrupt!");
						
						switch (handleBankruptcy) {
							case BacktestingDialog.HANDLE_BANKRUPTCY_STOP:
								continueSimulation = false;
								break;
							case BacktestingDialog.HANDLE_BANKRUPTCY_ADD_START_MONEY:
								money += startMoney;
								result.addTotalMoneyUsed(stockName, startMoney, s.getDate());
								break;
						}
					}
					
					result.addSell(s.getName(), price, transactionFee, stocks, s, money);
					specialVariableMap.put(SPECIAL_SELL_DAY, Double.valueOf(day));
					stocks = 0;
					
					if (LOGGER.isDebugEnabled()) {
						LOGGER.debug("Sell Stock: " + stockName + " at " + price + " (" + s + ")");
					}
					
					result.setMoney(money);
					result.setStockNum(stocks);
				}
				
				return continueSimulation;
			}
			
			private boolean isValidSell(String stockName, CustomSearchCalculatorHandler sellHandler, Map<String, Double> specialVariableMap, int day) {
				sellHandler.setReversedOffset(day);
				
				CustomSearchVisitor sellVisitor = new CustomSearchVisitor();
				sellVisitor.setSpecialVariableMap(specialVariableMap);
				sellVisitor.setInputText(searchSellText);
				sellVisitor.setCustomSearchCalculatorHandler(sellHandler);
				sellVisitor.setExceptionResult(false);
				boolean sellResult = sellVisitor.parse();
				return sellResult;
			}
			
			private boolean buyStock(StrategyFinderResult result, String stockName, Map<String, Double> specialVariableMap, int day) throws IOException {
				// init variables
				boolean continueSimulation = true;
				double money = result.getMoney();
				
				// buy the stocks
				Stock stoday = StockDataRegistry.getInstance().getStockList(stockName, StockApplicationSettings.TIME_BASE_DAILY, StockApplicationSettings.TIME_FRAME_MAX).get(day);
				Stock stomorrow = StockDataRegistry.getInstance().getStockList(stockName, StockApplicationSettings.TIME_BASE_DAILY, StockApplicationSettings.TIME_FRAME_MAX).get(day + 1);
				Pair<Stock, Double> stockPricePair = getPrice(stoday, stomorrow, priceStructureBuy);
				Stock s = stockPricePair.a;
				double price = stockPricePair.b;
				
				int stocks = (int)((money - transactionFee) / price);
				if (stocks <= 0) {
					LOGGER.info("Stock: " + stockName + ", Bankrupt!");
					
					switch (handleBankruptcy) {
						case StrategyFinderDialog.HANDLE_BANKRUPTCY_STOP:
							continueSimulation = false;
							break;
						case StrategyFinderDialog.HANDLE_BANKRUPTCY_ADD_START_MONEY:
							money += startMoney;
							// retry to buy
							stocks = (int)((money - transactionFee) / price);
							result.addTotalMoneyUsed(stockName, startMoney, s.getDate());
							break;
					}
				}
				money -= stocks * price + transactionFee;
				
				result.addBuy(s.getName(), price, transactionFee, stocks, s);
				specialVariableMap.put(SPECIAL_BUY_PRICE, price);
				specialVariableMap.put(SPECIAL_BUY_DAY, Double.valueOf(day));
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("Buy Stock: " + stockName + " at " + price + " (" + s + ")");
				}
				
				result.setMoney(money);
				result.setStockNum(stocks);
				
				return continueSimulation;
			}
			
			private double calcBuyValue(String stockName, CustomSearchCalculatorHandler buyComparisonHandler, int day) {
				buyComparisonHandler.setReversedOffset(day);
				
				StockValueVisitor buyValueVisitor = new StockValueVisitor();
				buyValueVisitor.setInputText(searchBuyText);
				buyValueVisitor.setCustomSearchCalculatorHandler(buyComparisonHandler);
				buyValueVisitor.setExceptionResult(0.0d);
				double valueResult = buyValueVisitor.parse();
				return valueResult;
			}
			
			private boolean isValidBuy(String stockName, CustomSearchCalculatorHandler buyHandler, int day) throws IOException {
				boolean buyResult = false;
				// the price for tomorrow must also be known
				if (day + 1 < StockDataRegistry.getInstance().getStockList(stockName, StockApplicationSettings.TIME_BASE_DAILY, StockApplicationSettings.TIME_FRAME_MAX).size()) {
					buyHandler.setReversedOffset(day);
					
					CustomSearchVisitor buyVisitor = new CustomSearchVisitor();
					buyVisitor.setInputText(searchBuyText);
					buyVisitor.setCustomSearchCalculatorHandler(buyHandler);
					buyVisitor.setExceptionResult(false);
					buyResult = buyVisitor.parse();
				}
				return buyResult;
			}
			
			@Override
			public void done() {
				try {
					Pair<List<String>, StrategyFinderResult> result = get();
					List<String> stockNameList = result.a;
					StrategyFinderResult strategyResult = result.b;
					
					if (LOGGER.isDebugEnabled()) {
						LOGGER.debug("stocks found: " + stockNameList);
					}
					
					String title = String.format("Strategy Finder Result (start: %.2f, fee: %.2f)", startMoney, transactionFee);
					
					StrategyFinderResultDialog resultDialog = new StrategyFinderResultDialog(title, strategyResult);
					resultDialog.setCustomizedTitle(customTitle);
					resultDialog.showDialog();
				} catch (InterruptedException | ExecutionException e) {
					LOGGER.error(e.getMessage(), e);
					
					ProgressView.getInstance().incrementToMax();
					
					JOptionPane.showMessageDialog(StockView.getInstance(), "Error computing plot: \"" + e.getMessage() + "\"", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		};
		worker.execute();
	}
	
}
