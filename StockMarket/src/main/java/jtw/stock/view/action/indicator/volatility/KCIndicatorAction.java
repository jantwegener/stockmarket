package jtw.stock.view.action.indicator.volatility;

import jtw.stock.calculator.IndicatorKCCalculator;
import jtw.stock.calculator.StockCalculator;
import jtw.stock.view.action.chart.AbstractChartAction;

/**
 * The action for the Keltner channel (KC) indicator.
 * 
 * @author Jan-Thierry Wegener
 * 
 * @see {@link https://www.tradingtechnologies.com/xtrader-help/x-study/technical-indicator-definitions/keltner-channel-kc/}
 */
public class KCIndicatorAction extends AbstractChartAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int period;

	private double factor;
	
	public KCIndicatorAction() {
	}

	@Override
	protected StockCalculator getCalculator() {
		return new IndicatorKCCalculator(period, factor);
	}

	public int getPeriod() {
		return period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}

	public double getFactor() {
		return factor;
	}

	public void setFactor(int factor) {
		this.factor = factor;
	}
}
