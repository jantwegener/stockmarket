package jtw.stock.view.action.indicator.momentum;

import jtw.stock.calculator.IndicatorROCCalculator;
import jtw.stock.calculator.StockCalculator;
import jtw.stock.view.action.chart.AbstractChartAction;

public class ROCIndicatorAction extends AbstractChartAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int period;
	
	public ROCIndicatorAction() {
	}

	@Override
	protected StockCalculator getCalculator() {
		return new IndicatorROCCalculator(period);
	}

	public int getPeriod() {
		return period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}
	
}
