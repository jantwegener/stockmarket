package jtw.stock.view.action.file;

import java.awt.event.ActionEvent;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

import org.antlr.v4.runtime.misc.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jtw.stock.io.CustomIndexReader;
import jtw.stock.main.StockApplicationSettings;
import jtw.stock.model.CustomSearchFormula;
import jtw.stock.model.Stock;
import jtw.stock.model.registry.StockDataRegistry;
import jtw.stock.view.ProgressView;
import jtw.stock.view.StockView;
import jtw.stock.view.action.AbstractStockAction;
import jtw.stock.view.action.file.util.customsearch.CustomSearchCalculatorHandler;
import jtw.stock.view.action.file.util.customsearch.CustomSearchVisitor;
import jtw.stock.view.component.AnalyzerDialog;
import jtw.stock.view.component.AnalyzerResult;
import jtw.stock.view.component.AnalyzerResultDialog;
import jtw.stock.view.component.CustomizableSearchDialog;
import jtw.stock.view.component.FileEndingNameFilter;
import jtw.stock.view.component.ListInputDialog;
import jtw.stock.view.component.SearchPanel.ExpandablePanel;
import jtw.util.BooleanUtil;

public class AnalyzerAction extends AbstractStockAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Log LOGGER = LogFactory.getLog(BacktestingAction.class);
	
	public AnalyzerAction() {
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		List<String> alreadyLoadedList = new ArrayList<>();
		File folder = new File(StockApplicationSettings.getBasePath());
		if (!folder.exists()) {
			String errorMsg = String.format("The path '%s' does not exist.", folder.getAbsolutePath());
			LOGGER.error(errorMsg);
			throw new RuntimeException(errorMsg);
		}
		File[] listFiles = folder.listFiles(new FileEndingNameFilter(".index"));
		if (listFiles != null) {
			for (File file : listFiles) {
				alreadyLoadedList.add(file.getName().substring(0, file.getName().length() - ".index".length()));
			}
		} else {
			LOGGER.warn(String.format("The path '%s' is empty.", folder.getAbsolutePath()));
		}

		ListInputDialog dialog = new ListInputDialog("Analyzer...", true, "", "Selectable Index", "Index Name", alreadyLoadedList.toArray(new String[] {}));
		dialog.showDialog();
		if (dialog.getButtonPressed() == ListInputDialog.YES_OPTION) {
			AnalyzerDialog analyzerDialog = new AnalyzerDialog("Analyzer...", true);
			
			// read the last searches
			String filePath = StockApplicationSettings.getLocalPath("AnalyzerAction", "lastSearch");
			loadLastSearch(analyzerDialog, filePath);
			analyzerDialog.showDialog();
			
			if (analyzerDialog.getButtonPressed() == CustomizableSearchDialog.YES_OPTION) {
				StockView.getInstance().resetMenuBar();
				
				String indexName = dialog.getInput();
				
				String domainText = analyzerDialog.getDomainInput();
				List<ExpandablePanel> panelDomainList = analyzerDialog.getDomainInputParameterList();
				
				String searchText = analyzerDialog.getSearchInput();
				List<ExpandablePanel> panelList = analyzerDialog.getSearchInputParameterList();
				
				Calendar startDate = analyzerDialog.getStartDateInput();
				Calendar endDate = analyzerDialog.getEndDateInput();
				
				// correct the start and end date so that we can use before and after correctly (inclusive instead of exclusive)
				startDate.add(Calendar.DATE, -1);
				endDate.add(Calendar.DATE, +1);
				
				dialog.dispose();
				
				// save the last search
				saveLastSearch(analyzerDialog, filePath);
				
				searchIndex(indexName, domainText, panelDomainList, searchText, panelList, startDate, endDate);
			}
		}
	}
	
	private void saveLastSearch(AnalyzerDialog analyzerDialog, String filePath) {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Saving last search...");
		}
		
		try (ObjectOutputStream output = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(filePath)))) {
			CustomSearchFormula[] formulaArray = analyzerDialog.getFormulas();
			output.writeObject(formulaArray[0]);
			output.writeObject(formulaArray[1]);
			
			Calendar startDate = analyzerDialog.getStartDateInput();
			Calendar endDate = analyzerDialog.getEndDateInput();
			
			output.writeObject(startDate);
			output.writeObject(endDate);
		} catch (IOException ioe) {
			JOptionPane.showMessageDialog(StockView.getInstance(), "Error saving last analyzer: \"" + ioe.getMessage() + "\"", "Error", JOptionPane.ERROR_MESSAGE);
			
			LOGGER.error("Error saving last analyzer", ioe);
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Done, saving last search.");
		}
	}
	
	private void loadLastSearch(AnalyzerDialog analyzerDialog, String filePath) {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Loading last search...");
		}
		
		try (ObjectInputStream input = new ObjectInputStream(new BufferedInputStream(new FileInputStream(filePath)))) {
			CustomSearchFormula searchFormula = (CustomSearchFormula) input.readObject();
			CustomSearchFormula domainFormula = (CustomSearchFormula) input.readObject();
			
			analyzerDialog.initFormulas(searchFormula, domainFormula);
			
			Calendar startDate = (Calendar) input.readObject();
			Calendar endDate = (Calendar) input.readObject();
			
			analyzerDialog.setStartDateInput(startDate);
			analyzerDialog.setEndDateInput(endDate);
		} catch (FileNotFoundException fnfe) {
			LOGGER.warn("File AnalyzerAction.lastSearch not found", fnfe);
		} catch (IOException | ClassNotFoundException ioe) {
			JOptionPane.showMessageDialog(StockView.getInstance(), "Error loading last analyzer: \"" + ioe.getMessage() + "\"", "Error", JOptionPane.ERROR_MESSAGE);
			
			LOGGER.error("Error saving last analyzer", ioe);
		}
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Done, loading last search.");
		}
	}

	/**
	 * @param indexName
	 */
	private void searchIndex(String indexName, String domainText, List<ExpandablePanel> panelDomainList, String searchText, List<ExpandablePanel> panelList, Calendar startDate, Calendar endDate) {
		
		SwingWorker<Pair<List<String>, List<AnalyzerResult>>, Void> worker = new SwingWorker<Pair<List<String>, List<AnalyzerResult>>, Void>() {
			
			@Override
			protected Pair<List<String>, List<AnalyzerResult>> doInBackground() throws Exception {
				List<String> foundStockList = new ArrayList<>();
				List<AnalyzerResult> resultList = new ArrayList<>();
				
				CustomIndexReader indexReader = new CustomIndexReader();
				indexReader.read(indexName);
				
				List<Pair<String, Integer>> stockList = indexReader.getIndexContentList();
				ProgressView.getInstance().reset(stockList.size());
				for (Pair<String, Integer> stockAmountPair : stockList) {
					String stockName = stockAmountPair.a;
					
					// compute all the calculators
					CustomSearchCalculatorHandler calculationHandler = new CustomSearchCalculatorHandler();
					calculationHandler.setStockName(stockName);
					calculationHandler.setExpandablePanelList(panelList);
					calculationHandler.compute();

					CustomSearchCalculatorHandler calculationDomainHandler = new CustomSearchCalculatorHandler();
					calculationDomainHandler.setStockName(stockName);
					calculationDomainHandler.setExpandablePanelList(panelDomainList);
					calculationDomainHandler.compute();
					
					AnalyzerResult result = new AnalyzerResult(stockName);
					
					List<Stock> referencedStockList = StockDataRegistry.getInstance().getStockList(stockName);
					
					for (int i = 1; i < referencedStockList.size(); i++) {
						Stock stoday = referencedStockList.get(i);
						
						if (stoday.getDate().after(startDate) && stoday.getDate().before(endDate)) {
							calculationHandler.setReversedOffset(i);
							calculationDomainHandler.setReversedOffset(i);
							
							// check if we are in the domain
							Boolean inDomain = true;
							if (!domainText.trim().equals("")) {
								CustomSearchVisitor domainVisitor = new CustomSearchVisitor();
								domainVisitor.setInputText(domainText);
								domainVisitor.setCustomSearchCalculatorHandler(calculationDomainHandler);
								try {
									inDomain = domainVisitor.parse();
								} catch (IndexOutOfBoundsException e) {
									LOGGER.error("Exception for stock " + stockName + ": " + e.getMessage());
								}
							}
							
							if (BooleanUtil.isTrue(inDomain)) {
								CustomSearchVisitor searchVisitor = new CustomSearchVisitor();
								searchVisitor.setInputText(searchText);
								searchVisitor.setCustomSearchCalculatorHandler(calculationHandler);
								boolean searchResult = false;
								try {
									searchResult = searchVisitor.parse();
								} catch (IndexOutOfBoundsException e) {
									LOGGER.error("Exception for stock " + stockName + ": " + e.getMessage());
								}
								
								if (searchResult) {
									if (LOGGER.isDebugEnabled()) {
										LOGGER.debug("Stock marker " + stockName + " (" + stoday + ")");
									}
									result.addMarkerAt(stoday);
								} else {
									result.addNegativeMarkerAt(stoday);
								}
								// always add a domain marker
								result.addDomainMarkerAt(stoday);
								
								result.increaseCounter();
							}
						}
					}
					
					foundStockList.add(result.getStockName());
					resultList.add(result);
					
					if (LOGGER.isDebugEnabled()) {
						LOGGER.debug("Stock: " + stockName + ", Marker: " + result.getMarkerSize());
					}
					
					ProgressView.getInstance().increment();
				}
				return new Pair<List<String>, List<AnalyzerResult>>(foundStockList, resultList);
			}
			
			@Override
			public void done() {
				try {
					Pair<List<String>, List<AnalyzerResult>> result = get();
					List<String> stockNameList = result.a;
					List<AnalyzerResult> resultList = result.b;
					
					if (LOGGER.isDebugEnabled()) {
						LOGGER.debug("stocks found: " + stockNameList);
					}
					
					String title = String.format("Analyzer result");
					
					AnalyzerResultDialog resultDialog = new AnalyzerResultDialog(title, resultList);
					resultDialog.showDialog();
				} catch (InterruptedException | ExecutionException e) {
					LOGGER.error(e.getMessage(), e);
					
					JOptionPane.showMessageDialog(StockView.getInstance(), "Error computing plot: \"" + e.getMessage() + "\"", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		};
		worker.execute();
	}
	
}
