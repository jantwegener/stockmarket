package jtw.stock.view.action.indicator.momentum;

import jtw.stock.calculator.IndicatorSTOCHCalculator;
import jtw.stock.calculator.StockCalculator;
import jtw.stock.view.action.chart.AbstractChartAction;

/**
 * The action for the STOCH (stochastic) indicator.
 * 
 * @author Jan-Thierry Wegener
 * 
 * @see {@link https://www.tradingtechnologies.com/xtrader-help/x-study/technical-indicator-definitions/stochastic-stoch/}
 */
public class STOCHIndicatorAction extends AbstractChartAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int period;
	
	private int kma;
	
	private int dma;
	
	public STOCHIndicatorAction() {
	}

	@Override
	protected StockCalculator getCalculator() {
		return new IndicatorSTOCHCalculator(period, kma, dma);
	}

	public int getPeriod() {
		return period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}

	public int getKma() {
		return kma;
	}

	public void setKma(int kma) {
		this.kma = kma;
	}
	

	public int getDma() {
		return dma;
	}

	public void setDma(int dma) {
		this.dma = dma;
	}
	
}
