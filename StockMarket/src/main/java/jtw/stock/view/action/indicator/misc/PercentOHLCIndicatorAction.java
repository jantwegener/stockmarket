package jtw.stock.view.action.indicator.misc;

import jtw.stock.calculator.IndicatorPercentOHLCCalculator;
import jtw.stock.calculator.StockCalculator;
import jtw.stock.view.action.chart.AbstractChartAction;

public class PercentOHLCIndicatorAction extends AbstractChartAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public PercentOHLCIndicatorAction() {
	}

	@Override
	protected StockCalculator getCalculator() {
		return new IndicatorPercentOHLCCalculator();
	}
	
}
