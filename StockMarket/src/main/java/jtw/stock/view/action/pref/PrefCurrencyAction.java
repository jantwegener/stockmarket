package jtw.stock.view.action.pref;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jtw.stock.main.StockApplicationSettings;
import jtw.stock.view.StockView;

public class PrefCurrencyAction implements ItemListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Log LOGGER = LogFactory.getLog(PrefCustomIndexAction.class);
	
	private String currencyCode;
	
	public PrefCurrencyAction(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		if (e.getStateChange() == ItemEvent.SELECTED) {
			LOGGER.debug("changing currency to: " + currencyCode);
		}
		
		StockApplicationSettings.setCurrency(currencyCode);
		StockView.getInstance().recompute();
	}
	
}
