package jtw.stock.view.action.overlay;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import org.jfree.chart.plot.XYPlot;

import jtw.stock.view.overlay.CrosshairOverlayObject;

public class CrosshairOverlayAction extends AbstractOverlayAction<CrosshairOverlayObject> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doUpdatePoints(Point2D[] pointArray, XYPlot[] plotArray, Rectangle2D[] screenDataAreaArray, int numValidPoints) {
		if (numValidPoints > 0) {
			getCurrentOverlayObject().updatePoint(pointArray[0]); 
			getCurrentOverlayObject().updatePlot(plotArray[0]);
			getCurrentOverlayObject().updateScreenDataArea(screenDataAreaArray[0]);
		}
	}
	
	@Override
	protected CrosshairOverlayObject doGetOverlayObject() {
		return new CrosshairOverlayObject("Crosshair");
	}

	protected int getMouseClickNeeded() {
		return 1;
	}
	
}
