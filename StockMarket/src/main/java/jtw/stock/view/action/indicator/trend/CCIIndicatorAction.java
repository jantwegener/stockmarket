package jtw.stock.view.action.indicator.trend;

import jtw.stock.calculator.IndicatorCCICalculator;
import jtw.stock.calculator.StockCalculator;
import jtw.stock.view.action.chart.AbstractChartAction;

public class CCIIndicatorAction extends AbstractChartAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int period = 0;
	
	public CCIIndicatorAction() {
	}

	@Override
	protected StockCalculator getCalculator() {
		return new IndicatorCCICalculator(period);
	}

	public int getPeriod() {
		return period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}

}
