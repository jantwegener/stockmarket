package jtw.stock.view.action.file.util.customsearch;

import org.antlr.v4.runtime.misc.Pair;
import org.antlr.v4.runtime.tree.ParseTree;

import jtw.stock.util.antlrgen.StockParser;

public class StockValueVisitor extends AbstractStockVisitor<Double> {
	
	public StockValueVisitor() {
	}
	
	@Override
	protected Double extractResult(Pair<Double, Boolean> result) {
		return result.a;
	}
	
	@Override
	protected ParseTree getParseTree(StockParser parser) {
		return parser.calcExpr();
	}
	
}
