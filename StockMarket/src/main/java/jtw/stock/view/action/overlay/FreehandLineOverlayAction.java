package jtw.stock.view.action.overlay;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import org.jfree.chart.plot.XYPlot;

import jtw.stock.view.overlay.LineOverlayObject;

public class FreehandLineOverlayAction extends AbstractOverlayAction<LineOverlayObject> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected LineOverlayObject doGetOverlayObject() {
		return new LineOverlayObject("Freehand Line", true, false);
	}

	@Override
	protected int getMouseClickNeeded() {
		return 2;
	}

	@Override
	protected void doUpdatePoints(Point2D[] pointArray, XYPlot[] plotArray, Rectangle2D[] screenDataAreaArray, int numValidPoints) {
		getCurrentOverlayObject().updatePoints(pointArray[0], pointArray[1]);
		getCurrentOverlayObject().updatePlot(plotArray[0]);
		getCurrentOverlayObject().updateScreenDataArea(screenDataAreaArray[0]);
	}

	@Override
	protected boolean isDatasetDependent() {
		return false;
	}
	
}
