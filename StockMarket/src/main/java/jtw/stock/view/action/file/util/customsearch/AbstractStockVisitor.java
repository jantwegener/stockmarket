package jtw.stock.view.action.file.util.customsearch;

import java.util.HashMap;
import java.util.Map;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.misc.Pair;
import org.antlr.v4.runtime.tree.ParseTree;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jtw.stock.util.antlrgen.StockBaseVisitor;
import jtw.stock.util.antlrgen.StockLexer;
import jtw.stock.util.antlrgen.StockParser;
import jtw.stock.util.antlrgen.StockParser.AdditionContext;
import jtw.stock.util.antlrgen.StockParser.AndContext;
import jtw.stock.util.antlrgen.StockParser.BracesContext;
import jtw.stock.util.antlrgen.StockParser.CalcExprContext;
import jtw.stock.util.antlrgen.StockParser.ConstantContext;
import jtw.stock.util.antlrgen.StockParser.DivisionContext;
import jtw.stock.util.antlrgen.StockParser.EqualToContext;
import jtw.stock.util.antlrgen.StockParser.FuncAbsContext;
import jtw.stock.util.antlrgen.StockParser.FuncMaxContext;
import jtw.stock.util.antlrgen.StockParser.FuncMinContext;
import jtw.stock.util.antlrgen.StockParser.FuncThresholdContext;
import jtw.stock.util.antlrgen.StockParser.GreaterThanContext;
import jtw.stock.util.antlrgen.StockParser.GreaterThanOrEqualContext;
import jtw.stock.util.antlrgen.StockParser.IfFunctionContext;
import jtw.stock.util.antlrgen.StockParser.LessThanContext;
import jtw.stock.util.antlrgen.StockParser.LessThanOrEqualContext;
import jtw.stock.util.antlrgen.StockParser.MultiplicationContext;
import jtw.stock.util.antlrgen.StockParser.NotContext;
import jtw.stock.util.antlrgen.StockParser.NotEqualToContext;
import jtw.stock.util.antlrgen.StockParser.OrContext;
import jtw.stock.util.antlrgen.StockParser.ParenExprContext;
import jtw.stock.util.antlrgen.StockParser.SpecialContext;
import jtw.stock.util.antlrgen.StockParser.SubtractionContext;
import jtw.stock.util.antlrgen.StockParser.VariableContext;
import jtw.stock.util.antlrgen.StockParser.VariableWithoutIndexContext;
import jtw.util.DoubleUtil;
import jtw.util.IntegerUtil;

public abstract class AbstractStockVisitor<T> extends StockBaseVisitor<Pair<Double, Boolean>> {

	private final transient Log log = LogFactory.getLog(AbstractStockVisitor.class);
	
	private String inputText;
	
	private CustomSearchCalculatorHandler handler = null;
	
	private Map<String, Double> specialVariableMap = new HashMap<>();
	
	/**
	 * If an exception has been caught while visiting the tree, it is stored here.
	 */
	private Exception caughtException;
	
	/**
	 * The result in case of an exception.
	 */
	private T exceptionResult;
	
	public AbstractStockVisitor() {
	}
	
	public void setSpecialVariableMap(Map<String, Double> specialVariableMap) {
		this.specialVariableMap.putAll(specialVariableMap);
	}
	
	public void setInputText(String inputText) {
		this.inputText = inputText;
	}
	
	public void setCustomSearchCalculatorHandler(CustomSearchCalculatorHandler handler) {
		this.handler = handler;
	}
	
	public T parse() {
		StockLexer stockLexer = new StockLexer(CharStreams.fromString(inputText));
		CommonTokenStream tokens = new CommonTokenStream(stockLexer);
		StockParser parser = new StockParser(tokens);
		ParseTree tree = getParseTree(parser);
		
		T retval;
		Pair<Double, Boolean> result = null;
		try {
			// reset any caught exception
			caughtException = null;
			result = visit(tree);
			retval = extractResult(result);
		} catch (Exception e) {
			caughtException = e;
			retval = getExceptionResult();
		}
		return retval;
	}
	
	public boolean isExceptionCaught() {
		return caughtException != null;
	}
	
	public Exception getCaughtException() {
		return caughtException;
	}
	
	/**
	 * Returns the result in case of an exception.
	 * 
	 * @return The default result.
	 */
	public T getExceptionResult() {
		return exceptionResult;
	}
	
	public void setExceptionResult(T exceptionResult) {
		this.exceptionResult = exceptionResult;
	}
	
	/**
	 * Extracts the final result which is then returned to the caller.
	 * 
	 * @param result The result from the parsing.
	 * 
	 * @return The final result.
	 */
	protected abstract T extractResult(Pair<Double, Boolean> result);
	
	/**
	 * Returns the {@link ParseTree} for the given {@link StockParser}.
	 * 
	 * @param parser The used parser.
	 * 
	 * @return The parse tree.
	 */
	protected abstract ParseTree getParseTree(StockParser parser);
	
	@Override
	public Pair<Double, Boolean> visitNot(NotContext ctx) {
		Pair<Double, Boolean> tmpResult = visit(ctx.boolOrExpr());
		Pair<Double, Boolean> notResult = new Pair<>(tmpResult.a, !tmpResult.b);
		return notResult;
	}
	
	@Override
	public Pair<Double, Boolean> visitParenExpr(ParenExprContext ctx) {
		// seems to be unnecessary but nevertheless we keep the implementation
		if (log.isTraceEnabled()) {
			log.trace("enter: visitParenExpr(" + ctx.getText() + ")");
		}
		Pair<Double, Boolean> result = visit(ctx.boolExpr());
		if (log.isTraceEnabled()) {
			log.trace("exit: visitParenExpr: " + result);
		}
		return result;
	}
	
	@Override
	public Pair<Double, Boolean> visitOr(OrContext ctx) {
		Pair<Double, Boolean> a = visit(ctx.boolOrExpr());
		Pair<Double, Boolean> b = visit(ctx.boolAndExpr());
		
		Pair<Double, Boolean> result = new Pair<>(null, a.b || b.b);
		return result;
	}
	
	@Override
	public Pair<Double, Boolean> visitAnd(AndContext ctx) {
		Pair<Double, Boolean> a = visit(ctx.boolAndExpr());
		Pair<Double, Boolean> b = visit(ctx.boolRelMathExpr());
		
		Pair<Double, Boolean> result = new Pair<>(null, a.b && b.b);
		return result;
	}
	
	@Override
	public Pair<Double, Boolean> visitLessThan(LessThanContext ctx) {
		Pair<Double, Boolean> a = visit(ctx.mathPlusExpr(0));
		Pair<Double, Boolean> b = visit(ctx.mathPlusExpr(1));
		
		Pair<Double, Boolean> result = new Pair<>(null, a.a.doubleValue() < b.a.doubleValue());
		return result;
	}
	
	@Override
	public Pair<Double, Boolean> visitGreaterThan(GreaterThanContext ctx) {
		Pair<Double, Boolean> a = visit(ctx.mathPlusExpr(0));
		Pair<Double, Boolean> b = visit(ctx.mathPlusExpr(1));
		
		Pair<Double, Boolean> result = new Pair<>(null, a.a.doubleValue() > b.a.doubleValue());
		return result;
	}
	
	@Override
	public Pair<Double, Boolean> visitLessThanOrEqual(LessThanOrEqualContext ctx) {
		Pair<Double, Boolean> a = visit(ctx.mathPlusExpr(0));
		Pair<Double, Boolean> b = visit(ctx.mathPlusExpr(1));
		
		Pair<Double, Boolean> result = new Pair<>(null, a.a.doubleValue() <= b.a.doubleValue());
		return result;
	}
	
	@Override
	public Pair<Double, Boolean> visitGreaterThanOrEqual(GreaterThanOrEqualContext ctx) {
		Pair<Double, Boolean> a = visit(ctx.mathPlusExpr(0));
		Pair<Double, Boolean> b = visit(ctx.mathPlusExpr(1));
		
		Pair<Double, Boolean> result = new Pair<>(null, a.a.doubleValue() >= b.a.doubleValue());
		return result;
	}
	
	@Override
	public Pair<Double, Boolean> visitEqualTo(EqualToContext ctx) {
		Pair<Double, Boolean> a = visit(ctx.mathPlusExpr(0));
		Pair<Double, Boolean> b = visit(ctx.mathPlusExpr(1));
		
		Pair<Double, Boolean> result = new Pair<>(null, a.a.doubleValue() == b.a.doubleValue());
		return result;
	}
	
	@Override
	public Pair<Double, Boolean> visitNotEqualTo(NotEqualToContext ctx) {
		Pair<Double, Boolean> a = visit(ctx.mathPlusExpr(0));
		Pair<Double, Boolean> b = visit(ctx.mathPlusExpr(1));
		
		Pair<Double, Boolean> result = new Pair<>(null, a.a.doubleValue() != b.a.doubleValue());
		return result;
	}
	
	@Override
	public Pair<Double, Boolean> visitAddition(AdditionContext ctx) {
		Pair<Double, Boolean> a = visit(ctx.mathPlusExpr());
		Pair<Double, Boolean> b = visit(ctx.mathMultExpr());
		
		Pair<Double, Boolean> result = new Pair<>(a.a + b.a, null);
		return result;
	}
	
	@Override
	public Pair<Double, Boolean> visitSubtraction(SubtractionContext ctx) {
		Pair<Double, Boolean> a = visit(ctx.mathPlusExpr());
		Pair<Double, Boolean> b = visit(ctx.mathMultExpr());
		
		Pair<Double, Boolean> result = new Pair<>(a.a - b.a, null);
		return result;
	}
	
	@Override
	public Pair<Double, Boolean> visitMultiplication(MultiplicationContext ctx) {
		Pair<Double, Boolean> a = visit(ctx.mathMultExpr());
		Pair<Double, Boolean> b = visit(ctx.atom());
		
		Pair<Double, Boolean> result = new Pair<>(a.a * b.a, null);
		return result;
	}
	
	@Override
	public Pair<Double, Boolean> visitDivision(DivisionContext ctx) {
		Pair<Double, Boolean> a = visit(ctx.mathMultExpr());
		Pair<Double, Boolean> b = visit(ctx.atom());
		
		Pair<Double, Boolean> result = new Pair<>(a.a / b.a, null);
		return result;
	}
	
	@Override
	public Pair<Double, Boolean> visitVariable(VariableContext ctx) {
		String var = ctx.var().name().getText();
		int index = IntegerUtil.toInt(ctx.var().index().getText(), -1);
		
		double resultDouble = handler.getReversedResult(var, index);
		Pair<Double, Boolean> result = new Pair<>(resultDouble, null);
		return result;
	}
	
	@Override
	public Pair<Double, Boolean> visitVariableMultipleIndex(StockParser.VariableMultipleIndexContext ctx) {
		String var = ctx.varMultipleIndex().name().getText();
		int index = IntegerUtil.toInt(ctx.varMultipleIndex().index().getText(), -1);
		int series = IntegerUtil.toInt(ctx.varMultipleIndex().index2().getText(), -1);
		
		double resultDouble = handler.getReversedResult(var, series, index);
		Pair<Double, Boolean> result = new Pair<>(resultDouble, null);
		return result;
	}
	
	@Override
	public Pair<Double, Boolean> visitVariableWithoutIndex(VariableWithoutIndexContext ctx) {
		String var = ctx.varNoIndex().name().getText();
		int index = 0;
		
		double resultDouble = handler.getReversedResult(var, index);
		Pair<Double, Boolean> result = new Pair<>(resultDouble, null);
		return result;
	}
	
	@Override
	public Pair<Double, Boolean> visitConstant(ConstantContext ctx) {
		Pair<Double, Boolean> result = new Pair<>(DoubleUtil.toDouble(ctx.getText()), null);
		return result;
	}
	
	@Override
	public Pair<Double, Boolean> visitBraces(BracesContext ctx) {
		Pair<Double, Boolean> result = visit(ctx.boolExpr());
		return result;
	}
	
	@Override public Pair<Double, Boolean> visitSpecial(SpecialContext ctx) {
		Pair<Double, Boolean> result = new Pair<>(specialVariableMap.get(ctx.getText()), null);
		return result;
	}
	
	@Override
	public Pair<Double, Boolean> visitIfFunction(IfFunctionContext ctx) {
		Pair<Double, Boolean> boolResult = visit(ctx.boolExpr());
		Pair<Double, Boolean> result;
		if (boolResult.b) {
			result = visit(ctx.trueExpr());
		} else {
			result = visit(ctx.falseExpr());
		}
		
		return result;
	}
	
	@Override
	public Pair<Double, Boolean> visitFuncMax(FuncMaxContext ctx) {
		double maxValue = Double.MIN_VALUE;
		for (CalcExprContext expr : ctx.calcExpr()) {
			Pair<Double, Boolean> exprResult = visit(expr);
			if (exprResult.a > maxValue) {
				maxValue = exprResult.a;
			}
		}
		Pair<Double, Boolean> result = new Pair<>(maxValue, null);
		return result;
	}
	
	@Override
	public Pair<Double, Boolean> visitFuncMin(FuncMinContext ctx) {
		double minValue = Double.MAX_VALUE;
		for (CalcExprContext expr : ctx.calcExpr()) {
			Pair<Double, Boolean> exprResult = visit(expr);
			if (exprResult.a < minValue) {
				minValue = exprResult.a;
			}
		}
		Pair<Double, Boolean> result = new Pair<>(minValue, null);
		return result;
	}
	
	@Override
	public Pair<Double, Boolean> visitFuncAbs(FuncAbsContext ctx) {
		Pair<Double, Boolean> exprResult = visit(ctx.calcExpr());
		Pair<Double, Boolean> result = new Pair<>(Math.abs(exprResult.a), null);
		return result;
	}
	
	@Override
	public Pair<Double, Boolean> visitFuncThreshold(FuncThresholdContext ctx) {
		Pair<Double, Boolean> thresholdValue = visit(ctx.thresholdValueExpr());
		Pair<Double, Boolean> exprValue = visit(ctx.calcExpr());
		
		Pair<Double, Boolean> result;
		if (exprValue.a >= thresholdValue.a) {
			result = new Pair<>(exprValue.a, null);
		} else {
			result = new Pair<>(0d, null);
		}
		return result;
	}
	
}
