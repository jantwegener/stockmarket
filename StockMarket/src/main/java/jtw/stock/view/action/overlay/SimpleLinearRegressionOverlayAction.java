package jtw.stock.view.action.overlay;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.List;

import org.jfree.chart.plot.XYPlot;
import org.jfree.data.general.DatasetUtilities;
import org.jfree.data.xy.XYDataset;

import jtw.stock.model.registry.StockDataRegistry;
import jtw.stock.util.XYDatasetListWrapper;
import jtw.stock.view.overlay.LineOverlayObject;
import jtw.util.MathUtil;
import jtw.util.ObjectToDouble;

public class SimpleLinearRegressionOverlayAction extends AbstractOverlayAction<LineOverlayObject> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	ObjectToDouble<Number> otd = new ObjectToDouble<Number>() {

		@Override
		public double objToDouble(Number o) {
			return o.doubleValue();
		}

	};

	@Override
	protected void doUpdatePoints(Point2D[] pointArray, XYPlot[] plotArray, Rectangle2D[] screenDataAreaArray, int numValidPoints) {
		if (numValidPoints >= 2) {
			double startX = pointArray[0].getX();
			double endX = pointArray[1].getX();
			
			XYDataset ds = StockDataRegistry.getInstance().getReferencedDataset();
			
			int[] indicesStart = DatasetUtilities.findItemIndicesForX(ds, 0, startX);
			int[] indicesEnd = DatasetUtilities.findItemIndicesForX(ds, 0, endX);
			if (indicesStart.length > 0 && indicesEnd.length > 0) {
				int startIndex = indicesStart[0];
				int stopIndex = indicesEnd[0];
				
				if (startIndex < 0) {
					startIndex = 0;
				}
				
				if (stopIndex < 0) {
					stopIndex = ds.getItemCount(0) - 1;
				}
				
				if (stopIndex < startIndex) {
					startIndex = indicesEnd[0];
					stopIndex = indicesStart[0];
				}

				List<Number> xList = new XYDatasetListWrapper(ds, 0, true);
				List<Number> yList = new XYDatasetListWrapper(ds, 0, false);
				
				double[] simpleLinearRegression = MathUtil.simpleLinearRegression(xList, yList, otd, otd, startIndex, stopIndex);
				double alpha = simpleLinearRegression[0];
				double beta = simpleLinearRegression[1];

				double xStart = xList.get(startIndex).doubleValue();
				double yStart = alpha + beta * xStart;
				double xEnd = xList.get(stopIndex).doubleValue();
				double yEnd = alpha + beta * xEnd;
				Point2D startPoint = new Point2D.Double(xStart, yStart);
				Point2D endPoint = new Point2D.Double(xEnd, yEnd);
				
				getCurrentOverlayObject().updatePoints(startPoint, endPoint);
				getCurrentOverlayObject().updatePlot(plotArray[0]);
				getCurrentOverlayObject().updateScreenDataArea(screenDataAreaArray[0]);
			}
		}
	}
	
	@Override
	protected LineOverlayObject doGetOverlayObject() {
		return new LineOverlayObject("Simple Linear Regression", false, true);
	}

	protected int getMouseClickNeeded() {
		return 2;
	}

	@Override
	protected boolean isDatasetDependent() {
		return false;
	}
	
}
