package jtw.stock.view.action.file.util.customsearch;

import org.antlr.v4.runtime.misc.Pair;
import org.antlr.v4.runtime.tree.ParseTree;

import jtw.stock.util.antlrgen.StockParser;

public class CustomSearchVisitor extends AbstractStockVisitor<Boolean> {
	
	public CustomSearchVisitor() {
	}

	@Override
	protected Boolean extractResult(Pair<Double, Boolean> result) {
		return result.b;
	}
	
	@Override
	protected ParseTree getParseTree(StockParser parser) {
		return parser.boolExpr();
	}
	
}
