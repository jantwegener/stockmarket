package jtw.stock.view.action.chart;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;

import javax.swing.AbstractButton;
import javax.swing.SwingUtilities;

import jtw.stock.calculator.CalculationFinishedListener;
import jtw.stock.calculator.StockCalculator;
import jtw.stock.model.registry.StockCalculatorFinishedListenerRegistry;
import jtw.stock.view.StockView;
import jtw.stock.view.action.AbstractStockAction;
import jtw.util.BooleanUtil;

public abstract class AbstractChartAction extends AbstractStockAction implements PropertyChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Returns the calculator to use for this action.
	 * 
	 * @return The calculator.
	 */
	protected abstract StockCalculator getCalculator();
	
	/** 
	 * The name of the plot. Is set when the plot is calculated. The name is needed when it shall be removed from the chart.
	 */
	private String plotName = null;
	
	public AbstractChartAction() {
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e != null && ((e.getModifiers() & ActionEvent.CTRL_MASK) > 0 || (e.getModifiers() & ActionEvent.SHIFT_MASK) > 0)) {
			handleCtrlShiftClick(e);
		} else {
			handleNormalClick(e);
		}
	}
	
	private void handleCtrlShiftClick(ActionEvent e) {
		System.out.println("CTRL");
	}

	protected void handleNormalClick(ActionEvent e) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				if (e == null) {
					activate();

					setSelected(true);
				} else if (e.getSource() instanceof AbstractButton) {
					AbstractButton aButton = (AbstractButton) e.getSource();
					boolean selected = aButton.getModel().isSelected();
					if (selected) {
						activate();

						setSelected(true);
					} else {
						deactivate();

						setSelected(false);
					}
				}
			}
		});
	}
	
	private void activate() {
		StockCalculator calc = getCalculator();
		calc.setCalculationFinishedListener(getCalculationFinishedListener(calc));
		plotName = calc.getName();
		calc.start();
	}
	
	/**
	 * Returns the {@link CalculationFinishedListener} to be used. By default, it is
	 * taken from the {@link StockCalculatorFinishedListenerRegistry}.
	 * 
	 * @param calc
	 *            The calculator used.
	 * 
	 * @return The {@link CalculationFinishedListener} for this action. 
	 *         By default it is looked up from the {@link StockCalculatorFinishedListenerRegistry}.
	 */
	protected CalculationFinishedListener getCalculationFinishedListener(StockCalculator calc) {
		return StockCalculatorFinishedListenerRegistry.getInstance().getCalculationFinishedListener(calc.getClass());
	}
	
	private void deactivate() {
		StockView.getInstance().removePlot(plotName);
	}
	
	public String getPlotName() {
		return plotName;
	}

	public void setSelected(boolean selected) {
		putValue(SELECTED_KEY, selected);
	}
	
	public boolean isSelected() {
		return BooleanUtil.isTrue(getValue(SELECTED_KEY));
	}
	
	public boolean shouldUpdateSelectedStateFromAction() {
		return true;
	}
	
}
