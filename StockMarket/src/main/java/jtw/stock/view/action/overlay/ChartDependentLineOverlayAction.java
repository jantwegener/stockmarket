package jtw.stock.view.action.overlay;

import jtw.stock.view.overlay.LineOverlayObject;

public class ChartDependentLineOverlayAction extends FreehandLineOverlayAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected LineOverlayObject doGetOverlayObject() {
		return new LineOverlayObject("Chart Dependend Line", true, false);
	}

	protected boolean isDatasetDependent() {
		return true;
	}
	
}
