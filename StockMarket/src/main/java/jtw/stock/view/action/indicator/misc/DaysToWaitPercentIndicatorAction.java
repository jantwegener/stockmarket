package jtw.stock.view.action.indicator.misc;

import jtw.stock.calculator.IndicatorDaysToWaitPercentCalculator;
import jtw.stock.calculator.StockCalculator;
import jtw.stock.view.action.chart.AbstractChartAction;

public class DaysToWaitPercentIndicatorAction extends AbstractChartAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private double percent;
	
	public DaysToWaitPercentIndicatorAction() {
		
	}

	@Override
	protected StockCalculator getCalculator() {
		return new IndicatorDaysToWaitPercentCalculator(percent);
	}
	
	public double getPercent() {
		return percent;
	}
	
	public void setPercent(double percent) {
		this.percent = percent;
	}
	
}
