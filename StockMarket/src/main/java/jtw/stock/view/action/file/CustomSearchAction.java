package jtw.stock.view.action.file;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

import org.antlr.v4.runtime.misc.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jtw.stock.io.CustomIndexReader;
import jtw.stock.io.CustomSearchWriter;
import jtw.stock.io.HistoryReader;
import jtw.stock.io.HistoryReaderFactory;
import jtw.stock.main.StockApplicationSettings;
import jtw.stock.model.CustomSearchFormula;
import jtw.stock.model.registry.StockDataRegistry;
import jtw.stock.view.ProgressView;
import jtw.stock.view.StockView;
import jtw.stock.view.action.AbstractStockAction;
import jtw.stock.view.action.file.util.customsearch.CustomSearchCalculatorHandler;
import jtw.stock.view.action.file.util.customsearch.CustomSearchVisitor;
import jtw.stock.view.component.CustomizableSearchDialog;
import jtw.stock.view.component.FileEndingNameFilter;
import jtw.stock.view.component.ListInputDialog;
import jtw.stock.view.component.SearchPanel.ExpandablePanel;

/**
 * The class handling the action for customizable searches.
 * 
 * <p>
 * Warning: This class manipulates the {@link ProgressView}.
 * </p> 
 * 
 * @author Jan-Thierry Wegener
 */
public class CustomSearchAction extends AbstractStockAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Log LOGGER = LogFactory.getLog(CustomSearchAction.class);
	
	public CustomSearchAction() {
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		List<String> alreadyLoadedList = new ArrayList<>();
		File folder = new File(StockApplicationSettings.getBasePath());
		if (!folder.exists()) {
			String errorMsg = String.format("The path '%s' does not exist.", folder.getAbsolutePath());
			LOGGER.error(errorMsg);
			throw new RuntimeException(errorMsg);
		}
		File[] listFiles = folder.listFiles(new FileEndingNameFilter(".index"));
		if (listFiles != null) {
			for (File file : listFiles) {
				alreadyLoadedList.add(file.getName().substring(0, file.getName().length() - ".index".length()));
			}
		} else {
			LOGGER.warn(String.format("The path '%s' is empty.", folder.getAbsolutePath()));
		}

		ListInputDialog dialog = new ListInputDialog("Search...", true, "", "Selectable Index", "Index Name", alreadyLoadedList.toArray(new String[] {}));
		dialog.showDialog();
		if (dialog.getButtonPressed() == ListInputDialog.YES_OPTION) {
			CustomizableSearchDialog customDialog = new CustomizableSearchDialog("Search...", true);
			customDialog.showDialog();
			
			if (customDialog.getButtonPressed() == CustomizableSearchDialog.YES_OPTION) {
				List<CustomSearchFormula> formulaList = customDialog.getFormulaList();
				
				// new formula may be stored
				if (customDialog.isNewFormula()) {
					// ask user if he wants to store the formula
					String formulaName = "";
					while (formulaName != null && formulaName.trim().equals("")) {
						formulaName = JOptionPane.showInputDialog(StockApplicationSettings.getMainFrame(), "Do you want to save the search? Please enter a name (must not be empty).", "Save formula...", JOptionPane.QUESTION_MESSAGE);
					}
					if (formulaName != null) {
						formulaList.get(0).setName(formulaName);
					} else {
						// user does not want to store the formula
						formulaList.remove(0);
					}
				}
				
				// store the formula list
				try {
					CustomSearchWriter writer = new CustomSearchWriter();
					writer.setFormulaList(formulaList);
					writer.write();
				} catch (IOException ioe) {
					LOGGER.error(ioe.getMessage(), ioe);

					JOptionPane.showMessageDialog(StockView.getInstance(), "Error writing history file: \"" + ioe.getMessage() + "\"", "Error", JOptionPane.ERROR_MESSAGE);
				}
				
				StockView.getInstance().resetMenuBar();
				
				String indexName = dialog.getInput();
				
				String searchText = customDialog.getInput();
				List<ExpandablePanel> panelList = customDialog.getInputParameterList();
				
				dialog.dispose();
				
				searchIndex(indexName, searchText, panelList);
			}
		}
	}

	/**
	 * @param indexName
	 */
	private void searchIndex(String indexName, String searchText, List<ExpandablePanel> panelList) {
		
		SwingWorker<List<String>, Void> worker = new SwingWorker<List<String>, Void>() {

			@Override
			protected List<String> doInBackground() throws Exception {
				List<String> foundStockList = new ArrayList<>();
				
				CustomIndexReader indexReader = new CustomIndexReader();
				indexReader.read(indexName);
				
				List<Pair<String, Integer>> stockList = indexReader.getIndexContentList();
				ProgressView.getInstance().reset(stockList.size());
				for (Pair<String, Integer> stockAmountPair : stockList) {
					String stockName = stockAmountPair.a;
					StockDataRegistry.getInstance().ensureUpToDate(stockName);
					
					// compute all the calculators
					CustomSearchCalculatorHandler handler = new CustomSearchCalculatorHandler();
					handler.setStockName(stockName);
					handler.setExpandablePanelList(panelList);
					handler.compute();
					
					CustomSearchVisitor visitor = new CustomSearchVisitor();
					visitor.setInputText(searchText);
					visitor.setCustomSearchCalculatorHandler(handler);
					boolean result = visitor.parse();
					
					if (result) {
						foundStockList.add(stockName);
					}
					ProgressView.getInstance().increment();
				}
				return foundStockList;
			}

			public void done() {
				try {
					List<String> stockNameList = get();
					
					if (LOGGER.isDebugEnabled()) {
						LOGGER.debug("stocks found: " + stockNameList);
					}
					
					OpenAction oa = AbstractStockAction.getAction(OpenAction.class);
					oa.setPreLoadedList(stockNameList);
					oa.triggerAction();
				} catch (InterruptedException | ExecutionException e) {
					LOGGER.error(e.getMessage(), e);

					JOptionPane.showMessageDialog(StockView.getInstance(), "Error computing plot: \"" + e.getMessage() + "\"", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		};
		worker.execute();
	}
	
}
