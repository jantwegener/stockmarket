package jtw.stock.view.action.pref;

import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.List;

import javax.swing.JOptionPane;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jtw.stock.io.CustomSearchWriter;
import jtw.stock.main.StockApplicationSettings;
import jtw.stock.model.CustomSearchFormula;
import jtw.stock.view.StockView;
import jtw.stock.view.action.AbstractStockAction;
import jtw.stock.view.component.CustomizableSearchDialog;

public class PrefCustomSearchAction extends AbstractStockAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Log log = LogFactory.getLog(PrefCustomSearchAction.class);

	@Override
	public void actionPerformed(ActionEvent e) {
		CustomizableSearchDialog customDialog = new CustomizableSearchDialog("Customize Search...", true);
		customDialog.showDialog();
		
		if (customDialog.getButtonPressed() == CustomizableSearchDialog.YES_OPTION) {
			List<CustomSearchFormula> formulaList = customDialog.getFormulaList();
			
			// new formula may be stored
			if (customDialog.isNewFormula()) {
				// ask user if he wants to store the formula
				String formulaName = "";
				while (formulaName != null && formulaName.trim().equals("")) {
					formulaName = JOptionPane.showInputDialog(StockApplicationSettings.getMainFrame(), "Do you want to save the search? Please enter a name (must not be empty).", "Save formula...", JOptionPane.QUESTION_MESSAGE);
				}
				if (formulaName != null) {
					formulaList.get(0).setName(formulaName);
				} else {
					// user does not want to store the formula
					formulaList.remove(0);
				}
			}
			
			// store the formula list
			try {
				CustomSearchWriter writer = new CustomSearchWriter();
				writer.setFormulaList(formulaList);
				writer.write();
			} catch (IOException ioe) {
				log.error(ioe.getMessage(), ioe);

				JOptionPane.showMessageDialog(StockView.getInstance(), "Error writing history file: \"" + ioe.getMessage() + "\"", "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

}
