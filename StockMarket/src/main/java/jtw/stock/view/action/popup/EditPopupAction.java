package jtw.stock.view.action.popup;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import jtw.stock.view.action.overlay.AbstractOverlayAction;
import jtw.stock.view.overlay.OverlayObject;

public class EditPopupAction extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private OverlayObject overlayObject;
	
	public EditPopupAction(OverlayObject oo) {
		super("Edit");

		overlayObject = oo;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		@SuppressWarnings("unchecked")
		AbstractOverlayAction<OverlayObject> action = AbstractOverlayAction.getAction(overlayObject.getActionClass());
		action.setCurrentOverlayObject(overlayObject);
		action.reset(e);
	}

}
