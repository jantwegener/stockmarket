package jtw.stock.view.action.chart;

import jtw.stock.calculator.StockCalculator;
import jtw.stock.calculator.StockHighCalculator;

public class ChartHighAction extends AbstractChartAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ChartHighAction() {
	}

	@Override
	protected StockCalculator getCalculator() {
		return new StockHighCalculator();
	}

}
