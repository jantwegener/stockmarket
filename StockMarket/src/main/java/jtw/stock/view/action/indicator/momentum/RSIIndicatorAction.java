package jtw.stock.view.action.indicator.momentum;

import jtw.stock.calculator.IndicatorRSICalculator;
import jtw.stock.calculator.StockCalculator;
import jtw.stock.view.action.chart.AbstractChartAction;

public class RSIIndicatorAction extends AbstractChartAction {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int period;
	
	public RSIIndicatorAction() {
	}

	@Override
	protected StockCalculator getCalculator() {
		return new IndicatorRSICalculator(period);
	}

	public int getPeriod() {
		return period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}
	
}
