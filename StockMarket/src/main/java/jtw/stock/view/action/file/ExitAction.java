package jtw.stock.view.action.file;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JFrame;

import jtw.stock.main.StockApplicationSettings;

public class ExitAction extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ExitAction() {
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JFrame frame = StockApplicationSettings.getMainFrame();
		frame.setVisible(false);
		frame.dispose();
		System.exit(0);
	}

}
