package jtw.stock.view.action.chart;

import jtw.stock.calculator.StockCalculator;
import jtw.stock.calculator.StockPercentageCalculator;

public class ChartPercentAction extends AbstractChartAction {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ChartPercentAction() {
	}
	
	@Override
	protected StockCalculator getCalculator() {
		return new StockPercentageCalculator();
	}
	
}
