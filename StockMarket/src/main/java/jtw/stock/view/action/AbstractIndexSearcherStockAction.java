package jtw.stock.view.action;

import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jtw.stock.main.StockApplicationSettings;
import jtw.stock.view.action.file.StrategyFinderAction;
import jtw.stock.view.component.CustomizableSearchDialog;
import jtw.stock.view.component.FileEndingNameFilter;
import jtw.stock.view.component.ListInputDialog;
import jtw.stock.view.component.StockDialog;

public abstract class AbstractIndexSearcherStockAction<T extends StockDialog> extends AbstractStockAction {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Log LOGGER = LogFactory.getLog(StrategyFinderAction.class);
	
	public AbstractIndexSearcherStockAction() {
	}
	
	/**
	 * Returns the title of the list input dialog.
	 * 
	 * @return The title of the list input dialog.
	 */
	protected abstract String getListInputDialogTitle();
	
	/**
	 * Returns a new stock dialog to show.
	 * 
	 * @return The new stock dialog.
	 */
	protected abstract T getNewStockDialog();
	
	@Override
	public void actionPerformed(ActionEvent e) {
		List<String> alreadyLoadedList = new ArrayList<>();
		File folder = new File(StockApplicationSettings.getBasePath());
		if (!folder.exists()) {
			String errorMsg = String.format("The path '%s' does not exist.", folder.getAbsolutePath());
			LOGGER.error(errorMsg);
			throw new RuntimeException(errorMsg);
		}
		File[] listFiles = folder.listFiles(new FileEndingNameFilter(".index"));
		if (listFiles != null) {
			for (File file : listFiles) {
				alreadyLoadedList.add(file.getName().substring(0, file.getName().length() - ".index".length()));
			}
		} else {
			LOGGER.warn(String.format("The path '%s' is empty.", folder.getAbsolutePath()));
		}

		ListInputDialog dialog = new ListInputDialog(getListInputDialogTitle(), true, "", "Selectable Index", "Index Name", alreadyLoadedList.toArray(new String[] {}));
		dialog.showDialog();
		if (dialog.getButtonPressed() == ListInputDialog.YES_OPTION) {
			T backtestingDialog = getNewStockDialog();
			backtestingDialog.showDialog();
			
			if (backtestingDialog.getButtonPressed() == CustomizableSearchDialog.YES_OPTION) {
				LOGGER.debug("User clicked YES");
				handleYesOption(backtestingDialog, dialog);
			} else {
				LOGGER.debug("User clicked NO");
				handleNoOption(backtestingDialog, dialog);
			}
		}
	}
	
	/**
	 * Handles the user click on the YES button.
	 * 
	 * @param backtestingDialog The dialog that sends the pressed button.
	 */
	protected abstract void handleYesOption(T backtestingDialog, ListInputDialog dialog);
	
	/**
	 * Handles the user click on the NO button. By default, this does nothing but disposing the dialogs.
	 * 
	 * @param backtestingDialog The dialog that sends the pressed button.
	 */
	protected void handleNoOption(T backtestingDialog, ListInputDialog dialog) {
		backtestingDialog.dispose();
		dialog.dispose();
	}
	
}
