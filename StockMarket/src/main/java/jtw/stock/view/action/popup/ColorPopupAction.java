package jtw.stock.view.action.popup;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JColorChooser;

import jtw.stock.view.OverlayView;
import jtw.stock.view.overlay.OverlayObject;

public class ColorPopupAction extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private OverlayObject overlayObject;
	private Component comp;

	public ColorPopupAction(Component comp, OverlayObject oo) {
		super("Color");
		
		overlayObject = oo;
		this.comp = comp;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		Color color = JColorChooser.showDialog(comp, "Choose the color", overlayObject.getMainColor());
		if (color != null) {
			overlayObject.setMainColor(color);
			OverlayView.getInstance().repaint();
		}
	}

}
