package jtw.stock.view.action.file;

import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

import org.antlr.v4.runtime.misc.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jtw.stock.io.CustomIndexReader;
import jtw.stock.main.StockApplicationSettings;
import jtw.stock.model.Stock;
import jtw.stock.model.registry.StockDataRegistry;
import jtw.stock.view.ProgressView;
import jtw.stock.view.StockView;
import jtw.stock.view.action.AbstractStockAction;
import jtw.stock.view.component.FileEndingNameFilter;
import jtw.stock.view.component.ListInputDialog;
import jtw.stock.view.component.MinMaxListInputDialog;

public class MinMaxSearchAction extends AbstractStockAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	Log LOGGER = LogFactory.getLog(MinMaxSearchAction.class);

	@Override
	public void actionPerformed(ActionEvent e) {
		List<String> alreadyLoadedList = new ArrayList<>();
		File folder = new File(StockApplicationSettings.getBasePath());
		if (!folder.exists()) {
			String errorMsg = String.format("The path '%s' does not exist.", folder.getAbsolutePath());
			LOGGER.error(errorMsg);
			throw new RuntimeException(errorMsg);
		}
		File[] listFiles = folder.listFiles(new FileEndingNameFilter(".index"));
		if (listFiles != null) {
			for (File file : listFiles) {
				alreadyLoadedList.add(file.getName().substring(0, file.getName().length() - ".index".length()));
			}
		} else {
			LOGGER.warn(String.format("The path '%s' is empty.", folder.getAbsolutePath()));
		}

		MinMaxListInputDialog dialog = new MinMaxListInputDialog("Search...", true, "", "Selectable Index", "Index Name", alreadyLoadedList.toArray(new String[] {}));
		dialog.showDialog();
		if (dialog.getButtonPressed() == ListInputDialog.YES_OPTION) {
			StockView.getInstance().resetMenuBar();

			double min = dialog.getMinInput();
			double max = dialog.getMaxInput();
			String stockName = dialog.getInput();
			dialog.dispose();
			searchIndex(stockName, min, max);
		}
	}

	/**
	 * @param indexName
	 */
	private void searchIndex(String indexName, double min, double max) {
		
		SwingWorker<List<Stock>, Void> worker = new SwingWorker<List<Stock>, Void>() {

			@Override
			protected List<Stock> doInBackground() throws Exception {
				List<Stock> foundStockList = new ArrayList<Stock>();
				
				CustomIndexReader reader = new CustomIndexReader();
				reader.read(indexName);

				List<Pair<String, Integer>> stockList = reader.getIndexContentList();
				
				ProgressView.getInstance().reset(stockList.size());
				for (Pair<String, Integer> stockNameAmountPair : stockList) {
					String stockName = stockNameAmountPair.a;
					LOGGER.info("Getting data for: " + stockName);
					List<Stock> stockDailyList = StockDataRegistry.getInstance().getDailyStockList(stockName);
					Stock stock = stockDailyList.get(stockDailyList.size() - 1);
					LOGGER.info("done");

					if (min <= stock.getClosing() && stock.getClosing() <= max) {
						foundStockList.add(stock);
					}
					ProgressView.getInstance().increment();
				}
				return foundStockList;
			}

			@Override
			public void done() {
				try {
					List<Stock> foundStockList = get();
					List<String> stockNameList = new ArrayList<>();
					
					LOGGER.debug("stocks found");
					for (Stock stock : foundStockList) {
						LOGGER.debug("> " + stock.getName() + ": " + stock.getCompanyName());
						stockNameList.add(stock.getName());
					}
					LOGGER.debug("end.");
					
					OpenAction oa = AbstractStockAction.getAction(OpenAction.class);
					oa.setPreLoadedList(stockNameList);
					oa.triggerAction();
				} catch (InterruptedException | ExecutionException e) {
					LOGGER.error(e.getMessage(), e);

					JOptionPane.showMessageDialog(StockView.getInstance(), "Error computing plot: \"" + e.getMessage() + "\"", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		};
		worker.execute();
	}
	
	protected String getLocalPath(String stockName, String ending) {
		return StockApplicationSettings.getBasePath() + stockName + "." + ending;
	}

}
