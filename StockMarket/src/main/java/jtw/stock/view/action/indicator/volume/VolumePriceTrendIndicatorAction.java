package jtw.stock.view.action.indicator.volume;

import jtw.stock.calculator.IndicatorVPTCalculator;
import jtw.stock.calculator.StockCalculator;
import jtw.stock.view.action.chart.AbstractChartAction;

public class VolumePriceTrendIndicatorAction extends AbstractChartAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public VolumePriceTrendIndicatorAction() {
	}
	
	@Override
	protected StockCalculator getCalculator() {
		IndicatorVPTCalculator calc = new IndicatorVPTCalculator();
		return calc;
	}

}
