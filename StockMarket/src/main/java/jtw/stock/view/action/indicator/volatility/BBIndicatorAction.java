package jtw.stock.view.action.indicator.volatility;

import jtw.stock.calculator.IndicatorBBCalculator;
import jtw.stock.calculator.StockCalculator;
import jtw.stock.view.action.chart.AbstractChartAction;

/**
 * The action for the Bollinger bands (BB) indicator.
 * 
 * @author Jan-Thierry Wegener
 * 
 * @see {@link https://www.tradingtechnologies.com/xtrader-help/x-study/technical-indicator-definitions/bollinger-band-bbands/}
 */
public class BBIndicatorAction extends AbstractChartAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int period;

	private double factor;
	
	public BBIndicatorAction() {
	}

	@Override
	protected StockCalculator getCalculator() {
		return new IndicatorBBCalculator(period, factor);
	}

	public int getPeriod() {
		return period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}

	public double getFactor() {
		return factor;
	}

	public void setFactor(int factor) {
		this.factor = factor;
	}
	
}
