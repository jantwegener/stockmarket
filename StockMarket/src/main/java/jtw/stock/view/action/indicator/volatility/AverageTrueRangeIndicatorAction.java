package jtw.stock.view.action.indicator.volatility;

import jtw.stock.calculator.IndicatorATRCalculator;
import jtw.stock.calculator.StockCalculator;
import jtw.stock.view.action.chart.AbstractChartAction;

public class AverageTrueRangeIndicatorAction extends AbstractChartAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int period = 0;
	
	public AverageTrueRangeIndicatorAction() {
	}
	
	@Override
	protected StockCalculator getCalculator() {
		IndicatorATRCalculator calc = new IndicatorATRCalculator(period);
		return calc;
	}

	public void setPeriod(int d) {
		period = d;
	}
	
	public int getPeriod() {
		return period;
	}

}
