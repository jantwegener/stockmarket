package jtw.stock.view.action.popup;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.UIManager;
import javax.swing.tree.DefaultMutableTreeNode;

import jtw.stock.view.OverlaySummaryView;
import jtw.stock.view.overlay.OverlayObject;

public class DeletePopupAction extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private OverlayObject overlayObject;
	private DefaultMutableTreeNode node;

	public DeletePopupAction(OverlayObject oo, DefaultMutableTreeNode node) {
		super("Delete", UIManager.getIcon("InternalFrame.closeIcon"));

		overlayObject = oo;
		this.node = node;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		OverlaySummaryView.getInstance().remove(overlayObject, node);
	}

}
