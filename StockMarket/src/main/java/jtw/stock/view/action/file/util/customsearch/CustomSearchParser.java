package jtw.stock.view.action.file.util.customsearch;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.antlr.v4.runtime.tree.TerminalNode;

import jtw.stock.util.antlrgen.StockBaseListener;
import jtw.stock.util.antlrgen.StockLexer;
import jtw.stock.util.antlrgen.StockParser;
import jtw.util.IntegerUtil;

public class CustomSearchParser extends StockBaseListener {

	private String inputText;
	
	private boolean result = false;
	
	private CustomSearchCalculatorHandler handler = null;
	
	public CustomSearchParser() {
	}
	
	public void setInputText(String inputText) {
		this.inputText = inputText;
	}
	
	public void setCustomSearchCalculatorHandler(CustomSearchCalculatorHandler handler) {
		this.handler = handler;
	}
	
	public void parse() {
		StockLexer stockLexer = new StockLexer(CharStreams.fromString(inputText));
		CommonTokenStream tokens = new CommonTokenStream(stockLexer);
		StockParser parser = new StockParser(tokens);
		ParseTree tree = parser.boolExpr();
		ParseTreeWalker walker = new ParseTreeWalker();
		walker.walk(this, tree);
	}
	
	public boolean getResult() {
		return result;
	}
	
//	/**
//	 * {@inheritDoc}
//	 *
//	 * <p>The default implementation does nothing.</p>
//	 */
//	@Override public void enterExpr(StockParser.ExprContext ctx) {
//	}
//	/**
//	 * {@inheritDoc}
//	 *
//	 * <p>The default implementation does nothing.</p>
//	 */
//	@Override
//	public void exitExpr(StockParser.ExprContext ctx) {
//		System.out.println();
//	}
//	/**
//	 * {@inheritDoc}
//	 *
//	 * <p>The default implementation does nothing.</p>
//	 */
//	@Override public void enterBoolexpr(StockParser.BoolexprContext ctx) { }
//	/**
//	 * {@inheritDoc}
//	 *
//	 * <p>The default implementation does nothing.</p>
//	 */
//	@Override public void exitBoolexpr(StockParser.BoolexprContext ctx) { }
//	/**
//	 * {@inheritDoc}
//	 *
//	 * <p>The default implementation does nothing.</p>
//	 */
//	@Override public void enterAndboolexpr(StockParser.AndboolexprContext ctx) { }
//	/**
//	 * {@inheritDoc}
//	 *
//	 * <p>The default implementation does nothing.</p>
//	 */
//	@Override public void exitAndboolexpr(StockParser.AndboolexprContext ctx) { }
//	/**
//	 * {@inheritDoc}
//	 *
//	 * <p>The default implementation does nothing.</p>
//	 */
//	@Override public void enterOrboolexpr(StockParser.OrboolexprContext ctx) { }
//	/**
//	 * {@inheritDoc}
//	 *
//	 * <p>The default implementation does nothing.</p>
//	 */
//	@Override public void exitOrboolexpr(StockParser.OrboolexprContext ctx) { }
//	/**
//	 * {@inheritDoc}
//	 *
//	 * <p>The default implementation does nothing.</p>
//	 */
//	@Override public void enterMathexpr(StockParser.MathexprContext ctx) { }
//	/**
//	 * {@inheritDoc}
//	 *
//	 * <p>The default implementation does nothing.</p>
//	 */
//	@Override public void exitMathexpr(StockParser.MathexprContext ctx) { }
//	/**
//	 * {@inheritDoc}
//	 *
//	 * <p>The default implementation does nothing.</p>
//	 */
//	@Override public void enterMultexpr(StockParser.MultexprContext ctx) { }
//	/**
//	 * {@inheritDoc}
//	 *
//	 * <p>The default implementation does nothing.</p>
//	 */
//	@Override public void exitMultexpr(StockParser.MultexprContext ctx) { }
//	/**
//	 * {@inheritDoc}
//	 *
//	 * <p>The default implementation does nothing.</p>
//	 */
//	@Override public void enterAddexpr(StockParser.AddexprContext ctx) { }
//	/**
//	 * {@inheritDoc}
//	 *
//	 * <p>The default implementation does nothing.</p>
//	 */
//	@Override public void exitAddexpr(StockParser.AddexprContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterVar(StockParser.VarContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override 
	public void exitVar(StockParser.VarContext ctx) {
		String var = ctx.name().getText();
		int index = IntegerUtil.toInt(ctx.index().getText(), -1);
		
		double result = handler.getResult(var, index);
		
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterName(StockParser.NameContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitName(StockParser.NameContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterIndex(StockParser.IndexContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitIndex(StockParser.IndexContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterCon(StockParser.ConContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitCon(StockParser.ConContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterEveryRule(ParserRuleContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitEveryRule(ParserRuleContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void visitTerminal(TerminalNode node) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void visitErrorNode(ErrorNode node) { }
	
}
