package jtw.stock.view.action.file;

import static jtw.stock.main.StockApplicationSettings.TIME_BASE_DAILY;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

import org.antlr.v4.runtime.misc.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jtw.stock.io.CustomIndexReader;
import jtw.stock.io.StockListHistoryReader;
import jtw.stock.main.StockApplicationSettings;
import jtw.stock.model.Stock;
import jtw.stock.model.registry.Country2CurrencyRegistry;
import jtw.stock.model.registry.CurrencyConversionRegistry;
import jtw.stock.model.registry.StockDataRegistry;
import jtw.stock.view.ProgressView;
import jtw.stock.view.StockView;
import jtw.stock.view.action.AbstractStockAction;
import jtw.stock.view.action.chart.AbstractChartAction;
import jtw.stock.view.action.chart.ChartClosingAction;
import jtw.stock.view.component.ListInputDialog;

public class OpenCustomIndexAction extends AbstractStockAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Log LOGGER = LogFactory.getLog(OpenCustomIndexAction.class);
	
	public OpenCustomIndexAction() {
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String[] alreadyLoadedArray = StockApplicationSettings.loadAlreadyAvailableArray(".index");

		ListInputDialog dialog = new ListInputDialog("Open Custom Index...", true, "", "Selectable Custom Index", "Custom Index Name", alreadyLoadedArray);
		dialog.showDialog();
		if (dialog.getButtonPressed() == ListInputDialog.YES_OPTION) {
			StockView.getInstance().resetMenuBar();
			
			String indexName = dialog.getInput();
			readIndex(indexName);
		}
	}
	
	private void readIndex(String indexName) {
		SwingWorker<Object, Void> worker = new SwingWorker<Object, Void>() {

			@Override
			protected Object doInBackground() throws Exception {
				CustomIndexReader indexReader = new CustomIndexReader();
				indexReader.read(indexName);
				List<Pair<String, Integer>> stockPairList = indexReader.getIndexContentList();
				
				StockView.getInstance().reset();
				
				ProgressView.getInstance().reset(2 * stockPairList.size() + 1);
				int minSize = Integer.MAX_VALUE;
				// read the stocks and cache them in the registry
				for (Pair<String, Integer> stockNameQuantityPair : stockPairList) {
					int size = StockDataRegistry.getInstance().getStockList(stockNameQuantityPair.a, TIME_BASE_DAILY).size();
					minSize = Math.min(size, minSize);
					
					ProgressView.getInstance().increment();
				}

				// compute the index
				List<Stock> stockReturnList = new ArrayList<>(minSize);
				for (Pair<String, Integer> stockNameQuantityPair : stockPairList) {
					List<Stock> stockList = StockDataRegistry.getInstance().getStockList(stockNameQuantityPair.a, TIME_BASE_DAILY);
					if (LOGGER.isInfoEnabled()) {
						LOGGER.info("Handling stock: " + stockNameQuantityPair.a);
					}

					int j = 0;
					for (int i = stockList.size() - minSize; i < stockList.size(); i++) {
						Stock stock = stockList.get(i);
						
						Stock indexStock;
						if (stockReturnList.size() > j) {
							indexStock = stockReturnList.get(j);
						} else {
							indexStock = new Stock();
							indexStock.setName(indexName);
							indexStock.setDate(stock.getDate());
							indexStock.setCurrency(Country2CurrencyRegistry.getInstance().getCurrencyFromStockName(stockNameQuantityPair.a));
							stockReturnList.add(indexStock);
						}
						
						double opening = CurrencyConversionRegistry.getInstance().convert(stock.getCurrency(), StockApplicationSettings.getCurrency(), stockNameQuantityPair.b * stock.getOpening());
						double closing = CurrencyConversionRegistry.getInstance().convert(stock.getCurrency(), StockApplicationSettings.getCurrency(), stockNameQuantityPair.b * stock.getClosing());
						double high = CurrencyConversionRegistry.getInstance().convert(stock.getCurrency(), StockApplicationSettings.getCurrency(), stockNameQuantityPair.b * stock.getHigh());
						double low = CurrencyConversionRegistry.getInstance().convert(stock.getCurrency(), StockApplicationSettings.getCurrency(), stockNameQuantityPair.b * stock.getLow());
						
						indexStock.addOpening(opening);
						indexStock.addClosing(closing);
						indexStock.addHigh(high);
						indexStock.addLow(low);
						indexStock.addVolume(stock.getVolume());
						
						j++;
					}

					ProgressView.getInstance().increment();
				}
				
				StockApplicationSettings.setStockName(indexName);
				StockApplicationSettings.getMainFrame().setTitle("Index: " + indexName);
				
				StockListHistoryReader stockReader = new StockListHistoryReader();
				stockReader.setInputList(stockReturnList);
				stockReader.read(indexName);

				StockDataRegistry.getInstance().putStockList(indexName, stockReader.getDailyList(), stockReader.getWeeklyList(), stockReader.getMonthlyList());
				
				ProgressView.getInstance().increment();
				
				return null;
			}

			@Override
			public void done() {
				try {
					// call get to receive any exception occurred during the computation
					get();
					
					AbstractChartAction action = AbstractChartAction.getAction(ChartClosingAction.class);
					action.triggerAction();
				} catch (InterruptedException | ExecutionException e) {
					LOGGER.error(e.getMessage(), e);
					ProgressView.getInstance().incrementToMax();
					
					JOptionPane.showMessageDialog(StockView.getInstance(), "Error opening custom index: \"" + indexName + "\" : \"" + e.getCause().getMessage() + "\"", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		};
		worker.execute();
	}
	
}
