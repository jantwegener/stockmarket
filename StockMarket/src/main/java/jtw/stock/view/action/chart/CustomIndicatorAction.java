package jtw.stock.view.action.chart;

import java.awt.event.ActionEvent;

import javax.swing.JFrame;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jtw.stock.calculator.StockCalculator;
import jtw.stock.main.StockApplicationSettings;
import jtw.stock.view.action.AbstractStockAction;
import jtw.stock.view.component.CustomizableIndicatorDialog;
import jtw.stock.view.component.CustomizableSearchDialog;

public class CustomIndicatorAction extends AbstractStockAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Log LOGGER = LogFactory.getLog(CustomizableSearchDialog.class);

	private String indicatorClazz;
	
	private String title;
	
	public CustomIndicatorAction() {
	}

	public String getIndicatorClazz() {
		return indicatorClazz;
	}

	public void setIndicatorClazz(String indicatorClazz) {
		this.indicatorClazz = indicatorClazz;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JFrame parent = StockApplicationSettings.getMainFrame();
		
		try {
			CustomizableIndicatorDialog dialog = new CustomizableIndicatorDialog(parent, title, true);
			dialog.setIndicatorClazzFromString(indicatorClazz);
			dialog.init();
			dialog.showDialog();
			if (dialog.getButtonPressed() == CustomizableIndicatorDialog.YES_OPTION) {
				StockCalculator calculator = dialog.getCalculator();
				calculator.initStandardCalculationFinishedListener();
				calculator.start();
			}
		} catch (ClassNotFoundException cnfe) {
			LOGGER.fatal("Instatiation of clazz " + indicatorClazz + " failed.", cnfe);
		}
	}

}
