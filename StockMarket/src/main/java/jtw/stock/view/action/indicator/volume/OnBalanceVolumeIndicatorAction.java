package jtw.stock.view.action.indicator.volume;

import jtw.stock.calculator.IndicatorOBVCalculator;
import jtw.stock.calculator.StockCalculator;
import jtw.stock.view.action.chart.AbstractChartAction;

public class OnBalanceVolumeIndicatorAction extends AbstractChartAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public OnBalanceVolumeIndicatorAction() {
	}
	
	@Override
	protected StockCalculator getCalculator() {
		IndicatorOBVCalculator calc = new IndicatorOBVCalculator();
		return calc;
	}

}
