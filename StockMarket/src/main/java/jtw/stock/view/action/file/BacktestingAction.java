package jtw.stock.view.action.file;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

import org.antlr.v4.runtime.misc.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jtw.stock.io.CustomIndexReader;
import jtw.stock.model.Stock;
import jtw.stock.model.registry.StockDataRegistry;
import jtw.stock.view.ProgressView;
import jtw.stock.view.StockView;
import jtw.stock.view.action.AbstractIndexSearcherStockAction;
import jtw.stock.view.action.file.util.customsearch.CustomSearchCalculatorHandler;
import jtw.stock.view.action.file.util.customsearch.CustomSearchVisitor;
import jtw.stock.view.component.BacktestingDialog;
import jtw.stock.view.component.BacktestingResult;
import jtw.stock.view.component.BacktestingResultDialog;
import jtw.stock.view.component.ListInputDialog;
import jtw.stock.view.component.SearchPanel.ExpandablePanel;

/**
 * The class handling the action for back testing.
 * 
 * <p>
 * Warning: This class manipulates the {@link ProgressView}.
 * </p> 
 * 
 * @author Jan-Thierry Wegener
 */
public class BacktestingAction extends AbstractIndexSearcherStockAction<BacktestingDialog> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Log LOGGER = LogFactory.getLog(BacktestingAction.class);
	
	/**
	 * The variable name for the price when buying the stock.
	 */
	public static final String SPECIAL_BUY_PRICE = "_BUY";
	/**
	 * The variable name for the day when buying the stock.
	 */
	public static final String SPECIAL_BUY_DAY = "_BUYDAY";
	/**
	 * The variable name for the day when selling the stock.
	 */
	public static final String SPECIAL_SELL_DAY = "_SELLDAY";
	/**
	 * The variable name for today.
	 */
	public static final String SPECIAL_TODAY = "_TODAY";
	
	public BacktestingAction() {
	}
	
	@Override
	protected String getListInputDialogTitle() {
		return "Back-Testing...";
	}

	@Override
	protected BacktestingDialog getNewStockDialog() {
		return new BacktestingDialog(getListInputDialogTitle(), true);
	}
	
	@Override
	protected void handleYesOption(BacktestingDialog backtestingDialog, ListInputDialog dialog) {
		LOGGER.debug("BacktestingAction YES option");
		StockView.getInstance().resetMenuBar();
		
		String indexName = dialog.getInput();

		String searchBuyText = backtestingDialog.getBuyInput();
		String searchSellText = backtestingDialog.getSellInput();
		List<ExpandablePanel> panelBuyList = backtestingDialog.getBuyInputParameterList();
		List<ExpandablePanel> panelSellList = backtestingDialog.getSellInputParameterList();
		double startMoney = backtestingDialog.getMoneyInput();
		double transactionFee = backtestingDialog.getTransactionFeeInput();
		int selectedPriceStructureBuy = backtestingDialog.getSelectedPriceStructureBuy();
		int selectedPriceStructureSell = backtestingDialog.getSelectedPriceStructureSell();
		int handleBankruptcy = backtestingDialog.getSelectedHandleBankruptcy();
		Calendar startDate = backtestingDialog.getStartDateInput();
		Calendar endDate = backtestingDialog.getEndDateInput();
		boolean isSameDaySellAllowed = backtestingDialog.isSameDaySellAllowed();
		
		String customTitle = backtestingDialog.getCustomTitle();
		
		// correct the start and end date so that we can use before and after correctly (inclusive instead of exclusive)
		startDate.add(Calendar.DATE, -1);
		endDate.add(Calendar.DATE, +1);
		
		dialog.dispose();
		
		searchIndex(indexName, searchBuyText, searchSellText, panelBuyList, panelSellList, startMoney, transactionFee,
				selectedPriceStructureBuy, selectedPriceStructureSell, handleBankruptcy, startDate, endDate, isSameDaySellAllowed, customTitle);
	}
	
	/**
	 * @param indexName
	 */
	private void searchIndex(String indexName, String searchBuyText, String searchSellText,
			List<ExpandablePanel> panelBuyList, List<ExpandablePanel> panelSellList, double startMoney, double transactionFee,
			int priceStructureBuy, int priceStructureSell, int handleBankruptcy, Calendar startDate, Calendar endDate,
			boolean isSameDaySellAllowed, String customTitle) {
		
		SwingWorker<Pair<List<String>, List<BacktestingResult>>, Void> worker = new SwingWorker<Pair<List<String>, List<BacktestingResult>>, Void>() {
			
			private Pair<Stock, Double> getPrice(Stock stoday, Stock stomorrow, int priceStructure) {
				Pair<Stock, Double> retval = null;
				switch (priceStructure) {
					case BacktestingDialog.BACKTESTING_PRICE_STRUCTURE_THIS_DAY_CLOSING:
						retval = new Pair<>(stoday, stoday.getClosing());
						break;
					case BacktestingDialog.BACKTESTING_PRICE_STRUCTURE_NEXT_DAY_OPENING:
						retval = new Pair<>(stomorrow, stomorrow.getOpening());
						break;
					case BacktestingDialog.BACKTESTING_PRICE_STRUCTURE_NEXT_DAY_CLOSING:
						retval = new Pair<>(stomorrow, stomorrow.getClosing());
						break;
					case BacktestingDialog.BACKTESTING_PRICE_STRUCTURE_HALF_OPEN_CLOSE:
						retval = new Pair<>(stoday, (stoday.getOpening() + stoday.getClosing()) / 2d);
						break;
					case BacktestingDialog.BACKTESTING_PRICE_STRUCTURE_HALF_HIGH_LOW:
						retval = new Pair<>(stoday, (stoday.getHigh() + stoday.getLow()) / 2d);
						break;
				}
				return retval;
			}
			
			private Pair<Boolean, Boolean> handleBuyStocks(BacktestingResult result, CustomSearchCalculatorHandler buyHandler, Map<String, Double> specialVariableMap, List<Stock> stockDailyList, int day) {
				specialVariableMap.put(SPECIAL_TODAY, Double.valueOf(day));
				
				boolean isBuyOption = true;
				boolean stopSimulation = false;
				int stocks = result.getStockNum();
				double money = result.getMoney();
				String stockName = result.getStockName();
				
				buyHandler.setReversedOffset(day);
				
				CustomSearchVisitor buyVisitor = new CustomSearchVisitor();
				buyVisitor.setInputText(searchBuyText);
				buyVisitor.setCustomSearchCalculatorHandler(buyHandler);
				buyVisitor.setExceptionResult(false);
				boolean buyResult = buyVisitor.parse();
				
				if (buyResult) {
					// buy the stocks
					Stock stoday = stockDailyList.get(day);
					Stock stomorrow = stockDailyList.get(day + 1);
					Pair<Stock, Double> stockPricePair = getPrice(stoday, stomorrow, priceStructureBuy);
					Stock s = stockPricePair.a;
					double price = stockPricePair.b;
					
					stocks = (int)((money - transactionFee) / price);
					while (!stopSimulation && stocks <= 0) {
						LOGGER.info("Stock: " + stockName + ", Bankrupt!");
						
						switch (handleBankruptcy) {
							case BacktestingDialog.HANDLE_BANKRUPTCY_STOP:
								stopSimulation = true;
								break;
							case BacktestingDialog.HANDLE_BANKRUPTCY_ADD_START_MONEY:
								money += startMoney;
								// retry to buy
								stocks = (int)((money - transactionFee) / price);
								result.addTotalMoneyUsed(startMoney, s.getDate());
								break;
						}
					}
					
					if (stocks > 0) {
						money -= stocks * price + transactionFee;
						
						result.addBuy(price, transactionFee, stocks, s);
						specialVariableMap.put(SPECIAL_BUY_PRICE, price);
						specialVariableMap.put(SPECIAL_BUY_DAY, Double.valueOf(day));
						if (LOGGER.isDebugEnabled()) {
							LOGGER.debug("Buy Stock: " + stockName + " at " + price + " (" + s + ")");
						}
						
						isBuyOption = !isBuyOption;
						
						result.setMoney(money);
						result.setStockNum(stocks);
					}
				}
				
				return new Pair<Boolean, Boolean>(isBuyOption, stopSimulation);
			}
			
			private Pair<Boolean, Boolean> handleSellStocks(BacktestingResult result, CustomSearchCalculatorHandler sellHandler, Map<String, Double> specialVariableMap, List<Stock> stockDailyList, int day) {
				specialVariableMap.put(SPECIAL_TODAY, Double.valueOf(day));
				
				boolean isBuyOption = false;
				boolean stopSimulation = false;
				
				int stocks = result.getStockNum();
				double money = result.getMoney();
				String stockName = result.getStockName();

				sellHandler.setReversedOffset(day);
				
				CustomSearchVisitor sellVisitor = new CustomSearchVisitor();
				sellVisitor.setSpecialVariableMap(specialVariableMap);
				sellVisitor.setInputText(searchSellText);
				sellVisitor.setCustomSearchCalculatorHandler(sellHandler);
				sellVisitor.setExceptionResult(false);
				boolean sellResult = sellVisitor.parse();
				
				if (sellResult) {
					// sell the stocks
					Stock stoday = stockDailyList.get(day);
					Stock stomorrow = stockDailyList.get(day + 1);
					Pair<Stock, Double> stockPricePair = getPrice(stoday, stomorrow, priceStructureSell);
					Stock s = stockPricePair.a;
					double price = stockPricePair.b;
					
					money += stocks * price - transactionFee;
					
					if (money <= 0) {
						LOGGER.info("Stock: " + stockName + ", Bankrupt!");
						
						switch (handleBankruptcy) {
							case BacktestingDialog.HANDLE_BANKRUPTCY_STOP:
								stopSimulation = true;
								break;
							case BacktestingDialog.HANDLE_BANKRUPTCY_ADD_START_MONEY:
								money += startMoney;
								result.addTotalMoneyUsed(startMoney, s.getDate());
								break;
						}
					}
					
					result.addSell(price, transactionFee, stocks, s, money);
					specialVariableMap.put(SPECIAL_SELL_DAY, Double.valueOf(day));
					stocks = 0;
					
					if (LOGGER.isDebugEnabled()) {
						LOGGER.debug("Sell Stock: " + stockName + " at " + price + " (" + s + ")");
					}
					
					isBuyOption = !isBuyOption;
					
					result.setMoney(money);
					result.setStockNum(stocks);
				}

				return new Pair<Boolean, Boolean>(isBuyOption, stopSimulation);
			}
			
			@Override
			protected Pair<List<String>, List<BacktestingResult>> doInBackground() throws Exception {
				List<String> foundStockList = new ArrayList<>();
				List<BacktestingResult> resultList = new ArrayList<>();
				
				CustomIndexReader indexReader = new CustomIndexReader();
				indexReader.read(indexName);
				
				List<Pair<String, Integer>> stockList = indexReader.getIndexContentList();
				ProgressView.getInstance().reset(stockList.size());
				for (Pair<String, Integer> stockAmountPair : stockList) {
					String stockName = stockAmountPair.a;
					List<Stock> stockDailyList = StockDataRegistry.getInstance().getDailyStockList(stockName);

					Stock firstDailyStock = stockDailyList.get(0);
					Stock lastDailyStock = stockDailyList.get(stockDailyList.size() - 1);
					
					// compute all the calculators
					CustomSearchCalculatorHandler buyHandler = new CustomSearchCalculatorHandler();
					buyHandler.setStockName(stockName);
					buyHandler.setExpandablePanelList(panelBuyList);
					buyHandler.compute();

					CustomSearchCalculatorHandler sellHandler = new CustomSearchCalculatorHandler();
					sellHandler.setStockName(stockName);
					sellHandler.setExpandablePanelList(panelSellList);
					sellHandler.compute();
					
					final Map<String, Double> specialVariableMap = new HashMap<String, Double>();
					specialVariableMap.put(SPECIAL_BUY_DAY, 0.0);
					specialVariableMap.put(SPECIAL_SELL_DAY, 0.0);
					specialVariableMap.put(SPECIAL_TODAY, 0.0);
					specialVariableMap.put(SPECIAL_BUY_PRICE, 0.0);
					
					boolean isBuyOption = true;
					double money = startMoney;
					int stocks = 0;
					BacktestingResult result = new BacktestingResult(stockName);
					result.setStock(lastDailyStock);
					result.setMoney(money);
					result.setStockNum(0);
					
					Calendar firstDate = startDate.before(firstDailyStock) ? firstDailyStock.getDate() : startDate;
					result.addTotalMoneyUsed(startMoney, firstDate);
					
					boolean stopSimulation = false;
					
					for (int i = 1; i < stockDailyList.size() - 1 && !stopSimulation; i++) {
						Stock stoday = stockDailyList.get(i);
						
						if (stoday.getDate().after(startDate) && stoday.getDate().before(endDate)) {
							// the stocks can be bought and sold the same day
							if (isSameDaySellAllowed) {
								if (isBuyOption) {
									Pair<Boolean, Boolean> retVal = handleBuyStocks(result, buyHandler, specialVariableMap, stockDailyList, i);
									isBuyOption = retVal.a;
									stopSimulation = retVal.b;
								} 
								
								if (!isBuyOption) {
									Pair<Boolean, Boolean> retVal = handleSellStocks(result, sellHandler, specialVariableMap, stockDailyList, i);
									isBuyOption = retVal.a;
									stopSimulation = retVal.b;
								}
							} else {
								// the stocks must be sold at a different day than they were bought
								// this is ensured by allowing either buy or sell at one time tick
								if (isBuyOption) {
									Pair<Boolean, Boolean> retVal = handleBuyStocks(result, buyHandler, specialVariableMap, stockDailyList, i);
									isBuyOption = retVal.a;
									stopSimulation = retVal.b;
								} else {
									Pair<Boolean, Boolean> retVal = handleSellStocks(result, sellHandler, specialVariableMap, stockDailyList, i);
									isBuyOption = retVal.a;
									stopSimulation = retVal.b;
								}
							}
						}
					}
					
					foundStockList.add(result.getStockName());
					resultList.add(result);
					
					if (LOGGER.isDebugEnabled()) {
						LOGGER.debug("Stock: " + stockName + ", Money: " + money + ", Stocks: " + stocks + " (" + (lastDailyStock.getClosing() * stocks) + ")");
					}
					
					ProgressView.getInstance().increment();
				}
				return new Pair<List<String>, List<BacktestingResult>>(foundStockList, resultList);
			}
			
			@Override
			public void done() {
				try {
					Pair<List<String>, List<BacktestingResult>> result = get();
					List<String> stockNameList = result.a;
					List<BacktestingResult> resultList = result.b;
					
					if (LOGGER.isDebugEnabled()) {
						LOGGER.debug("stocks found: " + stockNameList);
					}
					
					String title = String.format("Backtesting result (start: %.2f, fee: %.2f)", startMoney, transactionFee);
					
					BacktestingResultDialog resultDialog = new BacktestingResultDialog(title, resultList);
					resultDialog.setCustomizedTitle(customTitle);
					resultDialog.showDialog();
				} catch (InterruptedException | ExecutionException e) {
					LOGGER.error(e.getMessage(), e);
					
					JOptionPane.showMessageDialog(StockView.getInstance(), "Error computing plot: \"" + e.getMessage() + "\"", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		};
		worker.execute();
	}
	
}
