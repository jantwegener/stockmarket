package jtw.stock.view.action.indicator.misc;

import jtw.stock.calculator.IndicatorDaysBeforeBuyPercentCalculator;
import jtw.stock.calculator.StockCalculator;
import jtw.stock.view.action.chart.AbstractChartAction;

public class DaysBeforeBuyPercentIndicatorAction extends AbstractChartAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private double percent;
	
	public DaysBeforeBuyPercentIndicatorAction() {
		
	}

	@Override
	protected StockCalculator getCalculator() {
		return new IndicatorDaysBeforeBuyPercentCalculator(percent);
	}
	
	public double getPercent() {
		return percent;
	}
	
	public void setPercent(double percent) {
		this.percent = percent;
	}
	
}
