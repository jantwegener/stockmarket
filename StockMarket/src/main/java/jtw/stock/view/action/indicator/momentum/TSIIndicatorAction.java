package jtw.stock.view.action.indicator.momentum;

import jtw.stock.calculator.IndicatorTSICalculator;
import jtw.stock.calculator.StockCalculator;
import jtw.stock.view.action.chart.AbstractChartAction;

public class TSIIndicatorAction extends AbstractChartAction {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int r;
	private int s;
	private int signal;
	
	public TSIIndicatorAction() {
	}

	@Override
	protected StockCalculator getCalculator() {
		return new IndicatorTSICalculator(r, s, signal);
	}

	public int getR() {
		return r;
	}

	public void setR(int r) {
		this.r = r;
	}

	public int getS() {
		return s;
	}

	public void setS(int s) {
		this.s = s;
	}

	public int getSignal() {
		return signal;
	}

	public void setSignal(int signal) {
		this.signal = signal;
	}

}
