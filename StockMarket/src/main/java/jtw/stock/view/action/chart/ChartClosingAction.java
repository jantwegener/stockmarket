package jtw.stock.view.action.chart;

import jtw.stock.calculator.StockCalculator;
import jtw.stock.calculator.StockClosingCalculator;

public class ChartClosingAction extends AbstractChartAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ChartClosingAction() {
	}

	@Override
	protected StockCalculator getCalculator() {
		return new StockClosingCalculator();
	}

}
