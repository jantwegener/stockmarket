package jtw.stock.view.action;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;

import jtw.stock.view.StockView;
import jtw.util.BooleanUtil;
import jtw.util.gui.SpringJMenuInitializer;

public abstract class AbstractStockAction extends AbstractAction implements PropertyChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * A static map between the names of the actions and the actions.
	 */
	private static Map<String, AbstractStockAction> nameActionMap = new HashMap<>();

	public AbstractStockAction() {
		nameActionMap.put(this.getClass().getName(), this);
		
		addPropertyChangeListener(this);
	}

	/**
	 * Returns the action corresponding to the name.
	 * 
	 * @param name The name of the action. Usually the class name.
	 * 
	 * @return The corresponding action.
	 */
	public static AbstractStockAction getAction(String name) {
		return nameActionMap.get(name);
	}
	
	/**
	 * Returns the action corresponding to the class.
	 * 
	 * @param clazz The class of the action.
	 * 
	 * @return The corresponding action.
	 */
	@SuppressWarnings("unchecked")
	public static<E extends AbstractStockAction> E getAction(Class<E> clazz) {
		return (E) nameActionMap.get(clazz.getName());
	}
	
	/**
	 * Triggers the action by calling {@link #actionPerformed(ActionEvent)} with <code>null</code> as argument.
	 */
	public void triggerAction() {
		actionPerformed(null);
	}
	
	public void setDefaultEnabledState(Boolean state) {
		putValue(SpringJMenuInitializer.DEFAULT_ENABLED_STATE, state);
	}

	public static void resetToDefaultEnabledState(Action a) {
		Object b = a.getValue(SpringJMenuInitializer.DEFAULT_ENABLED_STATE);
		if (b instanceof Boolean) {
			a.setEnabled((Boolean) b);
		}
	}

	public void resetToDefaultEnabledState() {
		resetToDefaultEnabledState(this);
	}
	
	public static void resetToDefaultSelectedState(Action a) {
		a.putValue(SELECTED_KEY, Boolean.FALSE);
	}
	
	/**
	 * Listens for the property {@link StockView#IS_CHART_LOADED_KEY}. If a chart is loaded, then the action is enabled.
	 */
	public void propertyChange(PropertyChangeEvent evt) {
		if (StockView.IS_CHART_LOADED_KEY.equals(evt.getPropertyName())) {
			setEnabled(BooleanUtil.isTrue(evt.getNewValue()));
		}
	}

}
