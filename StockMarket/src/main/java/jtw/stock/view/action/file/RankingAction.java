package jtw.stock.view.action.file;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

import org.antlr.v4.runtime.misc.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jtw.stock.io.CustomIndexReader;
import jtw.stock.view.ProgressView;
import jtw.stock.view.StockView;
import jtw.stock.view.action.AbstractIndexSearcherStockAction;
import jtw.stock.view.action.file.util.customsearch.CustomSearchCalculatorHandler;
import jtw.stock.view.action.file.util.customsearch.StockValueVisitor;
import jtw.stock.view.component.ListInputDialog;
import jtw.stock.view.component.RankingDialog;
import jtw.stock.view.component.RankingResult;
import jtw.stock.view.component.RankingResultDialog;
import jtw.stock.view.component.SearchPanel.ExpandablePanel;

public class RankingAction extends AbstractIndexSearcherStockAction<RankingDialog> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Log LOGGER = LogFactory.getLog(RankingAction.class);
	
	public RankingAction() {
	}
	
	@Override
	protected String getListInputDialogTitle() {
		return "Ranking...";
	}
	
	@Override
	protected RankingDialog getNewStockDialog() {
		return new RankingDialog(getListInputDialogTitle(), true);
	}
	
	@Override
	protected void handleYesOption(RankingDialog rankingDialog, ListInputDialog dialog) {
		LOGGER.debug("StrategyFinderAction YES option");
		StockView.getInstance().resetMenuBar();
		
		String indexName = dialog.getInput();
		
		String inputText = rankingDialog.getInput();
		List<ExpandablePanel> panelList = rankingDialog.getInputParameterList();
		
		dialog.dispose();
		
		computeRanking(indexName, inputText, panelList, "");
	}
	
	private void computeRanking(String indexName, String inputText, List<ExpandablePanel> panelList, String customTitle) {

		SwingWorker<Pair<List<String>, RankingResult>, Void> worker = new SwingWorker<Pair<List<String>, RankingResult>, Void>() {
			
			@Override
			protected Pair<List<String>, RankingResult> doInBackground() throws Exception {
				CustomIndexReader indexReader = new CustomIndexReader();
				indexReader.read(indexName);
				
				List<Pair<String, Integer>> stockList = indexReader.getIndexContentList();
				
				List<String> stockNameList = new ArrayList<String>();
				
				RankingResult result = new RankingResult();
				
				ProgressView.getInstance().reset(stockList.size());
				
				for (Pair<String, Integer> stockAmountPair : stockList) {
					String stockName = stockAmountPair.a;
					
					CustomSearchCalculatorHandler comparisonHandler = new CustomSearchCalculatorHandler();
					comparisonHandler.setStockName(stockName);
					comparisonHandler.setExpandablePanelList(panelList);
					comparisonHandler.compute();
					
					double value = calcValue(stockName, comparisonHandler);
					result.add(stockName, value);
					
					stockNameList.add(stockName);

					ProgressView.getInstance().increment();
				}
				
				return new Pair<List<String>, RankingResult>(stockNameList, result);
			}
			
			private double calcValue(String stockName, CustomSearchCalculatorHandler comparisonHandler) {
				StockValueVisitor comparisonVisitor = new StockValueVisitor();
				comparisonVisitor.setInputText(inputText);
				comparisonVisitor.setCustomSearchCalculatorHandler(comparisonHandler);
				comparisonVisitor.setExceptionResult(0.0d);
				double valueResult = comparisonVisitor.parse();
				return valueResult;
			}
			
			@Override
			public void done() {
				try {
					Pair<List<String>, RankingResult> result = get();
					List<String> stockNameList = result.a;
					RankingResult rankingResult = result.b;
					
					if (LOGGER.isDebugEnabled()) {
						LOGGER.debug("stocks found: " + stockNameList);
					}
					
					String title = String.format("Ranking Result");
					
					RankingResultDialog resultDialog = new RankingResultDialog(title, rankingResult);
					resultDialog.setCustomizedTitle(customTitle);
					resultDialog.showDialog();
				} catch (InterruptedException | ExecutionException e) {
					LOGGER.error(e.getMessage(), e);
					
					ProgressView.getInstance().incrementToMax();
					
					JOptionPane.showMessageDialog(StockView.getInstance(), "Error computing plot: \"" + e.getMessage() + "\"", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		};
		
		worker.execute();
	}
	
}
