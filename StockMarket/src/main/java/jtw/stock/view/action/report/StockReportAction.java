package jtw.stock.view.action.report;

import java.awt.event.ActionEvent;
import java.util.List;

import jtw.stock.calculator.StockCalculator;
import jtw.stock.calculator.report.StandardReportCalculationFinishedListener;
import jtw.stock.main.StockApplicationSettings;
import jtw.stock.model.registry.StockCalculatorFinishedListenerRegistry;
import jtw.stock.view.action.AbstractStockAction;
import jtw.stock.view.report.ReportDialog;

public class StockReportAction extends AbstractStockAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<StockCalculator> calculatorList = null;
	
	private ReportDialog report;
	
	@Override
	public void actionPerformed(ActionEvent e) {
		report = new ReportDialog(StockApplicationSettings.getStockName(), calculatorList);
		report.pack();
		report.setVisible(true);
		
		for (StockCalculator c : calculatorList) {
			StandardReportCalculationFinishedListener listener = StockCalculatorFinishedListenerRegistry.getInstance().getReportCalculationFinishedListener(c.getName());
			listener.setReportDialog(report);
			
			c.setCalculationFinishedListener(listener);
			c.start();
		}
	}

	public List<StockCalculator> getCalculatorList() {
		return calculatorList;
	}

	public void setCalculatorList(List<StockCalculator> calculatorList) {
		this.calculatorList = calculatorList;
	}
	
}
