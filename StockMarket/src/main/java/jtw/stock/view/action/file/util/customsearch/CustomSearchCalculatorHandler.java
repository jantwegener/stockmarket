package jtw.stock.view.action.file.util.customsearch;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import org.jfree.data.xy.XYDataset;

import jtw.stock.calculator.CalculationFinishedListener;
import jtw.stock.calculator.StockCalculator;
import jtw.stock.view.component.SearchPanel.ExpandablePanel;

/**
 * This class handles the calculators and collects their results.
 * 
 * @author Jan-Thierry Wegener
 */
public class CustomSearchCalculatorHandler implements CalculationFinishedListener {
	
	/**
	 * A map between the variable name and the result.
	 */
	private Map<String, XYDataset> nameResultMap = new HashMap<>();
	
	private String stockName = null;
	private List<ExpandablePanel> expandablePanelList = null;
	
	private CountDownLatch doneSignal;
	
	/**
	 * Where to start the reversal. A value of -1 indicates that the full size of the series shall be used.
	 * A value of x means that only the first x values shall be considered.
	 */
	private int reversedOffset = -1;
	
	public CustomSearchCalculatorHandler() {
	}
	
	public CustomSearchCalculatorHandler(String stockName, List<ExpandablePanel> expandablePanelList) {
		setStockName(stockName);
		setExpandablePanelList(expandablePanelList);
	}
	
	public void setStockName(String stockName) {
		this.stockName = stockName;
	}
	
	public String getStockName() {
		return stockName;
	}
	
	public void setExpandablePanelList(List<ExpandablePanel> expandablePanelList) {
		this.expandablePanelList = expandablePanelList;
		int size = expandablePanelList.size();
		doneSignal = new CountDownLatch(size);
	}
	
	/**
	 * Returns the y-value of the series 0.
	 * 
	 * @param varName The variable name to get the result for.
	 * @param index The index to get the result for.
	 * 
	 * @return The y-value for series 0.
	 */
	public double getResult(String varName, int index) {
		return nameResultMap.get(varName).getYValue(0, index);
	}

	/**
	 * Returns the y-value of the given series.
	 * 
	 * @param varName The variable name to get the result for.
	 * @param index The index to get the result for.
	 * 
	 * @return The y-value for the given series.
	 */
	public double getResult(String varName, int series, int index) {
		return nameResultMap.get(varName).getYValue(series, index);
	}
	
	/**
	 * Returns the y-value of the given series. The index is recalculated by "size - 1 - index".
	 * 
	 * @param varName The variable name to get the result for.
	 * @param index The index to get the result for.
	 * 
	 * @return The y-value for the given series.
	 */
	public double getReversedResult(String varName, int series, int index) {
		XYDataset dataset = nameResultMap.get(varName);
		int offset = getReversedOffset(dataset, series) - 1 - index;
		return dataset.getYValue(series, offset);
	}

	/**
	 * Returns the y-value of the series 0. The index is recalculated by "size - 1 - index".
	 * 
	 * @param varName The variable name to get the result for.
	 * @param index The index to get the result for.
	 * 
	 * @return The y-value for series 0.
	 */
	public double getReversedResult(String varName, int index) {
		return getReversedResult(varName, 0, index);
	}
	
	private int getReversedOffset(XYDataset dataset, int series) {
		int retval = reversedOffset;
		if (retval < 0 || retval > dataset.getItemCount(series)) {
			retval = dataset.getItemCount(series);
		}
		return retval;
	}
	
	/**
	 * Sets the maximal reversed offset of the dataset to be considered.
	 * The reversed offset is adjusted for the internal calculations (i.e., + 1).
	 * 
	 * @param reversedOffset The new reversed offset of the dataset.
	 * 
	 * @see #getReversedResult(String, int)
	 */
	public void setReversedOffset(int reversedOffset) {
		this.reversedOffset = reversedOffset + 1;
	}
	
	/**
	 * Computes all the calculators for the given stock. This method halts until all calculators have finished their calculations.
	 * 
	 * @throws ClassNotFoundException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws InterruptedException
	 * @throws SecurityException 
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 */
	public void compute() throws ClassNotFoundException, InstantiationException, IllegalAccessException, InterruptedException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		for (ExpandablePanel ep : expandablePanelList) {
			String varName = ep.getVarChar();
			String selectedClass = ep.getSelectedClass();
			if (selectedClass != null) {
				String[] parameterArray = ep.getInputValueArray();
				
				Class<? extends StockCalculator> clazz = ClassLoader.getSystemClassLoader().loadClass(selectedClass).asSubclass(StockCalculator.class);
				StockCalculator calculator = clazz.getDeclaredConstructor().newInstance();
				calculator.setStockName(stockName);
				calculator.setParameter(parameterArray);
				calculator.setCalculationFinishedListener(this);
				calculator.setAdditionalInfo(varName);
				calculator.start();
			} else {
				doneSignal.countDown();
			}
		}
		
		doneSignal.await();
	}

	@Override
	public void calculationFinished(StockCalculator thisCalculator) {
		synchronized (nameResultMap) {
			String varName = thisCalculator.getAdditionalInfo();
			XYDataset dataset = thisCalculator.getDataset();
			nameResultMap.put(varName, dataset);
			doneSignal.countDown();
		}
	}
	
}
