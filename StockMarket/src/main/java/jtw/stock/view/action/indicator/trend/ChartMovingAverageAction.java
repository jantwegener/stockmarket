package jtw.stock.view.action.indicator.trend;

import jtw.stock.calculator.StockCalculator;
import jtw.stock.calculator.StockSimpleMovingAverageCalculator;
import jtw.stock.view.action.chart.AbstractChartAction;

public class ChartMovingAverageAction extends AbstractChartAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int days = 0;
	
	public ChartMovingAverageAction() {
	}
	
	public void setDays(int d) {
		days = d;
	}
	
	public int getDays() {
		return days;
	}

	@Override
	protected StockCalculator getCalculator() {
		StockSimpleMovingAverageCalculator calc = new StockSimpleMovingAverageCalculator();
		calc.setDays(days);
		return calc;
	}

}
