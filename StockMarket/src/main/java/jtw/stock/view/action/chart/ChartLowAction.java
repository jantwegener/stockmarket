package jtw.stock.view.action.chart;

import jtw.stock.calculator.StockCalculator;
import jtw.stock.calculator.StockLowCalculator;

public class ChartLowAction extends AbstractChartAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ChartLowAction() {
	}

	@Override
	protected StockCalculator getCalculator() {
		return new StockLowCalculator();
	}

}
