package jtw.stock.view.action.file;

import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jtw.stock.main.StockApplicationSettings;
import jtw.stock.model.registry.Country2CurrencyRegistry;
import jtw.stock.model.registry.StockDataRegistry;
import jtw.stock.model.registry.StockSymbolNameRegistry;
import jtw.stock.view.ProgressView;
import jtw.stock.view.StockView;
import jtw.stock.view.action.AbstractStockAction;
import jtw.stock.view.action.chart.AbstractChartAction;
import jtw.stock.view.action.chart.ChartCandleStickAction;
import jtw.stock.view.component.CSVFileNameFilter;
import jtw.stock.view.component.DialogButtonListener;
import jtw.stock.view.component.ListInputDialog;

public class OpenAction extends AbstractStockAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Log LOGGER = LogFactory.getLog(OpenAction.class);
	
	private List<String> preLoadedList = null;
	
	private IPostOpenAction postAction = null;

	public OpenAction() {
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (preLoadedList == null) {
			loadStockListFromFileBase();
		} else {
			loadStockListFromList();
		}
	}
	
	public void setPostOpenAction(IPostOpenAction postAction) {
		this.postAction = postAction;
	}
	
	public void setPreLoadedList(List<String> preLoadedList) {
		this.preLoadedList = preLoadedList;
	}
	
	private void loadStockListFromList() {
		showDialog(preLoadedList);
		// reset so that the normal behavior is triggered
		preLoadedList = null;
	}
	
	private void loadStockListFromFileBase() {
		List<String> alreadyLoadedList = new ArrayList<>();
		File folder = new File(StockApplicationSettings.getBasePath());
		if (!folder.exists()) {
			String errorMsg = String.format("The path '%s' does not exist.", folder.getAbsolutePath());
			LOGGER.error(errorMsg);
			throw new RuntimeException(errorMsg);
		}
		File[] listFiles = folder.listFiles(new CSVFileNameFilter());
		if (listFiles != null) {
			for (File file : listFiles) {
				alreadyLoadedList.add(file.getName().substring(0, file.getName().length() - ".csv".length()));
			}
		} else {
			LOGGER.warn(String.format("The path '%s' is empty.", folder.getAbsolutePath()));
		}
		
		showDialog(alreadyLoadedList);
	}
	
	private void showDialog(List<String> alreadyLoadedList) {
		ListInputDialog dialog = new ListInputDialog("Open...", false, "", "Locally Available Stocks", "Stock Name", alreadyLoadedList.toArray(new String[] {}));
		dialog.showDialog();
		dialog.addDialogButtonListener(new DialogButtonListener() {
			
			@Override
			public void buttonPressed(int button) {
				if (button == ListInputDialog.YES_OPTION || button == ListInputDialog.APPLY_OPTION) {
					StockView.getInstance().resetMenuBar();
					
					String stockName = dialog.getInput();
					initStock(stockName);
				}
			}
		});
	}
	
	private void initStock(String stockName) {
		SwingWorker<Object, Void> worker = new SwingWorker<Object, Void>() {

			@Override
			protected Object doInBackground() throws Exception {
				ProgressView.getInstance().startIndeterminedMode();
				
				StockView.getInstance().reset();
				StockApplicationSettings.setStockName(stockName);

				StockApplicationSettings.getMainFrame().setTitle(StockSymbolNameRegistry.getInstance().getCompany(stockName) + " (" + stockName + ", " + Country2CurrencyRegistry.getInstance().getCurrencyFromStockName(stockName) + ")");
				StockDataRegistry.getInstance().ensureUpToDate(stockName);

				ProgressView.getInstance().stopIndeterminedMode();
				return null;
			}

			@Override
			public void done() {
				try {
					// call get to receive any exception occurred during the computation
					get();

					AbstractChartAction action = AbstractChartAction.getAction(ChartCandleStickAction.class);
					action.triggerAction();
					if (postAction != null) {
						postAction.performPostAction(stockName);
						postAction = null;
					}
				} catch (InterruptedException | ExecutionException e) {
					LOGGER.error(e.getMessage(), e);
					ProgressView.getInstance().stopIndeterminedMode();

					JOptionPane.showMessageDialog(StockView.getInstance(), "Error computing plot: \"" + e.getMessage() + "\"", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
			
		};
		worker.execute();
	}
	
}
