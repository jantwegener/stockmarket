package jtw.stock.view.report;

import java.awt.Color;
import java.awt.GridLayout;
import java.util.Collection;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import jtw.stock.calculator.StockCalculator;
import jtw.stock.calculator.report.ReportResult;
import jtw.stock.main.StockMain;

public class ReportDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JPanel mainPanel;
	
	private static final Color BLUE = Color.BLUE.brighter().brighter();
	
	public ReportDialog(String stockName, Collection<StockCalculator> calculatorList) {
		super(StockMain.getMainFrame(), "Report for " + stockName, false);
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		init(calculatorList);
	}
	
	private void init(Collection<StockCalculator> calculatorList) {
		mainPanel = new JPanel(new GridLayout(0, 4));
		
		add(mainPanel);
	}
	
	public void setResult(List<ReportResult> resultList) {
		synchronized (this) {
			for (ReportResult r : resultList) {
				setResult(r);
			}
			pack();
		}
	}
	
	/**
	 * <p>
	 * The color scheme should be like
	 * <ul>
	 * <li>GREEN: buy,</li>
	 * <li>BLUE: neutral/hold,</li>
	 * <li>RED: sell/do not buy.</li>
	 * </ul>
	 * </p>
	 */
	private void setResult(ReportResult result) {
		String calculatorName = result.getName();
		String text = result.getText();
		
		Color color = Color.BLACK;
		switch (result.getSuggestion()) {
			case ReportResult.BUY:
				color = Color.GREEN;
				break;
			case ReportResult.HOLD:
				color = BLUE;
				break;
			case ReportResult.SELL:
				color = Color.RED;
				break;
		}

		JLabel label = new JLabel(calculatorName);
		JButton button = new JButton(text);
		button.setBackground(color);
		
		mainPanel.add(label);
		mainPanel.add(button);
	}
}
