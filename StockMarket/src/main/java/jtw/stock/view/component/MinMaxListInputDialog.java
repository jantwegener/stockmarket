package jtw.stock.view.component;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import jtw.util.DoubleUtil;

public class MinMaxListInputDialog extends ListInputDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final String DOUBLE_PATTERN = "[0-9]+(\\.)?[0-9]*";

	private JTextField inputMinTextField;
	
	private JTextField inputMaxTextField;
	
	public MinMaxListInputDialog(JFrame parent, String title, boolean modal, String descriptiveText, String listTitle, String textFieldText, String... listElements) {
		super(parent, title, modal, descriptiveText, listTitle, textFieldText, listElements);
		
		setInputValidator(() -> {
			List<String> retval = new ArrayList<>();
			if (!inputMinTextField.getText().matches(DOUBLE_PATTERN)) {
				retval.add("The min text input field does not contain a valid number.");
			}
			if (!inputMaxTextField.getText().matches(DOUBLE_PATTERN)) {
				retval.add("The max text input field does not contain a valid number.");
			}
			return retval.isEmpty() ? null : retval.toArray(String[]::new);
			});
	}

	public MinMaxListInputDialog(String title, boolean modal, String descriptiveText, String listTitle, String textFieldText, String... listElements) {
		super(title, modal, descriptiveText, listTitle, textFieldText, listElements);
		
		setInputValidator(() -> {
			List<String> retval = new ArrayList<>();
			if (!inputMinTextField.getText().matches(DOUBLE_PATTERN)) {
				retval.add("The min text input field does not contain a valid number.");
			}
			if (!inputMaxTextField.getText().matches(DOUBLE_PATTERN)) {
				retval.add("The max text input field does not contain a valid number.");
			}
			return retval.isEmpty() ? null : retval.toArray(String[]::new);
			});
	}
	
	protected JPanel[] initPanel(String descriptiveText, String listTitle, String textFieldText, String ... listElements) {
		JPanel[] allPanel = super.initPanel(descriptiveText, listTitle, textFieldText, listElements);
		
		inputMinTextField = new JTextField("min", 10);
		inputMaxTextField = new JTextField("max", 10);
		
		addFocusListener(inputMinTextField);
		addFocusListener(inputMaxTextField);
		
		allPanel[3].add(inputMinTextField);
		allPanel[3].add(inputMaxTextField);
		
		return allPanel;
	}
	
	public double getMinInput() {
		return DoubleUtil.toDouble(inputMinTextField.getText());
	}
	
	public double getMaxInput() {
		return DoubleUtil.toDouble(inputMaxTextField.getText());
	}
	
}
