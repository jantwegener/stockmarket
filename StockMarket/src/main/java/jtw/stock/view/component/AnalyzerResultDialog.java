package jtw.stock.view.component;

import java.awt.BasicStroke;
import java.awt.Color;
import java.util.List;

import javax.swing.JFrame;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;

import jtw.stock.main.StockApplicationSettings;
import jtw.stock.view.StockView;
import jtw.stock.view.action.file.IPostOpenAction;

public class AnalyzerResultDialog extends TableStockDialog {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Log LOGGER = LogFactory.getLog(AnalyzerResultDialog.class);
	
	private List<AnalyzerResult> resultList;
	
	public AnalyzerResultDialog(JFrame parent, String title, List<AnalyzerResult> resultList) {
		super(parent, title, false, 10, 5, resultList);
	}
	
	public AnalyzerResultDialog(String title, List<AnalyzerResult> resultList) {
		this(StockApplicationSettings.getMainFrame(), title, resultList);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	protected void init(Object[] additionalValues) {
		this.resultList = (List<AnalyzerResult>) additionalValues[0];
	}
	
	@Override
	protected String[] getColumnNames() {
		return new String[] {"Stock Name", "Num of Marker", "Percent", "Total Analysis"};
	}
	
	@Override
	protected Object[][] getTableData() {
		Object[][] data = new Object[resultList.size()][];
		
		for (int i = 0; i < resultList.size(); i++) {
			AnalyzerResult result = resultList.get(i);
			String stockName = result.getStockName();
			Integer markerNum = result.getMarkerSize();
			Integer totalNum = result.getCounter();
			Double markerPercentNum = (markerNum * 100d / totalNum);
			
			String markerNumStr = String.format("%d", markerNum);
			String percentStr = String.format("%.2f %%", markerPercentNum);
			
			data[i] = new Object[] {stockName, markerNumStr, percentStr, totalNum};
		}
		return data;
	}
	
	@Override
	protected IPostOpenAction getPostOpenAction(String stockName) {
		IPostOpenAction postAction = new IPostOpenAction() {
			
			@Override
			public void performPostAction(String stockName) {
				XYPlot mainPlot = StockView.getInstance().getMainPlot();
				AnalyzerResult result = null;
				for (AnalyzerResult br : resultList) {
					if (br.getStockName().equals(stockName)) {
						result = br;
						break;
					}
				}

				for (DateMark bsm : result.getNegativeMarkerList()) {
					ValueMarker marker = new ValueMarker(bsm.getDate().getTimeInMillis(), Color.ORANGE, new BasicStroke(1));
					mainPlot.addDomainMarker(marker);
				}
				
				// positive results
				for (DateMark bsm : result.getMarkerList()) {
					ValueMarker marker = new ValueMarker(bsm.getDate().getTimeInMillis(), Color.GREEN, new BasicStroke(1));
					mainPlot.addDomainMarker(marker);
				}
			}
		};
		return postAction;
	}
	
}
