package jtw.stock.view.component;

import static jtw.stock.view.component.ComponentConstant.SEARCH_HISTORY_FILE_NAME;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jtw.stock.main.StockApplicationSettings;
import jtw.stock.model.CustomSearchFormula;
import jtw.stock.view.component.SearchPanel.ExpandablePanel;
import jtw.util.DateUtil;

public class AnalyzerDialog extends StockDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static Log LOGGER = LogFactory.getLog(AnalyzerDialog.class);
	
	private SearchPanel searchPanel;
	
	private SearchPanel domainPanel;
	
	/**
	 * The start date of the simulation.
	 */
	private JTextField dateStartTextField;
	/**
	 * The end date of the simulation.
	 */
	private JTextField dateEndTextField;

	
	public AnalyzerDialog(JFrame parent, String title, boolean modal) {
		super(parent, title, modal);
		
		initPanel();
	}
	
	public AnalyzerDialog(String title, boolean modal) {
		this(StockApplicationSettings.getMainFrame(), title, modal);
	}

	public void initFormulas(CustomSearchFormula searchFormula, CustomSearchFormula domainFormula) {
		searchPanel.setCustomSearchFormula(searchFormula);
		domainPanel.setCustomSearchFormula(domainFormula);
	}
	
	public CustomSearchFormula[] getFormulas() {
		CustomSearchFormula[] formulaArray = new CustomSearchFormula[2];
		
		formulaArray[0] = searchPanel.getSelectedFormula();
		formulaArray[1] = domainPanel.getSelectedFormula();
		
		return formulaArray;
	}
	
	public boolean isNewFormula() {
		boolean retVal = searchPanel.isNewFormula();
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("isNewFormula() - returning: " + retVal);
		}
		
		return retVal;
	}
	
	@Override
	protected JPanel initCenter() {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("initCenter()");
		}
		
		searchPanel = new SearchPanel(SEARCH_HISTORY_FILE_NAME);
		searchPanel.setBorder(BorderFactory.createTitledBorder("Search"));
		
		domainPanel = new SearchPanel(SEARCH_HISTORY_FILE_NAME);
		domainPanel.setBorder(BorderFactory.createTitledBorder("Domain"));

		dateStartTextField = new JTextField("", 10);
		JLabel dateStartLabel = new JLabel("Start date (dd.mm.yyyy)");
		dateStartLabel.setLabelFor(dateStartTextField);
		dateEndTextField = new JTextField("", 10);
		JLabel dateEndLabel = new JLabel("End date (dd.mm.yyyy)");
		dateEndLabel.setLabelFor(dateEndTextField);

		JPanel datePanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
		datePanel.add(dateStartLabel);
		datePanel.add(dateStartTextField);
		datePanel.add(dateEndLabel);
		datePanel.add(dateEndTextField);

		JPanel southPanel = new JPanel();
		southPanel.setLayout(new BoxLayout(southPanel, BoxLayout.PAGE_AXIS));
		southPanel.setBorder(BorderFactory.createTitledBorder("Settings"));
		southPanel.add(datePanel);
		
		JPanel centerPanel = new JPanel(new GridLayout());
		centerPanel.add(searchPanel);
		centerPanel.add(domainPanel);
		
		JPanel mainPanel = new JPanel(new BorderLayout());
		mainPanel.add(centerPanel, BorderLayout.CENTER);
		mainPanel.add(datePanel, BorderLayout.SOUTH);
		
		return mainPanel;
	}
	
	public String getSearchInput() {
		String retVal = searchPanel.getInput();
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("getSearchInput() - returning: " + retVal);
		}
		
		return retVal;
	}
	
	public List<ExpandablePanel> getSearchInputParameterList() {
		return searchPanel.getInputParameterList();
	}
	
	public Calendar getStartDateInput() {
		// default: far back in the past
		Calendar retVal = DateUtil.toCalendar(dateStartTextField.getText(), "dd.MM.yyyy", "01.01.1800");
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("getStartDateInput() - returning: " + retVal);
		}
		
		return retVal;
	}
	
	public void setStartDateInput(Calendar startDate) {
		dateStartTextField.setText(DateUtil.toString(startDate));
	}
	
	public String getDomainInput() {
		String retVal = domainPanel.getInput();
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("getDomainInput() - returning: " + retVal);
		}
		
		return retVal;
	}
	
	public List<ExpandablePanel> getDomainInputParameterList() {
		return domainPanel.getInputParameterList();
	}
	
	public Calendar getEndDateInput() {
		// default: far into the future
		Calendar retVal = DateUtil.toCalendar(dateEndTextField.getText(), "dd.MM.yyyy", "01.01.2525");
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("getEndDateInput() - returning: " + retVal);
		}
		
		return retVal;
	}

	public void setEndDateInput(Calendar endDate) {
		dateEndTextField.setText(DateUtil.toString(endDate));
	}

	@Override
	protected URL getPathToHelp() {
		try {
			File f = new File("doc/HowtoStockLang.html");
			return f.toURL();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	protected String getHelpMenuItemName() {
		return "How-To Stock Lang";
	}
	
}
