package jtw.stock.view.component;

import javax.swing.JFrame;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jtw.stock.main.StockApplicationSettings;
import jtw.stock.model.registry.StockSymbolNameRegistry;

public class RankingResultDialog extends TableStockDialog {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Log LOGGER = LogFactory.getLog(StrategyFinderResultDialog.class);
	
	private RankingResult result;
	
	public RankingResultDialog(JFrame parent, String title, RankingResult result) {
		super(parent, title, false, 10, 5, result);
	}
	
	public RankingResultDialog(String title, RankingResult result) {
		this(StockApplicationSettings.getMainFrame(), title, result);
	}
	
	@Override
	protected void init(Object[] additionalValues) {
		this.result = (RankingResult) additionalValues[0];
	}
	
	@Override
	protected String[] getColumnNames() {
		return new String[] { "Rank", "Symbol", "Stock", "Value" };
	}
	
	@Override
	protected Object[][] getTableData() {
		String[][] data = new String[result.size()][];
		int rank = 0;
		for (RankingResultRecord record : result.getRankingSet()) {
			data[rank] = new String[]{ String.valueOf(rank + 1), record.getStockName(), StockSymbolNameRegistry.getInstance().getCompany(record.getStockName()), String.valueOf(record.getValue()) };
			rank++;
		}
		return data;
	}
	
	@Override
	protected int getStockSymbolColumn() {
		return 1;
	}
	
}
