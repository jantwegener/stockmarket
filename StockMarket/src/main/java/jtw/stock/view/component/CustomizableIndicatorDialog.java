package jtw.stock.view.component;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jtw.stock.calculator.StockCalculator;
import jtw.stock.main.StockApplicationSettings;

public class CustomizableIndicatorDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Log LOGGER = LogFactory.getLog(CustomizableSearchDialog.class);

	public static final int NO_OPTION = JOptionPane.NO_OPTION;
	
	public static final int YES_OPTION = JOptionPane.YES_OPTION;
	
	private JButton buttonOk = new JButton("OK");
	private JButton buttonCancel = new JButton("Cancel");
	
	private int buttonPressed = NO_OPTION;
	
	/**
	 * Contains the input of the user.
	 */
	private JTextField[] textFieldArray;
	
	private Class<? extends StockCalculator> indicatorClazz;
	
	private StockCalculator calculator;
	
	/**
	 * Stores the selected time base.
	 */
	private int timeBase = -1;
	
	public CustomizableIndicatorDialog(JFrame parent, String title, boolean modal, Class<? extends StockCalculator> indicatorClazz) {
		super(parent, title, modal);
		
		this.indicatorClazz = indicatorClazz;
	}
	
	public CustomizableIndicatorDialog(JFrame parent, String title, boolean modal) {
		this(parent, title, modal, null);
	}
	
	public CustomizableIndicatorDialog(String title, boolean modal) {
		this(StockApplicationSettings.getMainFrame(), title, modal);
	}
	
	public void showDialog() {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		pack();
		setLocationRelativeTo(getParent());
		setVisible(true);
	}
	
	/**
	 * Before calling this method, the method {@link #setIndicatorClazz(Class)} must have been called.
	 */
	public void init() {
		JPanel main = new JPanel(new BorderLayout());
		JPanel center = initCenter();
		JPanel south = initSouth();
		
		main.add(center, BorderLayout.CENTER);
		main.add(south, BorderLayout.SOUTH);
		add(main);

		validate();
		pack();
	}
	
	private JPanel initCenter() {
		JPanel panelTextfield = new JPanel();
		panelTextfield.setBorder(BorderFactory.createTitledBorder("Parameters"));
		
		try {
			calculator = indicatorClazz.newInstance();
			String[] nameArray = calculator.getParameterNameArray();
			String[] defaultValueArray = calculator.getParameterDefaultValueArray();
			
			textFieldArray = new JTextField[nameArray.length];
			
			JPanel panel = new JPanel(new GridLayout(nameArray.length, 2, 5, 2));
			for (int i = 0; i < nameArray.length; i++) {
				JTextField tf = new JTextField(defaultValueArray[i]);
				textFieldArray[i] = tf;
				
				panel.add(new JLabel(nameArray[i]));
				panel.add(tf);
			}
			panelTextfield.add(panel, BorderLayout.CENTER);
		} catch (InstantiationException | IllegalAccessException e) {
			LOGGER.fatal("Instatiation of clazz " + indicatorClazz.getName() + " failed.", e);
		}
		
		JRadioButton bDay = new JRadioButton("1d");
		JRadioButton bWeek = new JRadioButton("1w");
		JRadioButton bMonth = new JRadioButton("1m");

		ButtonGroup group = new ButtonGroup();
		group.add(bDay);
		group.add(bWeek);
		group.add(bMonth);

		switch (StockApplicationSettings.getTimeBase()) {
			case StockApplicationSettings.TIME_BASE_DAILY:
				bDay.setSelected(true);
				break;
			case StockApplicationSettings.TIME_BASE_WEEKLY:
				bWeek.setSelected(true);
				break;
			case StockApplicationSettings.TIME_BASE_MONTHLY:
				bMonth.setSelected(true);
				break;
		}

		ItemListener timeBaseListener = new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent ie) {
				if (ie.getStateChange() == ItemEvent.SELECTED) {
					if (ie.getSource().equals(bDay)) {
						timeBase = StockApplicationSettings.TIME_BASE_DAILY;
					} else if (ie.getSource().equals(bWeek)) {
						timeBase = StockApplicationSettings.TIME_BASE_WEEKLY;
					} else if (ie.getSource().equals(bMonth)) {
						timeBase = StockApplicationSettings.TIME_BASE_MONTHLY;
					}
				}
			}
		};
		
		bDay.addItemListener(timeBaseListener);
		bWeek.addItemListener(timeBaseListener);
		bMonth.addItemListener(timeBaseListener);
		
		JPanel panelBaseTime = new JPanel(new FlowLayout(FlowLayout.CENTER));
		panelBaseTime.setBorder(BorderFactory.createTitledBorder("Time Base"));
		panelBaseTime.add(bDay);
		panelBaseTime.add(bWeek);
		panelBaseTime.add(bMonth);
		
		JPanel panelMain = new JPanel(new GridLayout(2, 1));
		panelMain.add(panelTextfield);;
		panelMain.add(panelBaseTime);
		
		return panelMain;
	}

	private JPanel initSouth() {
		buttonCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				buttonPressed = NO_OPTION;
				setVisible(false);
				dispose();
			}
		});
		
		ActionListener okAction = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				buttonPressed = YES_OPTION;
				setVisible(false);
				dispose();
			}
		};
		
		buttonOk.addActionListener(okAction);
		
		JPanel south = new JPanel(new FlowLayout(FlowLayout.CENTER));
		south.add(buttonOk);
		south.add(buttonCancel);

		return south;
	}
	
	/**
	 * Returns the calculator with the correct settings.
	 * 
	 * @return The custom calculator.
	 */
	public StockCalculator getCalculator() {
		// init the calculator
		calculator.setParameter(getInputValueArray());
		calculator.setTimeBase(timeBase);
		return calculator;
	}

	public int getButtonPressed() {
		return buttonPressed;
	}
	
	public void setIndicatorClazzFromString(String indicatorClazzString) throws ClassNotFoundException {
		this.indicatorClazz = ClassLoader.getSystemClassLoader().loadClass(indicatorClazzString).asSubclass(StockCalculator.class);
	}

	public void setIndicatorClazz(Class<? extends StockCalculator> indicatorClazz) {
		this.indicatorClazz = indicatorClazz;
	}

	public Class<? extends StockCalculator> getIndicatorClazz() {
		return indicatorClazz;
	}
	
	private String[] getInputValueArray() {
		int len = 0;
		if (textFieldArray != null) {
			len = textFieldArray.length;
		}
		String[] retArray = new String[len];
		for (int i = 0; i < len; i++) {
			retArray[i] = textFieldArray[i].getText();
		}
		
		return retArray;
	}
	
}
