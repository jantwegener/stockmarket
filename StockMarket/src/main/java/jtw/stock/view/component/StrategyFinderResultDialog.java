package jtw.stock.view.component;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.swing.JFrame;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jtw.stock.main.StockApplicationSettings;
import jtw.stock.view.component.StrategyFinderResult.BuySellMark;
import jtw.stock.view.component.StrategyFinderResult.BuySellMoneyDevelopment;
import jtw.util.BooleanUtil;

public class StrategyFinderResultDialog extends TableStockDialog {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static String DATE_FORMAT = "dd.MM.yyyy";
	
	private Log LOGGER = LogFactory.getLog(StrategyFinderResultDialog.class);
	
	private StrategyFinderResult result;
	
	public StrategyFinderResultDialog(JFrame parent, String title, StrategyFinderResult result) {
		super(parent, title, false, 10, 5, result);
	}
	
	public StrategyFinderResultDialog(String title, StrategyFinderResult result) {
		this(StockApplicationSettings.getMainFrame(), title, result);
	}
	
	@Override
	protected void init(Object[] additionalValues) {
		this.result = (StrategyFinderResult) additionalValues[0];
	}
	
	@Override
	protected String[] getColumnNames() {
		return new String[] { "Stock", "Buy / Sell", "Date", "Amount", "Price", "Money", "Profit / Loss" };
	}
	
	@Override
	protected Object[][] getTableData() {
		List<BuySellMark> resultList = result.getBuySellList();
		List<BuySellMoneyDevelopment> developmentList = result.getDevelopmentList();
		String[][] data = new String[resultList.size()][];
		
		// start money
		double prevMoney = result.getStartMoney();
		
		SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
		for (int i = 0; i < resultList.size(); i++) {
			BuySellMark mark = resultList.get(i);
			String stockName = mark.getStockName();
			String buyMark = BooleanUtil.toString(mark.isBuyMark(), "Buy", "Sell");
			String date = format.format(mark.getDate().getTime());
			String amount = String.valueOf(mark.getStockNum());
			String price = String.format("%.2f", mark.getPrice());
			String money = mark.isBuyMark() ? "-" : String.format("%.2f", developmentList.get(i/2).getMoney());
			String profitLoss = "";
			if (!mark.isBuyMark()) {
				double currMoney = developmentList.get(i/2).getMoney();
				profitLoss = String.format("%.2f", currMoney - prevMoney);
				prevMoney = currMoney;
			}
			
			data[i] = new String[] { stockName, buyMark, date, amount, price, money, profitLoss };
		}
		
		return data;
	}
	
}
