package jtw.stock.view.component;

public class RankingResultRecord implements Comparable<RankingResultRecord> {
	
	private final String stockName;
	
	private final double value;
	
	public RankingResultRecord(String stockName, double value) {
		this.value = value;
		this.stockName = stockName;
	}
	
	public String getStockName() {
		return stockName;
	}
	
	public double getValue() {
		return value;
	}
	
	@Override
	public int compareTo(RankingResultRecord o) {
		return -Double.compare(value, o.value);
	}

	@Override
	public String toString() {
		return "RankingResultRecord [stockName=" + stockName + ", value=" + value + "]";
	}
	
}
