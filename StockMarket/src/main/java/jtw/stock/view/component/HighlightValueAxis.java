package jtw.stock.view.component;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Shape;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

import org.jfree.chart.axis.AxisState;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTick;
import org.jfree.chart.axis.Tick;
import org.jfree.chart.axis.TickType;
import org.jfree.chart.axis.ValueTick;
import org.jfree.text.TextUtilities;
import org.jfree.ui.RectangleEdge;
import org.jfree.ui.TextAnchor;

public class HighlightValueAxis extends NumberAxis {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final List<ValueTick> highlightTickList = new ArrayList<>();
	
	private String formatting = "%.2f";

	public HighlightValueAxis() {
		super();
	}

	public HighlightValueAxis(String label) {
		super(label);
	}
	
	public void addHighlightTick(double value) {
		NumberTick tick = new NumberTick(TickType.MAJOR, value, "", TextAnchor.CENTER_RIGHT, TextAnchor.CENTER_RIGHT, 0.0);
		highlightTickList.add(tick);
	}
	
	public void resetHighlightTicks() {
		highlightTickList.clear();
	}
	
	@Override
    public List<Tick> refreshTicks(Graphics2D g2, AxisState state, Rectangle2D dataArea, RectangleEdge edge) {
    	@SuppressWarnings("unchecked")
		List<Tick> result = super.refreshTicks(g2, state, dataArea, edge);
    	result.addAll(highlightTickList);
    	return result;
    }

	@Override
	protected AxisState drawTickMarksAndLabels(Graphics2D g2, double cursor, Rectangle2D plotArea, Rectangle2D dataArea, RectangleEdge edge) {
		AxisState state = super.drawTickMarksAndLabels(g2, cursor, plotArea, dataArea, edge);
		for (ValueTick tick : highlightTickList) {
			float[] anchorPoint = calculateAnchorPoint(tick, cursor, dataArea, edge);
			g2.setColor(Color.BLACK);
			
			String tickText = String.format(formatting, tick.getValue());
			
			// an arrow like shape
			Shape shape = getArrow(g2, anchorPoint, tickText);
			g2.fill(shape);
			
			g2.setColor(Color.YELLOW);
			TextUtilities.drawRotatedString(tickText, g2,
                    anchorPoint[0] - 10f, anchorPoint[1], 
                    tick.getTextAnchor(), tick.getAngle(), 
                    tick.getRotationAnchor());
		}
		return state;
	}
	
	private Shape getArrow(Graphics2D g2, float[] anchorPoint, String text) {
		int w = g2.getFontMetrics().stringWidth(text);
		int h = g2.getFontMetrics().getHeight();
		
		Polygon shape = new Polygon();
		shape.addPoint((int) (anchorPoint[0] - 1.5f * w), (int) (anchorPoint[1] - h / 2f));
		shape.addPoint((int) (anchorPoint[0] - 1.5f * w) + w, (int) (anchorPoint[1] - h / 2f));
		shape.addPoint((int) (anchorPoint[0]), (int) anchorPoint[1]);
		shape.addPoint((int) (anchorPoint[0] - 1.5f * w) + w, (int) (anchorPoint[1] + h / 2f));
		shape.addPoint((int) (anchorPoint[0] - 1.5f * w), (int) (anchorPoint[1] + h / 2f));
		
		return shape;
	}

}
