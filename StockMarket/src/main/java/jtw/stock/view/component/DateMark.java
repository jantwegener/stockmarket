package jtw.stock.view.component;

import java.util.Calendar;

public class DateMark {

	private final Calendar date;
	
	public DateMark(Calendar calendar) {
		this.date = calendar;
	}
	
	public Calendar getDate() {
		return date;
	}
	
}
