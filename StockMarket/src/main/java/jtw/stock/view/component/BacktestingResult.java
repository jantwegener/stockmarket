package jtw.stock.view.component;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import jtw.stock.model.Stock;

public class BacktestingResult {
	
	private final String stockName;
	
	private double totalMoneyUsed;
	
	private double money;
	
	private int stockNum;
	
	private Stock stock;
	
	private List<BuySellMark> buyList = new ArrayList<>();

	private List<BuySellMark> sellList = new ArrayList<>();
	
	private List<BuySellMoneyDevelopment> developmentList = new ArrayList<>();
	
	public BacktestingResult(String stockName) {
		this.stockName = stockName;
	}
	
	public String getStockName() {
		return stockName;
	}
	
	public double getTotalMoneyUsed() {
		return totalMoneyUsed;
	}
	
	public void addTotalMoneyUsed(double money, Calendar date) {
		totalMoneyUsed += money;
		
		BuySellMoneyDevelopment devel = new BuySellMoneyDevelopment(money, date);
		developmentList.add(devel);
	}
	
	public double getMoney() {
		return money;
	}
	
	public void setMoney(double money) {
		this.money = money;
	}
	
	public int getStockNum() {
		return stockNum;
	}
	
	public void setStockNum(int stockNum) {
		this.stockNum = stockNum;
	}
	
	public Stock getStock() {
		return stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}
	
	public List<BuySellMark> getBuyList() {
		return buyList;
	}

	public List<BuySellMark> getSellList() {
		return sellList;
	}
	
	public List<BuySellMoneyDevelopment> getMoneyDevelopmentList() {
		return developmentList;
	}
	
	public void addBuy(double price, double fee, int stockNum, Stock s) {
		BuySellMark mark = new BuySellMark(true, price, fee, stockNum, s.getDate());
		buyList.add(mark);
	}
	
	public void addSell(double price, double fee, int stockNum, Stock s, double money) {
		BuySellMark mark = new BuySellMark(false, price, fee, stockNum, s.getDate());
		sellList.add(mark);
		BuySellMoneyDevelopment devel = new BuySellMoneyDevelopment(money, s.getDate());
		developmentList.add(devel);
	}
	
	public class BuySellMark {
		
		private final boolean isBuyMark;
		
		private final double price;
		
		private final double fee;
		
		private final Calendar date;
		
		private final int stockNum;
		
		public BuySellMark(boolean isBuyMark, double price, double fee, int stockNum, Calendar calendar) {
			this.isBuyMark = isBuyMark;
			this.price = price;
			this.fee = fee;
			this.date = calendar;
			this.stockNum = stockNum;
		}
		
		public boolean isBuyMark() {
			return isBuyMark;
		}
		
		public double getPrice() {
			return price;
		}
		
		public double getFee() {
			return fee;
		}
		
		public Calendar getDate() {
			return date;
		}

		public int getStockNum() {
			return stockNum;
		}
		
	}
	
	public class BuySellMoneyDevelopment {
		
		private final double money;
		
		private final Calendar date;

		public BuySellMoneyDevelopment(double money, Calendar date) {
			this.money = money;
			this.date = date;
		}
		
		public double getMoney() {
			return money;
		}
		
		public Calendar getDate() {
			return date;
		}
		
	}
	
}
