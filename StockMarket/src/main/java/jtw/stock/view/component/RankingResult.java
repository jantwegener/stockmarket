package jtw.stock.view.component;

import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Holder for the ranking results.
 * 
 * @author Jan-Thierry Wegener
 */
public class RankingResult {
	
	private SortedSet<RankingResultRecord> rankingSet = new TreeSet<RankingResultRecord>();
	
	public RankingResult() {
	}
	
	public void add(String stockName, double value) {
		RankingResultRecord record = new RankingResultRecord(stockName, value);
		rankingSet.add(record);
	}
	
	public SortedSet<RankingResultRecord> getRankingSet() {
		return rankingSet;
	}
	
	public int size() {
		return rankingSet.size();
	}
	
}
