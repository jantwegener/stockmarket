package jtw.stock.view.component;

import java.io.File;
import java.io.FilenameFilter;

public class CSVFileNameFilter implements FilenameFilter {

	@Override
	public boolean accept(File dir, String name) {
		return name.endsWith(".csv");
	}

}
