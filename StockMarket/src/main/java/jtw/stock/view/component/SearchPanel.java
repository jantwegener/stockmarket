package jtw.stock.view.component;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import io.github.classgraph.ClassGraph;
import io.github.classgraph.ClassInfoList;
import io.github.classgraph.ScanResult;
import jtw.stock.calculator.StockCalculator;
import jtw.stock.io.CustomSearchReader;
import jtw.stock.model.CustomSearchFormula;
import jtw.stock.view.StockView;

public class SearchPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Log LOGGER = LogFactory.getLog(CustomizableSearchDialog.class);

	private boolean isNewFormula = false;
	
	private List<ExpandablePanel> expandablePanelList = new ArrayList<>();
	
	private JTextArea formulaField = new JTextArea(8, 50);
	
	private JList<CustomSearchFormula> formulaListComponent;
	
	private String searchFileName;
	
	public SearchPanel(String searchFileName) {
		super(new BorderLayout());
		
		this.searchFileName = searchFileName;
		
		init();
	}

	protected void init() {
		JPanel center = initCenter();
		
		JPanel east = initEast();

		if (center != null) {
			add(center, BorderLayout.CENTER);
		}
		if (east != null) {
			add(east, BorderLayout.EAST);
		}
	}
	
	public boolean isNewFormula() {
		return isNewFormula;
	}

	/**
	 * Returns the current list of all formulas including a new one (if new is selected).
	 * If the currently selected formula has been updated, the updated formula is returned.
	 * 
	 * @return The list of formulas.
	 */
	public List<CustomSearchFormula> getFormulaList() {
		List<CustomSearchFormula> formulaList = new ArrayList<>();
		
		ListModel<CustomSearchFormula> listModel = formulaListComponent.getModel();
		ListSelectionModel selectionModel = formulaListComponent.getSelectionModel();
		
		int indexAdjusting = -1;
		
		if (isNewFormula()) {
			CustomSearchFormula formula = getSelectedFormula();
			formulaList.add(formula);
			
			indexAdjusting = 0;
		}
		
		// the new formula is always at position 0
		for (int i = 1; i < listModel.getSize(); i++) {
			CustomSearchFormula formula = listModel.getElementAt(i);
			formulaList.add(formula);
		}
		
		if (!isNewFormula()) {
			int selectedIndex = selectionModel.getLeadSelectionIndex() + indexAdjusting;
			CustomSearchFormula formula = getSelectedFormula();
			formulaList.set(selectedIndex, formula);
		}
		
		return formulaList;
	}

	private void reset() {
		// removes the text
		formulaField.setText("");
		
		expandablePanelList.clear();
		
		// removes the added panels
		Container centerComp = null;
		for (Component c : getComponents()) {
			if (c instanceof Container && "CENTER".equals(c.getName())) {
				centerComp = (Container) c;
			}
		}
		
		remove(centerComp);
		
		JPanel center = initCenter();
		add(center, BorderLayout.CENTER);
	}

	public CustomSearchFormula getSelectedFormula() {
		ListModel<CustomSearchFormula> listModel = formulaListComponent.getModel();
		ListSelectionModel selectionModel = formulaListComponent.getSelectionModel();
		
		CustomSearchFormula formula = new CustomSearchFormula();
		String formulaText = getInput();
		
		if (selectionModel.getMinSelectionIndex() >= 0) {
			int selectionIndex = selectionModel.getLeadSelectionIndex();
			String name = listModel.getElementAt(selectionIndex).getName();
			formula.setName(name);
		}
		
		formula.init(expandablePanelList.size());
		formula.setText(formulaText);
		
		int index = 0;
		for (ExpandablePanel ep : expandablePanelList) {
			String varName = ep.getVarChar();
			String selectedClazz = ep.getSelectedClass();
			if (selectedClazz != null) {
				String[] parameterArray = ep.getInputValueArray();
				formula.setParameterName(index, varName);
				formula.setClazz(index, selectedClazz);
				formula.setParameterArray(index, parameterArray);
				index++;
			}
		}
		formula.trim(index);
		
		return formula;
	}
	
	public List<ExpandablePanel> getInputParameterList() {
		return expandablePanelList;
	}

	public String getInput() {
		return formulaField.getText();
	}

	protected JPanel initCenter() {
		JPanel mainCenter = new JPanel(new BorderLayout());
		mainCenter.setName("CENTER");
		
		JScrollPane pane = new JScrollPane(formulaField);
		pane.setMinimumSize(new Dimension(400, 200));
		
		mainCenter.add(pane, BorderLayout.CENTER);
		
		mainCenter.add(getNewExpandablePanel('A'), BorderLayout.SOUTH);
		
		return mainCenter;
	}
	
	protected JPanel initEast() {
		JPanel main = new JPanel(new BorderLayout());
		main.setName("EAST");
		
		final DefaultListModel<CustomSearchFormula> model = new DefaultListModel<>();
		// add a "new" formula
		CustomSearchFormula newFormula = new CustomSearchFormula();
		newFormula.setName("-New-");
		model.addElement(newFormula);
		
		CustomSearchReader reader = new CustomSearchReader(searchFileName);
		final List<CustomSearchFormula> formulaList = new ArrayList<>();
		try {
			reader.read();
			
			formulaList.addAll(reader.getFormulaList());
			for (CustomSearchFormula formula : formulaList) {
				model.addElement(formula);
			}
		} catch (FileNotFoundException fnfe) {
			LOGGER.error(fnfe.getMessage(), fnfe);
		} catch (IOException ioe) {
			LOGGER.error(ioe.getMessage(), ioe);

			JOptionPane.showMessageDialog(StockView.getInstance(), "Error reading history file: \"" + ioe.getMessage() + "\"", "Error", JOptionPane.ERROR_MESSAGE);
		}

		formulaListComponent = new JList<>(model);
		formulaListComponent.setLayoutOrientation(JList.VERTICAL);
		formulaListComponent.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		formulaListComponent.setVisibleRowCount(12);
		
		formulaListComponent.setCellRenderer(new DefaultListCellRenderer() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public Component getListCellRendererComponent(JList<?> list,
					Object value, int index, boolean isSelected, boolean cellHasFocus) {
				Component retVal = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
				if (value instanceof CustomSearchFormula) {
					String name = ((CustomSearchFormula) value).getName();
					setText(name);
				}
				return retVal;
			}
		});
		
		JPanel controlPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));

		final JButton upButton = new JButton("^");
		final JButton downButton = new JButton("v");
		final JButton removeButton = new JButton("Remove");
		
		upButton.setEnabled(false);
		downButton.setEnabled(false);
		removeButton.setEnabled(false);
		
		final ListSelectionModel selectionModel = formulaListComponent.getSelectionModel();
		
		upButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				int selectionIndex = selectionModel.getMaxSelectionIndex();
				int targetIndex = selectionIndex - 1;
				
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("moving the index " + selectionIndex + " to " + targetIndex);
				}
				
				if (targetIndex > 0) {
					CustomSearchFormula formula = model.getElementAt(selectionIndex);
					model.removeElementAt(selectionIndex);
					model.insertElementAt(formula, targetIndex);
				}
			}
		});

		downButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				int selectionIndex = selectionModel.getMaxSelectionIndex();
				int targetIndex = selectionIndex + 1;
				
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("moving the index " + selectionIndex + " to " + targetIndex);
				}
				
				// target > 0 ensures that something is indeed selected
				if (targetIndex > 0 && targetIndex < model.getSize()) {
					CustomSearchFormula formula = model.getElementAt(selectionIndex);
					model.removeElementAt(selectionIndex);
					model.insertElementAt(formula, targetIndex);
				}
			}
		});

		removeButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				int selectionIndex = selectionModel.getMaxSelectionIndex();
				
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("removing the index " + selectionIndex);
				}
				
				if (selectionIndex >= 0) {
					model.removeElementAt(selectionIndex);
				}
			}
		});

		formulaListComponent.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (!e.getValueIsAdjusting()) {
					ListSelectionModel lsm = (ListSelectionModel)e.getSource();
					if (!lsm.isSelectionEmpty()) {
						reset();
						
						int index = lsm.getLeadSelectionIndex();
						isNewFormula = index <= 0;
						if (!isNewFormula) {
							CustomSearchFormula formula = model.getElementAt(index);
							
							setCustomSearchFormula(formula);
						}
						revalidate();
					}
					
					// enable / disable the buttons
					if (lsm.isSelectionEmpty()) {
						upButton.setEnabled(false);
						downButton.setEnabled(false);
						removeButton.setEnabled(false);
					} else {
						if (lsm.getMaxSelectionIndex() > 0) {
							removeButton.setEnabled(true);
						} else {
							removeButton.setEnabled(false);
						}
						
						if (lsm.getMaxSelectionIndex() > 1) {
							upButton.setEnabled(true);
						} else {
							upButton.setEnabled(false);
						}
						if (lsm.getMaxSelectionIndex() > 0 && lsm.getMaxSelectionIndex() < model.getSize() - 1) {
							downButton.setEnabled(true);
						} else {
							downButton.setEnabled(false);
						}
					}
				}
			}
		});

		controlPanel.add(upButton);
		controlPanel.add(downButton);
		controlPanel.add(removeButton);
		
		JScrollPane pane = new JScrollPane(formulaListComponent);
		
		main.add(pane, BorderLayout.CENTER);
		main.add(controlPanel, BorderLayout.SOUTH);
		
		return main;
	}
	
	public void setCustomSearchFormula(CustomSearchFormula formula) {
		if (formula == null) {
			return;
		}
		
		formulaField.setText(formula.getText());
		ExpandablePanel expandablePanel = getRootExpandablePanel();
		for (int i = 0; i < formula.getClazzArray().length; i++) {
			expandablePanel.plusButton.doClick();
			expandablePanel.setSelectedClass(formula.getClazzArray()[i]);
			expandablePanel.setInputValueArray(formula.getParameterArray()[i]);
			expandablePanel = expandablePanel.nextPanel;
		}
	}
	
	private ExpandablePanel getRootExpandablePanel() {
		Container centerComp = null;
		for (Component c : getComponents()) {
			if ("CENTER".equals(c.getName())) {
				centerComp = (Container) c;
			}
		}
		
		ExpandablePanel expandablePanel = null;
		for (Component c : centerComp.getComponents()) {
			if ("EXPANDABLE_PANEL".equals(c.getName())) {
				expandablePanel = (ExpandablePanel) c;
			}
		}
		return expandablePanel;
	}
	
	private ExpandablePanel getNewExpandablePanel(final char nextChar) {
		ExpandablePanel panel = new ExpandablePanel(this, nextChar);
		panel.setName("EXPANDABLE_PANEL");
		expandablePanelList.add(panel);
		
		return panel;
	}

	public final class ExpandablePanel extends JPanel {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		private final static String BASE_PACKAGE = "jtw.stock.calculator";

		final JButton plusButton = new JButton("+");
		
		final char varChar;
		
		final SearchPanel superDialog;
		
		private JComboBox<String> comboBox;
		
		private JTextField[] textFieldArray = null;
		
		private String selectedClass = null;
		
		private ExpandablePanel nextPanel = null;
		
		public ExpandablePanel(final SearchPanel superDialog, final char varChar) {
			super(new BorderLayout());
			this.varChar = varChar;
			this.superDialog = superDialog;
			
			init();
		}
		
		private void init() {
			plusButton.addActionListener(new ActionListener() {
				boolean isExpanded = false;
				
				@Override
				public void actionPerformed(ActionEvent e) {
					if (!isExpanded) {
						plusButton.setText("" + varChar);
						isExpanded = true;
						
						JPanel pp = new JPanel(new BorderLayout());
						
						JComboBox<String> comboBox = getComboBox(pp);
						pp.add(comboBox, BorderLayout.NORTH);
						
						add(pp, BorderLayout.CENTER);
						
						nextPanel = getNewExpandablePanel((char)(varChar + 1));
						pp.add(nextPanel, BorderLayout.SOUTH);
						
						superDialog.validate();
					}
				}
			});

			add(plusButton, BorderLayout.NORTH);
		}
		
		public String[] getInputValueArray() {
			int len = 0;
			if (textFieldArray != null) {
				len = textFieldArray.length;
			}
			String[] retArray = new String[len];
			for (int i = 0; i < len; i++) {
				retArray[i] = textFieldArray[i].getText();
			}
			
			return retArray;
		}
		
		public void setInputValueArray(String[] inputValArray) {
			int len = Math.min(inputValArray.length, textFieldArray.length);
			for (int i = 0; i < len; i++) {
				textFieldArray[i].setText(inputValArray[i]);
			}
		}
		
		public String getVarChar() {
			return String.valueOf(varChar);
		}
		
		/**
		 * Returns the complete class name (with package) of the selected class name.
		 * 
		 * @return The selected class name.
		 */
		public String getSelectedClass() {
			String retval = null;
			if (selectedClass != null) {
				retval = BASE_PACKAGE + "." + selectedClass;
			}
			return retval;
		}
		
		public void setSelectedClass(String clazz) {
			String[] pkgAndClazz = clazz.split("\\.");
			comboBox.setSelectedItem(pkgAndClazz[pkgAndClazz.length - 1]);
			System.out.println();
		}

		private JComboBox<String> getComboBox(final JPanel pp) {
			try (ScanResult scanResult = new ClassGraph().enableAllInfo().acceptPackages(BASE_PACKAGE).scan()) {
				ClassInfoList calculatorClassList = scanResult.getSubclasses("jtw.stock.calculator.StockCalculator");
				String[] items = new String[calculatorClassList.size() + 1];
				items[0] = "-";
				for (int i = 0; i < calculatorClassList.size(); i++) {
					items[i + 1] = calculatorClassList.get(i).getSimpleName();
				}
				
				comboBox = new JComboBox<>(items);
				comboBox.addItemListener(new ItemListener() {
					
					@Override
					public void itemStateChanged(ItemEvent e) {
						if (e.getStateChange() == ItemEvent.SELECTED && !"-".equals(e.getItem())) {
							if (LOGGER.isDebugEnabled()) {
								LOGGER.debug(e.getItem().toString());
							}
							
							selectedClass = e.getItem().toString();
							
							try {
								Class<? extends StockCalculator> clazz = ClassLoader.getSystemClassLoader().loadClass(BASE_PACKAGE + "." + e.getItem()).asSubclass(StockCalculator.class);
								StockCalculator calculator = clazz.getDeclaredConstructor().newInstance();
								comboBox.setToolTipText(calculator.getTooltip());
								String[] nameArray = calculator.getParameterNameArray();
								String[] defaultValueArray = calculator.getParameterDefaultValueArray();
								
								textFieldArray = new JTextField[nameArray.length];
								
								JPanel panel = new JPanel(new GridLayout(nameArray.length, 2));
								for (int i = 0; i < nameArray.length; i++) {
									JTextField tf = new JTextField(defaultValueArray[i]);
									textFieldArray[i] = tf;
									
									panel.add(new JLabel(nameArray[i]));
									panel.add(tf);
								}

								if (pp.getComponentCount() >= 3) {
									pp.remove(2);
								}
								pp.add(panel, BorderLayout.CENTER);

								superDialog.validate();
							} catch (IllegalAccessException | NoSuchMethodException | InstantiationException | ClassNotFoundException | IllegalArgumentException | InvocationTargetException | SecurityException iae) {
								LOGGER.fatal("Instatiation of clazz " + selectedClass + " or accessing a method failed.", iae);
							}
						} else if (e.getStateChange() == ItemEvent.SELECTED) {
							selectedClass = null;
							textFieldArray = null;
						}
					}
				});
				return comboBox;
			}
		}
		
	}
	
	
}
