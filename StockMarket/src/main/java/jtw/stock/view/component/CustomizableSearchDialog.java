package jtw.stock.view.component;

import static jtw.stock.view.component.ComponentConstant.SEARCH_HISTORY_FILE_NAME;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jtw.stock.main.StockApplicationSettings;
import jtw.stock.model.CustomSearchFormula;
import jtw.stock.view.component.SearchPanel.ExpandablePanel;

public class CustomizableSearchDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Log LOGGER = LogFactory.getLog(CustomizableSearchDialog.class);

	public static final int NO_OPTION = JOptionPane.NO_OPTION;
	
	public static final int YES_OPTION = JOptionPane.YES_OPTION;
	
	private JButton buttonOk = new JButton("OK");
	private JButton buttonCancel = new JButton("Cancel");
	
	private int buttonPressed = NO_OPTION;
	
	private SearchPanel searchPanel = null;
	
	public CustomizableSearchDialog(JFrame parent, String title, boolean modal) {
		super(parent, title, modal);
		
		initPanel();
	}

	public CustomizableSearchDialog(String title, boolean modal) {
		this(StockApplicationSettings.getMainFrame(), title, modal);
	}

	public int getButtonPressed() {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Button pressed: " + buttonPressed);
		}
		return buttonPressed;
	}
	
	public boolean isNewFormula() {
		return searchPanel.isNewFormula();
	}
	
	/**
	 * Returns the current list of all formulas including a new one (if new is selected).
	 * If the currently selected formula has been updated, the updated formula is returned.
	 * 
	 * @return The list of formulas.
	 */
	public List<CustomSearchFormula> getFormulaList() {
		return searchPanel.getFormulaList();
	}

	public void showDialog() {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		validate();
		pack();
		setLocationRelativeTo(getParent());
		setVisible(true);
	}

	protected void initPanel() {
		JPanel main = new JPanel(new BorderLayout(10, 5));

		JPanel south = initSouth();
		
		JPanel center = initCenter();
		
		if (center != null) {
			main.add(center, BorderLayout.CENTER);
		}
		if (south != null) {
			main.add(south, BorderLayout.SOUTH);
		}
		
		add(main);
	}
	
	public CustomSearchFormula getSelectedFormula() {
		return searchPanel.getSelectedFormula();
	}
	
	public String getInput() {
		return searchPanel.getInput();
	}
	
	public List<ExpandablePanel> getInputParameterList() {
		return searchPanel.getInputParameterList();
	}
	
	protected JPanel initSouth() {
		buttonCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				buttonPressed = NO_OPTION;
				setVisible(false);
				dispose();
			}
		});
		
		ActionListener okAction = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				buttonPressed = YES_OPTION;
				setVisible(false);
				dispose();
			}
		};
		
		buttonOk.addActionListener(okAction);
		
		JPanel south = new JPanel(new FlowLayout(FlowLayout.CENTER));
		south.setName("SOUTH");
		south.add(buttonOk);
		south.add(buttonCancel);

		return south;
	}

	protected JPanel initCenter() {
		searchPanel = new SearchPanel(SEARCH_HISTORY_FILE_NAME);
		return searchPanel;
	}
		
}
