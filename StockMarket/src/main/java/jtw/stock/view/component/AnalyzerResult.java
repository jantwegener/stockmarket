package jtw.stock.view.component;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import jtw.stock.model.Stock;

public class AnalyzerResult {

	private final String stockName;
	
	/**
	 * How often the chart has been analyzed.
	 */
	private int counter;
	
	/**
	 * Marks the positive results in the domain.
	 */
	private List<DateMark> markerList = new ArrayList<>();

	/**
	 * Marks the negative results in the domain.
	 */
	private List<DateMark> negativeMarkerList = new ArrayList<>();
	
	/**
	 * Marks the points in time that are in the domain.
	 */
	private List<DateMark> domainMarkerList = new ArrayList<>();

	public AnalyzerResult(String stockName) {
		this.stockName = stockName;
	}

	public String getStockName() {
		return stockName;
	}

	public void addMarkerAt(Calendar date) {
		DateMark mark = new DateMark(date);
		markerList.add(mark);
	}

	public void addMarkerAt(Stock s) {
		DateMark mark = new DateMark(s.getDate());
		markerList.add(mark);
	}
	
	public List<DateMark> getMarkerList() {
		return markerList;
	}
	
	public int getMarkerSize() {
		return markerList.size();
	}

	public void addDomainMarkerAt(Calendar date) {
		DateMark mark = new DateMark(date);
		domainMarkerList.add(mark);
	}

	public void addDomainMarkerAt(Stock s) {
		DateMark mark = new DateMark(s.getDate());
		domainMarkerList.add(mark);
	}
	
	public List<DateMark> getDomainMarkerList() {
		return domainMarkerList;
	}
	
	public int getDomainMarkerSize() {
		return domainMarkerList.size();
	}

	public void addNegativeMarkerAt(Calendar date) {
		DateMark mark = new DateMark(date);
		negativeMarkerList.add(mark);
	}

	public void addNegativeMarkerAt(Stock s) {
		DateMark mark = new DateMark(s.getDate());
		negativeMarkerList.add(mark);
	}
	
	public List<DateMark> getNegativeMarkerList() {
		return negativeMarkerList;
	}
	
	public int getNegativeMarkerSize() {
		return negativeMarkerList.size();
	}
	
	public void increaseCounter() {
		counter++;
	}
	
	public int getCounter() {
		return counter;
	}

}
