package jtw.stock.view.component;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.concurrent.ExecutionException;

import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.SwingWorker;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jtw.stock.main.StockApplicationSettings;
import jtw.stock.model.registry.StockDataRegistry;
import jtw.stock.model.registry.StockSymbolNameRegistry;
import jtw.stock.view.ProgressView;
import jtw.stock.view.StockView;
import jtw.stock.view.action.chart.AbstractChartAction;
import jtw.stock.view.action.chart.ChartCandleStickAction;
import jtw.stock.view.action.file.IPostOpenAction;

public abstract class TableStockDialog extends StockDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Log LOGGER = LogFactory.getLog(TableStockDialog.class);
	
	private JTable table;
	
	public TableStockDialog(JFrame parent, String title, boolean modal, int hgap, int vgap, Object... additionalValues) {
		super(parent, title, modal, hgap, vgap, additionalValues);
	}
	
	public TableStockDialog(JFrame parent, String title, boolean modal) {
		super(parent, title, modal);
	}
	
	public TableStockDialog(String title, boolean modal) {
		super(title, modal);
	}
	
	/**
	 * Returns the column names of the table.
	 * 
	 * @return The names of the columns.
	 */
	protected abstract String[] getColumnNames();
	
	/**
	 * Returns the data of the table.
	 * 
	 * @return The data of the table.
	 */
	protected abstract Object[][] getTableData();
	
	/**
	 * There is no button to press.
	 */
	@Override
	protected JPanel initSouth() {
		return null;
	}
	
	@Override
	protected JPanel initCenter() {
		JPanel main = new JPanel(new BorderLayout());
		
		String[] columnNames = getColumnNames();
		Object[][] data = getTableData();
		
		TableModel tableModel = initTableModel(data, columnNames);
		JTable table = initTable(tableModel);
		
		JScrollPane scrollPane = new JScrollPane(table);
		table.setFillsViewportHeight(true);
		
		main.add(scrollPane, BorderLayout.CENTER);
		
		return main;
	}
	
	protected TableModel initTableModel(Object[][] data, String[] columnNames) {
		DefaultTableModel tableModel = new DefaultTableModel() {
			/**
			* 
			*/
			private static final long serialVersionUID = 1L;

			@Override
			public Class<?> getColumnClass(int columnIndex) {
				return String.class;
			}
			
			@Override
			public boolean isCellEditable(int row, int col) {
				return false;
			}
		};
		tableModel.setDataVector(data, columnNames);
		return tableModel;
	}
	
	protected JTable initTable(TableModel tableModel) {
		table = new JTable(tableModel);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		DefaultTableCellRenderer renderer = new DefaultTableCellRenderer() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
				if (column == 0 && value != null) {
					value = StockSymbolNameRegistry.getInstance().getCompany(value.toString());
				}
				super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
				if (!hasFocus()) {
					setBorder(new EmptyBorder(1, 6, 1, 6));
				}
				return this;
			}
		};
		table.setDefaultRenderer(String.class, renderer);

		JMenuItem openMenuItem = new JMenuItem("Open");
		openMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String stockName = tableModel.getValueAt(table.getSelectionModel().getLeadSelectionIndex(), getStockSymbolColumn()).toString();
				openStock(stockName);
			}
		});
		
		JPopupMenu popup = new JPopupMenu("Open");
		popup.add(openMenuItem);
		
		table.addMouseListener(new MouseAdapter() {
		    @Override
			public void mouseReleased(MouseEvent e) {
		    	if (e.isPopupTrigger()) {
		    		JTable source = (JTable) e.getSource();
                    int row = source.rowAtPoint(e.getPoint());
                    int column = source.columnAtPoint(e.getPoint());
                    
                    if (! source.isRowSelected(row)) {
                    	source.changeSelection(row, column, false, false);
                    }
                    
                    popup.show(e.getComponent(), e.getX(), e.getY());
		    	}
		    }
		});
		
		return table;
	}
	
	/**
	 * Returns the column index of the stock symbol. This is used when the stock shall be opened.
	 * 
	 * By default 0.
	 * 
	 * @return The column index.
	 */
	protected int getStockSymbolColumn() {
		return 0;
	}
	
	/**
	 * Opens the selected stock.
	 * 
	 * @param stockName The stock to open.
	 */
	protected final void openStock(String stockName) {

		SwingWorker<Object, Void> worker = new SwingWorker<Object, Void>() {

			@Override
			protected Object doInBackground() throws Exception {
				ProgressView.getInstance().startIndeterminedMode();
				
				StockView.getInstance().reset();
				StockApplicationSettings.setStockName(stockName);
				
				StockApplicationSettings.getMainFrame().setTitle(StockSymbolNameRegistry.getInstance().getCompany(stockName) + " (" + stockName + ")");
				StockDataRegistry.getInstance().ensureUpToDate(stockName);
				
				ProgressView.getInstance().stopIndeterminedMode();
				
				return null;
			}

			@Override
			public void done() {
				try {
					get();
					
					AbstractChartAction action = AbstractChartAction.getAction(ChartCandleStickAction.class);
					action.triggerAction();
					
					IPostOpenAction postAction = getPostOpenAction(stockName);
					if (postAction != null) {
						postAction.performPostAction(stockName);
					}
				} catch (InterruptedException | ExecutionException e) {
					LOGGER.error(e.getMessage(), e);
					ProgressView.getInstance().stopIndeterminedMode();
	
					JOptionPane.showMessageDialog(StockView.getInstance(), "Error computing plot: \"" + e.getMessage() + "\"", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
			
		};
		worker.execute();
	}
	
	/**
	 * Returns the post open action for the selected stock after opening it.
	 * 
	 * @param stockName The selected stock.
	 * 
	 * @return The {@link IPostOpenAction} or null if none shall be performed.
	 */
	protected IPostOpenAction getPostOpenAction(String stockName) {
		return null;
	}
	
	protected JTable getTable() {
		return table;
	}
	
	@Override
	protected void initAdditionalMenus(JMenuBar bar, JMenu fileMenu) {
		final JDialog thisDialog = this;
		
		JMenuItem exportItem = new JMenuItem("Export...");
		exportItem.setName("Export Menu Item");
		exportItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				final JFileChooser fc = new JFileChooser();
				fc.addChoosableFileFilter(new FileNameExtensionFilter("Comma Separated Value", ".csv"));
				int returnVal = fc.showSaveDialog(thisDialog);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File file = fc.getSelectedFile();
					if (!file.getName().toLowerCase().endsWith(".csv")) {
						file = new File(file.getAbsolutePath() + ".csv");
					}

					try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
						writeTable(writer);
					} catch (IOException ioe) {
						LOGGER.error(ioe.getMessage(), ioe);

						JOptionPane.showMessageDialog(StockView.getInstance(), "Error writing csv file: \"" + ioe.getMessage() + "\"", "Error", JOptionPane.ERROR_MESSAGE);
					}
					JOptionPane.showMessageDialog(StockView.getInstance(), "Export completed", "Done", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});
		exportItem.setAccelerator(KeyStroke.getKeyStroke("ctrl E"));
		
		JMenuItem copyItem = new JMenuItem("Copy to clipboard");
		copyItem.setName("Copy Menu Item");
		copyItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try (StringWriter writer = new StringWriter()) {
					writeTable(writer);
					String contents = writer.toString();
					
					if (!contents.isEmpty()) {
						Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
						StringSelection selection = new StringSelection(contents);
						clipboard.setContents(selection, null);
					}
				} catch (IOException ioe) {
					LOGGER.error(ioe.getMessage(), ioe);

					JOptionPane.showMessageDialog(StockView.getInstance(), "Error copying data to clipboard", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		copyItem.setAccelerator(KeyStroke.getKeyStroke("ctrl shift C"));
		
		fileMenu.add(exportItem);
		fileMenu.add(copyItem);
	}
	
	private void writeTable(Writer writer) throws IOException {
		if (getCustomizedTitle() != null) {
			writer.write(getCustomizedTitle());
			writer.write(System.lineSeparator());
		}
		
		StringBuilder headerBuffer = new StringBuilder();
		for (int column = 0; column < table.getColumnCount(); column++) {
			String name = table.getColumnName(column);
			headerBuffer.append('"');
			headerBuffer.append(name);
			headerBuffer.append("\",");
		}
		headerBuffer.deleteCharAt(headerBuffer.length() - 1);
		
		// write the current row
		writer.write(headerBuffer.toString());
		writer.write(System.lineSeparator());
		
		// loop through each row and column
		for (int row = 0; row < table.getRowCount(); row++) {
			StringBuilder rowBuffer = new StringBuilder();
			for (int column = 0; column < table.getColumnCount(); column++) {
				Object value = table.getValueAt(row, column);
				rowBuffer.append('"');
				rowBuffer.append(value);
				rowBuffer.append("\",");
			}
			rowBuffer.deleteCharAt(rowBuffer.length() - 1);
			
			// write the current row
			writer.write(rowBuffer.toString());
			writer.write(System.lineSeparator());
		}
	}
	
}
