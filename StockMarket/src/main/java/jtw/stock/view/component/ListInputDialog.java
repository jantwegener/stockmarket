package jtw.stock.view.component;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.springframework.util.StringUtils;

import jtw.stock.main.StockApplicationSettings;
import jtw.stock.model.registry.StockSymbolNameRegistry;
import jtw.stock.view.StockView;

public class ListInputDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final int NO_OPTION = JOptionPane.NO_OPTION;
	
	public static final int YES_OPTION = JOptionPane.YES_OPTION;
	
	public static final int APPLY_OPTION = 2;
	
	private JButton buttonOk = new JButton("OK");
	private JButton buttonApply = new JButton("Apply");
	private JButton buttonCancel = new JButton("Cancel");
	
	private int buttonPressed = NO_OPTION;
	
	private JTextField inputTextField = null;
	
	private boolean showApplyButton = true;
	
	/**
	 * By default, the validator always returns true.
	 */
	private InputValidator inputValidator = () -> null;
	
	private List<DialogButtonListener> buttonListenerList = new ArrayList<>();
	
	public ListInputDialog(JFrame parent, String title, boolean modal, String descriptiveText, String listTitle, String textFieldText, String ... listElements) {
		super(parent, title, modal);

		if (modal) {
			showApplyButton = false;
		}
		
		JPanel[] allPanel = initPanel(descriptiveText, listTitle, textFieldText, listElements);
		
		allPanel[1].add(allPanel[3], BorderLayout.SOUTH);
		allPanel[0].add(allPanel[1], BorderLayout.CENTER);
		allPanel[0].add(allPanel[2], BorderLayout.SOUTH);
		
		setContentPane(allPanel[0]);
		
//		setContentPane(initPanel(descriptiveText, listTitle, textFieldText, listElements)[0]);
	}
	
	public ListInputDialog(String title, boolean modal, String descriptiveText, String listTitle, String textFieldText, String ... listElements) {
		this(StockApplicationSettings.getMainFrame(), title, modal, descriptiveText, listTitle, textFieldText, listElements);
	}
	
	public void addDialogButtonListener(DialogButtonListener listener) {
		buttonListenerList.add(listener);
	}

	public int getButtonPressed() {
		return buttonPressed;
	}

	public void showDialog() {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		pack();
		setLocationRelativeTo(getParent());
		setVisible(true);
	}
	
	/**
	 * Initiates the panels. The returned panels are in the following order: main, center, south, inputPanel.
	 * 
	 * @param descriptiveText
	 * @param listTitle
	 * @param textFieldText
	 * @param listElements
	 * 
	 * @return
	 */
	protected JPanel[] initPanel(String descriptiveText, String listTitle, String textFieldText, String ... listElements) {
		JPanel main = new JPanel(new BorderLayout());

		buttonCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				buttonPressed = NO_OPTION;
				for (DialogButtonListener listener : buttonListenerList) {
					listener.buttonPressed(buttonPressed);
				}
				setVisible(false);
				dispose();
			}
		});
		
		ActionListener okAction = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				handleOkButtonPress();
			}
		};
		buttonOk.addActionListener(okAction);

		if (showApplyButton) {
			ActionListener applyAction = new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					buttonPressed = APPLY_OPTION;
					for (DialogButtonListener listener : buttonListenerList) {
						listener.buttonPressed(buttonPressed);
					}
				}
			};
			buttonApply.addActionListener(applyAction);
		}
		
		JPanel south = new JPanel(new FlowLayout(FlowLayout.CENTER));
		south.add(buttonOk);
		if (showApplyButton) {
			south.add(buttonApply);
		}
		south.add(buttonCancel);

		JPanel center = new JPanel(new BorderLayout());
		JPanel inputPanel = new JPanel();
		
		inputTextField = new JTextField(50);
		inputTextField.setText(textFieldText);
		addFocusListener(inputTextField);
		inputTextField.addActionListener(okAction);
		inputPanel.add(inputTextField);
		
		JList<String> list = new JList<>();
		list.setVisibleRowCount(-1);
		list.setCellRenderer(new MarginCellRenderer());
		list.setLayoutOrientation(JList.HORIZONTAL_WRAP);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (e.getValueIsAdjusting() == false) {
					inputTextField.setText(list.getSelectedValue());
				}
			}
		});
		list.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() >= 2 && !e.isConsumed()) {
					e.consume();
					handleOkButtonPress();
				}
			}
		});
		DefaultListModel<String> listModel = new DefaultListModel<>();
		for (String element : listElements) {
			listModel.addElement(element);
		}
		list.setModel(listModel);
		
		addInputListener(inputTextField, listElements, listModel);
		
		JScrollPane listScroller = new JScrollPane(list);
		listScroller.setPreferredSize(new Dimension(500, 180));
		center.add(listScroller, BorderLayout.CENTER);
		
		center.add(new JLabel(listTitle), BorderLayout.NORTH);
		center.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
		
		if (!StringUtils.hasText(descriptiveText)) {
			JLabel descriptionLabel = new JLabel(descriptiveText);
			descriptionLabel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
			main.add(descriptionLabel, BorderLayout.NORTH);
		}

//		center.add(inputPanel, BorderLayout.SOUTH);
//		main.add(center, BorderLayout.CENTER);
//		main.add(south, BorderLayout.SOUTH);
		
		JPanel[] allPanel = new JPanel[] {main, center, south, inputPanel};
		
		return allPanel;
	}
	
	private void handleOkButtonPress() {
		String[] errorMessages = inputValidator.validateInput();
		if (errorMessages == null) {
			buttonPressed = YES_OPTION;
			for (DialogButtonListener listener : buttonListenerList) {
				listener.buttonPressed(buttonPressed);
			}
			setVisible(false);
			dispose();
		} else {
			String msg = "";
			for (String em : errorMessages) {
				msg += "<li>" + em + "</li>";
			}
			JOptionPane.showMessageDialog(StockView.getInstance(), "<html>The input is not valid<br><ul>" + msg + "</ul></html>", "Input Validation Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	protected void addFocusListener(JTextField f) {
		f.addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent e) {
			}
			
			@Override
			public void focusGained(FocusEvent e) {
				f.setSelectionStart(0);
				f.setSelectionEnd(f.getText().length());
			}
		});
	}
	
	protected void addInputListener(JTextField f, String[] listElements, DefaultListModel<String> listModel) {
		f.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent e) {
				if ((e.getKeyCode() == KeyEvent.VK_BACK_SPACE)
					|| (KeyEvent.VK_A <= e.getKeyCode() && e.getKeyCode() <= KeyEvent.VK_Z)
					|| (KeyEvent.VK_0 <= e.getKeyCode() && e.getKeyCode() <= KeyEvent.VK_9)
					|| (e.getKeyCode() == KeyEvent.VK_SPACE)) {
					String textUpperCase = f.getText().toUpperCase();
					listModel.removeAllElements();
										
					for (String symbol : listElements) {
						String compName = StockSymbolNameRegistry.getInstance().getCompany(symbol);
						if (symbol.toUpperCase().contains(textUpperCase) || compName.toUpperCase().contains(textUpperCase)) {
							listModel.addElement(symbol);
						}
					}
				}
			}
			
		});
		
	}
	
	public void setInputValidator(InputValidator inputValidator) {
		this.inputValidator = inputValidator;
	}
	
	public String getInput() {
		return inputTextField.getText();
	}
	
	private static class MarginCellRenderer extends DefaultListCellRenderer {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public MarginCellRenderer() {
			super();
		}
		
		public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
			if (value != null) {
				value = StockSymbolNameRegistry.getInstance().getCompany(value.toString());
			}
			super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
			if (!hasFocus()) {
				setBorder(new EmptyBorder(1, 6, 1, 6));
			}
			return this;
		}
	}
	
}
