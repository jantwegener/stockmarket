package jtw.stock.view.component;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.net.URL;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jtw.stock.main.StockApplicationSettings;
import jtw.util.gui.HelpBrowserDialog;

/**
 * The base class for new dialogs.
 * 
 * @author Jan-Thierry Wegener
 */
public class StockDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Log LOGGER = LogFactory.getLog(StockDialog.class);
	
	public static final int NO_OPTION = JOptionPane.NO_OPTION;
	
	public static final int YES_OPTION = JOptionPane.YES_OPTION;
	
	private JButton buttonOk = new JButton("OK");
	private JButton buttonCancel = new JButton("Cancel");
	
	private int buttonPressed = NO_OPTION;
	
	private final int hgap;
	private final int vgap;
	
	private final String initialTitle;
	private String customTitle;
	
	public StockDialog(JFrame parent, String title, boolean modal, int hgap, int vgap, Object ... additionalValues) {
		super(parent, title, modal);
		
		this.initialTitle = title;
		this.hgap = hgap;
		this.vgap = vgap;
		
		initMenu();
		
		if (additionalValues != null) {
			init(additionalValues);
		}
		
		initPanel();
	}
	
	protected void init(Object[] additionalValues) {
	}

	public StockDialog(JFrame parent, String title, boolean modal) {
		this(parent, title, modal, 10, 5);
	}

	public StockDialog(String title, boolean modal) {
		this(StockApplicationSettings.getMainFrame(), title, modal);
	}
	
	/**
	 * Initializes the panels.
	 */
	protected void initPanel() {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("initPanel()");
		}
		
		JPanel main = new JPanel(new BorderLayout(hgap, vgap));

		JPanel south = initSouth();
		
		JPanel east = initEast();
		
		JPanel west = initWest();
		
		JPanel center = initCenter();
		
		JPanel north = initNorth();

		if (north != null) {
			main.add(north, BorderLayout.NORTH);
		}
		if (east != null) {
			main.add(east, BorderLayout.EAST);
		}
		if (west != null) {
			main.add(west, BorderLayout.WEST);
		}
		if (center != null) {
			main.add(center, BorderLayout.CENTER);
		}
		if (south != null) {
			main.add(south, BorderLayout.SOUTH);
		}
		
		add(main);
	}
	
	protected JPanel initNorth() {
		return null;
	}

	protected JPanel initCenter() {
		return null;
	}

	protected JPanel initWest() {
		return null;
	}

	protected JPanel initEast() {
		return null;
	}

	protected JPanel initSouth() {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("initSouth()");
		}
		
		buttonCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				buttonPressed = NO_OPTION;
				setVisible(false);
				dispose();
			}
		});
		
		ActionListener okAction = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				buttonPressed = YES_OPTION;
				setVisible(false);
				dispose();
			}
		};
		
		buttonOk.addActionListener(okAction);
		
		JPanel south = new JPanel(new FlowLayout(FlowLayout.CENTER));
		south.setName("SOUTH");
		south.add(buttonOk);
		south.add(buttonCancel);

		return south;
	}
	
	public int getButtonPressed() {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("getButtonPressed() - returning: " + buttonPressed);
		}
		
		return buttonPressed;
	}
	
	public void showDialog() {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		validate();
		pack();
		setLocationRelativeTo(getParent());
		setVisible(true);
	}

	protected void initMenu() {
		JMenuBar bar = new JMenuBar();
		
		JMenu fileMenu = new JMenu("File");
		fileMenu.setName("File Menu");
		JMenuItem changeTitleMenuItem = new JMenuItem("Change Title...");
		changeTitleMenuItem.setName("Change Title Menu Item");
		changeTitleMenuItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				setCustomizedTitle(JOptionPane.showInputDialog(StockApplicationSettings.getMainFrame(), "Enter a new title", "New title...", JOptionPane.QUESTION_MESSAGE));
			}
		});
		changeTitleMenuItem.setAccelerator(KeyStroke.getKeyStroke("control T"));
		
		fileMenu.add(changeTitleMenuItem);
		bar.add(fileMenu);
		
		initAdditionalMenus(bar, fileMenu);
		initHelpMenu(bar);
		
		setJMenuBar(bar);
	}

	/**
	 * Returns the new title or null if none was set.
	 * 
	 * @return The new title.
	 */
	public String getCustomizedTitle() {
		return customTitle;
	}
	
	public void setCustomizedTitle(String customTitle) {
		String title;
		if (customTitle != null && !customTitle.trim().isEmpty()) {
			title = String.format("%s (%s)", initialTitle, customTitle);
			this.customTitle = customTitle;
		} else {
			title = initialTitle;
			this.customTitle = null;
		}
		setTitle(title);
	}
	
	/**
	 * Can be used to add some more menus and menu items. This method is called before the bar is set to the {@link JDialog}.
	 * 
	 * @param bar The menu bar.
	 * @param fileMenu The standard menu "File".
	 */
	protected void initAdditionalMenus(JMenuBar bar, JMenu fileMenu) {
	}
	
	private void initHelpMenu(JMenuBar bar) {
		if (getPathToHelp() != null) {
			final JDialog thisDialog = this;
			// move the help menu to the right
			bar.add(Box.createHorizontalGlue());
			JMenu helpMenu = new JMenu("Help");
			helpMenu.setMnemonic(KeyEvent.VK_H);
			JMenuItem helpMenuItem = new JMenuItem(getHelpMenuItemName());
			helpMenuItem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					HelpBrowserDialog helpDialog = new HelpBrowserDialog(thisDialog, "", getPathToHelp());
					helpDialog.showDialog();
				}
			});
			helpMenu.add(helpMenuItem);
			bar.add(helpMenu);
		}
	}
	
	/**
	 * The path to the help file.
	 * 
	 * @return The path to the help file.
	 */
	protected URL getPathToHelp() {
		return null;
	}
	
	/**
	 * Returns the name of the menu item shown in the help menu.
	 * 
	 * @return The name of the help menu item under the help menu.
	 */
	protected String getHelpMenuItemName() {
		return null;
	}
	
}
