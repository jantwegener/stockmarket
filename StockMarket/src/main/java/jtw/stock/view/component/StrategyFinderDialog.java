package jtw.stock.view.component;

import static jtw.stock.view.component.ComponentConstant.COMPARISON_HISTORY_FILE_NAME;
import static jtw.stock.view.component.ComponentConstant.SEARCH_HISTORY_FILE_NAME;

import java.awt.GridLayout;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import jtw.stock.view.component.SearchPanel.ExpandablePanel;

public class StrategyFinderDialog extends BacktestingDialog {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private SearchPanel buyComparisonPanel;
	
	public StrategyFinderDialog(String title, boolean modal) {
		super(title, modal);
	}
	
	public List<ExpandablePanel> getBuyComparisonInputParameterList() {
		return buyComparisonPanel.getInputParameterList();
	}

	@Override
	protected JPanel initSearchPanel() {
		buyPanel = new SearchPanel(SEARCH_HISTORY_FILE_NAME);
		buyPanel.setBorder(BorderFactory.createTitledBorder("Buy Requirement"));
		
		buyComparisonPanel = new SearchPanel(COMPARISON_HISTORY_FILE_NAME);
		buyComparisonPanel.setBorder(BorderFactory.createTitledBorder("Buy Comparison"));
		
		sellPanel = new SearchPanel(SEARCH_HISTORY_FILE_NAME);
		sellPanel.setBorder(BorderFactory.createTitledBorder("Sell"));
		
		JPanel searchPanel = new JPanel(new GridLayout(1, 3));
		searchPanel.add(buyPanel);
		searchPanel.add(buyComparisonPanel);
		searchPanel.add(sellPanel);
		
		return searchPanel;
	}
	
	public String getBuyComparisonInput() {
		return buyComparisonPanel.getInput();
	}
	
	@Override
	protected URL getPathToHelp() {
		try {
			File f = new File("doc/HowtoStockLang.html");
			return f.toURI().toURL();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	protected String getHelpMenuItemName() {
		return "How-To Stock Lang";
	}
	
}
