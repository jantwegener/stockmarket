package jtw.stock.view.component;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;
import java.util.Currency;
import java.util.Map;
import java.util.Set;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.plaf.basic.BasicComboBoxRenderer;

import org.antlr.v4.runtime.misc.Pair;

import jtw.stock.model.registry.CurrencyConversionRegistry;
import jtw.util.DoubleUtil;

public class PrefCurrencyConversionDialog extends StockDialog {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JComboBox<Currency> currenyComboBox1;
	private JComboBox<Currency> currenyComboBox2;

	private JTextField conversionTextField1;
	private JTextField conversionTextField2;
	
	public PrefCurrencyConversionDialog(String title, boolean modal) {
		super(title, modal);
	}
	
	protected JPanel initCenter() {
		Map<Pair<String, String>, Pair<Double, Double>> conversionMap = CurrencyConversionRegistry.getInstance().getAllConversion();
		
		@SuppressWarnings("unchecked")
		Pair<Pair<String, String>, Pair<Double, Double>>[] listEntries = new Pair[conversionMap.size()];
		int counter = 0;
		for (Pair<String, String> p : conversionMap.keySet()) {
			Pair<Double, Double> m = conversionMap.get(p);
			Pair<Pair<String, String>, Pair<Double, Double>> listEntry = new Pair<>(p, m);
			listEntries[counter++] = listEntry;
		}
		JList<Pair<Pair<String, String>, Pair<Double, Double>>> allConversionRateList = new JList<>(listEntries);
		allConversionRateList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		@SuppressWarnings({ "unchecked", "serial" })
		ListCellRenderer<Object> renderer = new DefaultListCellRenderer() {

			@SuppressWarnings("rawtypes")
			@Override
			public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
				Pair<Pair<String, String>, Pair<Double, Double>> val = (Pair<Pair<String, String>, Pair<Double, Double>>) value;
				String valStr = val.a.a + " - " + val.a.b + "(" + val.b.a + " - " + val.b.b + ")";
				return super.getListCellRendererComponent(list, valStr, index, isSelected, cellHasFocus);
			}
			
		};
		
		allConversionRateList.setCellRenderer(renderer);
		
		JPanel newConversionRatePanel = new JPanel(new GridLayout(2, 2));
		currenyComboBox1 = initComboBox();
		currenyComboBox2 = initComboBox();
		
		conversionTextField1 = new JTextField(10);
		conversionTextField2 = new JTextField(10);
		
		newConversionRatePanel.add(currenyComboBox1);
		newConversionRatePanel.add(currenyComboBox2);
		
		newConversionRatePanel.add(conversionTextField1);
		newConversionRatePanel.add(conversionTextField2);
		
		allConversionRateList.addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (!e.getValueIsAdjusting()) {
					Pair<Pair<String, String>, Pair<Double, Double>> p = allConversionRateList.getModel().getElementAt(e.getFirstIndex());
					currenyComboBox1.setSelectedItem(Currency.getInstance(p.a.a));
					currenyComboBox2.setSelectedItem(Currency.getInstance(p.a.b));
					
					conversionTextField1.setText(String.valueOf(p.b.a));
					conversionTextField2.setText(String.valueOf(p.b.b));
				}
			}
		});
		
		JScrollPane allConversionScrollPane = new JScrollPane(allConversionRateList);
		
		JPanel mainPanel = new JPanel(new BorderLayout());
		mainPanel.add(allConversionScrollPane, BorderLayout.CENTER);
		mainPanel.add(newConversionRatePanel, BorderLayout.SOUTH);
		
		return mainPanel;
	}
	
	private JComboBox<Currency> initComboBox() {
		Set<Currency> currencySet = Currency.getAvailableCurrencies();
		
		JComboBox<Currency> currencyComboBox = new JComboBox<>(currencySet.toArray(new Currency[0]));
		@SuppressWarnings({ "serial" })
		ListCellRenderer<Object> renderer = new BasicComboBoxRenderer() {

			@SuppressWarnings("rawtypes")
			@Override
			public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
				Currency val = (Currency) value;
				String valStr = val.getDisplayName() + " (" + val.getCurrencyCode() + " / " + val.getSymbol() + ")";
				return super.getListCellRendererComponent(list, valStr, index, isSelected, cellHasFocus);
			}
			 
		};
		currencyComboBox.setRenderer(renderer);
		
		return currencyComboBox;
	}

	public String getCurrenyCode1() {
		return ((Currency) currenyComboBox1.getSelectedItem()).getCurrencyCode();
	}

	public String getCurrenyCode2() {
		return ((Currency) currenyComboBox2.getSelectedItem()).getCurrencyCode();
	}

	public double getConversion1() {
		return DoubleUtil.toDouble(conversionTextField1.getText(), 1.0);
	}

	public double getConversion2() {
		return DoubleUtil.toDouble(conversionTextField2.getText(), 1.0);
	}
	
}
