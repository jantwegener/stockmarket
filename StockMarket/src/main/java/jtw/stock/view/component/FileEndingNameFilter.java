package jtw.stock.view.component;

import java.io.File;
import java.io.FilenameFilter;

public class FileEndingNameFilter implements FilenameFilter {
	
	private final String ending;
	
	public FileEndingNameFilter(String ending) {
		if (ending.startsWith(".")) {
			this.ending = ending;
		} else {
			this.ending = "." + ending;
		}
	}

	@Override
	public boolean accept(File dir, String name) {
		return name.endsWith(ending);
	}


}
