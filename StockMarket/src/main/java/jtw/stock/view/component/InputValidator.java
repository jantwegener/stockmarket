package jtw.stock.view.component;

public interface InputValidator {
	
	/**
	 * Validates the input. If the input is not valid, pressing on ok opens a window with the error messages.
	 * 
	 * @return null if the input is valid, otherwise an array with the error messages.
	 */
	public String[] validateInput();
	
}
