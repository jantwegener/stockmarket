package jtw.stock.view.component;

public class ComponentConstant {

	public static final String SEARCH_HISTORY_FILE_NAME = "search";
	
	public static final String COMPARISON_HISTORY_FILE_NAME = "comparator";
	
}
