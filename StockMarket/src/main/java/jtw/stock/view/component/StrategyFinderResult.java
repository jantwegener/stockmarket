package jtw.stock.view.component;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jtw.stock.model.Stock;

public class StrategyFinderResult {
	
	private double startMoney;
	
	private double totalMoneyUsed;
	
	private double money;
	
	private int stockNum;
	
	private Stock stock;
	
	private List<BuySellMark> buyList = new ArrayList<>();
	
	private List<BuySellMark> sellList = new ArrayList<>();
	
	private List<BuySellMark> buySellList = new ArrayList<>();
	
	private List<BuySellMoneyDevelopment> developmentList = new ArrayList<>();
	
	private Set<String> stockNameSet = new HashSet<>();
	
	public StrategyFinderResult() {
	}
	
	public double getTotalMoneyUsed() {
		return totalMoneyUsed;
	}

	public void addTotalMoneyUsed(String stockName, double money, Calendar date) {
		totalMoneyUsed += money;
		
		BuySellMoneyDevelopment devel = new BuySellMoneyDevelopment(stockName, money, date);
		developmentList.add(devel);
	}
	
	public double getStartMoney() {
		return startMoney;
	}
	
	public void setStartMoney(double startMoney) {
		this.startMoney = startMoney;
	}
	
	public double getMoney() {
		return money;
	}
	
	public void setMoney(double money) {
		this.money = money;
	}
	
	public int getStockNum() {
		return stockNum;
	}
	
	public void setStockNum(int stockNum) {
		this.stockNum = stockNum;
	}
	
	public Stock getStock() {
		return stock;
	}
	
	public void setStock(Stock stock) {
		this.stock = stock;
	}
	
	public void addBuy(String stockName, double price, double fee, int stockNum, Stock s) {
		BuySellMark mark = new BuySellMark(stockName, true, price, fee, stockNum, s.getDate());
		buyList.add(mark);
		buySellList.add(mark);
		stockNameSet.add(stockName);
	}
	
	public void addSell(String stockName, double price, double fee, int stockNum, Stock s, double money) {
		BuySellMark mark = new BuySellMark(stockName, false, price, fee, stockNum, s.getDate());
		sellList.add(mark);
		BuySellMoneyDevelopment devel = new BuySellMoneyDevelopment(stockName, money, s.getDate());
		developmentList.add(devel);
		buySellList.add(mark);
		stockNameSet.add(stockName);
	}
	
	public List<BuySellMark> getBuySellList() {
		return buySellList;
	}
	
	public Set<String> getStockNameSet() {
		return stockNameSet;
	}
	
	public List<BuySellMoneyDevelopment> getDevelopmentList() {
		return developmentList;
	}
	
	public class BuySellMark {
		
		private final String stockName;
		
		private final boolean isBuyMark;
		
		private final double price;
		
		private final double fee;
		
		private final Calendar date;
		
		private final int stockNum;
		
		public BuySellMark(String stockName, boolean isBuyMark, double price, double fee, int stockNum, Calendar calendar) {
			this.stockName = stockName;
			this.isBuyMark = isBuyMark;
			this.price = price;
			this.fee = fee;
			this.date = calendar;
			this.stockNum = stockNum;
		}
		
		public String getStockName() {
			return stockName;
		}
		
		public boolean isBuyMark() {
			return isBuyMark;
		}
		
		public double getPrice() {
			return price;
		}
		
		public double getFee() {
			return fee;
		}
		
		public Calendar getDate() {
			return date;
		}

		public int getStockNum() {
			return stockNum;
		}
		
		@Override
		public String toString() {
			return "BuySellMark [stockName=" + stockName + ", isBuyMark=" + isBuyMark + ", price=" + price + ", fee="
					+ fee + ", date=" + date + ", stockNum=" + stockNum + "]";
		}
		
	}
	
	public class BuySellMoneyDevelopment {
		
		private final String stockName;
		
		private final double money;
		
		private final Calendar date;
		
		public BuySellMoneyDevelopment(String stockName, double money, Calendar date) {
			this.stockName = stockName;
			this.money = money;
			this.date = date;
		}
		
		public String getStockName() {
			return stockName;
		}
		
		public double getMoney() {
			return money;
		}
		
		public Calendar getDate() {
			return date;
		}
		
		@Override
		public String toString() {
			return "BuySellMoneyDevelopment [stockName=" + stockName + ", money=" + money + ", date=" + date + "]";
		}
		
	}
	
}
