package jtw.stock.view.component;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import jtw.stock.main.StockApplicationSettings;

public class MultipleTextInputDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final int NO_OPTION = JOptionPane.NO_OPTION;
	
	public static final int YES_OPTION = JOptionPane.YES_OPTION;
	
	private JButton buttonOk = new JButton("OK");
	private JButton buttonCancel = new JButton("Cancel");
	
	private int buttonPressed;
	
	private JTextField[] inputTextField = null;

	/**
	 * 
	 * 
	 * @param parent
	 * @param title
	 * @param modal
	 * @param horizontalAlignment whether the text is left to the input box (true) or above (false).
	 * @param descriptiveText
	 */
	public MultipleTextInputDialog(JFrame parent, String title, boolean modal, boolean horizontalAlignment, String ... descriptiveText) {
		super(parent, title, modal);
		
		setContentPane(initPanel(horizontalAlignment, descriptiveText));
	}
	
	public MultipleTextInputDialog(String title, boolean modal, boolean horizontalAlignment, String ... descriptiveText) {
		this(StockApplicationSettings.getMainFrame(), title, modal, horizontalAlignment, descriptiveText);
	}
	
	public int getButtonPressed() {
		return buttonPressed;
	}
	
	public void showDialog() {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		pack();
		setLocationRelativeTo(getParent());
		setVisible(true);
	}
	
	protected JPanel initPanel(boolean horizontalAlignment, String[] descriptiveText) {
		JPanel main = new JPanel(new BorderLayout());

		GridBagLayout gbl = new GridBagLayout();
		GridBagConstraints labelConstraint;
		GridBagConstraints textConstraint;
		
		if (horizontalAlignment) {
			labelConstraint = new GridBagConstraints();
			labelConstraint.weightx = 1;
			labelConstraint.gridx = 0;
			textConstraint = new GridBagConstraints();
			textConstraint.weightx = 5;
			textConstraint.gridx = 1;
			textConstraint.fill = GridBagConstraints.HORIZONTAL;
		} else {
			labelConstraint = new GridBagConstraints();
			labelConstraint.weightx = 1;
			labelConstraint.gridx = 0;
			labelConstraint.fill = GridBagConstraints.HORIZONTAL;
			textConstraint = new GridBagConstraints();
			textConstraint.weightx = 1;
			textConstraint.gridx = 0;
			textConstraint.fill = GridBagConstraints.HORIZONTAL;
		}
		
		JPanel center = new JPanel(gbl);
		inputTextField = new JTextField[descriptiveText.length];
		for (int i = 0; i < descriptiveText.length; i++) {
			inputTextField[i] = new JTextField();
			center.add(new JLabel(descriptiveText[i]), labelConstraint);
			center.add(inputTextField[i], textConstraint);
		}

		buttonCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				buttonPressed = NO_OPTION;
				setVisible(false);
				dispose();
			}
		});
		
		buttonOk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				buttonPressed = YES_OPTION;
				setVisible(false);
				dispose();
			}
		});
		
		JPanel south = new JPanel(new FlowLayout(FlowLayout.CENTER));
		south.add(buttonOk);
		south.add(buttonCancel);
		
		main.add(center, BorderLayout.CENTER);
		main.add(south, BorderLayout.SOUTH);
		
		return main;
	}

	public String[] getInput() {
		String[] text = new String[inputTextField.length];
		for (int i = 0; i < text.length; i++) {
			text[i] = inputTextField[i].getText();
		}
		return text;
	}
	
}
