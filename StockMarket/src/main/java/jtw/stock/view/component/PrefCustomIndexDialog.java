package jtw.stock.view.component;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import org.antlr.v4.runtime.misc.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jtw.stock.calculator.StockCalculator;
import jtw.stock.main.StockApplicationSettings;
import jtw.stock.model.registry.StockSymbolNameRegistry;
import jtw.util.IntegerUtil;

public class PrefCustomIndexDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Log log = LogFactory.getLog(PrefCustomIndexDialog.class);

	public static final int NO_OPTION = JOptionPane.NO_OPTION;
	
	public static final int YES_OPTION = JOptionPane.YES_OPTION;
	
	private JButton buttonOk = new JButton("OK");
	private JButton buttonCancel = new JButton("Cancel");
	
	private int buttonPressed = NO_OPTION;
	
	private List<Pair<String, Integer>> indexContentList;
	
	/**
	 * The model for the table on the right side (the content of the custom index).
	 */
	private DefaultTableModel rightModel;

	public PrefCustomIndexDialog(JFrame parent, String title, boolean modal, Class<? extends StockCalculator> indicatorClazz) {
		super(parent, title, modal);
	}
	
	public PrefCustomIndexDialog(JFrame parent, String title, boolean modal) {
		this(parent, title, modal, null);
	}
	
	public PrefCustomIndexDialog(String title, boolean modal) {
		this(StockApplicationSettings.getMainFrame(), title, modal);
	}
	
	public void showDialog() {
		init();
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		pack();
		setLocationRelativeTo(getParent());
		setVisible(true);
	}
	
	public void setIndexContentList(List<Pair<String, Integer>> indexContainList) {
		this.indexContentList = indexContainList;
	}
	
	public List<Pair<String, Integer>> getInput() {
		List<Pair<String, Integer>> stockQuantityPairList = new ArrayList<>();
		for (int i = 0; i < rightModel.getRowCount(); i++) {
			String stockName = rightModel.getValueAt(i, 0).toString();
			Integer quantity = IntegerUtil.toInt(rightModel.getValueAt(i, 1), 1);
			stockQuantityPairList.add(new Pair<String, Integer>(stockName, quantity));
		}
		return stockQuantityPairList;
	}
	
	private void init() {
		JPanel main = new JPanel(new BorderLayout());

		JPanel center = initCenter();
		JPanel south = initSouth();
		main.add(center, BorderLayout.CENTER);
		main.add(south, BorderLayout.SOUTH);
		
		add(main);
	}
	
	private JPanel initCenter() {		
		JPanel center = new JPanel(new GridBagLayout());
		
		List<String> leftContent = StockApplicationSettings.loadAlreadyAvailableList(".csv");
		List<Pair<String, Integer>> rightContent = indexContentList;
		
		DefaultTableModel leftModel = new DefaultTableModel() {
			/**
			* 
			*/
			private static final long serialVersionUID = 1L;

			@Override
			public Class<?> getColumnClass(int columnIndex) {
				return String.class;
			}
			
			@Override
			public boolean isCellEditable(int row, int col) {
				return false;
			}
		};
		leftModel.addColumn("Stock");
		
		rightModel = new DefaultTableModel() {
			/**
			* 
			*/
			private static final long serialVersionUID = 1L;
			
			@Override
			public Class<?> getColumnClass(int columnIndex) {
				switch (columnIndex) {
					case 0:
						return String.class;
					case 1:
						return Integer.class;
				}
				return String.class;
			}
			
			@Override
			public boolean isCellEditable(int row, int col) {
				if (col == 0) {
					return false;
				}
				return true;
			}
		};
		rightModel.addColumn("Stock");
		rightModel.addColumn("Quantity");
		
		// init the models
		for (Pair<String, Integer> p : rightContent) {
			rightModel.addRow(new Object[] {p.a, p.b});
			boolean isRemoved = leftContent.remove(p.a);

			if (log.isDebugEnabled()) {
				log.debug("added to right: " + p);
				log.debug("removed from left: " + isRemoved);
			}
		}
		for (String s : leftContent) {
			leftModel.addRow(new String[] {s});
			
			if (log.isDebugEnabled()) {
				log.debug("added to left: " + s);
			}
		}
		
		DefaultTableCellRenderer renderer = new DefaultTableCellRenderer() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
				if (value != null) {
					value = StockSymbolNameRegistry.getInstance().getCompany(value.toString()) + " (" + value + ")";
				}
				super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
				if (!hasFocus()) {
					setBorder(new EmptyBorder(1, 6, 1, 6));
				}
				return this;
			}
		};
		
		JTable leftList = new JTable(leftModel);
		leftList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		leftList.setDefaultRenderer(String.class, renderer);
		
		JTable rightList = new JTable(rightModel);
		rightList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		rightList.setDefaultRenderer(String.class, renderer);

		JScrollPane leftListScroller = new JScrollPane(leftList);
		JScrollPane rightListScroller = new JScrollPane(rightList);
		
		JButton moveLeftButton = new JButton("<<");
		JButton moveRightButton = new JButton(">>");
		
		moveLeftButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int[] selectedRows = rightList.getSelectedRows();
				for (int i = selectedRows.length - 1; i >= 0; i--) {
					String s = rightModel.getValueAt(selectedRows[i], 0).toString();
					rightModel.removeRow(selectedRows[i]);
					leftModel.addRow(new Object[] {s, 1});
				}
			}
			
		});

		moveRightButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int[] selectedRows = leftList.getSelectedRows();
				for (int i = selectedRows.length - 1; i >= 0; i--) {
					String s = leftModel.getValueAt(selectedRows[i], 0).toString();
					leftModel.removeRow(selectedRows[i]);
					rightModel.addRow(new Object[] {s, 1});
				}
			}
			
		});
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));
		buttonPanel.add(moveLeftButton);
		buttonPanel.add(moveRightButton);

		GridBagConstraints leftConstraint = new GridBagConstraints();
		leftConstraint.fill = GridBagConstraints.BOTH;
		leftConstraint.gridx = 0;
		leftConstraint.gridy = 0;
		leftConstraint.weightx = 10;
		leftConstraint.weighty = 100;

		GridBagConstraints centerConstraint = new GridBagConstraints();
		centerConstraint.gridx = 1;
		centerConstraint.gridy = 0;
		centerConstraint.weightx = 1;

		GridBagConstraints rightConstraint = new GridBagConstraints();
		rightConstraint.fill = GridBagConstraints.BOTH;
		rightConstraint.gridx = 2;
		rightConstraint.gridy = 0;
		rightConstraint.weightx = 10;
		rightConstraint.weighty = 100;
		
		center.add(leftListScroller, leftConstraint);
		center.add(buttonPanel, centerConstraint);
		center.add(rightListScroller, rightConstraint);
		
		return center;
	}

	private JPanel initSouth() {
		buttonCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				buttonPressed = NO_OPTION;
				setVisible(false);
				dispose();
			}
		});
		
		ActionListener okAction = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				buttonPressed = YES_OPTION;
				setVisible(false);
				dispose();
			}
		};
		
		buttonOk.addActionListener(okAction);
		
		JPanel south = new JPanel(new FlowLayout(FlowLayout.CENTER));
		south.add(buttonOk);
		south.add(buttonCancel);

		return south;
	}

	/**
	 * Returns the pressed button.
	 * 
	 * @return The returned button.
	 */
	public int getButtonPressed() {
		return buttonPressed;
	}
	
}
