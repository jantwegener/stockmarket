package jtw.stock.view.component;

import static jtw.stock.view.component.ComponentConstant.COMPARISON_HISTORY_FILE_NAME;

import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jtw.stock.model.CustomSearchFormula;
import jtw.stock.view.component.SearchPanel.ExpandablePanel;

/**
 * The dialog for the ranking.
 * 
 * @author Jan-Thierry Wegener
 */
public class RankingDialog extends StockDialog {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Log LOGGER = LogFactory.getLog(RankingDialog.class);
	
	private SearchPanel comparisonPanel;
	
	public RankingDialog(String title, boolean modal) {
		super(title, modal);
	}
	
	@Override
	protected JPanel initCenter() {
		JPanel searchPanel = new JPanel();
		
		comparisonPanel = new SearchPanel(COMPARISON_HISTORY_FILE_NAME);
		comparisonPanel.setBorder(BorderFactory.createTitledBorder("Buy Comparison"));
		
		searchPanel.add(comparisonPanel);
		
		return searchPanel;
	}
	
	public CustomSearchFormula getSelectedFormula() {
		CustomSearchFormula formula = comparisonPanel.getSelectedFormula();
		LOGGER.debug("Selected formula: '" + formula + "'");
		return formula;
	}
	
	public String getInput() {
		String input = comparisonPanel.getInput();
		LOGGER.debug("Comparison input: '" + input + "'");
		return input;
	}
	
	public List<ExpandablePanel> getInputParameterList() {
		List<ExpandablePanel> list = comparisonPanel.getInputParameterList();
		LOGGER.debug("Parameter list: '" + list + "'");
		return list;
	}
	
}
