package jtw.stock.view.component;

import static jtw.stock.view.component.ComponentConstant.SEARCH_HISTORY_FILE_NAME;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jtw.stock.main.StockApplicationSettings;
import jtw.stock.view.component.SearchPanel.ExpandablePanel;
import jtw.util.DateUtil;
import jtw.util.DoubleUtil;

public class BacktestingDialog extends StockDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Log LOGGER = LogFactory.getLog(BacktestingDialog.class);
	
	/**
	 * Selection of current day closing price structure.
	 */
	public static final int BACKTESTING_PRICE_STRUCTURE_THIS_DAY_CLOSING = 1;
	/**
	 * Selection of next day opening price structure.
	 */
	public static final int BACKTESTING_PRICE_STRUCTURE_NEXT_DAY_OPENING = 2;
	/**
	 * Selection of next day closing price structure.
	 */
	public static final int BACKTESTING_PRICE_STRUCTURE_NEXT_DAY_CLOSING = 3;
	/**
	 * Selection of current day half open and close price structure. This means price = (O + C) / 2,
	 * where O is the opening price and C is the closing price.
	 */
	public static final int BACKTESTING_PRICE_STRUCTURE_HALF_OPEN_CLOSE = 4;
	/**
	 * Selection of current day half high and low price structure. This means price = (H + L) / 2,
	 * where H is the high and L is the low.
	 */
	public static final int BACKTESTING_PRICE_STRUCTURE_HALF_HIGH_LOW = 8;
	
	/**
	 * Selection how bankruptcy is handled.
	 */
	public static final int HANDLE_BANKRUPTCY_STOP = 1;
	/**
	 * Selection how bankruptcy is handled.
	 */
	public static final int HANDLE_BANKRUPTCY_ADD_START_MONEY = 2;
	
	protected SearchPanel buyPanel;
	protected SearchPanel sellPanel;

	/**
	 * The start money.
	 */
	private JTextField moneyTextField;
	/**
	 * The fees for a transaction.
	 */
	private JTextField transactionFeeTextField;
	
	/**
	 * The start date of the simulation.
	 */
	private JTextField dateStartTextField;
	/**
	 * The end date of the simulation.
	 */
	private JTextField dateEndTextField;
	
	/**
	 * Text field for the custom title.
	 */
	private JTextField customTitleTextField;

	// Buy price structure
	private JRadioButton priceStructureThisDayClosingBuy;
	private JRadioButton priceStructureNextDayOpeningBuy;
	private JRadioButton priceStructureNextDayClosingBuy;
	private JRadioButton priceStructureHalfOpenCloseBuy;
	private JRadioButton priceStructureHalfHighLowBuy;
	// Sell price structure
	private JRadioButton priceStructureThisDayClosingSell;
	private JRadioButton priceStructureNextDayOpeningSell;
	private JRadioButton priceStructureNextDayClosingSell;
	private JRadioButton priceStructureHalfOpenCloseSell;
	private JRadioButton priceStructureHalfHighLowSell;

	private JRadioButton handleBankruptcyStop;
	private JRadioButton handleBankruptcyAddStartMoney;
	
	private JCheckBox isSameDaySellAllowedCheckBox;
	
	public BacktestingDialog(JFrame parent, String title, boolean modal) {
		super(parent, title, modal);
		
		initPanel();
	}

	public BacktestingDialog(String title, boolean modal) {
		this(StockApplicationSettings.getMainFrame(), title, modal);
	}
	
	public boolean isNewFormula() {
		return buyPanel.isNewFormula();
	}
	
	@Override
	protected void initPanel() {
		JPanel main = new JPanel(new BorderLayout(10, 5));

		JPanel south = initSouth();
		
		JPanel center = initCenter();
		
		if (center != null) {
			main.add(center, BorderLayout.CENTER);
		}
		if (south != null) {
			main.add(south, BorderLayout.SOUTH);
		}
		
		add(main);
	}
	
	protected JPanel initSearchPanel() {
		buyPanel = new SearchPanel(SEARCH_HISTORY_FILE_NAME);
		buyPanel.setBorder(BorderFactory.createTitledBorder("Buy"));
		
		sellPanel = new SearchPanel(SEARCH_HISTORY_FILE_NAME);
		sellPanel.setBorder(BorderFactory.createTitledBorder("Sell"));
		
		JPanel searchPanel = new JPanel(new GridLayout(1, 2));
		searchPanel.add(buyPanel);
		searchPanel.add(sellPanel);
		
		return searchPanel;
	}
	
	@Override
	protected JPanel initCenter() {
		JPanel searchPanel = initSearchPanel();
		
		customTitleTextField = new JTextField("", 50);
		JLabel customTitleLabel = new JLabel("Custom Title");
		customTitleLabel.setLabelFor(customTitleTextField);
		
		JPanel customTitlePanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
		customTitlePanel.add(customTitleLabel);
		customTitlePanel.add(customTitleTextField);
		
		moneyTextField = new JTextField("1000", 10);
		JLabel moneyLabel = new JLabel("Start-Money");
		moneyLabel.setLabelFor(moneyTextField);

		JPanel moneyPanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
		moneyPanel.add(moneyLabel);
		moneyPanel.add(moneyTextField);
		
		transactionFeeTextField = new JTextField("0", 10);
		JLabel transactionFeeLabel = new JLabel("Transaction Fee");
		transactionFeeLabel.setLabelFor(transactionFeeTextField);

		JPanel transactionFeePanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
		transactionFeePanel.add(transactionFeeLabel);
		transactionFeePanel.add(transactionFeeTextField);
		
		dateStartTextField = new JTextField("", 10);
		JLabel dateStartLabel = new JLabel("Start date (dd.mm.yyyy)");
		dateStartLabel.setLabelFor(dateStartTextField);
		dateEndTextField = new JTextField("", 10);
		JLabel dateEndLabel = new JLabel("End date (dd.mm.yyyy)");
		dateEndLabel.setLabelFor(dateEndTextField);
		
		isSameDaySellAllowedCheckBox = new JCheckBox("Allow same day sell");
		isSameDaySellAllowedCheckBox.setToolTipText("Allows selling the stocks the same day that they were bought.");
		isSameDaySellAllowedCheckBox.setSelected(true);
		
		JPanel isSameDaySellAllowedPanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
		isSameDaySellAllowedPanel.add(isSameDaySellAllowedCheckBox);
		
		JPanel datePanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
		datePanel.add(dateStartLabel);
		datePanel.add(dateStartTextField);
		datePanel.add(dateEndLabel);
		datePanel.add(dateEndTextField);
		
		
		JPanel priceStructurePanelBuy = new JPanel(new GridLayout(3, 2));
		priceStructurePanelBuy.setBorder(BorderFactory.createTitledBorder("Price Structure (Buy)"));
		priceStructureThisDayClosingBuy = new JRadioButton("Closing price");
		priceStructureThisDayClosingBuy.setToolTipText("Current day closing price");
		priceStructureNextDayOpeningBuy = new JRadioButton("Next day opening price");
		priceStructureNextDayOpeningBuy.setToolTipText("The opening price of the next day");
		priceStructureNextDayClosingBuy = new JRadioButton("Next day closing price");
		priceStructureNextDayClosingBuy.setToolTipText("The closing price of the next day");
		priceStructureHalfOpenCloseBuy = new JRadioButton("Half open and close");
		priceStructureHalfOpenCloseBuy.setToolTipText("(Open + Close) / 2");
		priceStructureHalfHighLowBuy = new JRadioButton("Half high and low");
		priceStructureHalfHighLowBuy.setToolTipText("(High + Low) / 2");
		
		ButtonGroup priceStructureGroupBuy = new ButtonGroup();
		priceStructureGroupBuy.add(priceStructureThisDayClosingBuy);
		priceStructureGroupBuy.add(priceStructureNextDayOpeningBuy);
		priceStructureGroupBuy.add(priceStructureNextDayClosingBuy);
		priceStructureGroupBuy.add(priceStructureHalfOpenCloseBuy);
		priceStructureGroupBuy.add(priceStructureHalfHighLowBuy);

		// default selection
		priceStructureThisDayClosingBuy.setSelected(true);
		
		priceStructurePanelBuy.add(priceStructureThisDayClosingBuy);
		priceStructurePanelBuy.add(priceStructureHalfOpenCloseBuy);
		priceStructurePanelBuy.add(priceStructureNextDayOpeningBuy);
		priceStructurePanelBuy.add(priceStructureHalfHighLowBuy);
		priceStructurePanelBuy.add(priceStructureNextDayClosingBuy);
		

		JPanel priceStructurePanelSell = new JPanel(new GridLayout(3, 2));
		priceStructurePanelSell.setBorder(BorderFactory.createTitledBorder("Price Structure (Sell)"));
		priceStructureThisDayClosingSell = new JRadioButton("Closing price");
		priceStructureThisDayClosingSell.setToolTipText("Current day closing price");
		priceStructureNextDayOpeningSell = new JRadioButton("Next day opening price");
		priceStructureNextDayOpeningSell.setToolTipText("The opening price of the next day");
		priceStructureNextDayClosingSell = new JRadioButton("Next day closing price");
		priceStructureNextDayClosingSell.setToolTipText("The closing price of the next day");
		priceStructureHalfOpenCloseSell = new JRadioButton("Half open and close");
		priceStructureHalfOpenCloseSell.setToolTipText("(Open + Close) / 2");
		priceStructureHalfHighLowSell = new JRadioButton("Half high and low");
		priceStructureHalfHighLowSell.setToolTipText("(High + Low) / 2");
		
		ButtonGroup priceStructureGroupSell = new ButtonGroup();
		priceStructureGroupSell.add(priceStructureThisDayClosingSell);
		priceStructureGroupSell.add(priceStructureNextDayOpeningSell);
		priceStructureGroupSell.add(priceStructureNextDayClosingSell);
		priceStructureGroupSell.add(priceStructureHalfOpenCloseSell);
		priceStructureGroupSell.add(priceStructureHalfHighLowSell);

		// default selection
		priceStructureThisDayClosingSell.setSelected(true);
		
		priceStructurePanelSell.add(priceStructureThisDayClosingSell);
		priceStructurePanelSell.add(priceStructureHalfOpenCloseSell);
		priceStructurePanelSell.add(priceStructureNextDayOpeningSell);
		priceStructurePanelSell.add(priceStructureHalfHighLowSell);
		priceStructurePanelSell.add(priceStructureNextDayClosingSell);
		
		JPanel priceStructurePanel = new JPanel(new GridLayout(1, 2));
		priceStructurePanel.add(priceStructurePanelBuy);
		priceStructurePanel.add(priceStructurePanelSell);
		
		// bankruptcy
		JPanel handleBankruptcyPanel = new JPanel(new GridLayout(2, 1));
		handleBankruptcyPanel.setBorder(BorderFactory.createTitledBorder("Handle Bankruptcy"));
		handleBankruptcyStop = new JRadioButton("Stop");
		handleBankruptcyStop.setToolTipText("Stops the simulation when bankrupt");
		handleBankruptcyAddStartMoney = new JRadioButton("Add start money");
		handleBankruptcyAddStartMoney.setToolTipText("Adds the start money to the bank account when bankrupt");

		ButtonGroup handleBankruptcyGroup = new ButtonGroup();
		handleBankruptcyGroup.add(handleBankruptcyStop);
		handleBankruptcyGroup.add(handleBankruptcyAddStartMoney);
		
		// default selection
		handleBankruptcyStop.setSelected(true);
		
		handleBankruptcyPanel.add(handleBankruptcyStop);
		handleBankruptcyPanel.add(handleBankruptcyAddStartMoney);
		
		
		JPanel southPanel = new JPanel();
		southPanel.setLayout(new BoxLayout(southPanel, BoxLayout.PAGE_AXIS));
		southPanel.setBorder(BorderFactory.createTitledBorder("Settings"));
		southPanel.add(customTitlePanel);
		southPanel.add(moneyPanel);
		southPanel.add(transactionFeePanel);
		southPanel.add(datePanel);
		southPanel.add(isSameDaySellAllowedPanel);
		southPanel.add(priceStructurePanel);
		southPanel.add(handleBankruptcyPanel);
		
		JPanel mainPanel = new JPanel(new BorderLayout());
		mainPanel.add(searchPanel, BorderLayout.CENTER);
		mainPanel.add(southPanel, BorderLayout.SOUTH);
		
		return mainPanel;
	}
	
	public double getMoneyInput() {
		return DoubleUtil.toDouble(moneyTextField.getText(), 1000d);
	}
	
	public double getTransactionFeeInput() {
		return DoubleUtil.toDouble(transactionFeeTextField.getText(), 0d);
	}
	
	public Calendar getStartDateInput() {
		// default: far back in the past
		return DateUtil.toCalendar(dateStartTextField.getText(), "dd.MM.yyyy", "01.01.1800");
	}

	public Calendar getEndDateInput() {
		// default: far into the future
		return DateUtil.toCalendar(dateEndTextField.getText(), "dd.MM.yyyy", "01.01.2525");
	}
	
	public boolean isSameDaySellAllowed() {
		return isSameDaySellAllowedCheckBox.isSelected();
	}

	public String getBuyInput() {
		return buyPanel.getInput();
	}

	public String getSellInput() {
		return sellPanel.getInput();
	}

	public List<ExpandablePanel> getBuyInputParameterList() {
		return buyPanel.getInputParameterList();
	}
	
	public List<ExpandablePanel> getSellInputParameterList() {
		return sellPanel.getInputParameterList();
	}
	
	public int getSelectedPriceStructureBuy() {
		int retval = -1;
		
		if (priceStructureThisDayClosingBuy.isSelected()) {
			retval = BACKTESTING_PRICE_STRUCTURE_THIS_DAY_CLOSING;
		} else if (priceStructureNextDayOpeningBuy.isSelected()) {
			retval = BACKTESTING_PRICE_STRUCTURE_NEXT_DAY_OPENING;
		} else if (priceStructureNextDayClosingBuy.isSelected()) {
			retval = BACKTESTING_PRICE_STRUCTURE_NEXT_DAY_CLOSING;
		} else if (priceStructureHalfOpenCloseBuy.isSelected()) {
			retval = BACKTESTING_PRICE_STRUCTURE_HALF_OPEN_CLOSE;
		} else if (priceStructureHalfHighLowBuy.isSelected()) {
			retval = BACKTESTING_PRICE_STRUCTURE_HALF_HIGH_LOW;
		}
		
		if (retval < 0) {
			LOGGER.error("Unknown selected price structure.");
		}
		
		return retval;
	}

	public int getSelectedPriceStructureSell() {
		int retval = -1;
		
		if (priceStructureThisDayClosingSell.isSelected()) {
			retval = BACKTESTING_PRICE_STRUCTURE_THIS_DAY_CLOSING;
		} else if (priceStructureNextDayOpeningSell.isSelected()) {
			retval = BACKTESTING_PRICE_STRUCTURE_NEXT_DAY_OPENING;
		} else if (priceStructureNextDayClosingSell.isSelected()) {
			retval = BACKTESTING_PRICE_STRUCTURE_NEXT_DAY_CLOSING;
		} else if (priceStructureHalfOpenCloseSell.isSelected()) {
			retval = BACKTESTING_PRICE_STRUCTURE_HALF_OPEN_CLOSE;
		} else if (priceStructureHalfHighLowSell.isSelected()) {
			retval = BACKTESTING_PRICE_STRUCTURE_HALF_HIGH_LOW;
		}
		
		if (retval < 0) {
			LOGGER.error("Unknown selected price structure.");
		}
		
		return retval;
	}

	public int getSelectedHandleBankruptcy() {
		int retval = -1;
		
		if (handleBankruptcyStop.isSelected()) {
			retval = HANDLE_BANKRUPTCY_STOP;
		} else if (handleBankruptcyAddStartMoney.isSelected()) {
			retval = HANDLE_BANKRUPTCY_ADD_START_MONEY;
		}
		
		if (retval < 0) {
			LOGGER.error("Unknown selected handle bankruptcy.");
		}
		
		return retval;
	}

	public String getCustomTitle() {
		return customTitleTextField.getText();
	}

	@Override
	protected URL getPathToHelp() {
		try {
			File f = new File("doc/HowtoStockLang.html");
			return f.toURI().toURL();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	protected String getHelpMenuItemName() {
		return "How-To Stock Lang";
	}
	
}
