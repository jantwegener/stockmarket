package jtw.stock.view.component;

/**
 * The {@link DialogButtonListener} has to be used when a dialog shall not be
 * modal. The listener waits for the user to press a button and decide what to
 * do with the dialog.
 * 
 * @author Jan-Thierry Wegener
 */
public interface DialogButtonListener {
	
	/**
	 * This method is called when a button is pressed.
	 * 
	 * @param button
	 */
	public void buttonPressed(int button);
	
}
