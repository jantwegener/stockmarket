package jtw.stock.view.component;

import java.awt.BasicStroke;
import java.awt.Color;
import java.util.List;

import javax.swing.JFrame;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.data.xy.XYDataset;

import jtw.stock.main.StockApplicationSettings;
import jtw.stock.util.ListXYDatasetWrapper;
import jtw.stock.view.StockView;
import jtw.stock.view.action.file.IPostOpenAction;
import jtw.stock.view.component.BacktestingResult.BuySellMark;
import jtw.stock.view.component.BacktestingResult.BuySellMoneyDevelopment;
import jtw.util.ObjectToDouble;

public class BacktestingResultDialog extends TableStockDialog {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Log LOGGER = LogFactory.getLog(BacktestingResultDialog.class);
	
	private List<BacktestingResult> resultList;
	
	public BacktestingResultDialog(JFrame parent, String title, List<BacktestingResult> resultList) {
		super(parent, title, false, 10, 5, resultList);
	}
	
	public BacktestingResultDialog(String title, List<BacktestingResult> resultList) {
		this(StockApplicationSettings.getMainFrame(), title, resultList);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	protected void init(Object[] additionalValues) {
		this.resultList = (List<BacktestingResult>) additionalValues[0];
	}
	
	@Override
	protected Object[][] getTableData() {
		String[][] data = new String[resultList.size()][];
		
		for (int i = 0; i < resultList.size(); i++) {
			BacktestingResult result = resultList.get(i);
			String stockName = result.getStockName();
			String restValue = result.getStockNum() > 0 ? String.format("%d (%.2f)", result.getStockNum(), result.getStockNum() * result.getStock().getClosing()) : "-";
			String numTransactions = String.valueOf(result.getBuyList().size() + result.getSellList().size());
			double finalMoney = result.getMoney() + result.getStockNum() * result.getStock().getClosing();
			String finalMoneyStr = String.format("%.2f", finalMoney);
			double lossProfit = finalMoney - result.getTotalMoneyUsed();
			String lossProfitStr = String.format("%.2f", lossProfit);
			
			double buyAndHold = finalMoney;
			if (result.getBuyList().size() > 0) {
				buyAndHold = result.getBuyList().get(0).getStockNum() * result.getStock().getClosing() - result.getBuyList().get(0).getStockNum() * result.getBuyList().get(0).getPrice() - result.getBuyList().get(0).getFee();
			}
			String buyAndHoldStr = String.format("%.2f (%.2f)", buyAndHold, lossProfit - buyAndHold);
			data[i] = new String[] {stockName, String.format("%.2f", result.getMoney()), restValue, numTransactions, String.format("%.2f", result.getTotalMoneyUsed()), finalMoneyStr, lossProfitStr, buyAndHoldStr};
		}
		return data;
	}
	
	@Override
	protected String[] getColumnNames() {
		return new String[] {"Stock Name", "Money", "Stocks (Value)", "Total Number of Transactions", "Total Money Added", "Current Money", "Profit / Loss (PaL)", "Buy & Hold (PaL)"};
	}
	
	@Override
	protected IPostOpenAction getPostOpenAction(String stockName) {
		IPostOpenAction postAction = new IPostOpenAction() {
			
			@Override
			public void performPostAction(String stockName) {
				XYPlot mainPlot = StockView.getInstance().getMainPlot();
				BacktestingResult result = null;
				for (BacktestingResult br : resultList) {
					if (br.getStockName().equals(stockName)) {
						result = br;
						break;
					}
				}

				for (BuySellMark bsm : result.getBuyList()) {
					ValueMarker marker = new ValueMarker(bsm.getDate().getTimeInMillis(), Color.GREEN, new BasicStroke(1));
					mainPlot.addDomainMarker(marker);
				}
				for (BuySellMark bsm : result.getSellList()) {
					ValueMarker marker = new ValueMarker(bsm.getDate().getTimeInMillis(), Color.RED, new BasicStroke(1));
					mainPlot.addDomainMarker(marker);
				}
				
				ObjectToDouble<BuySellMoneyDevelopment> xotd = new ObjectToDouble<BuySellMoneyDevelopment>() {
					@Override
					public double objToDouble(BuySellMoneyDevelopment o) {
						return o.getDate().getTimeInMillis();
					}
				};
				ObjectToDouble<BuySellMoneyDevelopment> yotd = new ObjectToDouble<BuySellMoneyDevelopment>() {
					@Override
					public double objToDouble(BuySellMoneyDevelopment o) {
						return o.getMoney();
					}
				};
				XYDataset ds = new ListXYDatasetWrapper<BuySellMoneyDevelopment>(result.getStockName() + "(money)", result.getMoneyDevelopmentList(), xotd, yotd);
				
				XYPlot subPlot = new XYPlot(ds, new DateAxis(), new HighlightValueAxis(), new StandardXYItemRenderer());
				StockView.getInstance().addPlot("Money Development", subPlot, true);
			}
		};
		return postAction;
	}
	
	
}
