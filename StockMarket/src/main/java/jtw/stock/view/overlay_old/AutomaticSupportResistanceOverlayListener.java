package jtw.stock.view.overlay_old;

import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.panel.AbstractOverlay;
import org.jfree.chart.panel.Overlay;
import org.jfree.chart.plot.XYPlot;
import org.jfree.ui.RectangleEdge;

import jtw.stock.view.StockView;

public class AutomaticSupportResistanceOverlayListener extends ClickOverlayAdapter {

	@Override
	protected Overlay getOverlay() {
		setDatasetDependend(false);
		setSingleClick(true);
		return new SupportResistanceOverlay();
	}
	
	private class SupportResistanceOverlay extends AbstractOverlay implements Overlay {

		@Override
		public void paintOverlay(Graphics2D g2, ChartPanel chartPanel) {
			if (getFrom() != null) {
				XYPlot plot = StockView.getInstance().getMainPlot();
				Rectangle2D dataArea = chartPanel.getScreenDataArea();

				ValueAxis xAxis = plot.getDomainAxis();
				RectangleEdge xAxisEdge = plot.getDomainAxisEdge();

				ValueAxis yAxis = plot.getRangeAxis();
				RectangleEdge yAxisEdge = plot.getRangeAxisEdge();

				double fx = xAxis.valueToJava2D(getFrom().getX(), dataArea, xAxisEdge);
				double fy = yAxis.valueToJava2D(getFrom().getY(), dataArea, yAxisEdge);

				double tx = xAxis.valueToJava2D(getTo().getX(), dataArea, xAxisEdge);
				double ty = yAxis.valueToJava2D(getTo().getY(), dataArea, yAxisEdge);
			}
		}
		
	}

}
