package jtw.stock.view.overlay_old;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import javax.swing.AbstractButton;

import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.panel.Overlay;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.general.DatasetUtilities;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.RectangleEdge;

import jtw.stock.main.StockApplicationSettings;
import jtw.stock.view.StockView;

public abstract class OverlayAdapter implements ActionListener, ChartMouseListener {

	private Overlay overlay = null;
	
	private boolean datasetDependend = true;
	
	public boolean isDatasetDependend() {
		return datasetDependend;
	}
	
	public void setDatasetDependend(boolean b) {
		this.datasetDependend = b;
	}
	
	protected Point2D computePoint(ChartMouseEvent event) {
        return computePoint(event.getTrigger().getX(), event.getTrigger().getY());
	}
	

	protected Point2D computePoint(double mx, double my) {
        Rectangle2D dataArea = StockView.getInstance().getScreenDataArea();
        XYPlot plot = StockView.getInstance().getMainPlot();
        ValueAxis xAxis = plot.getDomainAxis();
        double x = xAxis.java2DToValue(mx, dataArea, RectangleEdge.BOTTOM);
        // make the crosshairs disappear if the mouse is out of range
        if (!xAxis.getRange().contains(x)) { 
            x = Double.NaN;
        }
        double y = 0d;
        if (isDatasetDependend()) {
        	XYDataset ds = getDataset();
        	
        	y = DatasetUtilities.findYValue(ds, 0, x);
        } else {
        	ValueAxis yAxis = plot.getRangeAxis();
        	y = yAxis.java2DToValue(my, dataArea, RectangleEdge.LEFT);
        }
        Point2D p = new Point2D.Double(x, y);
        return p;
	}

	protected XYDataset getDataset() {
		XYDataset ds;
		if (StockApplicationSettings.isOpenReferenceDataset()) {
			ds = StockView.getInstance().getOpenDataset();
		} else if (StockApplicationSettings.isCloseReferenceDataset()) {
			ds = StockView.getInstance().getCloseDataset();
		} else if (StockApplicationSettings.isHighReferenceDataset()) {
			ds = StockView.getInstance().getHighDataset();
		} else if (StockApplicationSettings.isLowReferenceDataset()) {
			ds = StockView.getInstance().getLowDataset();
		} else {
			// default
			ds = StockView.getInstance().getCloseDataset();
		}
		return ds;
	}

	@Override
	public void chartMouseClicked(ChartMouseEvent event) {
	}

	@Override
	public void chartMouseMoved(ChartMouseEvent event) {
	}

	/**
	 * Should create a new overlay for each call.
	 * 
	 * @return The overlay.
	 */
	protected abstract Overlay getOverlay();
	
	/**
	 * Performs an additional action when the adapter is activated.
	 * 
	 * @return true when the overlay shall be shown, false otherwise.
	 */
	protected boolean activate() {
		return true;
	}

	/**
	 * Performs an additional action when the adapter is deactivated.
	 * 
	 * @return true when the overlay shall be shown, false otherwise.
	 */
	protected boolean deactivate() {
		return true;
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		AbstractButton b = (AbstractButton) ae.getSource();
		if (b.isSelected()) {
			if (activate()) {
				overlay = getOverlay();
				StockView.getInstance().addOverlay(overlay);
				StockView.getInstance().addChartMouseListener(this);
			}
		} else {
			if (deactivate()) {
				StockView.getInstance().removeOverlay(overlay);
				StockView.getInstance().removeChartMouseListener(this);
			}
		}
	}

}
