package jtw.stock.view.overlay_old;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.Rectangle2D;
import java.text.NumberFormat;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.panel.AbstractOverlay;
import org.jfree.chart.panel.Overlay;
import org.jfree.chart.plot.XYPlot;
import org.jfree.ui.RectangleEdge;

import jtw.stock.view.StockView;

public class FibonacciRetracementOverlayListener extends ClickOverlayAdapter {

	@Override
	protected Overlay getOverlay() {
		setDatasetDependend(false);
		return new FibonacciOverlay();
	}

	private class FibonacciOverlay extends AbstractOverlay implements Overlay {

		@Override
		public void paintOverlay(Graphics2D g2, ChartPanel chartPanel) {
			if (getFrom() != null && getTo() != null) {
				XYPlot plot = StockView.getInstance().getMainPlot();
				Rectangle2D dataArea = chartPanel.getScreenDataArea();

				ValueAxis xAxis = plot.getDomainAxis();
				RectangleEdge xAxisEdge = plot.getDomainAxisEdge();

				ValueAxis yAxis = plot.getRangeAxis();
				RectangleEdge yAxisEdge = plot.getRangeAxisEdge();

				double fx = xAxis.valueToJava2D(getFrom().getX(), dataArea, xAxisEdge);
				double fy = yAxis.valueToJava2D(getFrom().getY(), dataArea, yAxisEdge);

				double tx = xAxis.valueToJava2D(getTo().getX(), dataArea, xAxisEdge);
				double ty = yAxis.valueToJava2D(getTo().getY(), dataArea, yAxisEdge);

				// 0 %
				double y = fy;
				String ry = NumberFormat.getNumberInstance().format(yAxis.java2DToValue(y, dataArea, yAxisEdge));
				g2.drawLine((int) fx, (int) fy, (int) tx, (int) fy);
				g2.drawString("0%", (int) tx, (int) fy);
				g2.drawString(ry, (int) fx, (int) y);
				
				// 100%
				y = ty;
				ry = NumberFormat.getNumberInstance().format(yAxis.java2DToValue(y, dataArea, yAxisEdge));
				g2.drawLine((int) fx, (int) ty, (int) tx, (int) ty);
				g2.drawString("100%", (int) tx, (int) ty);
				g2.drawString(ry, (int) fx, (int) y);

				Stroke stroke = new BasicStroke(1f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[] {5, 10}, 0);
				g2.setStroke(stroke);
				
				// 38.2%
				y = (0.382 * (ty - fy) + fy);
				ry = NumberFormat.getNumberInstance().format(yAxis.java2DToValue(y, dataArea, yAxisEdge));
				g2.drawLine((int) fx, (int) y, (int) tx, (int) y);
				g2.drawString("38.2%", (int) tx, (int) y);
				g2.drawString(ry, (int) fx, (int) y);
				
				// 50%
				y = (0.5 * (ty - fy) + fy);
				g2.drawLine((int) fx, (int) y, (int) tx, (int) y);
				g2.drawString("50%", (int) tx, (int) y);
				g2.drawString(ry, (int) fx, (int) y);

				// 61.8%
				y = (0.618 * (ty - fy) + fy);
				ry = NumberFormat.getNumberInstance().format(yAxis.java2DToValue(y, dataArea, yAxisEdge));
				g2.drawLine((int) fx, (int) y, (int) tx, (int) y);
				g2.drawString("61.8%", (int) tx, (int) y);
				g2.drawString(ry, (int) fx, (int) y);
			}
		}

	}

}
