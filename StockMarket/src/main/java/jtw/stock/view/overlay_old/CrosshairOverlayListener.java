package jtw.stock.view.overlay_old;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.geom.Point2D;
import java.text.DateFormat;

import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.labels.CrosshairLabelGenerator;
import org.jfree.chart.panel.CrosshairOverlay;
import org.jfree.chart.panel.Overlay;
import org.jfree.chart.plot.Crosshair;

public class CrosshairOverlayListener extends OverlayAdapter {

	private Crosshair xCrosshair = new Crosshair(Double.NaN, Color.GRAY, new BasicStroke(0f));
	private Crosshair yCrosshair = new Crosshair(Double.NaN, Color.GRAY, new BasicStroke(0f));
	
	public CrosshairOverlayListener() {
	}
	
	public void chartMouseMoved(ChartMouseEvent event) {
		updateCrosshair(event);		
	}

	protected void updateCrosshair(ChartMouseEvent event) {
		Point2D p = computePoint(event);
		
        xCrosshair.setValue(p.getX());
        yCrosshair.setValue(p.getY());
	}
	
	@Override
	protected Overlay getOverlay() {
		CrosshairOverlay crosshairOverlay = new CrosshairOverlay();
		xCrosshair.setLabelVisible(true);
		xCrosshair.setLabelGenerator(new CrosshairLabelGenerator() {
			@Override
			public String generateLabel(Crosshair crosshair) {
				return DateFormat.getDateInstance().format(crosshair.getValue());
			}
		});
        yCrosshair.setLabelVisible(true);
        crosshairOverlay.addDomainCrosshair(xCrosshair);
        crosshairOverlay.addRangeCrosshair(yCrosshair);
		return crosshairOverlay;
	}

}
