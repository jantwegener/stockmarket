package jtw.stock.view.overlay_old;

import java.awt.geom.Point2D;

import javax.swing.SwingUtilities;

import org.jfree.chart.ChartMouseEvent;

import jtw.stock.view.StockView;

public abstract class ClickOverlayAdapter extends OverlayAdapter {

	private boolean isFirstClick = true;
	private boolean isSingleClick = false;
	private Point2D from = null;
	private Point2D to = null;
	
	public Point2D getFrom() {
		return from;
	}
	
	public Point2D getTo() {
		return to;
	}
	
	public boolean isSingleClick() {
		return isSingleClick;
	}
	
	/**
	 * Sets or unsets single click character. The normal behavior is that the first click sets the <code>from</code> point, while 
	 * the second click sets the <code>to</code> point.
	 * Single click means that only the <code>from</code> point is set, no matter how often the mouse button is clicked.
	 * 
	 * @param singleClick If <code>true</code> the single click mode is activated, <code>false</code> set the normal behavior.
	 */
	public void setSingleClick(boolean singleClick) {
		isSingleClick = singleClick;
	}
	
	@Override
	public void chartMouseClicked(ChartMouseEvent event) {
		if (SwingUtilities.isMiddleMouseButton(event.getTrigger())) {
			from = null;
			to = null;
			isFirstClick = true;
		} else {
			Point2D p = computePoint(event);
			if (isFirstClick || isSingleClick) {
				from = p;
			} else {
				to = p;
			}
			isFirstClick = !isFirstClick;
		}
	}

	@Override
	public void chartMouseMoved(ChartMouseEvent event) {
		Point2D p = computePoint(event);
		if (!isFirstClick) {
			to = p;
			StockView.getInstance().repaint();
		}
	}

}
