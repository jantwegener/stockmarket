package jtw.stock.view.overlay_old;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.text.NumberFormat;

import javax.swing.JFrame;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.panel.AbstractOverlay;
import org.jfree.chart.panel.Overlay;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.general.DatasetUtilities;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.RectangleEdge;

import jtw.stock.view.StockView;
import jtw.stock.view.component.MultipleTextInputDialog;

public class SellPercentOverlayListener extends OverlayAdapter {
	
	private JFrame parent = null;
	
	private double maxAmountPay = 0d;
	private double additionalCosts = 0d;
	
	public SellPercentOverlayListener(JFrame parent) {
		this.parent = parent;
	}

	@Override
	protected boolean activate() {
		MultipleTextInputDialog d = new MultipleTextInputDialog(parent, "", true, true, "Maximum amount to pay (Euro)", "Additional costs (total)");
		d.showDialog();
		boolean retval = false;
		if (d.getButtonPressed() == MultipleTextInputDialog.YES_OPTION) {
			retval = true;
			String[] input = d.getInput();
			try {
				maxAmountPay = Double.parseDouble(input[0]);
				additionalCosts = Double.parseDouble(input[1]);
			} catch (NumberFormatException nfe) {
				retval = false;
				nfe.printStackTrace();
			}
		}
		return retval;
	}

	@Override
	protected Overlay getOverlay() {
		return new SellOverlay();
	}

	private class SellOverlay extends AbstractOverlay implements Overlay {
		
		private SellOverlay() {
		}

		@Override
		public void paintOverlay(Graphics2D g2, ChartPanel chartPanel) {
			XYPlot plot = StockView.getInstance().getMainPlot();
			Rectangle2D dataArea = chartPanel.getScreenDataArea();

			ValueAxis xAxis = plot.getDomainAxis();
			RectangleEdge xAxisEdge = plot.getDomainAxisEdge();

			ValueAxis yAxis = plot.getRangeAxis();
			RectangleEdge yAxisEdge = plot.getRangeAxisEdge();

			XYDataset ds = getDataset();
			double fx = getDataset().getXValue(0, 0);
			double fy = DatasetUtilities.findYValue(ds, 0, fx);
			
			// correct the cost per share
			double numShare = Math.floor(maxAmountPay / fy);
			double totalCosts = numShare * fy + additionalCosts;
			double grossCostsPerShare = totalCosts / numShare; 
			
			double y = grossCostsPerShare;
			
			Point2D p = new Point2D.Double(xAxis.valueToJava2D(fx, dataArea, xAxisEdge), yAxis.valueToJava2D(y, dataArea, yAxisEdge));
			g2.drawString("0 %", (int) dataArea.getMaxX() - 30, (int) p.getY());
			g2.drawString(NumberFormat.getNumberInstance().format(y), (int) dataArea.getMinX() + 10, (int) p.getY());
			g2.drawLine((int) dataArea.getMinX(), (int) p.getY(), (int) dataArea.getMaxX(), (int) p.getY());

			y = fy + fy * 0.05;
			p = new Point2D.Double(xAxis.valueToJava2D(fx, dataArea, xAxisEdge), yAxis.valueToJava2D(y, dataArea, yAxisEdge));
			g2.drawString("5 %", (int) dataArea.getMaxX() - 30, (int) p.getY());
			g2.drawString(NumberFormat.getNumberInstance().format(y), (int) dataArea.getMinX() + 10, (int) p.getY());
			g2.drawLine((int) dataArea.getMinX(), (int) p.getY(), (int) dataArea.getMaxX(), (int) p.getY());

			y = fy + fy * 0.1;
			p = new Point2D.Double(xAxis.valueToJava2D(fx, dataArea, xAxisEdge), yAxis.valueToJava2D(y, dataArea, yAxisEdge));
			g2.drawString("10 %", (int) dataArea.getMaxX() - 30, (int) p.getY());
			g2.drawString(NumberFormat.getNumberInstance().format(y), (int) dataArea.getMinX() + 10, (int) p.getY());
			g2.drawLine((int) dataArea.getMinX(), (int) p.getY(), (int) dataArea.getMaxX(), (int) p.getY());

			y = fy + fy * 0.15;
			p = new Point2D.Double(xAxis.valueToJava2D(fx, dataArea, xAxisEdge), yAxis.valueToJava2D(y, dataArea, yAxisEdge));
			g2.drawString("15 %", (int) dataArea.getMaxX() - 30, (int) p.getY());
			g2.drawString(NumberFormat.getNumberInstance().format(y), (int) dataArea.getMinX() + 10, (int) p.getY());
			g2.drawLine((int) dataArea.getMinX(), (int) p.getY(), (int) dataArea.getMaxX(), (int) p.getY());

			// -y
			Stroke stroke = new BasicStroke(1f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[] { 5, 10 }, 0);
			g2.setStroke(stroke);

			y = fy - fy * 0.05;
			p = new Point2D.Double(xAxis.valueToJava2D(fx, dataArea, xAxisEdge), yAxis.valueToJava2D(y, dataArea, yAxisEdge));
			g2.drawString("-5 %", (int) dataArea.getMaxX() - 30, (int) p.getY());
			g2.drawString(NumberFormat.getNumberInstance().format(y), (int) dataArea.getMinX() + 10, (int) p.getY());
			g2.drawLine((int) dataArea.getMinX(), (int) p.getY(), (int) dataArea.getMaxX(), (int) p.getY());

			y = fy - fy * 0.1;
			p = new Point2D.Double(xAxis.valueToJava2D(fx, dataArea, xAxisEdge), yAxis.valueToJava2D(y, dataArea, yAxisEdge));
			g2.drawString("-10 %", (int) dataArea.getMaxX() - 30, (int) p.getY());
			g2.drawString(NumberFormat.getNumberInstance().format(y), (int) dataArea.getMinX() + 10, (int) p.getY());
			g2.drawLine((int) dataArea.getMinX(), (int) p.getY(), (int) dataArea.getMaxX(), (int) p.getY());

		}

	}

}
