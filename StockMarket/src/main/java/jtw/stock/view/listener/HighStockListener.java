package jtw.stock.view.listener;

import jtw.stock.control.HighStockController;
import jtw.stock.control.StockController;
import jtw.stock.main.StockApplicationSettings;

public class HighStockListener extends AbstractSelectionListener {

	public HighStockListener() {
	}
	
	@Override
	protected StockController getController() {
		return new HighStockController(StockApplicationSettings.getStockName());
	}

}
