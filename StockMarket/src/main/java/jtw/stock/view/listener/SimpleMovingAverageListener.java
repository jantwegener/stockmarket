package jtw.stock.view.listener;

import jtw.stock.control.SimpleMovingAverageStockController;
import jtw.stock.control.StockController;
import jtw.stock.main.StockApplicationSettings;

public class SimpleMovingAverageListener extends AbstractSelectionListener {

	private int daysAverage;
	
	public SimpleMovingAverageListener(int daysAverage) {
		this.daysAverage = daysAverage;
	}
	
	@Override
	protected StockController getController() {
		return new SimpleMovingAverageStockController(StockApplicationSettings.getStockName(), daysAverage);
	}

}
