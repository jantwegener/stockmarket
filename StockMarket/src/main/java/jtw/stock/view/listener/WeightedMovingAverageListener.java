package jtw.stock.view.listener;

import jtw.stock.control.StockController;
import jtw.stock.control.WeightedMovingAverageStockController;
import jtw.stock.main.StockApplicationSettings;

public class WeightedMovingAverageListener extends AbstractSelectionListener {

	private int daysAverage;
	
	public WeightedMovingAverageListener(int daysAverage) {
		this.daysAverage = daysAverage;
	}
	
	@Override
	protected StockController getController() {
		return new WeightedMovingAverageStockController(StockApplicationSettings.getStockName(), daysAverage);
	}

}
