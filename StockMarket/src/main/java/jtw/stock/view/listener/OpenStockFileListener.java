package jtw.stock.view.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.jfree.data.xy.DefaultXYDataset;

import jtw.stock.io.HistoryReader;
import jtw.stock.io.HistoryReaderFactory;
import jtw.stock.main.StockApplicationSettings;
import jtw.stock.model.Stock;
import jtw.stock.view.StockView;

public class OpenStockFileListener implements ActionListener {

	private final JFrame parentFrame;

	public OpenStockFileListener(JFrame frame) {
		parentFrame = frame;
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		final JFileChooser fc = new JFileChooser(StockApplicationSettings.getBasePath());
		int returnVal = fc.showOpenDialog(parentFrame);

		if (returnVal == JFileChooser.APPROVE_OPTION) {
			String stockNameFile = fc.getSelectedFile().getName();
			if (stockNameFile != null) {
				initStock(stockNameFile);
			}
		}
	}

	private void initStock(String stockNameFile) {
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				StockView.getInstance().reset();
				StockApplicationSettings.setStockName(stockNameFile.substring(0, stockNameFile.lastIndexOf('.')));

				parentFrame.setTitle(stockNameFile.substring(0, stockNameFile.lastIndexOf('.')));

				HistoryReader reader = HistoryReaderFactory.getHistoryReader(stockNameFile);
				try {
					reader.read(stockNameFile);

					List<Stock> stockList;
					if (StockApplicationSettings.isTimeBaseDaily()) {
						stockList = reader.getDailyList();
					} else if (StockApplicationSettings.isTimeBaseWeekly()) {
						stockList = reader.getWeeklyList();
					} else if (StockApplicationSettings.isTimeBaseMonthly()) {
						stockList = reader.getMonthlyList();
					} else {
						// default
						stockList = reader.getDailyList();
					}

					DefaultXYDataset dsOpen = new DefaultXYDataset();
					DefaultXYDataset dsClose = new DefaultXYDataset();
					DefaultXYDataset dsHigh = new DefaultXYDataset();
					DefaultXYDataset dsLow = new DefaultXYDataset();
					DefaultXYDataset dsVolume = new DefaultXYDataset();

					double[][] dataOpen = new double[2][stockList.size()];
					double[][] dataClose = new double[2][stockList.size()];
					double[][] dataHigh = new double[2][stockList.size()];
					double[][] dataLow = new double[2][stockList.size()];
					double[][] dataVolume = new double[2][stockList.size()];

					for (int i = 0; i < stockList.size(); i++) {
						Stock stock = stockList.get(i);
						dataOpen[0][i] = stock.getDate().getTimeInMillis();
						dataOpen[1][i] = stock.getOpening();
						dataClose[0][i] = stock.getDate().getTimeInMillis();
						dataClose[1][i] = stock.getClosing();
						dataHigh[0][i] = stock.getDate().getTimeInMillis();
						dataHigh[1][i] = stock.getHigh();
						dataLow[0][i] = stock.getDate().getTimeInMillis();
						dataLow[1][i] = stock.getLow();
						dataVolume[0][i] = stock.getDate().getTimeInMillis();
						dataVolume[1][i] = stock.getVolume();
					}

					dsOpen.addSeries("ds_open", dataOpen);
					dsClose.addSeries("ds_close", dataClose);
					dsHigh.addSeries("ds_high", dataHigh);
					dsLow.addSeries("ds_low", dataLow);
					dsVolume.addSeries("ds_volume", dataVolume);

					StockView.getInstance().initDataset(dsOpen, dsClose, dsHigh, dsLow, dsVolume);
				} catch (Exception e) {
					e.printStackTrace();

					JOptionPane.showMessageDialog(StockView.getInstance(),
							"Error computing plot: \"" + e.getMessage() + "\"", "Error", JOptionPane.ERROR_MESSAGE);
				}

				StockView.getInstance().resetMenuBar();
			}

		});
		t.start();
	}

}
