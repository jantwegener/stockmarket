package jtw.stock.view.listener;

import jtw.stock.control.StockController;
import jtw.stock.control.VolumeStockController;
import jtw.stock.main.StockApplicationSettings;

public class VolumeListener extends AbstractSelectionListener {

	public VolumeListener() {
	}
	
	@Override
	protected StockController getController() {
		return new VolumeStockController(StockApplicationSettings.getStockName());
	}

}
