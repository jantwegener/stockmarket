package jtw.stock.view.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import jtw.stock.control.StockController;
import jtw.stock.view.StockView;

public abstract class AbstractSelectionListener implements ActionListener, ItemListener {//extends AbstractAction {//implements ActionListener, ItemListener {

	private String plotName = null;
	
	public AbstractSelectionListener() {
	}
	
	/**
	 * Returns a new controller.
	 * 
	 * @return The corresponding controller fully initialized and ready to be used.
	 */
	protected abstract StockController getController();
	
	public void activate() {
		StockController controller = getController();
		plotName = controller.getPlotName();
		controller.computePlot();
	}
	
	protected void deactivate() {
		StockView.getInstance().removePlot(plotName);
	}
	
	@Override
	public void actionPerformed(ActionEvent ae) {
	}

	@Override
	public void itemStateChanged(ItemEvent ie) {
		if (ie.getStateChange() == ItemEvent.SELECTED) {
			activate();
		} else {
			deactivate();
		}
	}

}
