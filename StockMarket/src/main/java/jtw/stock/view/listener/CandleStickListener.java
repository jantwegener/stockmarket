package jtw.stock.view.listener;

import jtw.stock.control.CandleStickStockController;
import jtw.stock.control.StockController;
import jtw.stock.main.StockApplicationSettings;

public class CandleStickListener extends AbstractSelectionListener {

	public CandleStickListener() {
	}
	
	@Override
	protected StockController getController() {
		return new CandleStickStockController(StockApplicationSettings.getStockName());
	}

}
