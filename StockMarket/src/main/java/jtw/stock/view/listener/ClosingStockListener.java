package jtw.stock.view.listener;

import jtw.stock.control.ClosingStockController;
import jtw.stock.control.StockController;
import jtw.stock.main.StockApplicationSettings;

public class ClosingStockListener extends AbstractSelectionListener {

	public ClosingStockListener() {
	}
	
	@Override
	protected StockController getController() {
		return new ClosingStockController(StockApplicationSettings.getStockName());
	}

}
