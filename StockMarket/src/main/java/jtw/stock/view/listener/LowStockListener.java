package jtw.stock.view.listener;

import jtw.stock.control.LowStockController;
import jtw.stock.control.StockController;
import jtw.stock.main.StockApplicationSettings;

public class LowStockListener extends AbstractSelectionListener {

	public LowStockListener() {
	}
	
	@Override
	protected StockController getController() {
		return new LowStockController(StockApplicationSettings.getStockName());
	}
	
}
