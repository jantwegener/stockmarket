package jtw.stock.view.listener;

import jtw.stock.control.OpeningStockController;
import jtw.stock.control.StockController;
import jtw.stock.main.StockApplicationSettings;

public class OpeningStockListener extends AbstractSelectionListener {

	public OpeningStockListener() {
	}
	
	@Override
	protected StockController getController() {
		return new OpeningStockController(StockApplicationSettings.getStockName());
	}

}
