package jtw.stock.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JCheckBox;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreePath;

import jtw.stock.view.action.popup.ColorPopupAction;
import jtw.stock.view.action.popup.DeletePopupAction;
import jtw.stock.view.action.popup.EditPopupAction;
import jtw.stock.view.overlay.OverlayObject;

public class OverlaySummaryView extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static OverlaySummaryView INSTANCE = null;

	private JTree tree;

	private DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode("root");

	public OverlaySummaryView() {
//		setPreferredSize(new Dimension(200, getHeight()));
//		setMinimumSize(new Dimension(150, 50));
	}

	public synchronized static OverlaySummaryView getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new OverlaySummaryView();
			INSTANCE.init();
		}
		return INSTANCE;
	}

	public DefaultMutableTreeNode add(OverlayObject oo) {
		// TODO find good position
		OverlayView.getInstance().add(oo);
		
		
		DefaultMutableTreeNode parentNode = null;
		TreePath parentPath = tree.getSelectionPath();

		if (parentPath == null) {
			// There is no selection. Default to the root node.
			parentNode = rootNode;
		} else {
			parentNode = (DefaultMutableTreeNode) (parentPath.getLastPathComponent());
		}

		return addObject(parentNode, oo, true);
	}
	
	public void remove(OverlayObject oo, DefaultMutableTreeNode node) {
		OverlayView.getInstance().remove(oo.getId());
		((DefaultTreeModel)tree.getModel()).removeNodeFromParent(node);
	}

	public DefaultMutableTreeNode addObject(DefaultMutableTreeNode parent, Object child, boolean shouldBeVisible) {
		DefaultMutableTreeNode childNode = new DefaultMutableTreeNode(child);

		((DefaultTreeModel)tree.getModel()).insertNodeInto(childNode, parent, parent.getChildCount());

		if (shouldBeVisible) {
			tree.scrollPathToVisible(new TreePath(childNode.getPath()));
		}
		return childNode;
	}

	private void init() {
		tree = new JTree(rootNode);
		tree.setCellRenderer(new TreeOverlayRenderer(tree));

		JScrollPane treeView = new JScrollPane(tree);
		tree.setPreferredSize(new Dimension(150, 50));
		add(treeView);
	}
	
	private class TreeOverlayRenderer extends JCheckBox implements TreeCellRenderer {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		public TreeOverlayRenderer thisRenderer = this;
	    
		public TreeOverlayRenderer(JTree tree) {
			tree.addMouseListener(new MouseListener() {
				
				@Override
				public void mouseReleased(MouseEvent e) {
					TreePath tp = tree.getPathForLocation(e.getX(), e.getY());
	                if (tp == null) {
	                    return;
	                }
	                
	                if (e.isPopupTrigger()) {
	                	DefaultMutableTreeNode node = (DefaultMutableTreeNode) tp.getLastPathComponent();
	                	if (node.getUserObject() instanceof OverlayObject) {
		                	OverlayObject oo = (OverlayObject) node.getUserObject();
		                	
		                	JMenuItem editItem = new JMenuItem("Edit");
		                	editItem.setAction(new EditPopupAction(oo));
		                	JMenuItem deleteItem = new JMenuItem("Delete");
		                	deleteItem.setAction(new DeletePopupAction(oo, node));
		                	JMenuItem colorItem = new JMenuItem("Color");
		                	colorItem.setAction(new ColorPopupAction(e.getComponent(), oo));
		                	
		                	JPopupMenu menu = new JPopupMenu("Menu");
		                	menu.add(editItem);
		                	menu.add(deleteItem);
		                	menu.addSeparator();
		                	menu.add(colorItem);
		                	
		                	menu.show(e.getComponent(), e.getX(), e.getY());
	                	}
	                } else {
	                	OverlayView.getInstance().repaint();
	                }
				}
				
				@Override
				public void mousePressed(MouseEvent e) {
				}
				
				@Override
				public void mouseExited(MouseEvent e) {
				}
				
				@Override
				public void mouseEntered(MouseEvent e) {
				}
				
				@Override
				public void mouseClicked(MouseEvent e) {
				}
			});
		}

		public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
			if (value instanceof DefaultMutableTreeNode) {
				DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
				if (node.getUserObject() instanceof OverlayObject) {
					OverlayObject oo = (OverlayObject) node.getUserObject();
					oo.setSelected(selected);
					
					String text = oo.getName();
					this.setText(text);
					if (selected) {
						setForeground(UIManager.getColor("Tree.selectionForeground"));
						setBackground(UIManager.getColor("Tree.selectionBackground"));
					} else {
						setForeground(oo.getMainColor());
						setBackground(Color.WHITE);
					}
				} else {
					String text = "";
					this.setText(text);
					if (selected) {
						setBackground(Color.LIGHT_GRAY);
					} else {
						setBackground(Color.WHITE);
					}
				}
			}
			return this;
		}
	}

}
