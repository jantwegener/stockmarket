package jtw.stock.view;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.Action;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.SwingUtilities;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.data.xy.XYDataset;

import jtw.stock.main.StockApplicationSettings;
import jtw.stock.view.action.AbstractStockAction;
import jtw.stock.view.action.chart.AbstractChartAction;
import jtw.stock.view.component.HighlightValueAxis;

public class StockView extends ChartPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * The key when the property is changed.
	 */
	public static final String IS_CHART_LOADED_KEY = "isChartLoaded";

	private final static CombinedDomainXYPlot COMBINED_PLOT = new CombinedDomainXYPlot();

	private final static XYPlot MAIN_PLOT = new XYPlot();

	public final static HighlightValueAxis Y_AXIS_PRICE = new HighlightValueAxis("Price");

	public final static DateAxis X_AXIS_DATE = new DateAxis("Date");

	private static StockView INSTANCE = null;

	public static final int INDEX_OPEN = 0;
	public static final int INDEX_CLOSE = 1;
	public static final int INDEX_HIGH = 2;
	public static final int INDEX_LOW = 3;
	public static final int INDEX_VOLUME = 4;

	private final XYDataset[] stockDataset = new XYDataset[5];
	
	/**
	 * Shows whether a chart is loaded or not.
	 */
	private boolean isChartLoaded = false;

	/**
	 * Map between name and index in the plot.
	 */
	private final Map<String, Integer> nameIndexMap = new HashMap<>();

	private final Map<String, XYPlot> nameSubplotMap = new HashMap<>();

	/**
	 * A list of all actions with a default enabled state.
	 */
	private final List<Action> actionWithDefaultEnabledStateList = new ArrayList<>();

	/**
	 * The pointer to the last used index. The index must not be null so that the
	 * grid lines are not removed when the index 0 renderer is removed.
	 */
	private int lastIndex = 10;

	static {
		X_AXIS_DATE.setAutoRangeMinimumSize(5);
		X_AXIS_DATE.setAutoRange(true);

		Y_AXIS_PRICE.setAutoRangeIncludesZero(false);
		Y_AXIS_PRICE.setAutoRangeMinimumSize(5);
		Y_AXIS_PRICE.setAutoRange(true);
		
		COMBINED_PLOT.setOrientation(PlotOrientation.VERTICAL);
		COMBINED_PLOT.setRangeAxis(Y_AXIS_PRICE);
		COMBINED_PLOT.setDomainAxis(X_AXIS_DATE);
		COMBINED_PLOT.setDomainGridlinesVisible(true);
		COMBINED_PLOT.setRangeGridlinesVisible(true);

		MAIN_PLOT.setOrientation(PlotOrientation.VERTICAL);
		MAIN_PLOT.setRangeAxis(Y_AXIS_PRICE);
		MAIN_PLOT.setDomainAxis(X_AXIS_DATE);
		MAIN_PLOT.setDomainGridlinesVisible(true);
		MAIN_PLOT.setRangeGridlinesVisible(true);
		// set the renderer to show the grids
		MAIN_PLOT.setRenderer(new StandardXYItemRenderer());
		// allow movement per mouse + ctrl
		MAIN_PLOT.setDomainPannable(true);
		MAIN_PLOT.setRangePannable(true);

		COMBINED_PLOT.add(MAIN_PLOT, 4);
		COMBINED_PLOT.setDomainPannable(true);
	}

	private StockView() {
		super(new JFreeChart(COMBINED_PLOT), true, true, true, true, true);
		
		Dimension resolution = Toolkit.getDefaultToolkit().getScreenSize();
		setMaximumDrawWidth(resolution.width);
		setMaximumDrawHeight(resolution.height);
		
		// setMouseZoomable(true);
		setMouseWheelEnabled(true);
	}

	public synchronized static StockView getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new StockView();
		}
		return INSTANCE;
	}

	public void initDataset(XYDataset dsOpen, XYDataset dsClose, XYDataset dsHigh, XYDataset dsLow, XYDataset dsVolume) {
		stockDataset[INDEX_OPEN] = dsOpen;
		stockDataset[INDEX_CLOSE] = dsClose;
		stockDataset[INDEX_HIGH] = dsHigh;
		stockDataset[INDEX_LOW] = dsLow;
		stockDataset[INDEX_VOLUME] = dsVolume;
	}

	public XYDataset getDataset(int index) {
		return stockDataset[index];
	}

	public XYDataset getOpenDataset() {
		return getDataset(INDEX_OPEN);
	}

	public XYDataset getCloseDataset() {
		return getDataset(INDEX_CLOSE);
	}

	public XYDataset getHighDataset() {
		return getDataset(INDEX_HIGH);
	}

	public XYDataset getLowDataset() {
		return getDataset(INDEX_LOW);
	}

	public XYDataset getVolumeDataset() {
		return getDataset(INDEX_VOLUME);
	}

	public XYPlot getMainPlot() {
		return MAIN_PLOT;
	}
	
	public CombinedDomainXYPlot getCombinedPlot() {
		return COMBINED_PLOT;
	}

	public void setChartName(String chartName) {
		getChart().setTitle(chartName);
	}

	public synchronized void addPlot(String name, XYPlot plot, boolean subPlot) {
		if (subPlot) {
			nameSubplotMap.put(name, plot);

			COMBINED_PLOT.add(plot, 1);
		} else {
			nameIndexMap.put(name, lastIndex);

			MAIN_PLOT.setDataset(lastIndex, plot.getDataset());
			MAIN_PLOT.setRenderer(lastIndex, plot.getRenderer());

			lastIndex++;
		}

		repaint();

		setChartLoaded(!nameIndexMap.isEmpty());
	}

	public synchronized void removePlot(String name) {
		Integer index = nameIndexMap.remove(name);
		XYPlot plot = nameSubplotMap.remove(name);
		if (index != null) {
			MAIN_PLOT.setDataset(index, null);
			MAIN_PLOT.setRenderer(index, null);
		}
		if (plot != null) {
			COMBINED_PLOT.remove(plot);
		}
//		repaint();
	}

	public synchronized void reset() {
		for (Integer index : nameIndexMap.values()) {
			MAIN_PLOT.setDataset(index, null);
			MAIN_PLOT.setRenderer(index, null);
			MAIN_PLOT.clearDomainMarkers();
		}
		for (XYPlot plot : nameSubplotMap.values()) {
			COMBINED_PLOT.remove(plot);
		}
		
		StockView.Y_AXIS_PRICE.resetHighlightTicks();

		nameIndexMap.clear();
		nameSubplotMap.clear();
		repaint();

		setChartLoaded(!nameIndexMap.isEmpty());
	}

	public synchronized void recompute() {
		JFrame parentFrame = (JFrame) SwingUtilities.getWindowAncestor(this);
		if (parentFrame != null) {
			for (int i = 0; i < parentFrame.getJMenuBar().getMenuCount(); i++) {
				JMenu menu = parentFrame.getJMenuBar().getMenu(i);
				recompute(menu);
			}
		}
	}
	
	private void recompute(JMenu menu) {
		for (int j = 0; j < menu.getMenuComponentCount(); j++) {
			Component item = menu.getMenuComponent(j);
			if (item instanceof JCheckBoxMenuItem) {
				JCheckBoxMenuItem checkbox = ((JCheckBoxMenuItem) item);
				if (checkbox.isSelected() && checkbox.getAction() instanceof AbstractChartAction) {
					AbstractChartAction action = (AbstractChartAction)checkbox.getAction();
					
					String plotName = action.getPlotName();
					removePlot(plotName);
					
					action.triggerAction();
				}
			} else if (item instanceof JMenu) {
				recompute((JMenu)item);
			}
		}
	}

	public void addAllActionWithDefaultEnabledStateList(List<Action> actionWithDefaultEnabledStateList) {
		for (Action a : actionWithDefaultEnabledStateList) {
			this.actionWithDefaultEnabledStateList.add(a);
			if (a instanceof PropertyChangeListener) {
				addPropertyChangeListener(IS_CHART_LOADED_KEY, (PropertyChangeListener) a);
			}
		}
	}

	public void resetMenuBar() {
		for (Action a : actionWithDefaultEnabledStateList) {
			AbstractStockAction.resetToDefaultEnabledState(a);
			AbstractStockAction.resetToDefaultSelectedState(a);
		}

		setChartLoaded(!nameIndexMap.isEmpty());
	}

	/**
	 * Returns true when a chart is loaded, false otherwise.
	 * 
	 * @return true when a chart is loaded, false otherwise.
	 */
	public boolean isChartLoaded() {
		return isChartLoaded;
	}

	/**
	 * Sets the chart to loaded or not loaded.
	 * 
	 * @param isChartLoaded true when a chart has been successfully loaded, false otherwise.
	 */
	public void setChartLoaded(boolean isChartLoaded) {
		boolean oldValue = this.isChartLoaded;
		this.isChartLoaded = isChartLoaded;
		firePropertyChange(IS_CHART_LOADED_KEY, oldValue, isChartLoaded);
	}
	
}
