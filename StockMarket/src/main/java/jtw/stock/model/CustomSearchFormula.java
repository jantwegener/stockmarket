package jtw.stock.model;

import java.io.Serializable;

public class CustomSearchFormula implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * A memorizable name for the formula.
	 */
	private String name = null;

	private String text = null;
	
	private String[] clazzArray;

	private String[] parameterNameArray;
	
	private String[][] parameterArray;
	
	public CustomSearchFormula() {
	}
	
	public boolean isEmpty() {
		return text == null || text.isEmpty();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String[] getClazzArray() {
		return clazzArray;
	}

	public void setClazzArray(String[] clazzArray) {
		this.clazzArray = clazzArray;
	}
	
	public void init(int numOfClazzes) {
		clazzArray = new String[numOfClazzes];
		parameterArray = new String[numOfClazzes][];
		parameterNameArray = new String[numOfClazzes];
	}
	
	public void trim(int numOfClazzes) {
		if (numOfClazzes < clazzArray.length) {
			String[] tmpClazzArray = new String[numOfClazzes];
			String[][] tmpParameterArray = new String[numOfClazzes][];
			String[] tmpParameterNameArray = new String[numOfClazzes];

			System.arraycopy(clazzArray, 0, tmpClazzArray, 0, numOfClazzes);
			System.arraycopy(parameterArray, 0, tmpParameterArray, 0, numOfClazzes);
			System.arraycopy(parameterNameArray, 0, tmpParameterNameArray, 0, numOfClazzes);
			
			clazzArray = tmpClazzArray;
			parameterArray = tmpParameterArray;
			parameterNameArray = tmpParameterNameArray;
		}
	}
	
	public void setClazz(int index, String name) {
		clazzArray[index] = name;
	}

	public String[][] getParameterArray() {
		return parameterArray;
	}

	public void setParameterArray(String[][] parameterArray) {
		this.parameterArray = parameterArray;
	}
	
	public void setParameterArray(int index, String[] parameter) {
		this.parameterArray[index] = parameter;
	}

	public String[] getParamNameArray() {
		return parameterNameArray;
	}

	public void setParamNameArray(String[] paramNameArray) {
		this.parameterNameArray = paramNameArray;
	}

	public void setParameterName(int index, String parameterName) {
		this.parameterNameArray[index] = parameterName;
	}
	
	@Override
	public String toString() {
		return getName();
	}

}
