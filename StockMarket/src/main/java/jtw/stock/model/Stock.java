package jtw.stock.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Currency;

import jtw.stock.model.registry.StockSymbolNameRegistry;

public class Stock {

	private String name;
	private Calendar date;
	private Currency currency;

	private double opening;
	private double high;
	private double low;
	private double closing;

	private long volume;
	
	public Stock() {
	}
	
	public Stock(Stock stock) {
		this.name = stock.name;
		this.date = stock.date;
		this.currency = stock.currency;
		this.opening = stock.opening;
		this.high = stock.high;
		this.low = stock.low;
		this.closing = stock.closing;
		this.volume = stock.volume;
	}

	public Stock(String name, Calendar date, Currency currency, double opening, double closing, double high, double low, long volume) {
		this.name = name;
		this.date = date;
		this.currency = currency;
		this.opening = opening;
		this.high = high;
		this.low = low;
		this.closing = closing;
		this.volume = volume;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getCompanyName() {
		return StockSymbolNameRegistry.getInstance().getCompany(name);
	}

	public Calendar getDate() {
		return date;
	}

	public void setDate(Calendar date) {
		this.date = date;
	}
	
	public Currency getCurrency() {
		return currency;
	}
	
	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public double getOpening() {
		return opening;
	}

	public void setOpening(double opening) {
		this.opening = opening;
	}
	
	public void addOpening(double opening) {
		this.opening += opening;
	}

	public double getHigh() {
		return high;
	}

	public void setHigh(double high) {
		this.high = high;
	}

	public void addHigh(double high) {
		this.high += high;
	}

	public double getLow() {
		return low;
	}

	public void setLow(double low) {
		this.low = low;
	}

	public void addLow(double low) {
		this.low += low;
	}

	public double getClosing() {
		return closing;
	}

	public void setClosing(double closing) {
		this.closing = closing;
	}

	public void addClosing(double closing) {
		this.closing += closing;
	}

	public long getVolume() {
		return volume;
	}
	
	public void addVolume(long volume) {
		this.volume += volume;
	}

	public void setVolume(long volume) {
		this.volume = volume;
	}

	public String toString() {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		return df.format(name + " " + date.getTime()) + " " + opening + " " + high + " " + low + " " + closing + " " + volume;
	}

}
