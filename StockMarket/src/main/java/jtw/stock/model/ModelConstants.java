package jtw.stock.model;

public class ModelConstants {
	
	/**
	 * Constant for adjusting by 5 digits behind the decimal point.
	 */
	public static final double DECIMAL_POINT_ADJUSTMENT = Math.pow(10, 5);
	
}
