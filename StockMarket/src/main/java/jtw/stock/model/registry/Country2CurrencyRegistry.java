package jtw.stock.model.registry;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Currency;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jtw.stock.main.StockApplicationSettings;

public class Country2CurrencyRegistry {

	private Log LOGGER = LogFactory.getLog(Country2CurrencyRegistry.class);

	private static Country2CurrencyRegistry INSTANCE = null;
	
	private Map<String, Currency> countryCurrencyMap = new HashMap<>();

	private Country2CurrencyRegistry() {
		loadCountryCurrencyMap();
	}
	
	private void loadCountryCurrencyMap() {
		File countryCurrencyFile = new File(StockApplicationSettings.getLocalPath("countryCurrency", "map"));
		try (BufferedReader reader = new BufferedReader(new FileReader(countryCurrencyFile))) {
			String line = null;
			while ((line = reader.readLine()) != null) {
				if (line.trim().equals("")) {
					continue;
				}
				
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("Line: " + line);
				}
				
				String[] tmp = line.split("\\s");
				countryCurrencyMap.put(tmp[0], Currency.getInstance(tmp[1]));
			}
		} catch (IOException ioe) {
			LOGGER.error(ioe.getMessage(), ioe);
			throw new RuntimeException(ioe);
		}
	}
	
	public synchronized static Country2CurrencyRegistry getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Country2CurrencyRegistry();
		}
		return INSTANCE;
	}
	
	public Currency getCurrency(String country) {
		return countryCurrencyMap.get(country);
	}
	
	/**
	 * Retuns the currency using the stock name.
	 *  
	 * @param stockName The name of the stock, e.g., wdi.de, amz.us, ...
	 * 
	 * @return The corresponding currency.
	 */
	public Currency getCurrencyFromStockName(String stockName) {
		Currency retval = null;
		if (stockName.contains(".")) {
			String tmp = stockName.substring(stockName.lastIndexOf('.') + 1);
			retval = getCurrency(tmp);
		}
		return retval;
	}
	
}
