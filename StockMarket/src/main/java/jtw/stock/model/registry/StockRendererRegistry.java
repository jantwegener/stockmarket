package jtw.stock.model.registry;

import java.util.Map;

import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class StockRendererRegistry {

	private static StockRendererRegistry INSTANCE = null;
	
	private String rendererXMLFile = "plotRenderer.xml";
	
	private Map<String, String> plotRendererMap;
	
	private ConfigurableListableBeanFactory beanFactory;
	
	private StockRendererRegistry() {
	}
	
	@SuppressWarnings("unchecked")
	private void init() {
		try (ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(rendererXMLFile)) {
			plotRendererMap = (Map<String, String>) context.getBean("jtw.stock.model.registry.plotRendererMap");
			beanFactory = context.getBeanFactory();
		}
	}
	
	public synchronized static StockRendererRegistry getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new StockRendererRegistry();
			INSTANCE.init();
		}
		return INSTANCE;
	}
	
	/**
	 * Returns the renderer corresponding to the id. If the id is not found, then the default renderer is returned.
	 * 
	 * @param id The id of the plot, usually the class name of the calculator.
	 * 
	 * @return The corresponding renderer.
	 */
	public XYItemRenderer getRenderer(String id) {
		String beanId = plotRendererMap.get(id);
		XYItemRenderer c = null;
		if (beanId == null) {
			c = new StandardXYItemRenderer();
		} else {
			c = (XYItemRenderer)beanFactory.getBean(beanId);
		}
		return c;
	}
	
}
