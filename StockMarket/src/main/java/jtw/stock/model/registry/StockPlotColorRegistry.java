package jtw.stock.model.registry;

import java.awt.Color;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class StockPlotColorRegistry {

	private Log log = LogFactory.getLog(StockPlotColorRegistry.class);
	
	public static Color DEFAULT_COLOR = Color.BLACK;
	public static Color DEFAULT_SIGNAL_COLOR = Color.RED;
	
	private static StockPlotColorRegistry INSTANCE = null;
	
	private String colorXMLFile = "plotColor.xml";
	
	private Map<String, Color> plotColorMap;
	
	private StockPlotColorRegistry() {
	}
	
	@SuppressWarnings("unchecked")
	private void init() {
		try (ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(colorXMLFile)) {
			plotColorMap = (Map<String, Color>) context.getBean("jtw.stock.model.registry.plotColorMap");
		}
	}
	
	public synchronized static StockPlotColorRegistry getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new StockPlotColorRegistry();
			INSTANCE.init();
		}
		return INSTANCE;
	}
	
	/**
	 * Returns the color corresponding to the id. If the id is not found, then the default color is returned.
	 * 
	 * @param id The id of the plot, usually the class name of the calculator.
	 * 
	 * @return The corresponding color.
	 */
	public Color getColor(String id) {
		if (log.isDebugEnabled()) {
			log.debug("enter getColor(" + id + ")");
		}
		
		Color c = plotColorMap.get(id);
		if (c == null && id.endsWith("/1")) {
			c = DEFAULT_SIGNAL_COLOR;
		} else if (c == null) {
			c = DEFAULT_COLOR;
		}
		
		if (log.isDebugEnabled()) {
			log.debug("exit getColor: " + c);
		}
		
		return c;
	}
	
}
