package jtw.stock.model.registry;

import java.util.Map;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import jtw.stock.calculator.CalculationFinishedListener;
import jtw.stock.calculator.StandardStockCalculationFinishedAdapter;
import jtw.stock.calculator.report.StandardReportCalculationFinishedListener;

public class StockCalculatorFinishedListenerRegistry {

	public static CalculationFinishedListener DEFAULT_LISTENER = new StandardStockCalculationFinishedAdapter();
	
	private static StockCalculatorFinishedListenerRegistry INSTANCE = null;
	
	private String listenerXMLFile = "calculationFinishedListener.xml";
	
	private Map<String, CalculationFinishedListener> nameListenerMap;
	
	private Map<String, StandardReportCalculationFinishedListener> reportListenerMap;
	
	private StockCalculatorFinishedListenerRegistry() {
	}
	
	@SuppressWarnings("unchecked")
	private void init() {
		try (ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(listenerXMLFile)) {
			nameListenerMap = (Map<String,  CalculationFinishedListener>) context.getBean( "jtw.stock.model.registry.nameListenerMap");
			reportListenerMap = (Map<String,  StandardReportCalculationFinishedListener>) context.getBean( "jtw.stock.model.registry.reportListenerMap");
		}
	}
	
	public synchronized static StockCalculatorFinishedListenerRegistry getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new StockCalculatorFinishedListenerRegistry();
			INSTANCE.init();
		}
		return INSTANCE;
	}
	
	/**
	 * Returns the {@link CalculationFinishedListener} corresponding to the id. If the id is not found, then the default listener is returned.
	 * 
	 * @param id The id of the plot, usually the class name of the calculator.
	 * 
	 * @return The corresponding color.
	 */
	public CalculationFinishedListener getCalculationFinishedListener(Class<?> clazz) {
		return getCalculationFinishedListener(clazz.getName());
	}
	
	/**
	 * Returns the {@link CalculationFinishedListener} corresponding to the id. If the id is not found, then the default listener is returned.
	 * 
	 * @param id The id of the plot, usually the class name of the calculator.
	 * 
	 * @return The corresponding color.
	 */
	public CalculationFinishedListener getCalculationFinishedListener(String id) {
		CalculationFinishedListener listener = nameListenerMap.get(id);
		if (listener == null) {
			id = id.split(":")[0];
			listener = nameListenerMap.get(id);
			if (listener == null) {
				listener = DEFAULT_LISTENER;
			}
		}
		return listener;
	}
	
	/**
	 * Returns the {@link CalculationFinishedListener} corresponding to the id. If the id is not found, then the default listener is returned.
	 * 
	 * @param id The id of the plot, usually the class name of the calculator.
	 * 
	 * @return The corresponding color.
	 * 
	 * @throws IllegalArgumentException If the id is not known.
	 */
	public StandardReportCalculationFinishedListener getReportCalculationFinishedListener(String id) {
		StandardReportCalculationFinishedListener listener = reportListenerMap.get(id);
		if (listener == null) {
			String newid = id.split(":")[0];
			listener = reportListenerMap.get(newid);
			if (listener == null) {
				throw new IllegalArgumentException("Unknown id: '" + id + "'");
			}
		}
		return listener;
	}
	
}
