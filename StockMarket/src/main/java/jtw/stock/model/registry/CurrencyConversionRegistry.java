package jtw.stock.model.registry;

import java.io.IOException;
import java.util.Currency;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.antlr.v4.runtime.misc.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jtw.stock.io.CurrencyConversionReader;
import jtw.stock.io.CurrencyConversionWriter;

public class CurrencyConversionRegistry {
	
	private static Log LOGGER = LogFactory.getLog(CurrencyConversionRegistry.class);

	private static CurrencyConversionRegistry INSTANCE = null;
	
	private Map<Pair<String, String>, Pair<Double, Double>> currencyConversionMap = new HashMap<>();
	
	private Set<String> currencySet = new HashSet<>();
	
	private CurrencyConversionRegistry() throws IOException {
		CurrencyConversionReader reader = new CurrencyConversionReader();
		Map<Pair<String, String>, Pair<Double, Double>> tmpMap = reader.loadCurrencyConversions();
		for (Pair<String, String> p : tmpMap.keySet()) {
			Pair<Double, Double> money = tmpMap.get(p);
			addCurrencyConversion(p.a, p.b, money.a, money.b);
		}
	}
	
	public synchronized static CurrencyConversionRegistry getInstance() {
		if (INSTANCE == null) {
			try {
				INSTANCE = new CurrencyConversionRegistry();
			} catch (IOException e) {
				LOGGER.error(e.getMessage(), e);

				throw new RuntimeException(e.getMessage());
			}
		}
		return INSTANCE;
	}
	
	public void saveConversionMap() throws IOException {
		CurrencyConversionWriter writer = new CurrencyConversionWriter();
		writer.writeCurrencyConversions(currencyConversionMap);
	}
	
	/**
	 * Adds the currency conversion to the registry and the inverted conversion.
	 * 
	 * The inverted conversion is the inverse of the given one. E.g., given are EUR -> USD (1 -> 1.1) then the inverted is USD -> EUR (1.1. -> 1).
	 * 
	 * @param currencyCode1 The currency code.
	 * @param currencyCode2 The currency code.
	 * @param c1 The conversion rate.
	 * @param c2 The conversion rate.
	 */
	public void addCurrencyConversion(String currencyCode1, String currencyCode2, double c1, double c2) {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(String.format("Add currency conversion: %s - %s # %f - %f", currencyCode1, currencyCode2, c1, c2));
		}
		
		Pair<String, String> code2code = new Pair<>(currencyCode1, currencyCode2);
		Pair<Double, Double> money2money = new Pair<>(c1, c2);
		
		currencyConversionMap.put(code2code, money2money);

		code2code = new Pair<>(currencyCode2, currencyCode1);
		money2money = new Pair<>(c2, c1);
		
		currencyConversionMap.put(code2code, money2money);

		currencySet.add(currencyCode1);
		currencySet.add(currencyCode2);
	}
	
	public Pair<Double, Double> getConversion(String currencyCode1, String currencyCode2) {
		Pair<Double, Double> retVal;
		if (currencyCode1.equals(currencyCode2)) {
			retVal = new Pair<Double, Double>(1.0, 1.0);
		} else {
			Pair<String, String> code2code = new Pair<>(currencyCode1, currencyCode2);
			retVal = currencyConversionMap.get(code2code);
		}
		return retVal;
	}
	
	public Map<Pair<String, String>, Pair<Double, Double>> getAllConversion() {
		return currencyConversionMap;
	}
	
	public Set<String> getAvailableCurrencies() {
		return currencySet;
	}
	
	/**
	 * Converts the amount and rounds it.
	 * 
	 * @param currencyCodeFrom
	 * @param currencyCodeTo
	 * @param amount
	 * 
	 * @return The rounded value.
	 */
	public long convert(Currency currencyCodeFrom, Currency currencyCodeTo, long amount) {
		return Math.round(convert(currencyCodeFrom.getCurrencyCode(), currencyCodeTo.getCurrencyCode(), amount));
	}

	public double convert(Currency currencyCodeFrom, Currency currencyCodeTo, double amount) {
		return convert(currencyCodeFrom.getCurrencyCode(), currencyCodeTo.getCurrencyCode(), amount);
	}
	
	public double convert(String currencyCodeFrom, String currencyCodeTo, double amount) {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("convert: " + currencyCodeFrom + ", " + currencyCodeTo + ", " + amount);
		}
		Pair<Double, Double> p = getConversion(currencyCodeFrom, currencyCodeTo);
		double convertionRate = p.b / p.a;
		double retVal = amount * convertionRate;
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("convertionRate: " + convertionRate);
			LOGGER.debug("retVal: " + retVal);
		}
		return retVal;
	}
	
}
