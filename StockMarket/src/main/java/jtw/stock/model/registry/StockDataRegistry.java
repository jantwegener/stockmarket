package jtw.stock.model.registry;

import static jtw.stock.io.IoConstants.COL_CLOSE;
import static jtw.stock.io.IoConstants.COL_DATE;
import static jtw.stock.io.IoConstants.COL_HIGH;
import static jtw.stock.io.IoConstants.COL_LOW;
import static jtw.stock.io.IoConstants.COL_OPEN;
import static jtw.stock.io.IoConstants.COL_VOLUME;
import static jtw.stock.io.IoConstants.DB_FILE_ENDING;
import static jtw.stock.io.IoConstants.INTERNAL_DATE_PATTERN;
import static jtw.stock.main.Version.VERSION;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jfree.data.xy.AbstractXYDataset;
import org.jfree.data.xy.XYDataset;
import org.springframework.stereotype.Component;

import jtw.stock.io.HistoryReader;
import jtw.stock.io.HistoryReaderFactory;
import jtw.stock.io.InternalHistoryReader;
import jtw.stock.io.IoConstants;
import jtw.stock.main.StockApplicationSettings;
import jtw.stock.model.Stock;

/**
 * This class stores the stock data and updates the data if necessary.
 * 
 * @author Jan-Thierry Wegener
 */
@Component
public final class StockDataRegistry {
	
	private final transient Log log = LogFactory.getLog(StockDataRegistry.class);
	
	private static StockDataRegistry INSTANCE = null;

	private Map<String, Data> stockNameDataMap = new HashMap<>();

	public synchronized static StockDataRegistry getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new StockDataRegistry();
		}
		return INSTANCE;
	}

	/**
	 * Puts the stocks to the registry. This can be used for data that is not stored onto the hard drive but is needed for further computations.
	 * 
	 * @param stockName
	 *            The name of the stock to put.
	 * @param dailyList
	 *            The daily list of the stock.
	 * @param weeklyList
	 *            The weekly list of the stock.
	 * @param monthlyList
	 *            The monthly list of the stock.
	 */
	public void putStockList(String stockName, List<Stock> dailyList, List<Stock> weeklyList, List<Stock> monthlyList) {
		Data data = new Data(dailyList, weeklyList, monthlyList);
		stockNameDataMap.put(stockName, data);
	}
	
	/**
	 * Appends the stocks to the registry effectively merging the existing and given data.
	 * 
	 * @param stockName
	 * @param dailyList
	 * @param weeklyList
	 * @param monthlyList
	 */
	private void appendStockList(String stockName, List<Stock> dailyList, List<Stock> weeklyList, List<Stock> monthlyList) {
		Data data = stockNameDataMap.get(stockName);
		if (data == null) {
			data = new Data(dailyList, weeklyList, monthlyList);
			stockNameDataMap.put(stockName, data);
		} else {
			// update daily
			Stock lastDaily = data.getDailyList().get(data.getDailyList().size() - 1);
			boolean append = false;
			for (Stock s : dailyList) {
				if (append || s.getDate().after(lastDaily.getDate())) {
					data.getDailyList().add(s);
					// the data is already sorted
					append = true;
				}
			}
			
			// update weekly
			Stock lastWeekly = data.getWeeklyList().get(data.getWeeklyList().size() - 1);
			append = false;
			for (Stock s : weeklyList) {
				if (append || s.getDate().after(lastWeekly.getDate())) {
					data.getWeeklyList().add(s);
					// the data is already sorted
					append = true;
				}
			}
			
			// update monthly
			Stock lastMonthly = data.getMonthlyList().get(data.getMonthlyList().size() - 1);
			append = false;
			for (Stock s : monthlyList) {
				if (append || s.getDate().after(lastMonthly.getDate())) {
					data.getMonthlyList().add(s);
					// the data is already sorted
					append = true;
				}
			}
		}
	}

	/**
	 * Ensures that the data is up to date.
	 * 
	 * @param stockName The name of the stock.
	 */
	private void updateStockData(String stockName) throws IOException {
		boolean isUpToDate = false;
		// check if we have some data in our memory
		if (!containsStockData(stockName)) {
			// no data loaded into memory
			// so we check for data on the hard disk
			if (StockApplicationSettings.isFileExists(stockName, DB_FILE_ENDING)) {
				HistoryReader<CSVRecord> reader = new InternalHistoryReader();
				int read = reader.read(stockName);
				if (read == 0) {
					log.warn("No data read for stock: " + stockName);
				} else {
					// put the data into our registry
					putStockList(stockName, reader.getDailyList(), reader.getWeeklyList(), reader.getMonthlyList());
					isUpToDate = isStockDataUpToDate(stockName);
				}
			}
		} else {
			isUpToDate = isStockDataUpToDate(stockName);
		}
		
		if (!isUpToDate) {
			log.debug("Updating data for: " + stockName);
			// download the data and merge with existing data
			HistoryReader<?> reader = HistoryReaderFactory.getHistoryReader(stockName);
			int read = reader.read(stockName);
			if (read > 0) {
				appendStockList(stockName, reader.getDailyList(), reader.getWeeklyList(), reader.getMonthlyList());
				// write the new data
				write(stockName);
				log.debug("Wrote data for stock: " + stockName);
			} else {
				log.error("Could not download any data for stock: " + stockName);
			}
		}
	}
	
	private void write(String stockName) throws IOException {
		List<Stock> stockList = stockNameDataMap.get(stockName).getDailyList();
		String file = StockApplicationSettings.getLocalPath(stockName, IoConstants.DB_FILE_ENDING);
		CSVFormat csvFormat = CSVFormat.DEFAULT.builder().setHeaderComments("jtw.stock.StockMarket", "Version " + VERSION).setHeader(COL_DATE, COL_OPEN, COL_CLOSE, COL_HIGH, COL_LOW, COL_VOLUME).build();
		try (CSVPrinter printer = new CSVPrinter(new FileWriter(file), csvFormat)) {
			for (Stock s : stockList) {
				SimpleDateFormat df = new SimpleDateFormat(INTERNAL_DATE_PATTERN);
				printer.printRecord(df.format(s.getDate().getTime()), s.getOpening(), s.getClosing(), s.getHigh(), s.getLow(), s.getVolume());
			}
		}
	}
	
	private boolean isStockDataUpToDate(String stockName) {
		boolean isDataUpToDate = false;
		
		List<Stock> stockList = stockNameDataMap.get(stockName).getDailyList();
		Stock lastStock = stockList.get(stockList.size() - 1);
		
		// now we check the date of the last stock
		LocalDate today = LocalDate.now();
		LocalDate lastStockDate = lastStock.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		if (today.isEqual(lastStockDate)) {
			// the stock data was download today
			isDataUpToDate = true;
		} else if (today.minus(Period.ofDays(1)).isEqual(lastStockDate)) {
			// the stock was downloaded yesterday
			// still considering as accurate since most free data is not yet freely awailable
			isDataUpToDate = true;
		} else if (today.getDayOfWeek().equals(DayOfWeek.SATURDAY) && today.minus(Period.ofDays(1)).isEqual(lastStockDate)) {
			// Saturday, stock exchange is closed
			isDataUpToDate = true;	
		} else if (today.getDayOfWeek().equals(DayOfWeek.SUNDAY) && today.minus(Period.ofDays(2)).isEqual(lastStockDate)) {
			// Sunday, stock exchange is closed
			// Saturday, stock exchange was closed as well
			isDataUpToDate = true;	
		}
		
		return isDataUpToDate;
	}
	
	/**
	 * Checks if stock data is already available in memory.
	 * 
	 * @param stockName The name of the stock.
	 * 
	 * @return true if data is already available, false otherwise.
	 */
	private boolean containsStockData(String stockName) {
		return stockNameDataMap.containsKey(stockName);
	}
	
	/**
	 * Ensures that the data is up to date in the databank. This can be used to ensure that the data is available before calculations or to update the progress bar.
	 * 
	 * @param stockName The stock to ensure that is up to date.
	 */
	public void ensureUpToDate(String stockName) throws IOException {
		updateStockData(stockName);
	}
	
	/**
	 * Returns the stocks for the given stock name.
	 * 
	 * @param stockName
	 *            The stocks to return.
	 * 
	 * @return The list of stocks corresponding to the name.
	 */
	public List<Stock> getStockList(String stockName) {
		return getStockList(stockName, StockApplicationSettings.getTimeBase());
	}

	/**
	 * Returns the stocks for the given stock name.
	 * 
	 * @param stockName
	 *            The stocks to return.
	 * @param timeBase
	 *            The stock list to return.
	 * 
	 * @return The list of stocks corresponding to the name.
	 */
	public List<Stock> getStockList(String stockName, int timeBase) {
		return getStockList(stockName, timeBase, StockApplicationSettings.getTimeFrame());
	}

	/**
	 * Returns the stocks for the given stock name.
	 * 
	 * @param stockName
	 *            The stocks to return.
	 * @param timeBase
	 *            The stock list to return.
	 * @param timeFrame
	 *            The time frame to use.
	 * 
	 * @return The list of stocks corresponding to the name.
	 */
	public List<Stock> getStockList(String stockName, int timeBase, int timeFrame) {
		try {
			updateStockData(stockName);
		} catch (IOException ioe) {
			throw new RuntimeException(ioe);
		}
		
		Data data = stockNameDataMap.get(stockName);

		List<Stock> stockList = null;
		if (data != null) {
			switch (timeBase) {
				case StockApplicationSettings.TIME_BASE_DAILY:
					stockList = data.getDailyList();
					break;
				case StockApplicationSettings.TIME_BASE_WEEKLY:
					stockList = data.getWeeklyList();
					break;
				case StockApplicationSettings.TIME_BASE_MONTHLY:
					stockList = data.getMonthlyList();
					break;
				default:
					stockList = data.getDailyList();
			}
		}
		
		// filter stocks that are not within the given time frame
		// only do that if there is something to filter
		if (timeFrame != StockApplicationSettings.TIME_FRAME_MAX) {
			Calendar earliestTime = StockApplicationSettings.computeEarliestTime(timeFrame);
			stockList = filterForTimeFrame(stockList, earliestTime);
		}
		
		return stockList;
	}
	
	public List<Stock> getDailyStockList(String stockName) {
		return getStockList(stockName, StockApplicationSettings.TIME_BASE_DAILY);
	}
	
	/**
	 * Filters the stocks that are not in the time frame.
	 * 
	 * @param stockList The list to filter.
	 * 
	 * @return A new list containing only those stocks that are within the time frame.
	 */
	private List<Stock> filterForTimeFrame(List<Stock> stockList, Calendar earliestTime) {
		return stockList.stream().filter(x -> StockApplicationSettings.isInTimeFrame(x, earliestTime)).collect(Collectors.toList());
	}

	public double getReferencedStockValue(Stock stock) {
		return getReferencedStockValue(stock, StockApplicationSettings.getReferenceDataset());
	}
	
	public double getReferencedStockValue(Stock stock, int ref) {
		double value = 0d;
		switch (ref) {
			case StockApplicationSettings.REFERENCE_DATASET_OPEN:
				value = stock.getOpening();
				break;
			case StockApplicationSettings.REFERENCE_DATASET_CLOSE:
				value = stock.getClosing();
				break;
			case StockApplicationSettings.REFERENCE_DATASET_HIGH:
				value = stock.getHigh();
				break;
			case StockApplicationSettings.REFERENCE_DATASET_LOW:
				value = stock.getLow();
				break;
			case StockApplicationSettings.REFERENCE_DATASET_VOLUME:
				value = stock.getVolume();
				break;
			default:
				value = stock.getOpening();
		}
		return value;
	}

	/**
	 * Returns the data set referenced by the current settings.
	 * 
	 * @return The current data set.
	 */
	public XYDataset getReferencedDataset() {
		XYDataset ds = new ReferencedXYDataset(StockApplicationSettings.getStockName());
		return ds;
	}
	
	/**
	 * Returns the number of days between the dates of the first and the last stocks.
	 * 
	 * @return The span.
	 */
	public int getXSpan() {
		List<Stock> stockList = stockNameDataMap.get(StockApplicationSettings.getStockName()).getDailyList();
		int result = 0;
		if (stockList.size() >= 2) {
			result = computeDaysBetween(stockList.get(0).getDate(), stockList.get(stockList.size() - 1).getDate());
		}
		return result;
	}
	
	/**
	 * Computes the number of days between the two dates. This method should not often be called as it is quite expensive.
	 * 
	 * @param c1
	 * @param c2
	 * 
	 * @return
	 */
	private int computeDaysBetween(Calendar c1, Calendar c2) {
		return (int) ChronoUnit.DAYS.between(LocalDate.of(c1.get(Calendar.YEAR), c1.get(Calendar.MONTH) + 1, c1.get(Calendar.DATE)),
				LocalDate.of(c2.get(Calendar.YEAR), c2.get(Calendar.MONTH) + 1, c2.get(Calendar.DATE)));
//		return (int) ((c2.getTimeInMillis() - c1.getTimeInMillis()) / (1000 * 60 * 60 * 24));
	}
	
	/**
	 * Searches the stock for the given date.
	 * 
	 * @param stockName The name of the stock.
	 * @param date The date of the stock.
	 * 
	 * @return The stock at the given date or null if it does not exist.
	 */
	public Stock getDailyStock(String stockName, Calendar date) {
		Stock stock = null;
		List<Stock> dailyList = stockNameDataMap.get(stockName).getDailyList();
		
		Stock searchStock = new Stock("", date, null, 0, 0, 0, 0, 0);
		Comparator<Stock> dateComparator = (o1, o2) -> o1.getDate().compareTo(o2.getDate());
		
		int index = Collections.binarySearch(dailyList, searchStock, dateComparator);
		if (index >= 0) {
			stock = dailyList.get(index);
		}
		
		return stock;
	}
	
	/**
	 * Searches the index of the stock for the given date.
	 * 
	 * @param stockName The name of the stock.
	 * @param date The date of the stock.
	 * 
	 * @return The index of the stock at the given date or -1 if it does not exist.
	 * 
	 * @see Collections#binarySearch(List, Object, Comparator)
	 */
	public int getDailyStockIndex(String stockName, Calendar date) {
		List<Stock> dailyList = stockNameDataMap.get(stockName).getDailyList();
		
		Stock searchStock = new Stock("", date, null, 0, 0, 0, 0, 0);
		Comparator<Stock> dateComparator = (o1, o2) -> o1.getDate().compareTo(o2.getDate());
		
		int index = Collections.binarySearch(dailyList, searchStock, dateComparator);
		return index;
	}
	
	private class Data {
		private List<Stock> dailyList = new ArrayList<>();

		private List<Stock> weeklyList = new ArrayList<>();

		private List<Stock> monthlyList = new ArrayList<>();

		public Data(List<Stock> dailyList, List<Stock> weeklyList, List<Stock> monthlyList) {
			this.dailyList = dailyList;
			this.weeklyList = weeklyList;
			this.monthlyList = monthlyList;
		}

		public List<Stock> getDailyList() {
			return dailyList;
		}

		public List<Stock> getWeeklyList() {
			return weeklyList;
		}

		public List<Stock> getMonthlyList() {
			return monthlyList;
		}

	}
	
	private class ReferencedXYDataset extends AbstractXYDataset implements XYDataset {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		private final List<Stock> stockList;
		
		public ReferencedXYDataset(String stockName) {
			stockList = getStockList(stockName);
		}
		
		@Override
		public int getSeriesCount() {
			return 1;
		}

		@Override
		public Comparable<?> getSeriesKey(int series) {
			throw new UnsupportedOperationException();
		}

		@Override
		public int getItemCount(int series) {
			return stockList.size();
		}

		@Override
		public Number getX(int series, int item) {
			return stockList.get(item).getDate().getTimeInMillis();
		}

		@Override
		public Number getY(int series, int item) {
			return getReferencedStockValue(stockList.get(item));
		}
		
	}
}
