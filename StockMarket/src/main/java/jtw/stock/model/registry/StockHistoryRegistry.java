package jtw.stock.model.registry;

import java.util.ArrayList;
import java.util.List;

public final class StockHistoryRegistry {

	private static StockHistoryRegistry INSTANCE = null;
	
	private List<String> stockList = new ArrayList<>();

	private StockHistoryRegistry() {
	}
	
	public synchronized static StockHistoryRegistry getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new StockHistoryRegistry();
		}
		return INSTANCE;
	}
	
	public void add(String stock) {
		stockList.add(stock);
	}
	
	public List<String> getStockList() {
		return stockList;
	}
	
	public String removeStock(int index) {
		return stockList.remove(index);
	}
	
}
