package jtw.stock.model.registry;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jtw.stock.main.StockApplicationSettings;

public final class StockSymbolNameRegistry {

	Log LOGGER = LogFactory.getLog(StockSymbolNameRegistry.class);
	
	private static StockSymbolNameRegistry INSTANCE = null;

	/**
	 * A map between the symbol and the company name.
	 */
	private Map<String, String> stockSymbolNameMap = new HashMap<>();

	public synchronized static StockSymbolNameRegistry getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new StockSymbolNameRegistry();
			INSTANCE.init();
		}
		return INSTANCE;
	}
	
	/**
	 * Returns the name of the company or the symbol if no mapping exists.
	 * 
	 * @param symbol The symbol of the company.
	 * 
	 * @return The company name of the stock symbol, or the symbol if no mapping exists.
	 */
	public String getCompany(String symbol) {
		String compName = stockSymbolNameMap.get(symbol);
		if (compName == null) {
			compName = symbol;
		}
		return compName;
	}

	private void init() {
		String symbolNameFile = StockApplicationSettings.getBasePath() + "symbolName.map";
		try (BufferedReader reader = new BufferedReader(new FileReader(symbolNameFile))) {
			String line = null;
			while ((line = reader.readLine()) != null) {
				line = line.trim();
				if (line.startsWith("#") || line.equals("")) {
					continue;
				}
				
				String[] ll = line.split("\\s", 2);
				String oldValue = stockSymbolNameMap.put(ll[0], ll[1]);
				if (oldValue != null) {
					LOGGER.warn("Symbol defined twice: " + ll[0] + ": new=" + ll[1] + "; old=" + oldValue);
				}
			}
		} catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
		}
	}

}
