package jtw.stock.main;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JRadioButton;

import jtw.stock.view.StockView;

public class ReferenceDatabaseSelectionListener implements ItemListener {

	private JRadioButton bOpen = null;
	private JRadioButton bClose = null;
	private JRadioButton bHigh = null;
	private JRadioButton bLow = null;
	
	public ReferenceDatabaseSelectionListener(JRadioButton bOpen, JRadioButton bClose, JRadioButton bHigh, JRadioButton bLow) {
		this.bOpen = bOpen;
		this.bClose = bClose;
		this.bHigh = bHigh;
		this.bLow = bLow;
	}
	
	@Override
	public void itemStateChanged(ItemEvent ie) {
		if (ie.getStateChange() == ItemEvent.SELECTED) {
			if (ie.getSource().equals(bOpen)) {
				StockApplicationSettings.setReferenceDataset(StockApplicationSettings.REFERENCE_DATASET_OPEN);	
			} else if (ie.getSource().equals(bClose)) {
				StockApplicationSettings.setReferenceDataset(StockApplicationSettings.REFERENCE_DATASET_CLOSE);	
			} else if (ie.getSource().equals(bHigh)) {
				StockApplicationSettings.setReferenceDataset(StockApplicationSettings.REFERENCE_DATASET_HIGH);	
			} else if (ie.getSource().equals(bLow)) {
				StockApplicationSettings.setReferenceDataset(StockApplicationSettings.REFERENCE_DATASET_LOW);	
			}
			StockView.getInstance().recompute();
		}
//		StockView.getInstance().repaint();
	}

}
