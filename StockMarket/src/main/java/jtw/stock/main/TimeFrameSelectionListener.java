package jtw.stock.main;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import jtw.stock.view.StockView;

public class TimeFrameSelectionListener implements ItemListener, ChangeListener {

	private JRadioButton bWeek = null;
	private JRadioButton bMonth = null;
	private JRadioButton bYear = null;
	private JRadioButton b5Year = null;
	private JRadioButton bMax = null;
	
	private JSlider slider = null;
	
	private static TimeFrameSelectionListener INSTANCE = null;
	
	protected TimeFrameSelectionListener() {
	}
	
	public static synchronized TimeFrameSelectionListener getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new TimeFrameSelectionListener();
		}
		return INSTANCE;
	}
	
	@Override
	public void itemStateChanged(ItemEvent ie) {
		if (ie.getStateChange() == ItemEvent.SELECTED) {
			if (ie.getSource().equals(bWeek)) {
				StockApplicationSettings.setTimeFrame(StockApplicationSettings.TIME_FRAME_WEEK);
			} else if (ie.getSource().equals(bMonth)) {
				StockApplicationSettings.setTimeFrame(StockApplicationSettings.TIME_FRAME_MONTH);
			} else if (ie.getSource().equals(bYear)) {
				StockApplicationSettings.setTimeFrame(StockApplicationSettings.TIME_FRAME_YEAR);
			} else if (ie.getSource().equals(b5Year)) {
				StockApplicationSettings.setTimeFrame(StockApplicationSettings.TIME_FRAME_5_YEAR);
			} else if (ie.getSource().equals(bMax)) {
				StockApplicationSettings.setTimeFrame(StockApplicationSettings.TIME_FRAME_MAX);
			}
			StockView.getInstance().recompute();
		}
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		if (e.getSource().equals(slider)) {
			if (!slider.getValueIsAdjusting()) {
				int customTimeFrame = -(int)slider.getValue();
				StockApplicationSettings.setTimeFrame(customTimeFrame);
				StockView.getInstance().recompute();
			}
		}
	}

	public JRadioButton getButtonWeek() {
		return bWeek;
	}

	public void setButtonWeek(JRadioButton bWeek) {
		this.bWeek = bWeek;
	}

	public JRadioButton getButtonMonth() {
		return bMonth;
	}

	public void setButtonMonth(JRadioButton bMonth) {
		this.bMonth = bMonth;
	}

	public JRadioButton getButtonYear() {
		return bYear;
	}

	public void setButtonYear(JRadioButton bYear) {
		this.bYear = bYear;
	}

	public JRadioButton getButton5Year() {
		return b5Year;
	}

	public void setButton5Year(JRadioButton b5Year) {
		this.b5Year = b5Year;
	}

	public JRadioButton getButtonMax() {
		return bMax;
	}

	public void setButtonMax(JRadioButton bMax) {
		this.bMax = bMax;
	}

	public JSlider getSlider() {
		return slider;
	}

	public void setSlider(JSlider slider) {
		this.slider = slider;
	}

}
