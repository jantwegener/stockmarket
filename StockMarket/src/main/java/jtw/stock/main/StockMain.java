package jtw.stock.main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.io.IOException;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JSlider;
import javax.swing.SwingUtilities;

import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

import jtw.stock.model.registry.CurrencyConversionRegistry;
import jtw.stock.view.OverlaySummaryView;
import jtw.stock.view.ProgressView;
import jtw.stock.view.StockView;
import jtw.stock.view.action.pref.PrefCurrencyAction;
import jtw.util.gui.SpringJMenuInitializer;

public class StockMain {
	
	private static final String MENU_PREFERENCE_ID = "jtw.menu.Preferences";
	
	private static ApplicationFrame frame;
	
	public static void main(String[] args) throws Exception {
		SwingUtilities.invokeLater(new Runnable() {
		    public void run() {
		        try {
					createAndShowGUI();
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
		    }
		});
	}
	
	public static JFrame getMainFrame() {
		return frame;
	}
	
	/**
	 * Returns the menu with the given menu name or null if there is no menu with the given name.
	 * 
	 * @param menuName The name of the menu.
	 * 
	 * @return The menu with the given name.
	 */
	public static JMenuItem getJMenu(String menuName) {
		JMenuBar bar = frame.getJMenuBar();
		for (int i = 0; i < bar.getMenuCount(); i++) {
			JMenu m = bar.getMenu(i);
			JMenuItem item = getJMenu(menuName, m);
			if (item != null) {
				return item;
			}
		}
		
		return null;
	}
	
	private static JMenuItem getJMenu(String menuName, JMenuItem currentMenu) {
		if (menuName.equals(currentMenu.getName())) {
			return currentMenu;
		}
		
		if (currentMenu instanceof JMenu) {
			for (int i = 0; i < ((JMenu) currentMenu).getItemCount(); i++) {
				JMenuItem x = getJMenu(menuName, ((JMenu) currentMenu).getItem(i));
				if (x != null) {
					return x;
				}
			}
		}
		
		return null;
	}
	
	private static void createAndShowGUI() throws IOException {
		frame = new ApplicationFrame("Stock");
		StockApplicationSettings.setMainFrame(frame);
		frame.setExtendedState(frame.getExtendedState() | ApplicationFrame.MAXIMIZED_BOTH);
		
		SpringJMenuInitializer minit = new SpringJMenuInitializer("menu.xml");
		JMenuBar bar = minit.getJMenuBar();
		JMenu preferenceMenu = minit.getMenu(MENU_PREFERENCE_ID);
		JMenu currencyMenu = new JMenu("Currency");
		ButtonGroup currencyGroup = new ButtonGroup();
		for (String currencyCode : CurrencyConversionRegistry.getInstance().getAvailableCurrencies()) {
			JRadioButtonMenuItem currencyMenuItem = new JRadioButtonMenuItem(currencyCode);
			currencyMenuItem.addItemListener(new PrefCurrencyAction(currencyCode));
			// select euro as default
			if ("EUR".equals(currencyCode)) {
				currencyMenuItem.setSelected(true);
			}
			
			currencyMenu.add(currencyMenuItem);
			
			currencyGroup.add(currencyMenuItem);
		}
		preferenceMenu.add(currencyMenu);
		frame.setJMenuBar(bar);

		JPanel mainPanel = new JPanel(new BorderLayout());
		frame.setContentPane(mainPanel);

		mainPanel.add(getNorthPanel(), BorderLayout.NORTH);
		mainPanel.add(StockView.getInstance(), BorderLayout.CENTER);
		mainPanel.add(OverlaySummaryView.getInstance(), BorderLayout.EAST);
		mainPanel.add(ProgressView.getInstance(), BorderLayout.SOUTH);
		
		StockView.getInstance().addAllActionWithDefaultEnabledStateList(minit.getActionWithDefaultEnabledStateList());
		StockView.getInstance().resetMenuBar();

		frame.pack();

		RefineryUtilities.centerFrameOnScreen(frame);
		frame.setVisible(true);
	}
	
	private static JPanel getNorthPanel() {
		JPanel north = new JPanel(new GridLayout(2, 2));
		
		north.add(getTimeFrameControlPanel());
		north.add(getTimeBaseControlPanel());
		north.add(getCustomTimeFrameControlPanel());
		north.add(getReferenceDatasetControlPanel());
		
		return north;
	}
	
	private static JPanel getCustomTimeFrameControlPanel() {
		JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));

		TimeFrameSelectionListener timeFrameListener = TimeFrameSelectionListener.getInstance();
		
		int max  = 100;
		JSlider customTimeFrameSlider = new JSlider(JSlider.HORIZONTAL, 0, max, max);
		customTimeFrameSlider.setMajorTickSpacing(10);
		customTimeFrameSlider.setPaintTicks(true);
		customTimeFrameSlider.setPaintLabels(true);
		customTimeFrameSlider.addChangeListener(timeFrameListener);
		Dimension d = customTimeFrameSlider.getPreferredSize();
		d.width += 100;
		customTimeFrameSlider.setPreferredSize(d);
		
		timeFrameListener.setSlider(customTimeFrameSlider);
		
		p.add(customTimeFrameSlider);
		
		return p;
	}
	
	/**
	 * The panel to control the visible time frame. This means the data that is loaded (week, month, year, five years, max).
	 * 
	 * @return
	 */
	private static JPanel getTimeFrameControlPanel() {
		JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));

		JRadioButton bWeek = new JRadioButton("week");
		JRadioButton bMonth = new JRadioButton("month");
		JRadioButton bYear = new JRadioButton("year");
		JRadioButton b5Year = new JRadioButton("5 years");
		JRadioButton bMax = new JRadioButton("max");

		TimeFrameSelectionListener timeFrameListener = TimeFrameSelectionListener.getInstance();
		timeFrameListener.setButtonWeek(bWeek);
		timeFrameListener.setButtonMonth(bMonth);
		timeFrameListener.setButtonYear(bYear);
		timeFrameListener.setButton5Year(b5Year);
		timeFrameListener.setButtonMax(bMax);
		
		bWeek.addItemListener(timeFrameListener);
		bMonth.addItemListener(timeFrameListener);
		bYear.addItemListener(timeFrameListener);
		b5Year.addItemListener(timeFrameListener);
		bMax.addItemListener(timeFrameListener);

		ButtonGroup group = new ButtonGroup();
		group.add(bWeek);
		group.add(bMonth);
		group.add(bYear);
		group.add(b5Year);
		group.add(bMax);

		bMax.setSelected(true);

		p.add(bWeek);
		p.add(bMonth);
		p.add(bYear);
		p.add(b5Year);
		p.add(bMax);

		return p;
	}

	/**
	 * The panel to control the time base. This means the underlying basis for the calculations (daily, weekly, monthly).
	 * 
	 * @return The control panel.
	 */
	private static JPanel getTimeBaseControlPanel() {
		JPanel p = new JPanel(new FlowLayout(FlowLayout.RIGHT));

		JRadioButton bDay = new JRadioButton("1d");
		JRadioButton bWeek = new JRadioButton("1w");
		JRadioButton bMonth = new JRadioButton("1m");

		TimeBaseSelectionListener timeBaseListener = new TimeBaseSelectionListener(bDay, bWeek, bMonth);
		
		bDay.addItemListener(timeBaseListener);
		bWeek.addItemListener(timeBaseListener);
		bMonth.addItemListener(timeBaseListener);

		ButtonGroup group = new ButtonGroup();
		group.add(bDay);
		group.add(bWeek);
		group.add(bMonth);

		bDay.setSelected(true);

		p.add(bDay);
		p.add(bWeek);
		p.add(bMonth);

		return p;
	}

	private static JPanel getReferenceDatasetControlPanel() {
		JPanel p = new JPanel(new FlowLayout(FlowLayout.RIGHT));

		JRadioButton bOpen = new JRadioButton("open");
		JRadioButton bClose = new JRadioButton("close");
		JRadioButton bHigh = new JRadioButton("high");
		JRadioButton bLow = new JRadioButton("low");

		ReferenceDatabaseSelectionListener datasetListener = new ReferenceDatabaseSelectionListener(bOpen, bClose, bHigh, bLow);
		bOpen.addItemListener(datasetListener);
		bClose.addItemListener(datasetListener);
		bHigh.addItemListener(datasetListener);
		bLow.addItemListener(datasetListener);

		ButtonGroup group = new ButtonGroup();
		group.add(bOpen);
		group.add(bClose);
		group.add(bHigh);
		group.add(bLow);
		
		bClose.setSelected(true);
		
		p.add(bOpen);
		p.add(bClose);
		p.add(bHigh);
		p.add(bLow);
		
		return p;
	}

}
