package jtw.stock.main;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jfree.ui.ApplicationFrame;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import jtw.stock.model.Stock;
import jtw.stock.model.registry.StockDataRegistry;
import jtw.stock.view.component.FileEndingNameFilter;

public final class StockApplicationSettings {

	private static Log LOGGER = LogFactory.getLog(StockApplicationSettings.class);
	
	private static ApplicationFrame MAIN_FRAME;
	
	public static final int TIME_BASE_DAILY = 1;
	public static final int TIME_BASE_WEEKLY = 2;
	public static final int TIME_BASE_MONTHLY = 3;
	
	public static final int TIME_FRAME_WEEK = 1;
	public static final int TIME_FRAME_MONTH = 2;
	public static final int TIME_FRAME_YEAR = 3;
	public static final int TIME_FRAME_5_YEAR = 4;
	public static final int TIME_FRAME_MAX = 5;
	
	public static final int REFERENCE_DATASET_OPEN = 0;
	public static final int REFERENCE_DATASET_CLOSE = 1;
	public static final int REFERENCE_DATASET_HIGH = 2;
	public static final int REFERENCE_DATASET_LOW = 3;
	public static final int REFERENCE_DATASET_VOLUME = 4;
	
	private static String STOCK_NAME = null;
	
	private static Currency CURRENCY = null;

	private static String SETTING_FILE_XML = "settings.xml";
	
	private static final String BASE_PATH;
	
	private static int TIME_BASE = TIME_BASE_DAILY;

	/**
	 * The visible and loaded stocks are within the time frame.
	 */
	private static int TIME_FRAME = TIME_FRAME_MAX;
	
	private static int REFERENCE_DATASET;
	
	private static Calendar EARLIEST_TIME = Calendar.getInstance();
	
	static {
		if (System.getProperty("jtw.setting.basePath") != null) {
			if (System.getProperty("jtw.setting.basePath").endsWith(File.separator)) {
				BASE_PATH = System.getProperty("jtw.setting.basePath");
			} else {
				BASE_PATH = System.getProperty("jtw.setting.basePath") + File.separator;
			}
		} else {
			try (ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(SETTING_FILE_XML)) {
				BASE_PATH = (String) context.getBean("jtw.setting.basePath");
			}
		}
	}
	
	public static int getReferenceDataset() {
		return REFERENCE_DATASET;
	}
	
	public static void setReferenceDataset(int ds) {
		REFERENCE_DATASET = ds;
	}

	public static boolean isOpenReferenceDataset() {
		return REFERENCE_DATASET == REFERENCE_DATASET_OPEN;
	}

	public static boolean isCloseReferenceDataset() {
		return REFERENCE_DATASET == REFERENCE_DATASET_CLOSE;
	}

	public static boolean isHighReferenceDataset() {
		return REFERENCE_DATASET == REFERENCE_DATASET_HIGH;
	}

	public static boolean isLowReferenceDataset() {
		return REFERENCE_DATASET == REFERENCE_DATASET_LOW;
	}
	
	public static int getTimeBase() {
		return TIME_BASE;
	}
	
	public static boolean isTimeBaseDaily() {
		return TIME_BASE == TIME_BASE_DAILY;
	}
	
	public static boolean isTimeBaseWeekly() {
		return TIME_BASE == TIME_BASE_WEEKLY;
	}

	public static boolean isTimeBaseMonthly() {
		return TIME_BASE == TIME_BASE_MONTHLY;
	}
	
	public static void setTimeBase(int timeBase) {
		StockApplicationSettings.TIME_BASE = timeBase;
	}
	
	public static int getTimeFrame() {
		return StockApplicationSettings.TIME_FRAME;
	}
	
	public static void setTimeFrame(int timeFrame) {
		StockApplicationSettings.TIME_FRAME = timeFrame;

		// reset the time
		EARLIEST_TIME = computeEarliestTime(timeFrame);
	}
	
	public static Calendar computeEarliestTime(int timeFrame) {
		Calendar earliestTime = Calendar.getInstance();
		
		if (timeFrame < 0) {
			// a custom time frame
			// using percents
			int tf = (int) ((timeFrame / 100d) * StockDataRegistry.getInstance().getXSpan());
			earliestTime.add(Calendar.DATE, tf);
		} else {
			switch (timeFrame) {
				case TIME_FRAME_WEEK:
					// 7 days in a week
					earliestTime.add(Calendar.DATE, -7);
					break;
				case TIME_FRAME_MONTH:
					// one month
					earliestTime.add(Calendar.MONTH, -1);
					break;
				case TIME_FRAME_YEAR:
					// one year
					earliestTime.add(Calendar.YEAR, -1);
					break;
				case TIME_FRAME_5_YEAR:
					// one year
					earliestTime.add(Calendar.YEAR, -5);
					break;
				case TIME_FRAME_MAX:
					earliestTime.set(1970, 0, 1);
					break;
				default:
					earliestTime.set(1970, 0, 1);
			}
		}
		
		return earliestTime;
	}
	
	/**
	 * Tests whether the given stock is within the currently set time frame.
	 * 
	 * @param s The stock to test.
	 * 
	 * @return The current time frame.
	 */
	public static boolean isInTimeFrame(Stock s) {
		return isInTimeFrame(s, EARLIEST_TIME);
	}
	
	/**
	 * Tests whether the given stock is within the currently set time frame.
	 * 
	 * @param s The stock to test.
	 * @param earliestTime The earliest for the time frame.
	 * 
	 * @return The current time frame.
	 */
	public static boolean isInTimeFrame(Stock s, Calendar earliestTime) {
		return earliestTime.before(s.getDate());
	}
	
	public static String getStockName() {
		return STOCK_NAME;
	}

	public static void setStockName(String stockName) {
		STOCK_NAME = stockName;
	}
	
	public static Currency getCurrency() {
		return CURRENCY;
	}
	
	public static void setCurrency(String currencyCode) {
		CURRENCY = Currency.getInstance(currencyCode);
	}
	
	public static void setCurrency(Currency currencyCode) {
		CURRENCY = currencyCode;
	}
	
	public static String getBasePath() {
		return BASE_PATH;
	}

	/**
	 * Returns the path to the given file name with the given ending.
	 * 
	 * @param fileName The name of the file.
	 * @param ending The ending of the file with or without ".".
	 * 
	 * @return The path to the file.
	 */
	public static String getLocalPath(String fileName, String ending) {
		if (!ending.startsWith(".")) {
			ending = "." + ending;
		}
		return StockApplicationSettings.getBasePath() + fileName + ending;
	}
	
	public static String[] loadAlreadyAvailableArray(String ending) {
		return loadAlreadyAvailableList(ending).toArray(new String[] {});
	}
	
	public static List<String> loadAlreadyAvailableList(String ending) {
		if (!ending.startsWith(".")) {
			ending = "." + ending;
		}
		List<String> alreadyLoadedList = new ArrayList<>();
		File folder = new File(StockApplicationSettings.getBasePath());
		if (!folder.exists()) {
			String errorMsg = String.format("The path '%s' does not exist.", folder.getAbsolutePath());
			LOGGER.error(errorMsg);
			throw new RuntimeException(errorMsg);
		}
		File[] listFiles = folder.listFiles(new FileEndingNameFilter(ending));
		if (listFiles != null) {
			for (File file : listFiles) {
				alreadyLoadedList.add(file.getName().substring(0, file.getName().length() - ending.length()));
			}
		} else {
			LOGGER.warn(String.format("The path '%s' is empty.", folder.getAbsolutePath()));
		}
		return alreadyLoadedList;
	}
	
	public static boolean isFileExists(String name, String ending) {
		File f = new File(getLocalPath(name, ending));
		return f.exists();
	}

	public static ApplicationFrame getMainFrame() {
		return MAIN_FRAME;
	}

	public static void setMainFrame(ApplicationFrame mainFrame) {
		MAIN_FRAME = mainFrame;
	}
	
}
