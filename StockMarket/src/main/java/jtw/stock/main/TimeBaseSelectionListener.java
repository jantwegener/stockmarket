package jtw.stock.main;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JRadioButton;

import jtw.stock.view.StockView;

public class TimeBaseSelectionListener implements ItemListener {

	private JRadioButton bDay = null;
	private JRadioButton bWeek = null;
	private JRadioButton bMonth = null;
	
	public TimeBaseSelectionListener(JRadioButton bDay, JRadioButton bWeek, JRadioButton bMonth) {
		this.bDay = bDay;
		this.bWeek = bWeek;
		this.bMonth = bMonth;
	}
	
	@Override
	public void itemStateChanged(ItemEvent ie) {
		if (ie.getStateChange() == ItemEvent.SELECTED) {
			if (ie.getSource().equals(bDay)) {
				StockApplicationSettings.setTimeBase(StockApplicationSettings.TIME_BASE_DAILY);
			} else if (ie.getSource().equals(bWeek)) {
				StockApplicationSettings.setTimeBase(StockApplicationSettings.TIME_BASE_WEEKLY);
			} else if (ie.getSource().equals(bMonth)) {
				StockApplicationSettings.setTimeBase(StockApplicationSettings.TIME_BASE_MONTHLY);
			}
			StockView.getInstance().recompute();
		}
	}

}
