package jtw.stock.control;

import java.util.List;

import jtw.stock.model.Stock;

public class WeightedMovingAverageStockController extends SimpleMovingAverageStockController {
	
	public WeightedMovingAverageStockController(String stockName, int daysAverage) {
		super(stockName, "WeightedMovingAverage", daysAverage);
	}
	
	protected String getChartName() {
		return getStockName() + " (" + getDaysAverage() + ")";
	}

	protected String getDisplayName() {
		return "Weighted Moving Average (" + getDaysAverage() + ")";
	}

	protected double computeAverage(List<Stock> stockList, int index) {
		double retval = 0d;
		int sumDays = 0;
		for (int i = 0; i < getDaysAverage(); i++) {
			if (index + i < stockList.size()) {
				Stock stock = stockList.get(index + i);
				int day = (getDaysAverage() - i);
				sumDays += day;
				retval += day * stock.getClosing();
			} else {
				retval = -1;
				break;
			}
		}
		retval /= sumDays;
		return retval;
	}
	
}
