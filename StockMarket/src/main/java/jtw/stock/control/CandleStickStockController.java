package jtw.stock.control;

import java.util.Date;
import java.util.List;

import org.jfree.chart.labels.HighLowItemLabelGenerator;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.CandlestickRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.xy.DefaultOHLCDataset;
import org.jfree.data.xy.OHLCDataItem;

import jtw.stock.model.Stock;

public class CandleStickStockController extends StockController {

	public CandleStickStockController(String stockName) {
		super(stockName, "Candle");
	}

	@Override
	protected String getDisplayName() {
		return "Candle Stick";
	}

	@Override
	protected XYPlot doComputePlot(List<Stock> stockList) {
		OHLCDataItem[] data = new OHLCDataItem[stockList.size()];
		for (int i = 0; i < stockList.size(); i++) {
			Stock s = stockList.get(i);
			Date date = s.getDate().getTime();
			double open = s.getOpening();
			double high = s.getHigh();
			double low = s.getLow();
			double close = s.getClosing();
			double volume = s.getVolume();

			data[i] = new OHLCDataItem(date, open, high, low, close, volume);
		}
		
		DefaultOHLCDataset dataset = new DefaultOHLCDataset(getStockName(), data);

		final XYItemRenderer renderer = new CandlestickRenderer(3d, false, new HighLowItemLabelGenerator());

		XYPlot plot = new XYPlot(dataset, X_AXIS_DATE, Y_AXIS_PRICE, renderer);

		return plot;
	}

}
