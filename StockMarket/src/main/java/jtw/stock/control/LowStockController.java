package jtw.stock.control;

import java.util.List;

import jtw.stock.model.Stock;

public class LowStockController extends StandardStockController {

	public LowStockController(String stockName) {
		this(stockName, "Low");
	}
	
	protected LowStockController(String stockName, String name) {
		super(stockName, name);
	}

	protected String getDisplayName() {
		return "Low";
	}

	protected double doComputeData(List<Stock> stockList, Stock s, int index, int xy) {
		double retval = 0;
		switch (xy) {
		case 0:
			retval = s.getDate().getTimeInMillis();
			break;
		case 1:
			retval = s.getLow();
			break;
		}
		return retval;
	}
	
}
