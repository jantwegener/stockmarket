package jtw.stock.control;

import java.util.List;

import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.xy.DefaultXYDataset;

import jtw.stock.model.Stock;

public abstract class StandardStockController extends StockController {
	
	protected StandardStockController(String stockName, String name) {
		super(stockName, name);
	}

	protected int getDataSize(List<Stock> stockList) {
		return stockList.size();
	}

	protected double[][] computeData(List<Stock> stockList) {
		int size = getDataSize(stockList);
		double[][] data = new double[2][size];

		for (int i = 0; i < size; i++) {
			Stock s = stockList.get(i);
			data[0][i] = doComputeData(stockList, s, i, 0);
			data[1][i] = doComputeData(stockList, s, i, 1);
		}

		return data;
	}

	protected double doComputeData(List<Stock> stockList, Stock s, int index, int xy) {
		double retval = 0;
		switch (xy) {
		case 0:
			retval = s.getDate().getTimeInMillis();
			break;
		case 1:
			retval = s.getClosing();
			break;
		}
		return retval;
	}
	
	protected String getChartName() {
		return getStockName();
	}

	@Override
	protected XYPlot doComputePlot(List<Stock> stockList) {
		double[][] data = computeData(stockList);

		DefaultXYDataset dataset = new DefaultXYDataset();
		dataset.addSeries(getDisplayName(), data);

		final XYItemRenderer renderer = new StandardXYItemRenderer();

		XYPlot plot = new XYPlot(dataset, X_AXIS_DATE, Y_AXIS_PRICE, renderer);

		return plot;
	}

}
