package jtw.stock.control;

import java.util.List;

import javax.swing.JOptionPane;

import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;

import jtw.stock.main.StockApplicationSettings;
import jtw.stock.model.Stock;
import jtw.stock.model.registry.StockDataRegistry;
import jtw.stock.view.StockView;

public abstract class StockController {

	private final String stockName;

	private final String name;

	protected final static NumberAxis Y_AXIS_PRICE = new NumberAxis("Price");
	protected final static NumberAxis Y_AXIS_VOLUME = new NumberAxis("Volume");

	protected final static DateAxis X_AXIS_DATE = new DateAxis("Date");

	static {
		Y_AXIS_PRICE.setAutoRangeIncludesZero(false);
		Y_AXIS_PRICE.setAutoRangeMinimumSize(5);
		Y_AXIS_PRICE.setAutoRange(true);

		// X_AXIS_DATE.set
	}

	protected StockController(String stockName, String name) {
		if (stockName.endsWith(".csv")) {
			this.stockName = stockName.substring(0, stockName.length() - ".csv".length());
		} else {
			this.stockName = stockName;
		}
		this.name = name;
	}

	public boolean isSubPlot() {
		return false;
	}

	public String getName() {
		return name;
	}

	public String getStockName() {
		return stockName;
	}

	public String getPlotName() {
		return stockName + "_" + name;
	}
	
	protected abstract String getDisplayName();
	
	public void computePlot() {
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					StockApplicationSettings.setStockName(stockName);

					List<Stock> stockList = StockDataRegistry.getInstance().getStockList(stockName);

					XYPlot plot = doComputePlot(stockList);
					StockView.getInstance().addPlot(getPlotName(), plot, isSubPlot());
				} catch (Exception e) {
					e.printStackTrace();

					JOptionPane.showMessageDialog(StockView.getInstance(),
							"Error computing plot: \"" + e.getMessage() + "\"", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		t.start();
	}
	
	/**
	 * Computes the plot.
	 */
	protected abstract XYPlot doComputePlot(List<Stock> stockList);

}
