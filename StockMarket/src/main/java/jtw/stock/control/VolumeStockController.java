package jtw.stock.control;

import java.util.List;

import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.xy.DefaultXYDataset;

import jtw.stock.model.Stock;

public class VolumeStockController extends StockController {

	public VolumeStockController(String stockName) {
		super(stockName, "Volume");
	}

	public String getDisplayName() {
		return "Volume";
	}
	
	public boolean isSubPlot() {
		return true;
	}
	
	@Override
	protected XYPlot doComputePlot(List<Stock> stockList) {
		int size = stockList.size();
		double[][] data = new double[2][size];

		for (int i = 0; i < size; i++) {
			Stock s = stockList.get(i);
			data[0][i] = s.getDate().getTimeInMillis();
			data[1][i] = s.getVolume();
		}

		DefaultXYDataset dataset = new DefaultXYDataset();
		dataset.addSeries(getDisplayName(), data);

		final XYItemRenderer renderer = new StandardXYItemRenderer();

		XYPlot plot = new XYPlot(dataset, X_AXIS_DATE, Y_AXIS_VOLUME, renderer);

		return plot;
	}

}
