package jtw.stock.control;

import java.util.List;

import jtw.stock.model.Stock;

public class HighStockController extends StandardStockController {

	public HighStockController(String stockName) {
		this(stockName, "High");
	}
	
	protected HighStockController(String stockName, String name) {
		super(stockName, name);
	}

	protected String getDisplayName() {
		return "High";
	}

	protected double doComputeData(List<Stock> stockList, Stock s, int index, int xy) {
		double retval = 0;
		switch (xy) {
		case 0:
			retval = s.getDate().getTimeInMillis();
			break;
		case 1:
			retval = s.getHigh();
			break;
		}
		return retval;
	}
	
}
