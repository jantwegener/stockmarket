package jtw.stock.control;

import java.util.List;

import jtw.stock.model.Stock;

public class ClosingStockController extends StandardStockController {

	public ClosingStockController(String stockName) {
		this(stockName, "Close");
	}
	
	protected ClosingStockController(String stockName, String name) {
		super(stockName, name);
	}

	protected double doComputeData(List<Stock> stockList, Stock s, int index, int xy) {
		double retval = 0;
		switch (xy) {
		case 0:
			retval = s.getDate().getTimeInMillis();
			break;
		case 1:
			retval = s.getClosing();
			break;
		}
		return retval;
	}
	
	protected String getDisplayName() {
		return "Close";
	}

}
