package jtw.stock.control;

import java.util.List;

import jtw.stock.io.HistoryReader;
import jtw.stock.model.Stock;

public class SimpleMovingAverageStockController extends StandardStockController {

	private int daysAverage;

	public SimpleMovingAverageStockController(String stockName, int daysAverage) {
		this(stockName, "SimpleMovingAverage", daysAverage);
	}

	protected SimpleMovingAverageStockController(String stockName, String name, int daysAverage) {
		super(stockName, name);
		this.daysAverage = daysAverage;
	}

	protected String getChartName() {
		return getStockName() + " (" + daysAverage + ")";
	}

	protected String getDisplayName() {
		return "Simple Moving Average (" + daysAverage + ")";
	}

	protected List<Stock> getStockList(HistoryReader reader) {
		List<Stock> stockList = reader.getDailyList();
		return stockList;
	}

	protected int getDataSize(List<Stock> stockList) {
		return Math.max(0, stockList.size() - daysAverage);
	}

	public int getDaysAverage() {
		return daysAverage;
	}

	protected double doComputeData(List<Stock> stockList, Stock s, int index, int xy) {
		double retval = 0;
		switch (xy) {
		case 0:
			retval = s.getDate().getTimeInMillis();
			break;
		case 1:
			retval = computeAverage(stockList, index);
			break;
		}
		return retval;
	}

	protected double computeAverage(List<Stock> stockList, int index) {
		double retval = 0d;
		for (int i = 0; i < daysAverage; i++) {
			if (index + i < stockList.size()) {
				Stock stock = stockList.get(index + i);
				retval += stock.getClosing();
			} else {
				retval = -1;
				break;
			}
		}
		retval /= daysAverage;
		return retval;
	}

}
