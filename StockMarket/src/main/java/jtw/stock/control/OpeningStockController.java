package jtw.stock.control;

import java.util.List;

import jtw.stock.model.Stock;

public class OpeningStockController extends StandardStockController {

	public OpeningStockController(String stockName) {
		this(stockName, "Open");
	}
	
	protected OpeningStockController(String stockName, String name) {
		super(stockName, name);
	}

	protected String getDisplayName() {
		return "Open";
	}

	protected double doComputeData(List<Stock> stockList, Stock s, int index, int xy) {
		double retval = 0;
		switch (xy) {
		case 0:
			retval = s.getDate().getTimeInMillis();
			break;
		case 1:
			retval = s.getOpening();
			break;
		}
		return retval;
	}
	
}
