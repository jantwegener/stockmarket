package jtw.stock.util;

import java.awt.geom.Point2D;
import java.util.Arrays;
import java.util.List;

import org.jfree.data.xy.XYDataset;

import jtw.stock.model.Stock;

public class ValueFinder {
	
	/**
	 * Finds the maximum within a radius.
	 * 
	 * @param ds 
	 * @param index
	 * @param radius
	 * 
	 * @return
	 */
	public static Point2D findPointMaxInRadius(XYDataset ds, int index, int radius) {
		double x = 0;
		double y = Double.NEGATIVE_INFINITY;

		for (int i = index - radius; i < index + radius; i++) {
			if (i >= 0 && i < ds.getItemCount(0) && y < ds.getYValue(0, i)) {
				x = ds.getXValue(0, i);
				y = ds.getYValue(0, i);
			}
		}
		
		Point2D retPoint = null;
		if (y > 0) {
			retPoint = new Point2D.Double(x, y);
		}
		
		return retPoint;
	}

	/**
	 * Finds the maximum within a radius.
	 * 
	 * @param ds 
	 * @param index
	 * @param radius
	 * 
	 * @return
	 */
	public static Point2D findPointMinInRadius(XYDataset ds, int index, int radius) {
		double x = 0;
		double y = Double.POSITIVE_INFINITY;

		for (int i = index - radius; i < index + radius; i++) {
			if (i >= 0 && i < ds.getItemCount(0) && y > ds.getYValue(0, i)) {
				x = ds.getXValue(0, i);
				y = ds.getYValue(0, i);
			}
		}
		
		Point2D retPoint = null;
		if (y > 0) {
			retPoint = new Point2D.Double(x, y);
		}
		
		return retPoint;
	}
	
	/**
	 * Returns the X coordinates for the maximal Y values.
	 * For example if ds = [(1, 7), (2, 4), (3, 2), (4, 9), (5, 3)] and m = 2 then the method returns [4, 1].
	 * 
	 * @param ds The data set.
	 * @param m The number of maximal values.
	 * @param startIndex Where to start the search (inclusive).
	 * @param stopIndex Where to stop the search (exclusive).
	 * 
	 * @return The x-coordinates for the m maximal Y values.
	 */
	public static double[] findXMax(XYDataset ds, int m, int startIndex, int stopIndex) {
		double[] x = new double[m];
		double[] y = new double[m];
		Arrays.fill(x, Double.NEGATIVE_INFINITY);
		Arrays.fill(y, Double.NEGATIVE_INFINITY);
		
		for (int i = startIndex; i < stopIndex; i++) {
			int j = getIndexMax(ds.getYValue(0, i), y);
			if (j >= 0) {
				shiftRight(x, j);
				shiftRight(y, j);
				x[j] = ds.getXValue(0, i);
				y[j] = ds.getYValue(0, i);
			}
		}
		
		return x;
	}
	
	public static double[] findXMin(XYDataset ds, int m, int startIndex, int stopIndex) {
		double[] x = new double[m];
		double[] y = new double[m];
		Arrays.fill(x, Double.POSITIVE_INFINITY);
		Arrays.fill(y, Double.POSITIVE_INFINITY);
		
		for (int i = startIndex; i < stopIndex; i++) {
			int j = getIndexMin(ds.getYValue(0, i), y);
			if (j >= 0) {
				shiftRight(x, j);
				shiftRight(y, j);
				x[j] = ds.getXValue(0, i);
				y[j] = ds.getYValue(0, i);
			}
		}
		
		return x;
	}
	
	public static double findYMax(List<Stock> stockList, int startIndex, int stopIndex) {
    	if (startIndex < 0) {
    		startIndex = 0;
    	}
    	if (stopIndex > stockList.size()) {
    		stopIndex = stockList.size();
    	}
    	
		double y = -Double.MAX_VALUE;
		for (int i = startIndex; i < stopIndex; i++) {
			Stock s = stockList.get(i);
			if (y < s.getHigh()) {
				y = s.getHigh();
			}
		}
		return y;
	}

	public static double findYMin(List<Stock> stockList, int startIndex, int stopIndex) {
    	if (startIndex < 0) {
    		startIndex = 0;
    	}
    	if (stopIndex > stockList.size()) {
    		stopIndex = stockList.size();
    	}
    	
		double y = Double.MAX_VALUE;
		for (int i = startIndex; i < stopIndex; i++) {
			Stock s = stockList.get(i);
			if (y > s.getLow()) {
				y = s.getLow();
			}
		}
		return y;
	}
	
	private static void shiftRight(double[] val, int index) {
		for (int i = val.length - 2; i >= index; i--) {
			val[i + 1] = val[i];
		}
	}

	private static int getIndexMax(double yValue, double[] yCurrMax) {
		int index = -1;
		
		for (int i = 0; i < yCurrMax.length && index < 0; i++) {
			if (yCurrMax[i] < yValue) {
				index = i;
			}
		}
		
		return index;
	}

	private static int getIndexMin(double yValue, double[] yCurrMin) {
		int index = -1;
		
		for (int i = 0; i < yCurrMin.length && index < 0; i++) {
			if (yCurrMin[i] > yValue) {
				index = i;
			}
		}
		
		return index;
	}
	
}
