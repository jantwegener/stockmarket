package jtw.stock.util;

import java.util.List;

import org.jfree.data.xy.AbstractXYDataset;
import org.jfree.data.xy.XYDataset;

import jtw.util.ObjectToDouble;

public class ListXYDatasetWrapper<E> extends AbstractXYDataset implements XYDataset {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final List<E> underlyingList;
	
	private final ObjectToDouble<E> xotd;
	
	private final ObjectToDouble<E> yotd;
	
	private final String seriesKey;
	
	public ListXYDatasetWrapper(String seriesKey, List<E> underlyingList, ObjectToDouble<E> xotd, ObjectToDouble<E> yotd) {
		this.seriesKey = seriesKey;
		this.underlyingList = underlyingList;
		this.xotd = xotd;
		this.yotd = yotd;
	}
	
	@Override
	public int getSeriesCount() {
		return 1;
	}

	@Override
	public Comparable<?> getSeriesKey(int series) {
		return seriesKey;
	}

	@Override
	public int getItemCount(int series) {
		return underlyingList.size();
	}

	@Override
	public Number getX(int series, int item) {
		return xotd.objToDouble(underlyingList.get(item));
	}

	@Override
	public Number getY(int series, int item) {
		return yotd.objToDouble(underlyingList.get(item));
	}
	
}
