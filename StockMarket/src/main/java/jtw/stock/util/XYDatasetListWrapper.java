package jtw.stock.util;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.jfree.data.xy.XYDataset;

public class XYDatasetListWrapper implements List<Number> {
	
	/**
	 * The referenced series.
	 */
	private int series;
	
	/**
	 * The underlying dataset.
	 */
	private XYDataset ds;
	
	private boolean xReference = true;
	
	public XYDatasetListWrapper(XYDataset ds, int series, boolean xReference) {
		this.ds = ds;
		this.series = series;
		this.xReference = xReference;
	}
	
	public void switchToXReference() {
		xReference = true;
	}

	public void switchToYReference() {
		xReference = false;
	}

	@Override
	public Number get(int index) {
		if (index < 0) {
			index = ds.getItemCount(series) - 1;
		}
		return xReference ? ds.getX(series, index) : ds.getY(series, index);
	}

	@Override
	public int size() {
		return ds.getItemCount(series);
	}

	@Override
	public boolean isEmpty() {
		return ds.getItemCount(series) == 0;
	}

	@Override
	public boolean contains(Object o) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Iterator<Number> iterator() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Object[] toArray() {
		throw new UnsupportedOperationException();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean add(Number e) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean remove(Object o) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean addAll(Collection<? extends Number> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean addAll(int index, Collection<? extends Number> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void clear() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Number set(int index, Number element) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void add(int index, Number element) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Number remove(int index) {
		throw new UnsupportedOperationException();
	}

	@Override
	public int indexOf(Object o) {
		throw new UnsupportedOperationException();
	}

	@Override
	public int lastIndexOf(Object o) {
		throw new UnsupportedOperationException();
	}

	@Override
	public ListIterator<Number> listIterator() {
		throw new UnsupportedOperationException();
	}

	@Override
	public ListIterator<Number> listIterator(int index) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<Number> subList(int fromIndex, int toIndex) {
		throw new UnsupportedOperationException();
	}

}
