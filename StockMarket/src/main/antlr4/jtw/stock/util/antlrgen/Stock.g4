// The stock value grammar
grammar Stock;

options { tokenVocab=StockLexer; }


calcExpr
	: mathPlusExpr
	;

mathPlusExpr
	: mathPlusExpr PLUS mathMultExpr	# Addition
	| mathPlusExpr MINUS mathMultExpr	# Subtraction
	| mathMultExpr						# ToMathMult
	;

mathMultExpr
	: mathMultExpr MULT atom			# Multiplication
	| mathMultExpr DIV atom				# Division
	| atom								# ToAtom
	;

atom
	: special							# SpecialVariables
	| varMultipleIndex					# VariableMultipleIndex
	| var								# Variable
	| varNoIndex						# VariableWithoutIndex
	| con								# Constant
	| mathFunction						# Function
	| LPAREN boolExpr RPAREN			# Braces
	;

mathFunction
	: ifFunction
	| funcMax
	| funcMin
	| funcAbs
	| funcThreshold
	;

// A[5][1]
varMultipleIndex
	: name LBRAK index RBRAK LBRAK index2 RBRAK
	;

// A[5]
var
	: name LBRAK index RBRAK
	;

varNoIndex
	: name
	;

name    : LETTER ;
index   : NUMBER ;
index2  : NUMBER ;
con     : NUMBER ;
special : SPECIALPREFIX SPECIALVAR;

ifFunction
	: LBRACE boolExpr IFMARKER trueExpr IFSEP falseExpr RBRACE
	;

trueExpr : calcExpr;
falseExpr : calcExpr;

funcMax : MAX LPAREN calcExpr (COMMA calcExpr)+ RPAREN;
funcMin : MIN LPAREN calcExpr (COMMA calcExpr)+ RPAREN;
funcAbs : ABS LPAREN calcExpr RPAREN;
funcThreshold : THRESHOLD LPAREN thresholdValueExpr COMMA calcExpr RPAREN; // if expr >= thresholdValueExpr => expr else 0

thresholdValueExpr : calcExpr;

// boolean expressions

boolExpr
	: NOT boolOrExpr					# Not
	| LPAREN boolExpr RPAREN			# ParenExpr
	| boolOrExpr						# ToBoolExpr
	;


boolOrExpr
	: boolOrExpr OR boolAndExpr			# Or
	| boolAndExpr						# ToAndExpr
	;

boolAndExpr
	: boolAndExpr AND boolRelMathExpr	# And
	| boolRelMathExpr					# ToBoolRelMath
	;

boolRelMathExpr
	: mathPlusExpr LT mathPlusExpr		# LessThan
	| mathPlusExpr GT mathPlusExpr		# GreaterThan
	| mathPlusExpr LTE mathPlusExpr		# LessThanOrEqual
	| mathPlusExpr GTE mathPlusExpr		# GreaterThanOrEqual
	| mathPlusExpr EQ mathPlusExpr		# EqualTo
	| mathPlusExpr NEQ mathPlusExpr		# NotEqualTo
	| mathPlusExpr						# ToMathPlusExpr
	;

