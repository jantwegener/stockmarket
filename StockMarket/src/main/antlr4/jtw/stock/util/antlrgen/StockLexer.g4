
// Stock Lexer
lexer grammar StockLexer;

LETTER : [A-Z] ;
NUMBER : ('-' | '+')? [0-9]+ ('.' [0-9]+)? ;
SPECIALPREFIX : '_';
SPECIALVAR : [A-Z]+;

LT     : '<' ;
GT     : '>' ;
EQ     : '=' ;
NEQ    : '!=' ;
LTE    : '<=' ;
GTE    : '>=' ;

PLUS   : '+' ;
MINUS  : '-' ;
DIV    : '/' ;
MULT   : '*' ;

AND    : '&' ;
OR     : '|' ;
NOT    : '!' ;

LPAREN : '(' ;
RPAREN : ')' ;

LBRAK  : '[' ;
RBRAK  : ']' ;

LBRACE  : '{' ;
RBRACE  : '}' ;

COMMA : ',';

MAX : 'max'; // max of the given values
MIN : 'min'; // min of the given values
ABS : 'abs'; // absolute value
THRESHOLD : 'threshold'; // 0 unless value is greater than the threshold

IFMARKER : '?';
IFSEP : ':';

WS : [ \t\r\n]+ -> skip ; // skip spaces, tabs, newlines

